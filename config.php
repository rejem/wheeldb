<?php

return array(
    //enable debug mode. this directive has various usage over the framework.
    //disable it on production environment.
    'debug' => TRUE,
    //pre-configured database connections
    'connections' => array(
        'default' => array(
            'driver' => 'MySQL',
            'host' => '127.0.0.1',
            'user' => 'root',
            'password' => 'root',
            'schema'=>'test',
            'loglevel' => WDB\Query\Log::LOG_WRITE,
        ),
        'log' => array(
            'driver' => 'MySQL',
            'host' => '127.0.0.1',
            'user' => 'root',
            'password' => 'root',
            'schema' => 'wdb',
        ),
    ),
    //directories
    //directory with NeonWheel model definitions
    'modeldir' => __DIR__ . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR,
    //directory with read-write access, for cache etc.
    'datadir' => __DIR__ . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR,
    //directory with language files
    'langdir' => __DIR__ . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR,
    //directory with grammar files
    'grammarsdir' => __DIR__ . DIRECTORY_SEPARATOR . 'grammars' . DIRECTORY_SEPARATOR,
    //directory with webui templates
    'templatesdir' => __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR,
    //directory with 3rd party library classes (for auto-loader)
    'libdir' => array(
        __DIR__ . DIRECTORY_SEPARATOR . '3rdparty' . DIRECTORY_SEPARATOR,
    ),
    //selected language identifier
    'lang' => 'cs',
    //user classes namespace or class prefix
    'userWrappersClassPrefix' => '\\WDB\\Wrapper\\User\\',
    'userWebUIClassPrefix' => '\\WDB\\WebUI\\User\\',
    //default record naming column identifier if no @nameColumn is present in table annotations
    'recordNameColumn' => 'name',
    'passwordSalt' => '&DuG0nG*$dugong'
);