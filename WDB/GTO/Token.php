<?php
namespace WDB\GTO;
use WDB,
    WDB\Exception;

/**
 * @property-read string $type
 * @property-read string $content
 * 
 * @author Richard Ejem
 * @package WDB
 */
class Token extends WDB\Structure\Structure {
    
    public static function create($type, $content)
    {
        return new self(array('type'=>$type, 'content'=>$content));
    }
}