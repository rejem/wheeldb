<?php
namespace WDB\GTO;
use WDB,
    WDB\Exception;

/**
 * Grammar representation object
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read array $rules
 * @property-read array $parsingTable
 * @property-read array $lambdaRules
 * @property-read array $aliases
 */
class Grammar extends WDB\Structure\Structure
{
    public static function create($rules, $parsingTable, $lambdaRules, $aliases)
    {
        return new self(array('rules'=>$rules, 'parsingTable'=>$parsingTable, 'lambdaRules'=>$lambdaRules, 'aliases'=>$aliases));
    }
}