<?php

namespace WDB\GTO;
use WDB,
    WDB\Exception;

/**
 * Grammar To Object language definition class interface.
 * 
 * @author Richard Ejem
 * @package WDB
 */
interface iLanguage
{
    /**
     * Get language keywords (literals taken each as token of the same name)
     * 
     * @return string
     */
    public function getKeywords();
    
    /**
     * Get array of regex patterns to parse tokens.
     * 
     * The format of the array should be (token name)=>(regex pattern).
     * Regex patterns are PCRE compatible, but without leading and trailing delimiter.
     * Used PCRE modifiers to these REGEX by default: "Axis"
     * You can override default modifiers by definig $pcreModifiers property to your
     * iLanguage implementing class.
     * 
     * Tokens are looked for in the same order as they are in the returned.
     * So, if you have a token "ABCDE" and all other combinations of letters represent
     * one another type of token ([A-Z]+), place the regex matching ABCDE before [A-Z].
     * 
     * @return array
     */
    public function getTokenPatterns();
    
    /**
     * Return list of token names which can be thrown away (like insignificant whitespaces).
     * 
     * @return array
     */
    public function getTossTokens();
    
    /**
     * Get grammar definition.
     * 
     * @return Grammar
     */
    public function getGrammar();
    
    /**
     * Transform Start nonterminal derivation tree into an object.
     * 
     * @param array $args
     * @return object
     */
    public function objectFromStart($args);
}