<?php
namespace WDB\GTO;
use WDB,
    WDB\Exception;

/**
 * Grammar rule representation object
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $nonTerminal
 * @property-read array $rewriteTo
 */
class Rule extends WDB\Structure\Structure
{
    public static function create($nonTerminal, $rewriteTo)
    {
        return new self (array('nonTerminal'=>$nonTerminal, 'rewriteTo'=>$rewriteTo));
    }
}