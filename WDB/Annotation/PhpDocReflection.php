<?php

namespace WDB\Annotation;

/**
 * Annotation reader for PhpDoc.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class PhpDocReflection extends Reflection
{
    /**
     * Factory method - creates annotation phpdoc reflection from an object supporting getDocComment().
     *
     * @param object any (reflection) object supporting getDocComment() method
     * @return PhpDocReflection
     * 
     * @throws WDB\Exception\BadArgument
     */
    public static function fromReflObject($obj)
    {
        return Reflection::_fromReflObject($obj, __CLASS__);
    }
    
    /**Override*/
    protected function parseTag(&$name, $data)
    {
        if (in_array($name, array('property', 'property-read', 'property-write')))
        {
            $read = ($name != 'property-write');
            $write = ($name != 'property-read');
            $name = 'property';
            return new Property($data, $read, $write);
        } else
        {
            return parent::parseTag($name, $data);
        }
    }    
}