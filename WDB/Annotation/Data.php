<?php
namespace WDB\Annotation;
use WDB;

/**
 * General single annotation information (text only)
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @property-read string $text 
 */
class Data extends \WDB\BaseObject
{

    protected $text = '';

    public function __construct($data)
    {
        $this->text = $data;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($value)
    {
        $this->text = $value;
    }
    public function appendLine($text)
    {
        $this->text .= PHP_EOL;
        $this->text .= $text;
    }
    
    public function __toString()
    {
        return $this->text;
    }

}