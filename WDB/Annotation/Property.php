<?php
namespace WDB\Annotation;
use WDB;

/**
 * Annotation information about PHP property.
 * Parsed annotations: property, property-read, property-write
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $propName
 * @property-read string $propType
 * @property-read bool $read
 * @property-read bool $write
 */
class Property extends Data
{

    protected $propname = '';
    protected $proptype;
    protected $read;
    protected $write;

    public function __construct($data, $read, $write)
    {
        $data = preg_split('~\\s+~', $data, 3);
        $this->read = (bool) $read;
        $this->write = (bool) $write;
        $this->proptype = $data[0];
        if (count($data) > 1)
            $this->propname = substr($data[1], 1);
        if (count($data) > 2)
            parent::__construct($data[2]);
    }

    public function getPropName()
    {
        return $this->propname;
    }

    public function getPropType()
    {
        return $this->proptype;
    }

    public function getRead()
    {
        return $this->read;
    }

    public function getWrite()
    {
        return $this->write;
    }

}