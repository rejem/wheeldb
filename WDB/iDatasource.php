<?php
namespace WDB;

/**
 * Objects implementing this interface can provide sorted and filtered data.
 * 
 * Result of configured filters is retrieved via fetch() method as
 * a WDB\iRowCollection object.
 * Filters are configured by other iDatasource methods.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iDatasource extends
    \Countable
        //count() method should return count of records which would be returned with currently set filter
{
    /**
     * Filters only records with specified column values.
     * Supports fluent interface.
     * 
     * @param array|FilterRule $condition,... associative array with [column name]=>[column value] or FilterRule value
     * @return iDatasource
     */
    public function filter($condition);
    
    /**
     * Filters only records not having specified column values. (negation to filter())
     * Supports fluent interface.
     * 
     * @param array associative array with [column name]=>[column value]
     * @return iDatasource
     */
    public function not(array $condition);
    
    /**
     * Sorts the result by a key.
     * @param string key
     * @param bool (true is ascending, false is descending)
     * @param bool $prepend prepend or append this rule
     * @return iDatasource
     */
    public function sort($key, $asc = TRUE, $prepend = TRUE);
    
    /**
     * Sets selection to subset of (page*pageSize, pagesize).
     * @param int zero-based page number
     * @return iDatasource fluent interface
     */
    public function page($number);
    
    /**
     * returns selected page number.
     * 
     * @return int
     */
    public function getPage();
    
    /**
     * Returns only a subset starting with $offset and of length $count.
     * 
     * @param int offset
     * @param int count
     * @return iDatasource
     */
    public function subset($offset, $count);
    
    /**
     * configures page size supplied to page().
     * 
     * @param int page number
     * @return iDatasource
     */
    public function pageSize($number);
    
    /**
     * returns page size supplied to page().
     * 
     * @return int
     */
    public function getPageSize();
    
    /**
     * Restricts list of retrieved columns to these specified.
     * 
     * @param string $column1,... list of columns to be retrieved
     * @return iDatasource
     */
    public function select();

    /**
     * get only distinct rows
     * 
     * @return iDatasource
     */
    public function distinct($distinct = TRUE);
    
    /**
     * get filtered data through this datasource object
     * 
     * @return WDB\iRowCollection
     */
    public function fetch();
}