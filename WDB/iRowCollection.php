<?php
namespace WDB;

/**
 * Traversable collection of table rows (basically two-dimensional array),
 * generally used for browsing table data.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iRowCollection extends \Countable, \ArrayAccess, \Iterator
{
    /**
     * returns value of first column of first returned row
     * 
     * useful for queries for a signle value, i.e. count of table rows
     * 
     * @return mixed */
    public function singleValue();
    
    /**
     * returns first returned row
     * 
     * @return WDB\Query\SelectedRow
     */
    public function singleRow();
    
    /**
     * Returns array of all contained rows
     * 
     * @return WDB\Query\SelectedRow[]
     */
    public function allRows();
}