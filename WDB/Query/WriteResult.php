<?php
namespace WDB\Query;

/**
 * Result of write query execution (Insert, Update, Delete).
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read int $affectedRows
 * @property-read int $insertId
 * @property-read Query $query
 * @property-read bool $success
 */
class WriteResult extends \WDB\Structure\Structure implements iQueryResult
{
}
