<?php
namespace WDB\Query;

/**
 * @property WDB\Query\iSelectResult|NULL $result
 * @property int|NULL $count
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class SelectCache extends \WDB\Structure\Structure
{
}
