<?php
namespace WDB\Query\Element;
use WDB;

/**
 * Dual table - "dual" on Oracle, none on MySQL. Table consisting of one dummy column
 * and one dummy row, useful for table-independent queries.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Dual implements iTableSource
{
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_dual();
    }
    public function alias($alias)
    {
        return AliasedTableSource::create($this, $alias);
    }
}
