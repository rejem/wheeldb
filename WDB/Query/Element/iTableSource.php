<?php
namespace WDB\Query\Element;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iTableSource extends iElement
{
    /**
     *
     * @param string $alias
     * @return AliasedTableSource
     */
    public function alias($alias);
}