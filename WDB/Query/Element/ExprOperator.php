<?php
namespace WDB\Query\Element;
use WDB;

/**
 * Query expression binary infix operator
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ExprOperator extends InfixOperator implements iExpression
//TODO: getter functions for operator type and expressions
{
    const PLUS = '+';
    const MINUS = '-';
    const TIMES = '*';
    const DIVIDE = '/';
    
    protected static $supportedOps = array(self::PLUS, self::MINUS, self::TIMES, self::DIVIDE);
    
    /**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
    public static function plus()
    {
        return new self(func_get_args(), self::PLUS);
    }
    /**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
    public static function minus()
    {
        return new self(func_get_args(), self::MINUS);
    }
    /**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
    public static function times()
    {
        return new self(func_get_args(), self::TIMES);
    }
    /**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
    public static function divide()
    {
        return new self(func_get_args(), self::DIVIDE);
    }
    
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_exprOp($this->expressions, $this->op);
    }
}
