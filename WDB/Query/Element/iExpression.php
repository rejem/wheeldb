<?php
namespace WDB\Query\Element;

/**
 * Element representing an expression resulting in a value
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iExpression extends iElement
{
}
