<?php
namespace WDB\Query\Element\Datatype;
use WDB;
/**
 * Encapsulation class for various database data types. Used for object query builder.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iDatatype extends WDB\Query\Element\iExpression
{
    /**
     * creates the datatype object and sets its value from input php value (including NULL corresponding to database NULL value)
     * 
     * @param mixed $value
     */
    public function __construct($value);
    
    /**
     * Returns PHP native value of this object
     * 
     * @return mixed
     */
    public function getValue();
    
    /**
     * Checks whether the value is set to null
     * 
     * @return bool
     */
    public function isNULL();
}