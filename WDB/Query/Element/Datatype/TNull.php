<?php
namespace WDB\Query\Element\Datatype;
use WDB,
    WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class TNull extends AbstractType
{
    public function equals(iDatatype $to)
    {
        return $to instanceof TNull || $to->value == NULL;
    }
    
    public function getValue()
    {
        return NULL;
    }
    
    public function __toString()
    {
        return '';
    }

    public function __construct($value = null){}

    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_null();
    }
}
