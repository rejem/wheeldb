<?php
namespace WDB\Query\Element\Datatype;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Date extends DateTime
{   
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_datetime($this->value, TRUE, FALSE);
    }
    
    public function equals(iDatatype $to)
    {
        return \get_class($to) === \get_class($this) && $this->value->format('Y-m-d') == $to->value->format('Y-m-d');
    }
    
    public function __toString()
    {
        return $this->value === NULL ? '' : $this->value->format('Y-m-d');
    }
}
