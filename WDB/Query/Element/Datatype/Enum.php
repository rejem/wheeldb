<?php
namespace WDB\Query\Element\Datatype;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @property-read string $value
 */
class Enum extends String
{   
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_enum($this->value);
    }
}
