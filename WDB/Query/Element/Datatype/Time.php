<?php
namespace WDB\Query\Element\Datatype;
use WDB,
    WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Time extends DateTime
{   
    
    /**
     *
     * @param string|\DateInterval $value 
     * @throws Exception\InvalidValueFormat
     */
    public function __construct($value)
    {
        if (!($value instanceof \DateInterval) && $value !== NULL)
        {
            try
            {
                if (preg_match('~([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})', $value))
                {
                    $value = 'P0000-00-00T'.$value;
                }
                $value = new \DateInterval($value);
            }
            Catch (Exception $e)
            {
                throw new Exception\InvalidValueFormat("string '$value' cannot be parsed as date/time information.", 0, $e);
            }
        }
        $this->value = $value;
        
    }
    
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_time($this->value);
    }
    
    public function __toString()
    {
        return $this->value === NULL ? '' : $this->value->format('H:i:s');
    }
    
    public static function webUIFieldClass()
    {
        return '\\WDB\\WebUI\\FieldTime';
    }
}
