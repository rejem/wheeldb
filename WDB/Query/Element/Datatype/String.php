<?php
namespace WDB\Query\Element\Datatype;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @property-read string $value
 */
class String extends AbstractType
{
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        $value = $this->value;
        if (strtolower($driver->getCharset()) == 'utf8')
        {
            $value = WDB\Database::validateBrokenUTF($value);
        }
        return $driver->tosql_string($value);
    }
    
    public function __construct($value)
    {
        if ($value !== NULL) $value = (string)$value;
        $this->value = $value;
    }
}
