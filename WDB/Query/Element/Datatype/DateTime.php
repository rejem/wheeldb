<?php
namespace WDB\Query\Element\Datatype;
use WDB,
    WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @property-read \DateTime $value
 */
class DateTime extends AbstractType
{

    /**
     *
     * @param string|\DateTime $value 
     * @throws Exception\InvalidValueFormat
     */
    public function __construct($value)
    {
        if (!($value instanceof \DateTime) && $value !== NULL)
        {
            try
            {
                $value = new \DateTime($value);
            }
            Catch (Exception $e)
            {
                throw new Exception\InvalidValueFormat("string '$value' cannot be parsed as date/time information.", 0, $e);
            }
        }
        $this->value = $value;
        
    }
    public function getValue()
    {
        return $this->value;
    }
    
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_datetime($this->value, TRUE, TRUE);
    }
    
    public function __toString()
    {
        return $this->value === NULL ? '' : $this->value->format('Y-m-d H:i:s');
    }
}
