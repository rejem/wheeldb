<?php
namespace WDB\Query\Element\Datatype;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @property-read double $value
 */
class Float extends AbstractType
{
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_number($this->value);
    }
    
    public function __construct($value)
    {
        if ($value !== NULL) $value = (double)$value;
        $this->value = $value;
    }
}
