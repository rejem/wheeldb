<?php
namespace WDB\Query\Element\Datatype;
use WDB,
    WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property array $value
 */
class Set extends AbstractType
{   
    /**
     *
     * @param array $value 
     * @throws Exception\BadArgument
     */
    public function __construct($value)
    {
        if ($value !== NULL && !is_array($value)) throw new Exception\BadArgument("value of Set datatype must be an array or null");
        $this->value = $value;
    }
    
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_set($this->value);
    }
    
    public function equals(iDatatype $to)
    {
        return get_class($to) === get_class($this) && count(array_diff($this->value, $to->value)) == 0 && count(array_diff($to->value, $this->value)) == 0;
    }
    
    public function __toString()
    {
        return implode(', ', $this->value);
    }
}
