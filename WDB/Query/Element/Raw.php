<?php
namespace WDB\Query\Element;
use WDB;

/**
 * Raw database-native query part. Not translated by driver, inserted to query as is.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Raw implements iTableSource, iCondition
{
    /**@var string*/
    private $string;

    /**@param string*/
    public function __construct($string)
    {
        $this->string = $string;
    }

    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $this->string;
    }

    public function alias($alias)
    {
        return AliasedTableSource::create($this, $alias);
    }
}
