<?php
namespace WDB\Query\Element;
use WDB,
    WDB\Exception;

/**
 * Join USING clause
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class JoinUsing extends WDB\BaseCollection implements iElement
{
    /**
     * check element type before inserting into collection.
     */
    protected function processValue($value, $offset)
    {
        if (is_string($value))
        {
            $value = ColumnIdentifier::parse($value);
        }
        elseif (!$value instanceof ColumnIdentifier)
        {
            throw new Exception\BadArgument("Join USING clause can contain only column identifiers (ColumnIdentifier|string)!");
        }
        return $value;
    }

    public static function create()
    {
        return new self(func_get_args());
    }

    public function __construct()
    {
        foreach (WDB\Utils\Arrays::linearize(func_get_args()) as $arg)
        {
            $this[] = $arg;
        }
    }

    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_joinUsingClause($this);
    }
}
