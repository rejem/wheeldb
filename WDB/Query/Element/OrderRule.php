<?php
namespace WDB\Query\Element;
use WDB;

/**
 * ORDER BY single rule.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property iExpression $expression
 * @property bool $ascending
 * @property bool $descending
 */
class OrderRule extends WDB\BaseObject
{
    const ASC = TRUE;
    const DESC = FALSE;
    
    /**@var iExpression*/
    protected $expr;
    protected $ascending;
    
    public function __construct($expr, $dir = self::ASC)
    {
        $this->expr = $expr;
        $this->ascending = (bool)$dir;
    }
    
    /**@param iExpression $expr */
    public function setExpression(iExpression $expr)
    {
            $this->expr = $expr;
    }
    
    /**@return iExpression */
    public function getExpression()
    {
        return $this->expr;
    }
    
    /**@param bool  */
    public function setDescending($descending)
    {
        $this->ascending = !$ascending;
    }
    
    /**@return bool */
    public function getDescending()
    {
        return !$this->ascending;
    }
    
    /**@param bool */
    public function setAscending($ascending)
    {
        $this->ascending = (bool)$ascending;
    }
    
    /**@return bool */
    public function getAscending()
    {
        return $this->ascending;
    }
    
    /**
     * set ordering direction to ascending.
     * 
     * @return OrderRule fluent interface
     */
    public function asc()
    {
        $this->setAscending(TRUE);
        return $this;
    }

    /**
     * set ordering direction to descending.
     * 
     * @return OrderRule fluent interface
     */
    public function desc()
    {
        $this->setAscending(FALSE);
        return $this;
    }
}