<?php
namespace WDB\Query\Element;
/**
 * Condition (compare or logical join of more compares, result is true or false)
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iCondition extends iElement
{
}
