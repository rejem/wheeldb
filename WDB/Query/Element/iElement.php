<?php
namespace WDB\Query\Element;
use WDB;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iElement
{
    /**
     * Converts iElement object to a database query part.
     * 
     * @param WDB\SQLDriver\iSQLDriver driver for conversion
     * @return string raw database query part
     */
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver);
}