<?php
namespace WDB\Query\Element;
use WDB;

/**
 * Query logic operator
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class LogicOperator extends InfixOperator implements iCondition
{
    const L_AND = 'AND';
    const L_OR = 'OR';
    const L_NOT = 'NOT';
    const L_XOR = 'XOR';
    
    protected static $supportedOps = array(self::L_AND, self::L_OR, self::L_NOT, self::L_XOR);
    
    /**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
    public static function lAnd()
    {
        return new self(func_get_args(), self::L_AND);
    }
    /**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
    public static function lOr()
    {
        return new self(func_get_args(), self::L_OR);
    }
    /**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
    public static function lXor()
    {
        return new self(func_get_args(), self::L_XOR);
    }
    /**
     *
     * @param iExpression $expr
     * @return self
     */
    public static function lNot($expr)
    {
        return new self(array($expr), self::L_NOT);
    }
    
    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_logic($this->expressions, $this->op);
    }
}
