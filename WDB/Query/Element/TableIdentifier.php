<?php
namespace WDB\Query\Element;
use WDB,
    WDB\Exception;
/**
 * Table identifier consiting of table name and optionally schema name.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read string|NULL $schema
 * @property-read string $table
 */
class TableIdentifier extends WDB\Structure\Structure implements iTableSource
{
    /**
     * @param string table name
     * @param string|NULL schema name
     * @throws WDB\Exception\BadArgument
     */
    public function __construct($table, $schema = NULL)
    {
        if (!is_string($table)) throw new Exception\BadArgument("table name must be string.");
        if ($schema !== NULL && !is_string($schema)) throw new Exception\BadArgument("schema name must be string or NULL.");

        parent::__construct(array('table'=>$table, 'schema'=>$schema));
    }

    public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
    {
        return $driver->tosql_table($this);
    }

    public function alias($alias)
    {
        return AliasedTableSource::create($this, $alias);
    }
}
