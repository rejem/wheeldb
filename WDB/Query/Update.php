<?php
namespace WDB\Query;
use WDB,
    WDB\Exception;

/**
 * SQL UPDATE query builder
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property array $columns colname=>value
 */
class Update extends SUDQuery
{
    /**@var array column name=>value pairs*/
    protected $columns;
    
    /**
     *
     * @param Element\TableIdentifier|string table to update records in
     * @param array column name=>value pairs
     * @param array|Element\iCondition $cond
     */
    public function __construct($table = NULL, $columns = array(), $where = NULL)
    {
        $this->setTable($table);
        $this->setColumns($columns);
        if ($where instanceof Element\iCondition || $where === NULL)
        {
            $this->setWhere($where);
        }
        elseif (is_array($where))
        {
            $this->filter($where);
        }
        else
        {
            throw new Exception\BadArgument("Unknown where argument type (use iCondition or array): ".print_r($where, true));
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="UPDATE columns">
    /**
     * Add a column name=>value part to the query.
     *
     * @param string column name
     * @param mixed value
     * @return Update fluent interface
     */
    public function addColumn($column, $value = NULL)
    {
        $this->columns[$column] = $value;
        return $this;
    }
    
    /**
     * Get all column=>value pairs in the query.
     *
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }
    
    /**
     * Set all column=>value pairs in the query.
     *
     * @param array column name=>value pairs
     * @return Update fluent interface
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
        return $this;
    }
    //</editor-fold>
}