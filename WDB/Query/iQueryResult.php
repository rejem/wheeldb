<?php
namespace WDB\Query;
/**
 * Result of WDB\Database::query execution
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read Query $query 
 */
interface iQueryResult
{
}
