<?php
namespace WDB\Query;
use WDB,
    WDB\Exception;

/**
 * SQL DELETE query builder
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Delete extends SUDQuery
{  
    /**
     * @param Element\iTableSource|string|NULL target table
     * @param Element\iCondition condition
     */
    public function __construct($table = NULL, $cond = array())
    {
        $this->setTable($table);
        if ($cond instanceof Element\iCondition)
        {
            $this->setWhere($cond);
        }
        elseif (is_array($cond))
        {
            $this->filter($cond);
        }
        else
        {
            throw new Exception\BadArgument("Unknown where argument type (use iCondition or array): ".print_r($where, true));
        }
    }
}