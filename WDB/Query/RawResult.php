<?php
namespace WDB\Query;
use WDB;

/**
 * Result of raw query execution.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read Query $query
 * @property-read bool $success
 * @property-read mixed $nativeResult value returned by platform sql driver
 */
class RawResult extends WDB\Structure\Structure implements iQueryResult
{
}
