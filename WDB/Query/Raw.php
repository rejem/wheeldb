<?php
namespace WDB\Query;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class Raw extends Query
{
    private $query;
    public function __construct($query)
    {
        $this->query = $query;
    }

    public function __toString()
    {
        return $this->query;
    }
}
