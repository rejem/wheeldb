<?php
namespace WDB\Query;
use WDB;

/**
 * Result of Select query execution.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iSelectResult extends iQueryResult, WDB\iRowCollection
{   
    /**
     * returns foreign key columns in result.
     * 
     * @return WDB\Structure\ForeignKey[]
     * @throws WDB\Exception\InvalidOperation if database is not bound so SelectResult cannot search for foreign key data.
     */
    public function getForeignKeys();
    
    /**
     * iSelectResult may use the database object to look for foreign keys on the original table.
     * 
     * @param WDB\Database database object the result was performed on
     */
    public function bindDatabase (WDB\Database $db);

}
