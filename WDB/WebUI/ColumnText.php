<?php
namespace WDB\WebUI;
use WDB;

/**
  * Simple column extension for text fields. Since Column class's default methods expects a textual information,
  * there is nothing extra in this class.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnText extends Column
{
    /**
     * translates native value to HTML editing input field value when using default input type=text.
     *
     * @param mixed $v
     * @return string 
     */
    protected function HTMLInputTextValue($v)
    {
        if ($v === NULL) return '';
        return $v;
    }
    
    protected function getInputHTMLNode(WDB\Wrapper\iRecord $record)
    {
        $input = parent::getInputHTMLNode($record);
        $input['value'] = $this->HTMLInputTextValue($input['value']);
        return $input;
    }
    
    public function getEditingHTML(WDB\Wrapper\iRecord $record)
    {
        return $this->getInputHTMLNode($record)->__toString();   
    }
}