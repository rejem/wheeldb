<?php
namespace WDB\WebUI;
use WDB;

/**
  * Date column implementation
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  * @todo add some nice javascript calendar
  */
class ColumnDate extends ColumnText
{
    public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
    {
        return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_date'));
    }
    
    protected function HTMLInputTextValue($v)
    {
        if ($v === NULL) return date('j.n.Y');
        if (is_object($v) && method_exists($v, 'format'))
        {
            return $v->format('j.n.Y');
        }
        return (string)$v;
    }
    
    protected function getInputHTMLNode(WDB\Wrapper\iRecord $record)
    {
        $input = parent::getInputHTMLNode($record);
        $input->addClass('tcal');
        return $input;
    }
    
    public function getDetailedHTML(WDB\Wrapper\iRecord $record)
    {
        $val = $record[$this->name];
        return $this->getStringRepresentation($val);
    }
    
    private function getStringRepresentation($value)
    {
        if ($value === NULL) return WDB\Lang::s ('webui.notset');
        return $value->format('j.n.Y');
    }
    
    public function getFilterStringValue($val)
    {
        return $this->getStringRepresentation($val);
    }
}