<?php
namespace WDB\WebUI;
use WDB;

/**
  * Numeric column
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnNumeric extends ColumnText
{
    /**@var string JavaScript regex pattern to filter input values directly when typing*/
    protected $numPattern = '/^(-|\\+)?[0-9 ]*((\\.|,)[0-9 ]*)?$/';
    public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
    {
        return array_merge(parent::getSurroundingCSSClasses($record), array(' wdb_field_numeric'));
    }
    public function getEditingHTML(WDB\Wrapper\iRecord $record)
    {
        $input = $this->getInputHTMLNode($record);
        $input['onchange'] = $input['onkeyup'] = 'wdb.filterInput(this, '.$this->numPattern.')';
        return $input->__toString();   
    }
}