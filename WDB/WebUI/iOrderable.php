<?php
namespace WDB\WebUI;
use WDB;

/**
  * Orderable column WebUI.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iOrderable
{
    /**
     * Determines whether records can be ordered by this column
     * 
     * @return bool
     */
    public function isOrderable();
    
    /**
     * @param bool $asc
     * @return bool
     */
    public function isToggledOrder($asc = TRUE);
}