<?php
namespace WDB\WebUI;
use WDB;

/**
  * Boolean column
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnBoolean extends Column
{
    public function getDetailedHTML(WDB\Wrapper\iRecord $record)
    {
        return htmlspecialchars($this->getStringRepresentation($record[$this->name]));
    }
    
    private function getStringRepresentation($value)
    {
        return $value ? WDB\Lang::s('webui.boolean.yes') : WDB\Lang::s('webui.boolean.no');
    }
    
    public function getFilterStringValue($val)
    {
        return $this->getStringRepresentation($val);
    }
    
    protected function fetchInputValue($v)
    {
        return $v ? 1 : 0;
    }

    public function isNullable()
    {
        return false;
    }

    
    public function getEditingHTML(WDB\Wrapper\iRecord $record)
    {
        $input_name = $this->table->getRequest()->getSaveFieldName($this->name);
        $input = new HTMLNode('input');
        $input['type'] = 'checkbox';
        $input['name'] = $input_name.'[value]';
        $input['value'] = '1';
        if ($record[$this->name])
        {
            $input['checked'] = 'checked';
        }
        return $input; 
    }
}