<?php
namespace WDB\WebUI;
use WDB;

/**
 * Interface for webui tables.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iTable extends \Iterator, \Countable, \ArrayAccess
{
    // <editor-fold defaultstate="collapsed" desc="constants">
    /** Constant for addColumn $after argument - add column as first in the table */
    const FIRST = TRUE;
    /** Constant for addColumn $after argument - add column as last in the table */
    const LAST = FALSE;
    
    /** State of UI - brief list of list all records */
    const STATE_LIST = 'list';
    /** State of UI - detailed output of one record */
    const STATE_DETAIL = 'detail';
    /** State of UI - edit interface for a record */
    const STATE_EDIT = 'edit';
    /** State of UI - default one */
    const STATE_DEFAULT = self::STATE_LIST;
    /** State of UI - auto-detect from context */
    const STATE_AUTO = NULL;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Table identification">
    /**
     * Returns name of this table
     * 
     * @return string
     */
    public function getName();
    
    /**
     * Returns human-readable title of this table.
     * 
     * @return string
     */
    public function getTitle();
    
    /**
     * Returns a table locator if this table has its base in database or NULL otherwise.
     * 
     * @return WDB\Structure\TableLocator
     */
    public function getTableLocator();
    // </editor-fold>
    
    /**
     * Retrieves record selected by current http request
     *
     * @return iRecord
     */
    public function getSelectedRecord();
    
    // <editor-fold defaultstate="collapsed" desc="request handling">
    /**
     * Returns Request object bound to this table
     *
     * @return iRequest
     */
    public function getRequest();
    
    /**
     * Sets Request object for this table
     * 
     * @param iRequest $rqi
     */
    public function setRequest(iRequest $rqi);

    /**
     * Perform events based on http request, especially those which should be
     * performed before commiting headers
     */
    public function handleRequest();
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="templating">
    /**
     * Configures template manager class
     * 
     * @param iTemplateAdapter $manager
     */
    public function setTemplateAdapterClass(iTemplateAdapter $manager);
    
    /**
     * Echoes HTML code of selected state on output.
     * 
     * @param int state
     */
    public function show($state = self::STATE_AUTO);
    
    /**
     * Prepares HTML code of selected state and returns it as string
     *
     * @param int $state
     * @return string
     */
    public function generateHTML($state = self::STATE_AUTO);
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="column operations">
    /**
     * Returns all columns in this table.
     * 
     * @return \WDB\WebUI\iColumn[]
     */
    public function getColumns();
    
    /**
     * browses all columns in this table and creates a unique name identifier (for a new one)
     */
    public function createUniqueColumnName();
    
    /**
     * Adds a column to a specified position. 
     * 
     * @param iColumn
     * @param string|bool after which column it should be added; TRUE = first, FALSE = last
     * @return iTable Fluent interface - returns itself
     */
    public function addColumn(iColumn $column, $after = self::LAST);
    
    /**
     * sets the order of displayed columns. i.e. $table->order('name','address','phone');
     * 
     * @param string $col1,...
     * @return iTable
     */
    public function order();
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="input request data parsing">
    /**
     * Event fired after all fields of input row are parsed and loaded into selected record
     * 
     * @return WDB\Event\Event
     */
    public function getOnValidateInput();
    
    /**
     * Fetches input from iRequest to the record if requested record editation
     * 
     * @param WDB\Wrapper\iRecord
     */
    public function parseInput(WDB\Wrapper\iRecord $record);
    // </editor-fold>
    
    
    /**
     * @return \WDB\iDatasource
     */
    public function getDatasource();
    
    /**
     * Returns true if this table shoud be listed in schema webui table list.
     * 
     * @return bool
     */
    public function isListedInSchemaAdmin();
    
    /**
     * Returns true if this table is accessible by webui
     * 
     * @return bool
     */
    public function isWebUIAccessible();
    
    /**
     * Checks a table for non-null non-writable column paradox.
     * The issue is that if a column has a NOT NULL constraint, no default value
     * and is not writable by WDB WebUI, new records cannot be created with such table
     * (because there is neither valid default value for the column nor user
     * can specify it).
     * 
     * @return bool
     */
    public function isNewRecordCapable();
    
    /**
     * returns data of all bound validators to this table's record to pass to javascript validator clone
     *
     * @param WDB\Wrapper\iRecord
     * @return array
     */
    public function getValidatorData(WDB\Wrapper\iRecord $record);
    
    /**
     * Get class for column webui for a column identified by name
     * 
     * @param string column name
     * @return string|NULL
     */
    public function getColumnWebUIClass($columnName);
    
    /**
     * Get HTML code representing table edit status (i.e. validation errors)
     * 
     * @return string
     */
    public function getHTMLEditStatus();
}