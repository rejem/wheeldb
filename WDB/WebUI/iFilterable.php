<?php
namespace WDB\WebUI;
use WDB;

/**
  * Orderable column WebUI.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iFilterable
{
    /**
     * Determines whether records can be filtered by this column
     * 
     * @return bool
     */
    public function isFilterable();

    /**
     * @return bool
     */
    public function isToggledFilter();
    
    /**
     * @return array associative key=>title array of options
     */
    public function getFilterOptions();
    
    /**
     * @return string representation of wrapper layer value
     */
    public function getFilterStringValue($val);
    
    /**
     * @return string
     */
    public function getFilteredValue();
}