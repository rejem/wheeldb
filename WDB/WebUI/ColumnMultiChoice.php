<?php
namespace WDB\WebUI;
use WDB,
    WDB\Exception;

/**
  * Base multichoice column implementation through checkbox list.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnMultiChoice extends Column implements WDB\iSelectOptions
{
    /**
     * @var string
     * @see ColumnSelect::$nullKey
     */
    private $nullKey;
    
    /**
     *
     * @param WDB\Wrapper\iColumn $columnWrapper
     * @param iTable $tableWebUI 
     * @throws Exception\BadArgument
     */
    public function __construct(WDB\Wrapper\iColumn $columnWrapper, iTable $tableWebUI)
    {
        if (!$columnWrapper instanceof WDB\iSelectOptions)
        {
            throw new Exception\BadArgument("column webui under FieldCheckboxes must implement \\WDB\\iSelectOptions");
        }
        parent::__construct($columnWrapper, $tableWebUI);
        
        $this->nullKey = '=null';
        while (array_key_exists($this->nullKey, $this->getOptions()))
        {
            $this->nullKey .= '=';
        }
    }
    public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
    {
        return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_multichoice'));
    }
    
    public function getDetailedHTML(WDB\Wrapper\iRecord $record)
    {
        $list = array();
        $options = $this->getOptions();
        foreach ($record[$this->name] as $item)
        {
            $list[] = isset($options[$item]) ? $options[$item] : '?';
        }
        return htmlspecialchars(implode(',', $list));
    }
    
    protected function fetchInputValue($v)
    {
        if (!is_array($v)) return array(); //if none selected (got NULL) or invalid value submitted (user hack attempt)
        return $v;
    }
    
    
    public function getEditingHTML_nullator(WDB\Wrapper\iRecord $record)
    {
        $val = $record[$this->name];
        if ($this->isNullable())
        {
            $input_name = $this->table->getRequest()->getSaveFieldName($this->name);
            return '<input type="checkbox" name="'.htmlspecialchars($input_name).'[notnull]" value="1" '.($val !== null ? 'checked="checked"' : '').'
                    onclick="var inp = $(\'input[name=&quot;'.str_replace(array('[', ']'), array('\\[', '\\]'), htmlspecialchars($input_name)).'\[value\]\[\]&quot;]\');
                        if (this.checked) inp.removeAttr(\'disabled\'); else inp.attr(\'disabled\', true);"/>';
        }
        else
        {
            return '';
        }
    }
    
    public function getEditingHTML(WDB\Wrapper\iRecord $record)
    {
        $val = $record[$this->name];
        $input_name = $this->table->getRequest()->getSaveFieldName($this->name);
        $out = '';
        $valuesName = $input_name.'[value][]';
        
        $i = 0;
        foreach ($this->getOptions() as $key=>$text)
        {
            $checkbox = new HTMLNode('input');
            $checkbox['type'] = 'checkbox';
            $checkbox['name'] = $valuesName;
            $checkbox['id'] = str_replace(array('[', ']'), '_', $valuesName).$i;
            $checkbox['value'] = $key;
            if ($val !== NULL && in_array($key, $val))
            {
                $checkbox['checked'] = 'checked';
            }
            if ($this->isNullable() && $val === NULL)
            {
                $checkbox['disabled'] = 'disabled';
            }
            
            $out .= "<div class=\"wdb_multi_elem\">".$checkbox.'<label for="'.$checkbox['id'].'">'.htmlspecialchars($text)."</label></div>\n";
            ++$i;
        }
        
        return $out;
    }
    
    public function getOptions()
    {
        return $this->columnWrapper->getOptions();
    }
    
    public function isFilterable()
    {
        return false;
    }
    public function isOrderable()
    {
        return false;
    }
}