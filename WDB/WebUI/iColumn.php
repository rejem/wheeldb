<?php
namespace WDB\WebUI;
use WDB;

/**
  * column WebUI.
  *
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iColumn
{
    /**display column on list view*/
    const DISPLAY_LIST = 1;
    /**display column on detail view*/
    const DISPLAY_DETAIL = 2;
    /**display column on edit view*/
    const DISPLAY_EDIT = 12;
    /**display column on edit view, but only as readonly value*/
    const DISPLAY_EDIT_READONLY = 4;
    /**default value for displaying automatically managed columns like auto_increments.*/
    const DISPLAY_AUTOCOLUMN_DEFAULT = 0;
    /**represents all display modes*/
    const DISPLAY_ALL = 15;

    // <editor-fold defaultstate="collapsed" desc="identification and table binding">
    /**
     * Returns name identifier of this column
     *
     * @return string
     */
    public function getName();

    /**
     * Returns human-readable title of this column
     *
     * @return string
     */
    public function getTitle();

    /**
     * Returns html code representing human-readable title of this column
     * (for table headings etc)
     *
     * @return string
     */
    public function getTitleHTML();

    /**
     * returns table this column is bound to
     *
     * @return iTable
     */
    public function getTable();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="column display modes configuration">
    /**
     * add display mode(s) to this column
     *
     * @param int $mode mode bitmask
     */
    public function addDisplayMode($mode);

    /**
     * remove display mode(s) from this column
     *
     * @param int $mode mode bitmask
     */
    public function removeDisplayMode($mode);

    /**
     * sets display modes mask for this column
     *
     * @param int $mode mode bitmask
     */
    public function setDisplayModes($mode);

    /**
     * returns display modes mask for this column
     *
     * @return int mode bitmask
     */
    public function getDisplayModes();

    /**
     * Returns whether this column has to be displayed in record list
     *
     * @return bool
     */
    public function isDisplayedList();

    /**
     * Returns whether this column has to be displayed in record editor
     * as an editable item
     *
     * @return bool
     */
    public function isDisplayedEdit();

    /**
     * Returns whether this column has to be displayed in record editor
     * (check isDisplayedEdit() for whether it also has to be editable)
     *
     * @return bool
     */
    public function isDisplayedEditRead();

    /**
     * Returns whether this column has to be displayed in record detail
     *
     * @return bool
     */
    public function isDisplayedDetail();

    /**
     * Returns whether this column has to be displayed in specified mode
     * (or one of modes if more modes are set)
     *
     * @return bool
     */
    public function isDisplayed($mode);
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="column properties">
    /**
     * Returns whether this column can have a null value or not
     */
    public function isNullable();

    /**
     * Returns validation rules for this column
     *
     * @return \WDB\Validation\Rules
     */
    public function getRules();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTML generation">
    /**
     * Returns html code representing this field's simplified value to display i.e. in record listing
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
    public function getSimpleHTML(WDB\Wrapper\iRecord $record);

    /**
     * Returns html code representing this field's value to display in record detail
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
    public function getDetailedHTML(WDB\Wrapper\iRecord $record);

    /**
     * Returns html code representing this field's datatype and value to display in record editor
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
    public function getEditingHTML(WDB\Wrapper\iRecord $record);

    /**
     * Returns html code displayed in separate column for setting NULL value.
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
    public function getEditingHTML_nullator(WDB\Wrapper\iRecord $record);

    /**
     * Returns html code representing this column's status (i.e. validation error)
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
    public function getHTMLEditStatus(WDB\Wrapper\iRecord $record);

    /**
     * Gets CSS classes of the surrounding HTML element.
     *
     * @param WDB\Wrapper\iRecord $record
     * @return array
     */
    public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record);
    // </editor-fold>

    /**
     * Reads and parses value of this column from request
     *
     * @param WDB\Wrapper\iRecord
     * @param string|array input string or array value
     * @return mixed parsed value
     */
    public function parseInputValue(WDB\Wrapper\iRecord $record, $value);

    /**
     * Returns true if this column can be initialized with a default value,
     * i.e. a database column which can be null or has a default non-null vaue.
     *
     * @return bool
     */
    public function isDefaultInitializable();
}