<?php
namespace WDB\WebUI;
use WDB;

/**
  * Datetime column implementation
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnDateTime extends ColumnText
{
    public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
    {
        return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_datetime'));
    }
    
    protected function HTMLInputTextValue($v)
    {
        if ($v === NULL) return date('j.n.Y G:i:s'); //default is current
        if (is_object($v) && method_exists($v, 'format'))
        {
            return $v->format('j.n.Y G:i:s');
        }
        return (string)$v;
    }
    
    public function getDetailedHTML(WDB\Wrapper\iRecord $record)
    {
        $val = $record[$this->name];
        return $this->getStringRepresentation($val);
    }   
    
    private function getStringRepresentation($value)
    {
        if ($value === NULL) return WDB\Lang::s ('webui.notset');
        return $value->format('j.n.Y G:i:s');
    }
    
    public function getFilterStringValue($val)
    {
        return $this->getStringRepresentation($val);
    }
}