<?php
namespace WDB\WebUI;
use WDB;

/**
 * Factory for creating standard WebUI Column objects for column wrappers.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnFactory
{
    /**
     *
     * @param WDB\Wrapper\iColumn
     * @param iTable
     * @return iColumn
     */
    public static function create(WDB\Wrapper\iColumn $columnWrapper, iTable $tableWebUI)
    {
        $tClass = $tableWebUI->getColumnWebUIClass($columnWrapper->getName());
        if ($tClass === NULL)
        {
            $class = '\\WDB\\WebUI\\ColumnText';
        }
        else
        {
            $class = WDB\Utils\System::findClass($tClass, WDB\Config::read('userWebUIClassPrefix'));
        }
        return new $class($columnWrapper, $tableWebUI);
    }
}
