<?php
namespace WDB\WebUI;
use WDB;

/**
 * Factory methods for creating WebUI tables
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class TableFactory
{
    /**
     *
     * @param \WDB\Wrapper\iTable
     * @return \WDB\WebUI\Table
     */
    public static function fromWrapper(WDB\Wrapper\table $table)
    {
        $tClass = $table->getWebUIClass();
        if ($tClass === NULL)
        {
            $class = '\\WDB\\WebUI\\Table';
        }
        else
        {
            $class = WDB\Utils\System::findClass($tClass, WDB\Config::read('userWebUIClassPrefix'));
        }
        return new $class($table);
    }
    /**
     *
     * @param \WDB\Analyzer\iTable
     * @return \WDB\WebUI\Table
     */
    public static function fromAnalyzer(WDB\Analyzer\iTable $table)
    {
        $wrapper = WDB\Wrapper\TableFactory::fromAnalyzer($table);
        return self::fromWrapper($wrapper);
    }
    
    /**
     *
     * @param string
     * @param string schema name
     * @param WDB\Database database connection
     * @return \WDB\WebUI\iTable
     */
    public static function fromName($table, $schema = NULL, WDB\Database $database = NULL)
    {
        $wrapper = WDB\Wrapper\TableFactory::fromName($table, $schema, $database);
        return self::fromWrapper($wrapper);
    }
}
