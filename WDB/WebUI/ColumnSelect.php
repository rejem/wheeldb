<?php
namespace WDB\WebUI;
use WDB,
    WDB\Exception;

/**
  * Column of a choice select value (i.e. ENUM).
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnSelect extends Column implements WDB\iSelectOptions
{
    /**@var string label of the "nothing selected" option*/
    public $unselectedText;
    
    /**@var string id of the "nothing selected" option*/
    private $nullKey;
    /**@todo re-implement the column the way that "nothing selected" has an unique id (i.e. prefix all orher ids
     * with something different). Current implementation can fail in a theoretical case when a row with the same
     * value as currently used for "nothing" id is inserted into database after user loads the record and before
     * he saves it.
     */
    
    public function getOptions()
    {
        return $this->columnWrapper->getOptions();
    }
    
    /**
     *
     * @param WDB\Wrapper\iColumn
     * @param iTable
     * @throws Exception\BadArgument
     */
    public function __construct(WDB\Wrapper\iColumn $columnWrapper, iTable $tableWebUI)
    {
        if (!$columnWrapper instanceof WDB\iSelectOptions)
        {
            throw new Exception\BadArgument("column wrapper for ColumnSelect webui must implement \\WDB\\iSelectOptions");
        }
        parent::__construct($columnWrapper, $tableWebUI);

        $this->unselectedText = WDB\Lang::s('webui.edit.noneSelected');
        $this->nullKey = '=!null';
        while (array_key_exists($this->nullKey, $this->getOptions()))
        {
            $this->nullKey .= '=';
        }
    }
    
    public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
    {
        return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_select'));
    }
    
    public function getEditingHTML(WDB\Wrapper\iRecord $record)
    {
        //output header
        $input_name = $this->table->getRequest()->getSaveFieldName($this->name);
        $output = '<select name="'.htmlspecialchars($input_name).'[value]" data-null="'.htmlspecialchars($this->nullKey).'">';
        
        $current_value = $record->getField($this->name)->getStringValue();
        
        //"null" option
        $option = new HTMLNode('option');
        
        if ($current_value === NULL)
        {
            $option['selected'] = 'selected';
        }
        $option['rel']='null';
        $option['value']=$this->nullKey;
        $option->content = $this->unselectedText;
        $output .= $option->__toString()."\n";
        
        //get options from column object
        foreach ($this->getOptions() as $key=>$text)
        {
            $option = new HTMLNode('option');
            if ($key == $current_value)
            {
                $option['selected'] = 'selected';
            }
            $option['value']=$key;
            $option->content = $text;
            $output .= $option->__toString()."\n";
       }
        $output .= '</select>';
        
        return $output;
    }
    
    public function parseInputValue(WDB\Wrapper\iRecord $record, $value)
    {
        return $this->parseInputValueNoNullable($record, $value);
    }
    
    public function getDetailedHTML(WDB\Wrapper\iRecord $record)
    {
        $opt = $this->getOptions();
        return htmlspecialchars(isset($opt[$record->getField($this->name)->getStringValue()]) ? $opt[$record->getField($this->name)->getStringValue()] : WDB\Lang::s('webui.notset'));
    }
    
    public function getEditingHTML_nullator(WDB\Wrapper\iRecord $record)
    {
        //null is expressed by a special select field
        return '';
    }
    
    protected function fetchInputValue($v)
    {
        if ($v === $this->nullKey) return NULL;
        return $v;
    }
}