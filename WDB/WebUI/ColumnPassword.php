<?php
namespace WDB\WebUI;
use WDB,
    WDB\Exception;

/**
  * Password column.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnPassword extends ColumnText
{
    public function getEditingHTML(WDB\Wrapper\iRecord $record)
    {
        $input = $this->getInputHTMLNode($record);
        $input['type'] = 'password';
        $input['value'] = '';
        $input->addClass('wdbPasswordInput');
        if ($record->isNew())
        {
            return $input->__toString();
        }
        else
        {
            $input_name = $this->table->getRequest()->getSaveFieldName($this->name);
            $doChange = new HTMLNode('input');
            $doChange['type'] = 'checkbox';
            $doChange['class'] = 'wdbPasswordInputChange';
            $doChange['name'] = $input_name.'[doChange]';
            $doChange['id'] = $input_name.'[doChange]';
            return $input->__toString().$doChange->__toString()."<label for=\"{$input_name}[doChange]\">".WDB\Lang::s('webui.passwordFieldChange')."</label>";
        }
    }
    
    public function parseInputValue(WDB\Wrapper\iRecord $record, $value)
    {
        if ($record->isNew() || !empty($value['doChange']))
        {
            parent::parseInputValue($record, $value);
        }
    }
    
    protected function getDefaultDisplayModes()
    {
        return parent::getDefaultDisplayModes() &~ iColumn::DISPLAY_DETAIL &~ iColumn::DISPLAY_LIST;
    }
    
    public static function hash($v)
    {
        return sha1($v.WDB\Config::read('passwordSalt'));
    }
    
    protected function fetchInputValue($v)
    {
        return self::hash($v);
    }
    public function isFilterable()
    {
        return false;
    }
    public function isOrderable()
    {
        return false;
    }
}