<?php
namespace WDB\WebUI;
use WDB;

/*
 * Simple php-file templating system
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class SimpleTemplateAdapter extends WDB\BaseObject implements iTemplateAdapter
{
    /**@var array template variables*/
    protected $data;
    
    /**@var string template id*/
    protected $template;
    
    
    public function __construct($templateId)
    {
        $this->template = $templateId;
        $this->data = array();
    }
    
    public function setTemplate($templateId)
    {
        $this->template = $templateId;
    }
    
    public function getTemplate()
    {
        return $this->template;
    }
    
    public function __get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : NULL;
    }
    
    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }
    
    public function parse($templateId = NULL)
    {
        ob_start();
        $this->show($templateId);
        return ob_get_clean();
    }

    public function show($templateId = NULL)
    {
        if ($templateId === NULL) $templateId = $this->template;
        require WDB\Config::read('templatesdir').str_replace('.', DIRECTORY_SEPARATOR, $templateId).'.php';
    }
    
}