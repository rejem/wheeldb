<?php
namespace WDB\WebUI;
use WDB;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iTemplateAdapter
{
    /**
     * @param string template identifier
     */
    public function __construct($templateId);
    /**
     * @param string template identifier
     * @return self
     */
    public function setTemplate($templateId);
    /**
     * @return string
     */
    public function getTemplate();
    /**
     * Gets template variable with magic getter
     * 
     * @param string
     */
    public function __get($key);
    /**
     * Sets template variable with magic setter
     * 
     * @param string
     * @param mixed
     */
    public function __set($key, $value);
    /**
     * returns parsed template as string
     * 
     * @param string override configured template identifier
     * @return string
     */
    public function parse($templateId = NULL);
    /**
     * prints parsed template to output
     * 
     * @param string override configured template identifier
     */
    public function show($templateId = NULL);
}