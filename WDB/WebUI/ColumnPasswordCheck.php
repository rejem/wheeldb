<?php
namespace WDB\WebUI;
use WDB;
/**
 * The "repeat password" column.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnPasswordCheck extends WDB\BaseObject implements WDB\WebUI\iColumn
{
    /**@var Table $tableWebUI */
    protected $table;
    
    /**@var int display flags*/
    protected $display;
    
    /**@var string*/
    protected $title;
    
    /**@var string*/
    protected $name;
    
    /**@var string*/
    protected $compareTo;
    
    /**
     * @var \WDB\Validation\ColumnRules only for javascript clone. Server-side validation
     * executes on WebUI layer and therefore cannot be validated by a wrapper validator.
     */
    protected $rules;
    
    /**
     *
     * @param iTable webui table
     * @param string column title
     * @param string password column name to match
     * @param string column name (or null to auto-generate)
     */
    public function __construct(iTable $tableWebUI, $title, $passwordColumn, $name = NULL)
    {
        $this->table = $tableWebUI;
        $this->table->getOnValidateInput()->addListener(
                new WDB\Event\CallbackListener(
                        array($this, 'validateComparePasswords')
                )
        );
        $this->title = $title;
        if ($name === NULL || isset($tableWebUI->columns[$name]))
        {
            $this->name = $tableWebUI->createUniqueColumnName();
        }
        else
        {
            $this->name = $name;
        }
        $this->setDisplayModes(iColumn::DISPLAY_EDIT);
        $this->compareTo = $passwordColumn;
        $this->rules = new WDB\Validation\ColumnRules($title);
        $this->rules->equals($passwordColumn);
    }
    
    public function getCompareTo()
    {
        return $this->compareTo;
    }
    
    public function getTable()
    {
        return $this->table;
    }
    
    public function addDisplayMode($mode)
    {
        $this->display |= $mode;
    }
    public function removeDisplayMode($mode)
    {
        $this->display &= ~$mode;
    }
    public function setDisplayModes($mode)
    {
        $this->display = (int)$mode;
    }
    public function getDisplayModes()
    {
        return $this->display;
    }
    public function isDisplayedList()
    {
        return $this->isDisplayed(iColumn::DISPLAY_LIST);
    }
    public function isDisplayedEdit()
    {
        return $this->isDisplayed(iColumn::DISPLAY_EDIT);
    }
    public function isDisplayedEditRead()
    {
        return $this->isDisplayed(iColumn::DISPLAY_EDIT_READONLY);
    }
    
    public function isDisplayedDetail()
    {
        return $this->isDisplayed(iColumn::DISPLAY_DETAIL);
    }
    
    public function isDisplayed($mode)
    {
        return ($this->display & $mode) != 0;
    }
    
    public function getTitleHTML()
    {
        return htmlspecialchars($this->title);
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function isNullable()
    {
        return FALSE;
    }
    
    public function getRules()
    {
        return $this->rules;
    }
    
    public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
    {
        return array('wdb_field_pwdcheck');
    }
    
    public function getEditingHTML(WDB\Wrapper\iRecord $record)
    {
        $input_name = $this->table->getRequest()->getSaveFieldName($this->name);
        $input = new HTMLNode('input');
        $input['type'] = 'password';
        $input['name'] = $input_name;
        $input['value'] = $record[$this->name];
        return $input->__toString();
    }
    
    public function validateComparePasswords(WDB\Wrapper\iRecord $record)
    {
        $value = ColumnPassword::hash($this->table->getRequest()->getEdited($this->name));
        if ($value != $record[$this->compareTo]) throw new Exception\ValidationFailed
            (array(WDB\Lang::s('webui.passwordNotMatch')));
    }
    
    public function parseInputValue(WDB\Wrapper\iRecord $record, $value)
    {
        //do nothing, value is read when comparing passwords
    }

    public function getDetailedHTML(WDB\Wrapper\iRecord $record)
    {
        return '';
    }

    public function getEditingHTML_nullator(WDB\Wrapper\iRecord $record)
    {
        return '';
    }

    public function getSimpleHTML(WDB\Wrapper\iRecord $record)
    {
        return '';
    }
    public function getHTMLEditStatus(WDB\Wrapper\iRecord $record)
    {
        return implode('<br>', $record->getField($this->name)->getValidationErrors());
    }
    public function isDefaultInitializable()
    {
        return TRUE;
    }
    public function isOrderable()
    {
        return FALSE;
    }
}