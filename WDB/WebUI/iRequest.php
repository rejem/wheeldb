<?php
namespace WDB\WebUI;
use WDB;

/**
 * Input request to webui. Specifies view to display and modifications to data model.
 * Usually and by default, it is implemented by HttpRequest class for requests coming from HTTP,
 * but there may be implemented other request suppliers and bound to WebUI schema/tables.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iRequest
{
    /**
     * reads current state identifier (list, edit etc...)
     * 
     * @return string
     */
    public function getRequestedState();
    
    /**
     * get http/html identifier for an html field
     * 
     * @param string $name
     * @return string
     */
    public function getSaveFieldName($name);
    
    /**
     * reads particular value from input
     * 
     * @return string
     */
    public function getEdited($key);
    
    /**
     * reads row data from input
     * 
     * @return array associative array colname=>value
     */
    public function getAllEdited();
    
    /**
     * get http link to a different state of the component
     * 
     * @param int $state 
     * @param string|bool|null $selectedRecordKey
     *  string for existing record
     *  bool(TRUE) for new record
     *  null for no record pointout
     * @return Uri
     */
    public function getStateLink($state, $selectedRecordKey = NULL);
    
    /**
     * get key for selected record to display/edit
     * @return string
     */
    public function getSelectedRecordKey();
    
    /**
     * get name of html hidden input indicating that an edit form was sent
     * @return string
     */
    public function getEditControlName();
    
    /**
     * determines whether an edit form was submitted
     * @return bool
     */
    public function isEditSubmitted();
    
    /**
     * determines whether a new record is selected (instead of existing record selected by getSelectedRecordKey)
     * @return bool
     */
    public function isNewRecord();
    
    /**
     * get http link to a script deleting specified record
     * 
     * @return Uri
     */
    public function getDeleteLink($recordKey);
    
    /**
     * redirects after succesfully saving a record
     */
    public function redirectSuccessfulSave($recordKey = NULL);
    

    /**
     * determines whether there is request to delete a record
     * 
     * @return bool
     */
    public function isDeleteRequest();
    
    /**
     * gets string key of a record requested to be deleted
     * 
     * @return string
     */
    public function getDeleteKey();
    
    /**
     * get uri parameter name for record listing page number
     * 
     * @return string
     */
    public function getPageParamName();
    
    /**
     * Get current page number (zero-based, -1 difference from web information)
     * 
     * @return int
     */
    public function getCurrentPage();
    
    /**
     * Configure datasource object for a table with requested page, ordering, filtering etc.
     * 
     * @param WDB\Wrapper\iTable
     * @return WDB\iDatasource configured instance
     */
    public function configureDatasource(WDB\Wrapper\iTable $table);
    
    /**
     * link to get sorted table ordered by passed column identifier
     * 
     * @param string column identifier
     * @param bool ascending
     * @return Uri
     */
    public function orderLink($column, $asc = TRUE);
    
    /**
     * link to filter table by a column
     * 
     * @param string column identifier
     * @param mixed value
     * @return Uri
     */
    public function filterLink($column, $value);
    
    /**
     *
     * @param string $table
     * @return Uri 
     */
    public function getSelectTableLink($table);
    
    /**
     * @return string
     */
    public function getSelectedTable();
    
    /**
     * @return string
     */
    public function getOrderColumn();
    
    /**
     * @return bool
     */
    public function isOrderAsc();
    
    
    /**
     * @return array
     */
    public function getFilters();
}