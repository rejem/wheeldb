<?php
namespace WDB\WebUI;
use WDB;

/**
  * Time column implementation
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnTime extends ColumnText
{
    public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
    {
        return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_time'));
    }
    
    protected function HTMLInputTextValue($v)
    {
        if ($v === NULL) return date('G:i:s');
        if (is_object($v) && method_exists($v, 'format'))
        {
            return $v->format('G:i:s');
        }
        return (string)$v;
    }
    
    public function getDetailedHTML(WDB\Wrapper\iRecord $record)
    {
        $val = $record[$this->name];
        return $this->getStringRepresentation($val);
    }   
    
    private function getStringRepresentation($value)
    {
        if ($value === NULL) return WDB\Lang::s ('webui.notset');
        return $value->format('G:i:s');
    }
    
    public function getFilterStringValue($val)
    {
        return $this->getStringRepresentation($val);
    }
}