<?php
namespace WDB\WebUI\Structure;
use WDB;

/**
 * table column meta information
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $name table name
 * @property-read string $title human-readable table title
 */
class TableLink extends WDB\Structure\Structure{}
