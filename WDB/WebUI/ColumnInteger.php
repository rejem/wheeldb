<?php
namespace WDB\WebUI;
use WDB;

/**
  * Integer column
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnInteger extends ColumnNumeric
{
    protected $numPattern = '/^(-|\\+)?[0-9 ]*$/';
}