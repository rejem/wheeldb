<?php
namespace WDB\Utils;
use WDB;

/**
 * Object delegate to pass array by reference. Original php array can be changed through this class's object.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ArrayDelegate extends WDB\BaseCollection
{
    public function __construct(array &$source)
    {
        $this->contents = &$source;
    }
}