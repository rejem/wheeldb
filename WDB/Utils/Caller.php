<?php
namespace WDB\Utils;

/**
 * Can call method or callable object on demand. Arguments can be prepared earlier before call.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Caller
{
    /**@var callback*/
    private $receiver;
    /**@var array*/
    public $arguments;
    
    /**
     * Prepares the callback
     *
     * @param callback
     * @param mixed $arg,... arguments to callback
     */
    public function __construct($receiver)
    {
        $x = func_get_args();
        array_shift($x);
        $this->receiver = $receiver;
        $this->arguments = $x;
    }
    
    /**
     * PHP magic method to directly call object as a function.
     *
     * @return mixed
     */
    public function __invoke()
    {
        return $this->call();
    }
    
    /**
     * Calls the callback.
     * 
     * @return mixed
     */
    public function call()
    {
        return call_user_func_array($this->receiver, $this->arguments);
    }
}
