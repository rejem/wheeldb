<?php
namespace WDB\Utils;
use WDB;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class OrderedIterator extends WDB\BaseCollection
{
    protected $order;
    
    /**
     *
     * @param array values
     * @param array of indexes of the first array in the desired order
     */
    public function __construct($array, $order)
    {
        $this->contents = $array;
        $this->order = $order;
    }

    public function current()
    {
        return $this->contents[current($this->order)];
    }

    public function key()
    {
        return current($this->order);
    }

    public function next()
    {
        $k = next($this->order);
        if ($this->valid())
        {
            return $this->contents[$k];
        }
        else
        {
            return FALSE;
        }
    }

    public function rewind()
    {
        reset($this->order);
    }

    public function valid()
    {
        $key = key($this->order);
        return ($key !== NULL && $key !== FALSE);
    }
}
