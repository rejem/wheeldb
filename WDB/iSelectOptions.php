<?php

namespace WDB;

/**
 * Objects implementing this interface can supply associative array of options.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iSelectOptions
{
    /**
     * Returns associative array containing options for select box, checkbox/radio button list etc.
     * 
     * @return array
     */
    public function getOptions();
}
