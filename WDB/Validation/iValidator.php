<?php
namespace WDB\Validation;
use WDB;

/**
 * 
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iValidator extends WDB\Event\iEventListener
{   
    /**
     * Gets validation rules data, i.e. for javascript clone
     * 
     * @param \WDB\Wrapper\iRecord $record
     * @param mixed $data structural data to add validation rules etc.
     */
    public function fetchValidatorData(WDB\Wrapper\iRecord $record, &$data);
    //TODO change $data to a structure
    
    /**
     * PHP5.3 doesn't allow re-definition of interface method
     * arguments, but you should always expect an iRecord argument
     * with validator event.
     */
    // public function raise(WDB\Wrapper\iRecord $record = NULL);
}