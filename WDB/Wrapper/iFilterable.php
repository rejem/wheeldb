<?php
namespace WDB\Wrapper;
use WDB;

/**
  * Orderable column WebUI.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iFilterable
{
    /**
     * Determines whether records can be filtered by this column
     * 
     * @return bool
     */
    public function isFilterable();

    /**
     * @param mixed $value
     * @return bool
     */
    public function filterDatasource(WDB\iDatasource $datasource, $value);
}