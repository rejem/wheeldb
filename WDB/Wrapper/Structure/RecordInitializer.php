<?php
namespace WDB\Wrapper\Structure;
use WDB;

/**
 * @property-read WDB\Wrapper\iTable $table
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
abstract class RecordInitializer extends WDB\Structure\Structure {}
