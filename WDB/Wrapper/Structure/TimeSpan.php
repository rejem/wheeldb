<?php
namespace WDB\Wrapper\Structure;
use WDB;

/**
 * @property-read int $id
 * @property-read \DateTime $from
 * @property-read \DateTime $to
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class TimeSpan extends WDB\Structure\Structure
{
    /**@var WDB\Wrapper\TimeDependentTable*/
    private $table;

    /**
     *
     * @param WDB\Wrapper\TimeDependentTable $table
     * @param int $id
     * @param \DateTime $from
     * @param \DateTime $to
     */
    public static function create(WDB\Wrapper\TimeDependentTable $table, $id, \DateTime $from = NULL, \DateTime $to = NULL)
    {
        $result = new self(array('id'=>$id, 'from'=>$from, 'to'=>$to));
        $result->table = $table;
        return $result;
    }

    /**
     * get the record referenced by this timespan.
     * @return WDB\Wrapper\iRecord
     */
    public function getTheRecord()
    {
        return $this->table->getRecordByTimeId($this->id);
    }
}
