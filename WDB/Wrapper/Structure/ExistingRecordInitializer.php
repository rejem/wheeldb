<?php
namespace WDB\Wrapper\Structure;
use WDB;

/**
 * @property-read \WDB\Query\SelectedRow $row
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ExistingRecordInitializer extends RecordInitializer {}
