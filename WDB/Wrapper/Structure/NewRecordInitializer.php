<?php
namespace WDB\Wrapper\Structure;
use WDB;

/**
 * @property-read array $data
 * @property-read int $insertMode
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class NewRecordInitializer extends RecordInitializer {}
