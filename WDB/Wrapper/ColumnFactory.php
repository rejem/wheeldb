<?php
namespace WDB\Wrapper;
use WDB;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnFactory
{
    /**
     * Creates a column wrapper
     * 
     * @param WDB\Analyzer\Column
     * @param iTable
     * @return iColumn
     */
    public static function create(WDB\Analyzer\Column $columnAnalyzer, iTable $tableWrapper)
    {
        $tClass= $tableWrapper->getColumnWrapperClass($columnAnalyzer->name);
        if ($tClass === NULL)
        {
            $class = '\\WDB\\Wrapper\\Column';
        }
        else
        {
            $class = WDB\Utils\System::findClass($tClass, WDB\Config::read('userWrappersClassPrefix'));
        }
        return new $class($tableWrapper, $columnAnalyzer);
    }
    
    /**
     * Creates a column wrapper for a foreign key
     * 
     * @param WDB\Analyzer\Column
     * @param iTable
     * @return ColumnForeign
     */
    public static function createForeign(WDB\Structure\ForeignKeyData $key, iTable $table, array $analyzerColumns)
    {
        //TODO customize class
        $c = new ColumnForeign($key, $table, $analyzerColumns);
        return $c;
    }
}
