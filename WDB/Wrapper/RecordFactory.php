<?php
namespace WDB\Wrapper;
use WDB;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class RecordFactory
{
    public static function load(iTable $table, WDB\Query\SelectedRow $row)
    {
        $class = self::getClass($table);
        
        return new $class(new Structure\ExistingRecordInitializer(
                array(
                    'table'=>$table,
                    'row'=>$row,
                )
                ));
    }
    
    private static function getClass(iTable $table)
    {
        $tClass = $table->getRecordWrapperClass();
        if ($tClass === NULL)
        {
            return '\\WDB\\Wrapper\\Record';
        }
        else
        {
            return WDB\Utils\System::findClass($tClass, WDB\Config::read('userWrappersClassPrefix'));
        }
    }
    
    public static function create(iTable $table, array $data = array(), $mode = WDB\Query\Insert::UNIQUE)
    {
        $class = self::getClass($table);
        
        return new $class(new Structure\NewRecordInitializer(
                array(
                    'table'=>$table,
                    'data'=>$data,
                    'insertMode'=>$mode,
                )
                ));
    }
}
