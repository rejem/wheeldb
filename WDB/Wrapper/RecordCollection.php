<?php
namespace WDB\Wrapper;
use WDB;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class RecordCollection extends WDB\BaseCollection
{
    /**@var iTable*/
    private $table;
    
    /**
     *
     * @param WDB\Query\SelectedRow[]
     * @param iTable
     */
    public function __construct($rows, iTable $table)
    {
        $this->contents = $rows;
        $this->table = $table;
    }
    
    /**@override*/
    protected function outValue($value, $offset)
    {
        if ($value === FALSE) return FALSE;
        return $this->table->fetchRecord($value);
    }
}
