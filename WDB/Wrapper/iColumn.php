<?php
namespace WDB\Wrapper;
use WDB;

/**
 * Interface for table wrapper's column.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @todo eliminate loadField and createField
 */
interface iColumn
{
    // <editor-fold defaultstate="collapsed" desc="Column identification">
    /**
     * Returns name identifier for this column
     *
     * @return string
     */
    public  function getName();
    
    /**
     * Returns human-readable title for this column
     *
     * @return type 
     */
    public  function getTitle();
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Column database properties">
    /**
     * Returns default value for fields in this column
     *
     * @return mixed
     */
    public function getDefault();
    
    /**
     * Returns true when this column is filled automatically (i.e. by database,
     * auto_increment, current_timestamp etc)
     * 
     * @return bool
     */
    public function isAutoColumn();
    
    /**
     * Returns true when this column is filled automatically (i.e. by database,
     * auto_increment, current_timestamp etc)
     * 
     * @return bool
     */
    public function isAutoIncrement();

    /**
     * Returns true when fields in this column can have a null value
     *
     * @return bool
     */
    public function isNullable();
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Column validation">
    /**
     * Returns validation rules defined for this column
     *
     * @return WDB\Validation\Rules
     */
    public function getRules();
    
    /**
     * Adds validation rule to this column
     * 
     * @param int rule (enumeration of WDB\Validation\ColumnRules::*
     * @param string error message
     * @param mixed validator argument
     * @return self fluent interface
     */
    public function rule($rule, $message = NULL, $argument = NULL);
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="WebUI Column configuration">
    /**
     * Returns name of webui class for this object (or null if default should be used)
     *
     * @return string
     */
    public function getWebUIClass();
    
    /**
     * Configures webui display mode
     * 
     * @param int $original
     * @return int
     * @see WDB\Analyzer\iColumn::getWebUIDisplayMode
     */
    public function getWebUIDisplayMode($original = NULL);
    // </editor-fold>
    
    /**
     * Returns table object this column belongs to
     * 
     * @return iTable
     */
    public function getTable();
    
    /**
     * Converts PHP value to SQL query string part using an SQL driver.
     * 
     * @param mixed
     * @return WDB\Query\Element\Datatype\iDatatype
     */
    public function valueToDatatype($value);
    
    /**
     * Returns true when both values are equal in context of this column.
     * 
     * @param mixed
     * @param mixed
     * @return bool
     */
    public function valuesEqual($first, $second);
    
    /**
     * Create Field object to provide it to Record.
     * 
     * @param bool is new record
     * @param mixed value
     * @param WDB\Query\SelectedRow whole record row
     * @return Field
     */
    public function createField($isNew, $value, WDB\Query\SelectedRow $row = NULL);
    
    /**
     * Adds a value from Field to the array which represents the row which will be saved to database.
     * 
     * @param array the row
     * @param mixed the value
     */
    public function saveToRow(array &$row, $value);
    
    /**
     * Raise the OnSaved event after a record with this column is saved.
     * 
     * @param WDB\Wrapper\iRecord
     */
    public function raiseOnSaved(WDB\Wrapper\iRecord $record);
    
    /**
     * Get the OnSaved event control class
     * 
     * @return WDB\Event\Event
     */
    public function getOnSaved();
    
    /**
     * Returns true if a datasource can be ordered by this column
     * 
     * @return bool
     */
    public function isOrderable();
    
    /**
     * May transform field value stored inside iRecord Field into a complex object or just pass it through.
     * 
     * @param mixed $value value from Field
     * @return mixed transformed value
     */
    public function getFieldValue($value);

    /**
     * get string representation for this column's value.
     */
    public function getStringValue($value);
    
    /**
     * May transform complex value passed by user to store inside iRecord Field or just pass it through.
     * 
     * @param mixed $value value from user
     * @return mixed value to store to Field
     */
    public function setFieldValue($value);
    
    /**
     * Configure datasource to be sorted by this column.
     * 
     * @param WDB\iDatasource $ds
     * @param bool ascending
     * @param bool prepend order rule to other ds rules
     */
    public function sortDatasource(WDB\iDatasource $datasource, $asc = TRUE, $prepend = TRUE);
}