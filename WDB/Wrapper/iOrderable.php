<?php
namespace WDB\Wrapper;
use WDB;

/**
  * Orderable column WebUI.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iOrderable
{   
        
    /**
     * Returns true if a datasource can be ordered by this column
     * 
     * @return bool
     */
    public function isOrderable();
    
    /**
     * Configures datasource to order by this column
     * 
     * @param WDB\iDatasource $datasource
     * @param bool $asc ascending if true, descending if false
     * @param bool $append if false, the order rule is prepended.
     */
    public function sortDatasource(WDB\iDatasource $datasource, $asc = true, $prepend = true);
}