<?php
namespace WDB\Wrapper;

/**
 * Table with base data stored in database under WDB\Database.
 * These methods are not intended for WebUI, only for records in this table which stores their data to database.
 * Database layer should be completely separated from WebUI.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iDBTable extends iTable
{
    /**
     * Get table analyzer. NOT part of iTable interface - this serves to a Wrapper\Record bound to database
     * to create save queries.
     *
     * @return \WDB\Analyzer\TableView
     */
    public function getTableAnalyzer();

    /**
     * Return database connection where this table is stored.
     *
     * @return \WDB\Database
     */
    public function getDatabase();

    /**
     * Get source table query with columns listed in $readFields array.
     *
     * @param array $cfg
     * @return \WDB\Query\Select
     */
    public function getDatasource(array $cfg = array());
}