<?php
namespace WDB\Wrapper;
use WDB,
    WDB\Exception;

/* @property-read array $options
 * @property-read int $webUIDisplayMode
 */
class ColumnSelect extends Column implements \WDB\iSelectOptions
{   
    protected $webUIClass = '\\WDB\\WebUI\\ColumnSelect';
    public function __construct($tableWrapper, $columnAnalyzer)
    {
        if (!$columnAnalyzer instanceof WDB\iSelectOptions)
        {
            throw new Exception\BadArgument("column analyzer for ColumnSelect class must implement \WDB\iSelectOptions interface");
        }
        parent::__construct($tableWrapper, $columnAnalyzer);
    }
    
    public function getOptions()
    {
        return $this->columnAnalyzer->getOptions();
    }
}