<?php
namespace WDB\Wrapper;

/**
 * Column with base data stored in database under WDB\Database.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iDBColumn extends iColumn
{
    /**
     * Adds fields to the array argument to be read from database when loading table with this column.
     * 
     * @param WDB\Query\Element\SelectField[]
     */
    public function fetchReadFields(&$fields);
    
}