<?php
namespace WDB\SQLDriver;
use WDB,
    WDB\Query\Element;

/**
 * WDB SQL driver interface
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iSQLDriver
{
    /**
     * connects to a database server.
     *
     * @param WDB\Structure\ConnectionConfig $connection
     * @throws WDB\Exception\ConnectionFailed
     */
    public function connect(WDB\Structure\ConnectionConfig $connection);

    /**
     * disconnect from a database server.
     */
    public function disconnect();

    /**
     * set database connection session character set
     * @param string $charset charset identifier
     */
    public function setCharset($charset);

    /**
     * get database connection session character set
     * @return string $charset charset identifier
     */
    public function getCharset();

    /**
     * @param string $schema
     */
    public function changeSchema($schema);

    /**
     * @param WDB\Query\Query
     * @return iQueryResult
     */
    public function query(WDB\Query\Query $query);

    /**
     * Sends a raw, platform-dependent query string.
     *
     * @param string
     * @param bool if true, multiple queries are allowed in one call
     * @return iQueryResult
     */
    public function queryRaw($query, $multi = FALSE);

    /**
     * @param WDB\Query\Select
     * @return iSelectResult
     */
    public function select(WDB\Query\Select $query);

    /**
     * @param WDB\Query\Insert
     * @return WriteResult
     */
    public function insert(WDB\Query\Insert $query);

    /**
     * @param WDB\Query\Update
     * @return WriteResult
     */
    public function update(WDB\Query\Update $query);

    /**
     * @param WDB\Query\Delete
     * @return WriteResult
     */
    public function delete(WDB\Query\Delete $query);

    /**
     * Compile and return query string
     *
     * @param WDB\Query\Query $q
     * @return string
     */
    public function queryString(WDB\Query\Query $q);

    /**
     * @param WDB\Analyzer\Schema $schema
     * @return WDB\Analyzer\TableView[] */
    public function getTablesViews(WDB\Analyzer\Schema $schema);

    /**
     * @param WDB\Analyzer\TableView $table
     * @return WDB\Structure\TableColumns */
    public function getTableColumns(WDB\Analyzer\TableView $table);

    /**
     * @param string schema name
     * @return bool
     */
    public function schemaExists($schema);

    public function startTransaction();

    public function commit();
    public function rollback();

    /**
     * @param string function name
     * @param string[] arguments
     * @param bool by default, WDB uses own function names for cross-database compatibility. If native is true,
     *  database function with specified name is directly referenced and no WDB internal naming is used.
     *
     */
    public function tosql_function($name, $args, $native = FALSE);

    /**
     * @param Element\iExpression
     * @param Element\iExpression
     * @param Element\Compare
     * @return string
     */
    public function tosql_compare($expr1, $expr2, $operator);

    /**
     * @param Element\iExpression[]
     * @param Element\LogicOperator
     * @return string
     */
    public function tosql_logic($expressions, $operator);

    /**
     * @param Element\ColumnIdentifier
     * @return string
     */
    public function tosql_column(Element\ColumnIdentifier $identifier);

    /**
     * @param Element\TableIdentifier
     * @return string
     */
    public function tosql_table(Element\TableIdentifier $table);

    /**
     * @param Element\Join
     * @return string
     */
    public function tosql_tableJoin(Element\Join $join);

    /**
     * @param Element\AliasedTableSource
     * @return string
     */
    public function tosql_aliasedTableSource(Element\AliasedTableSource $ts);

    /**
     * @param Element\JoinUsing
     * @return string
     */
    public function tosql_joinUsingClause(Element\JoinUsing $using);

    /**
     * escape and quote literal string
     *
     * @param string
     * @return string
     */
    public function tosql_string($value);

    /**
     * escape and quote identifier
     *
     * @param string
     * @return string
     */
    public function tosql_identifier($value);

    /**
     * @param int|float|double|string
     * @return string
     */
    public function tosql_number($value);

    /**
     * @param \DateTime|NULL $value
     * @param bool $date
     * @param bool $time
     * @return string
     */
    public function tosql_datetime(\DateTime $value = NULL, $date, $time);

    /**
     * @param string|NULL
     * @return string
     */
    public function tosql_enum($value);

    /**
     * @param array|NULL
     * @return string
     */
    public function tosql_set($value);

    /**
     * Returns select parsed as sub-query (usually in parentheses)
     *
     * @param Select
     * @return string
     */
    public function tosql_subselect(WDB\Query\Select $select);

    /**
     * "Dual" table source - dummy table with one column and one row
     *
     * @return string
     */
    public function tosql_dual();

    /**
     * @param \DateInterval
     * @return string
     */
    public function tosql_time(\DateInterval $value = NULL);

    /**
     * NULL value literal
     *
     * @return string
     */
    public function tosql_null();

}