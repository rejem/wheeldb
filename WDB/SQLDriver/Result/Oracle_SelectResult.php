<?php
namespace WDB\SQLDriver\Result;
use WDB,
    WDB\Exception;

//TODO: re-implement so that fetch_all will be used.

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Oracle_SelectResult extends WDB\BaseObject implements WDB\Query\iSelectResult
{
    /**@var resource oracle statement*/
    private $resultObject;
    /**@var WDB\Query\Select*/
    private $query;
    /**@var WDB\Database*/
    private $database = NULL;
    
    /**@return WDB\Database|NULL*/
    public function getDatabase()
    {
        return $this->database;
    }
    
    /**@return WDB\Query\Select*/
    public function getQuery()
    {
        return $this->query;
    }
    
    /**@var int $offset */
    private $offset;
    
    /**@var stdClass[] $fields*/
    private $fields;
    
    /**
     * @param WDB\Query\Select original select query
     * @param \mysqli_result mysqli result object
     */
    public function __construct(WDB\Query\Select $query, $resultObject)
    {
        $this->resultObject = $resultObject;
        $this->query = $query;
        $this->offset = 0;
        $this->fields = array();
        for ($i = 0; $i < oci_num_fields($resultObject); ++$i)
        {
            $this->fields[oci_field_name($resultObject, $i+1)] = array(
                    'type'=>oci_field_type($resultObject, $i+1),
                    'typeRaw'=>  oci_field_type_raw($resultObject, $i+1),
                );
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="iSelectResult implementation">
    public function bindDatabase (WDB\Database $db)
    {
        $this->database = $db;
    }
    
    public function singleValue()
    {
        $this->rewind();
        if (!$this->valid()) return NULL;
        $row = $this->current();
        return $row[0];
    }
    
    public function singleRow()
    {
        $this->rewind();
        if (!$this->valid()) return NULL;
        return $this->current();
    }
    
    public function allRows()
    {
        $result = array();
        foreach ($this as $row)
        {
            $result[] = $row;
        }
        return $result;
    }

    //TODO implement
    public function getForeignKeys()
    {
        /* Oracle does not support retrieving original column from executed statements.
         * They will be read from SelectField fields with ColumnIdentifiers with table-qualified
         * columns. Columns without table qualificator will not work as foreign keys.
         */
        throw new Exception\Unimplemented();
        $foreignKeys = array();
        if ($this->database === NULL) throw new Exception\InvalidOperation("select result has not bound database connection.");
        $schema = $this->database->schema;
        $fields = array();
        //first create mapping which database columns are present in the result and what are their names
        foreach ($this->fields as $field)
        {
            if ($field->orgtable && !isset($fields[$field->orgtable]))
            {
                $fields[$field->orgtable] = array();
            }
            if ($field->orgname) $fields[$field->orgtable][$field->orgname] = $field->name;
        }
        //then, read foreign keys leading from these columns
        foreach ($this->fields as $field)
        {
            if (isset($schema->tables[$field->orgtable]) && isset($schema->tables[$field->orgtable]->columns[$field->orgname]))
            {
                
                $fk = $schema->tables[$field->orgtable]->columns[$field->orgname]->foreignKeys;
                foreach ($fk as $fkey)
                {
                    //check if all columns of the current foreign key are present in result
                    //and get their names
                    $success = true;
                    foreach ($fkey->columns as $column)
                    {
                        if (!isset($fields[$field->orgtable][$column]))
                        {
                            $success = false;
                            break;
                        }
                    }
                    if ($success)
                    {
                        $foreignKeys[$fkey->name] = $fkey;
                    }
                }
            }
        }
        return $foreignKeys;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Countable implementation">
    public function count()
    {
        $qCount = clone $this->query;
        $qCount->setFields(WDB\Query\Element\DBFunction::countRows());
        return $qCount->run()->singleValue();
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="ArrayAccess implementation">
    public function offsetExists($offset)
    {
        return is_numeric($offset) && $offset < $this->count();
    }

    /**
     *
     * @param int offset
     * @return WDB\Query\SelectedRow 
     * @throws Exception\OutOfBounds on non-existent offset
     */
    public function offsetGet($offset)
    {
        throw new Exception\UnsupportedByDriver("Oracle driver does not support direct offset access, use iterator instead");
    }

    /**
     * Invalid operation on database result.
     *
     * @param int offset
     * @parma mixed value
     * @throws Exception\InvalidOperation
     */
    public function offsetSet($offset, $value)
    {
        throw new Exception\InvalidOperation("database result is read only");
    }

    /**
     * Invalid operation on database result.
     *
     * @param int offset
     * @throws Exception\InvalidOperation
     */
    public function offsetUnset($offset)
    {
        throw new Exception\InvalidOperation("database result is read only");
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Iterator implementation">
    public function current()
    {
        if ($this->lastResult === NULL)
        {
            return $this->next();
        }
        if ($this->lastResult === FALSE)
        {
            return FALSE;
        }
        $result = new WDB\Query\SelectedRow($this, $this->lastResult);
        return $result;
    }
    

    public function key()
    {
        return $this->offset;
    }

    public function next()
    {
        ++$this->offset;
        $result = $this->fetchResult();
        if ($result !== FALSE)
        {
            return new WDB\Query\SelectedRow($this, $result);
        }
        else
        {
            return FALSE;
        }
    }

    public function rewind()
    {
        if ($this->offset != 0)
        {
            $this->offset = 0;
            oci_execute($this->resultObject);
            $this->lastResult = NULL;
        }
    }

    public function valid()
    {
        return ($this->offset >= 0 && $this->current() != FALSE);
    }
    // </editor-fold>
    
    private $lastResult = NULL;
    
    private function fetchResult()
    {
        $row = oci_fetch_assoc($this->resultObject);
        if ($row === FALSE)
        {
            $this->lastResult = FALSE;
        }
        else
        {
            foreach ($row as $key=>&$val)
            {
                if ($val === NULL) continue;
                if (in_array($this->fields[$key]['typeRaw'], array(12,187))) //DATE and TIMESTAMP types
                {
                    $val = new \DateTime($val);
                }
                if (is_object($val) && get_class($val) == 'OCI-Lob')
                {
                    $val = $val->load();
                }
            }
            unset($val); // fixes PHP bug with referenced foreach
            $this->lastResult = $row;
        }
        return $this->lastResult;
    }
}