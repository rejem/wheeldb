<?php
namespace WDB\Analyzer;
use WDB;

/**
 * Interface for database column information.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iColumn
{
    // <editor-fold defaultstate="collapsed" desc="column configuration">
    /**
     * Returns name identifier of this column
     *
     * @return string
     */
    public function getName();
    
    /**
     * Returns ColumnIdentifier class for this column
     *
     * @return \WDB\Query\Element\ColumnIdentifier
     */
    public function getIdentifier();
    
    /**
     * Returns human-readable title of this column (it is
     * advised to return identifier name when no title is present)
     *
     * @return string
     */
    public function getTitle();
    
    /**
     * returns true when this column is filled automatically by database (auto_increment, current_timestamp etc)
     * 
     * @return bool
     */
    public function isAutoColumn();
    
    /**
     * Returns default value for this column
     * 
     * @return \WDB\Datatype\iDatatype
     */
    public function getDefault();
    
    /**
     * Returns true if this column is nullable.
     * 
     * @return bool
     */
    public function isNullable();
    
    /**
     * retrieves annotations obejct applied to this column
     * 
     * @return Annotation\Reflection
     */
    public function getAnnotations();
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Wrapper and WebUI classname provider">
    /**
     * Returns name of wrapper class for this column or null if none specified
     * (wrapper factory then should use default one)
     * 
     * @return string|null
     * 
     * @throws WDB\Exception\ClassNotFound
     * @see \WDB\Wapper\iColumn
     * 
     */
    public function getWrapperClass();
    
    /**
     * Returns name of webui class for this column or null if none specified
     * (webui factory then should use default one)
     * 
     * @return string|null
     * 
     * @throws WDB\Exception\ClassNotFound
     * @see \WDB\WebUI\iColumn
     */
    public function getWebUIClass();
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="higher layer configuration provider">
    
    /**
     * gets bit pattern integer representation of display modes configuration loaded by
     * analyzer.
     * 
     * If $original parameter is present, display modes not explicitly configured
     * by analyzer are perserved and only these explicitly configured are set
     * in return value.
     * 
     * @param int $original
     * @return int
     * @see \WDB\WebUI\Column::$display
     */
    public function getWebUIDisplayMode($original = null);

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="related objects">
    /**
     * Returns table analyzer this column is bound to
     *
     * @return iTable
     */
    public function getTable();
    
    /**
     * Returns identifier names of foreign keys this column is part of
     *
     * @return array
     */
    public function getForeignKeys();
    
    // </editor-fold>
    
    /**
     * Configures Rules object to meet some database constraints(size of numeric types,
     * length of textual types etc)
     * 
     * @param \WDB\Validation\ColumnRules $rules
     */
    public function fetchValidationRules(WDB\Validation\ColumnRules $rules);
    
    // <editor-fold defaultstate="collapsed" desc="Column value type functions">
    
    /**
     * Converts PHP value to SQL query string part using an SQL driver.
     * 
     * @param mixed
     * @return WDB\Query\Element\Datatype\iDatatype
     */
    public function valueToDatatype($value);
    
    /**
     * Sets Record's Field value with an appropriate type from an user value
     * 
     * @param mixed
     * @return mixed
     */
    public function setFieldValue($value);
    
    /**
     * Get string representation of a value in this column
     * 
     * @param mixed value
     * @return string
     */
    public function getStringValue($value);
    // </editor-fold>
}