<?php
namespace WDB\Analyzer;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnFloat extends Column
{
    private $double;
    /**
     *
     * @param string column name
     * @param TableView table object
     * @param \WDB\Structure\ColumnMetaInfo column information
     * @param bool determines if it is a double-precision float
     */
    public function __construct($name, TableView $table, WDB\Structure\ColumnMetaInfo $meta, $double_precision)
    {
        parent::__construct($name, $table, $meta);
        $this->double = (bool)$double_precision;
    }
    public function getDouble()
    {
        return $this->double;
    }
    
    // <editor-fold defaultstate="collapsed" desc="iColumn overrides">    
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\Float($value);
    }
    
    public function getWebUIClass()
    {
        return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnNumeric');
    }
    
    public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
    {
        parent::fetchValidationRules($rules);
        $rules->float();
        if (!$this->meta->nullable && !$this->meta->isAutoColumn)
        {
            $rules->required();
        }
    }
    // </editor-fold>
}
