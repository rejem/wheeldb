<?php
namespace WDB\Analyzer;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnTime extends ColumnTimeInfo
{
    protected $stringFormat = 'G:i:s';
    
    // <editor-fold desc="iColumn overrides">
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\Time($value);
    }
    
    public function getWebUIClass()
    {
        return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnTime');
    }
    public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
    {
        parent::fetchValidationRules($rules);
        $rules->pattern('~^'.ColumnTimeInfo::$timePattern.'$~x',
                WDB\Lang::s('validator.messages.time'));
    }
    // </editor-fold>
    
    // <editor-fold desc="ColumnTimeInfo abstracts impl.">
    public function hasTime() { return TRUE; }
    public function hasDate() { return FALSE; }
    
    public function getStringValue($value)
    {
        return $value instanceof \DateInterval ? $value->format($this->stringFormat) : (string)$value;
    }
    
    public function setFieldValue($value)
    {
        try
        {
            return $value instanceof \DateInterval ? $value : new \DateInterval($value);
        }
        catch (\Exception $ex)
        {
            return $value;
        }
    }
    // </editor-fold>
}