<?php
namespace WDB\Analyzer;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class ColumnDateTime extends ColumnTimeInfo
{
    protected $stringFormat = 'j.n.Y G:i:s';
    
    // <editor-fold desc="iColumn overrides">
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\DateTime($value);
    }
    
    public function getWebUIClass()
    {
        return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnDateTime');
    }
    
    public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
    {
        parent::fetchValidationRules($rules);
        $rules->pattern('~^'.ColumnTimeInfo::$datePattern.ColumnTimeInfo::$timePattern.'$~x',
                WDB\Lang::s('validator.messages.datetime'));
    }
    // </editor-fold>
    
    // <editor-fold desc="ColumnTimeInfo abstracts impl.">
    public function hasTime() { return TRUE; }
    public function hasDate() { return TRUE; }
    // </editor-fold>
}