<?php
namespace WDB\Analyzer;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class View extends TableView
{
    public function getDisplayDefault()
    {
        $a = parent::getDisplayDefault();
        array_unshift($a, "-edit");
        return $a;
    }
    
    public function isUIEdit()
    {
        return false;
    }

    public function isUIDelete()
    {
        return false;
    }
}
