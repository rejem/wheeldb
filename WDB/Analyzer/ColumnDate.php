<?php
namespace WDB\Analyzer;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnDate extends ColumnTimeInfo
{
    protected $stringFormat = 'j.n.Y';
    protected $_wdbType = 'Date';
    
    // <editor-fold desc="iColumn overrides">
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\Date($value);
    }
    public function getWebUIClass()
    {
        return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnDate');
    }
    
    public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
    {
        parent::fetchValidationRules($rules);
        $rules->pattern('~^'.ColumnTimeInfo::$datePattern.'$~x',
                WDB\Lang::s('validator.messages.date'));
    }
    // </editor-fold>
    
    // <editor-fold desc="ColumnTimeInfo abstracts impl.">
    public function hasTime() { return FALSE; }
    public function hasDate() { return TRUE; }
    // </editor-fold>
    
}