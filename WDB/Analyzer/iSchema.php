<?php
namespace WDB\Analyzer;
use WDB;

/**
 * Interface for database schemas.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iSchema
{
    /**
     * Returns name identifier of schema
     * 
     * @return string
     */
    public function getName();
    
    /**
     * Returns array of all table entities (tables, views) in schema
     * 
     * @return iTable[]
     */
    public function getTables();
    
    /**
     * Returns database this schema originates from.
     * 
     * @return WDB\Database
     */
    public function getDatabase();
    
    /**
     * Returns model configuration
     * 
     * @return array
     */
    public function getModel();
}