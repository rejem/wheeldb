<?php
namespace WDB\Analyzer;
use WDB;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnInteger extends Column
{
    private $min;
    private $max;
    
    /**
     *
     * @param string column name
     * @param TableView table object
     * @param WDB\Structure\ColumnMetaInfo column information
     * @param string[] range - $range[0] specifies minimum of the number, $range[1] maximum.
     */
    public function __construct($name, TableView $table, WDB\Structure\ColumnMetaInfo $meta, $range = NULL)
    {
        parent::__construct($name, $table, $meta);
        if ($range !== NULL)
        {
            $this->min = (string)$range[0];
            $this->max = (string)$range[1];
            if (!$meta->signed)
            {
                $this->max = bcadd($this->max, -$this->min);
                $this->min = '0';
            }
        }
        else
        {
            $this->min = $this->max = NULL;
        }
    }
    
    /**
     * Return count of valid digits in number
     *
     * @return int
     */
    public function getPrecision()
    {
        return $this->meta->precision;
    }
    
    /**
     * Return number scale. Shifts the number in database  XXX*10^<scale>
     *
     * @return int
     */
    public function getScale()
    {
        return $this->meta->scale;
    }
    
    /**
     * Returns true if column's values are signed (can be negative)
     *
     * @return bool
     */
    public function getSigned()
    {
        return $this->meta->signed;
    }
    
    /**
     * Returns maximum integer value determined by database datatype (not a user constraint)
     * 
     * Represented by string because database value range can be (and often is) greater than PHP integer range
     * 
     * @return string
     */
    public function getMaxValue()
    {
        return $this->max;
    }
    
    /**
     * Returns maximum integer value determined by database datatype (not a user constraint)
     * 
     * Represented by string because database value range can be (and often is) greater than PHP integer range
     * 
     * @return string
     */
    public function getMinValue()
    {
        return $this->min;
    }
    
    // <editor-fold defaultstate="collapsed" desc="iColumn overrides">    
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\Integer($value);
    }
    
    public function getWebUIClass()
    {
        return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnInteger');
    }
    
    public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
    {
        parent::fetchValidationRules($rules);
        $rules->integer();
        if ($this->min !== NULL)
        {
            $rules->minValue($this->min);
        }
        if ($this->max !== NULL)
        {
            $rules->maxValue($this->max);
        }
        if (!$this->isNullable() && !$this->isAutoColumn())
        {
            $rules->required();
        }
    }
    // </editor-fold>
}
