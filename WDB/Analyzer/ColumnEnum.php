<?php
namespace WDB\Analyzer;
use WDB,
    WDB\Exception;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnEnum extends Column implements WDB\iSelectOptions
{
    /**@var array key=>value pairs*/
    protected $dictionary;
    
    
    /**
     *
     * @param string column name
     * @param TableView table object
     * @param WDB\Structure\ColumnMetaInfo column information
     * @param array key=>value pairs of options
     */
    public function __construct($name, TableView $table, WDB\Structure\ColumnMetaInfo $meta, array $dictionary)
    {
        parent::__construct($name, $table, $meta);
        $this->dictionary = array();
        foreach ($dictionary as $val)
        {
            $this->dictionary[$val]=$val;
        }        
    }
    
    /**
     * Returns true if specified key exists in options dictionary
     *
     * @param string key
     * @return bool
     */
    public function keyExists($value)
    {
        return in_array($value, $this->dictionary);   
    }
    
    // <editor-fold defaultstate="collapsed" desc="WDB\iSelectOptions implementation">
    public function getOptions()
    {
        return $this->dictionary;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="iColumn overrides">
    
    public function getWrapperClass()
    {
        $c = parent::getWrapperClass();
        if ($c === NULL) $c = '\\WDB\\Wrapper\\ColumnSelect';
        return $c;
    }
    
    public function getWebUIClass()
    {
        return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnSelect');
    }
    
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\Enum($value);
    }
    
    public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
    {
        parent::fetchValidationRules($rules);
        if (!$this->isNullable())
        {
            $rules->required();
        }
    }
    // </editor-fold>
}
