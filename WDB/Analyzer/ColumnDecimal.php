<?php
namespace WDB\Analyzer;
use WDB;

/**
 * Decimal datatype column.
 * Note: the number range is defined by scale and precision, there is no other limitation.
 * 
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnDecimal extends Column
{
    // <editor-fold defaultstate="collapsed" desc="iColumn overrides">
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\Float($value);
    }
    
    public function getWebUIClass()
    {
        return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnNumeric');
    }
    
    public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
    {
        parent::fetchValidationRules($rules);
        $rules->float();
        if (!$this->meta->nullable && !$this->meta->isAutoColumn)
        {
            $rules->required();
        }
    }
    // </editor-fold>
}
