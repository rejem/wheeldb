<?php
namespace WDB\Analyzer;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnSet extends Column implements WDB\iSelectOptions
{
    // <editor-fold defaultstate="collapsed" desc="iColumn overrides">
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\Set($value);
    }
    
    public function getWebUIClass()
    {
        $c = parent::getWrapperClass();
        if ($c === NULL) $c = '\\WDB\\WebUI\\ColumnMultiChoice';
        return $c;
    }
    
    public function getWrapperClass()
    {
        $c = parent::getWrapperClass();
        if ($c === NULL) $c = '\\WDB\\Wrapper\\ColumnSelect';
        return $c;
    }

    // </editor-fold>
    
    /**@var array key=>value pairs*/
    protected $dictionary;
    
    
    /**
     *
     * @param string column name
     * @param TableView table object
     * @param WDB\Structure\ColumnMetaInfo column information
     * @param array key=>value pairs of options
     */
    public function __construct($name, TableView $table, WDB\Structure\ColumnMetaInfo $meta, array $dictionary)
    {
        parent::__construct($name, $table, $meta);
        $this->dictionary = array();
        foreach ($dictionary as $val)
        {
            $this->dictionary[$val]=$val;
        }        
    }
    
    /**
     * Returns true if specified key exists in options dictionary
     *
     * @param string key
     * @return bool
     */
    public function keyExists($value)
    {
        return in_array($value, $this->dictionary);   
    }
    
    // <editor-fold defaultstate="collapsed" desc="WDB\iSelectOptions implementation">
    public function getOptions()
    {
        return $this->dictionary;
    }
    // </editor-fold>
}
