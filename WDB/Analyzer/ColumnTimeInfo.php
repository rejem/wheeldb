<?php
namespace WDB\Analyzer;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB;
 */
abstract class ColumnTimeInfo extends Column
{
    protected $stringFormat = '';
    protected $_wdbType = 'DateTime';
    /**
     * indicates whether this column type contains information about time.
     * @return bool
     */
    abstract public function hasTime();

    /**
     * indicates whether this column type contains information about date.
     * @return bool
     */
    abstract public function hasDate();

    protected static $datePattern = '\\s*(?:
               (?:0?[1-9]|[1-2][0-9]|3[0-1]) # 31 days
               \\s*[.-/]\\s*
               (?:1|3|5|7|8|10|12)
               |
               (?:0?[1-9]|[1-2][0-9]|30) # 30 days
               \\s*[.-/]\\s*
               (?:4|6|9|11)
               |
               (?:0?[1-9]|1[0-9]|2[0-8]) # 29 days (we will not check for existence of 29 february and also two rare history cases of 30 feb)
               \\s*[.-/]\\s*
               2)
               \\s*[.-/]\\s*
            [0-9]{4}\\s*';
    protected static $timePattern = '\\s*
                            (?:[0-1]?[0-9]|2[0-3]) #hour
                            \\s*[.-/:]\\s*
                            [0-5][0-9] #minute
                            (\\s*[.-/:]\\s*[0-5][0-9])? #second (obligatory)
                            \\s*
                             ';

    public function getStringValue($value)
    {
        return $value instanceof \DateTime ? $value->format($this->stringFormat) : (string)$value;
    }

    public function setFieldValue($value)
    {
        if ($value === NULL) return NULL;
        try
        {
            $ret = $value instanceof \DateTime ? $value : new \DateTime($value);
            $ret->_wdbType = $this->_wdbType;
            return $ret;
        }
        catch (\Exception $ex)
        {
            return $value;
        }
    }
}