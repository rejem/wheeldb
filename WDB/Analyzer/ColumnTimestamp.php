<?php
namespace WDB\Analyzer;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnTimestamp extends ColumnTimeInfo
{
    /**@var bool*/
    private $onupd_curts;
    /**
     *
     * @param string column name
     * @param TableView table object
     * @param WDB\Structure\ColumnMetaInfo meta info
     * @param bool determines if on update current_timestamp is set
     */
    public function __construct($name, iTable $table, WDB\Structure\ColumnMetaInfo $meta, $onupd_curts = FALSE)
    {
        parent::__construct($name, $table, $meta);
        $this->onupd_curts = (bool)$onupd_curts;
    }
    
    /**
     * Returns true if this column has ON UPDATE CURRENT_TIMESTAMP property (supported by i.e. MySQL, can be emulated on others)
     *
     * @return bool
     */
    public function isOnUpdateCurrentTimestamp()
    {
        return $this->onupd_curts;
    }
    
    // <editor-fold desc="iColumn overrides">
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\DateTime($value);
    }
    
    public function getWebUIClass()
    {
        return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnDateTime');
    }
    // </editor-fold>
    
    // <editor-fold desc="ColumnTimeInfo abstracts impl.">
    public function hasTime() { return TRUE; }
    public function hasDate() { return TRUE; }
    // </editor-fold>
}