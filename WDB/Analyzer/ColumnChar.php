<?php
namespace WDB\Analyzer;
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 */
class ColumnChar extends Column
{
    // <editor-fold defaultstate="collapsed" desc="iColumn overrides">
    public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
    {
        parent::fetchValidationRules($rules);
        if ($this->meta->charMaxLength !== NULL)
        {
            $rules->maxLength($this->meta->charMaxLength);
        }
    }
    
    public function valueToDatatype($value)
    {
        return new WDB\Query\Element\Datatype\String($value);
    
        
    }
    
    public function getWebUIClass()
    {
        return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnText');
    }
    // </editor-fold>
    
    /**
     * Get maximum character length.
     *
     * @return int
     */
    public function getMaxLength()
    {
        return $this->meta->charMaxLength;
    }
}
