<?php
namespace WDB\Analyzer;
use WDB;

/**
 * Interface for database table information.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iTable
{  
    // <editor-fold defaultstate="collapsed" desc="Table identification">
    /**
     * Returns name of this table
     * 
     * @return string
     */
    public function getName();
    
    /**
     * creates table identifier object for this table.
     * 
     * @return WDB\Query\Element\TableIdentifier
     */
    public function getIdentifier();
    
    /**
     * creates table identifier object for this table.
     * 
     * @return string
     */
    public function getTitle();
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Wrapper and WebUI class configuration">
    /**
     * Returns name of wrapper class for this table or null if none specified
     * (wrapper factory then should use default one)
     *
     * @return string
     * @see \WDB\Wrapper\iTable
     */

    public function getWrapperClass();
    
    /**
     * Returns name of wrapper class for records in this table or null if none specified
     * (wrapper factory then should use default one)
     *
     * @return string
     * @see \WDB\Wrapper\iRecord
     */
    public function getRecordWrapperClass();
    
    /**
     * Returns name of webui class for this table or null if none specified
     * (webui factory then should use default one)
     *
     * @return string
     * @see \WDB\WebUI\iTable
     */
    public function getWebUIClass();   
    
    /**
     * Returns true if records in this table can display detail in WebUI.
     * 
     * @return bool
     */
    public function isUIDetail();

    /**
     * Returns true if records in this table can be edited in WebUI.
     * 
     * @return bool
     */
    public function isUIEdit();

    /**
     * Returns true if records in this table can be deleted in WebUI.
     * 
     * @return bool
     */
    public function isUIDelete();
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="related objects">
    /**
     * Returns table schema object this table belongs to
     *
     * @return iSchema
     */
    public function getSchema();
    
    /**
     * Returns columns of this table
     *
     * @return iColumn[]
     */
    public function getColumns();
    
    /**
     * Returns names of columns in this table that form a primary key
     *
     * @return array
     */
    public function getPrimaryKey();
    
    /**
     * Returns unique keys - arrays of names of columns in this table that form
     * an unique key. Keys of master array are names of unique key constraints.
     * 
     *
     * @return array
     */
    public function getUniqueKeys();
    
    /**
     * Returns foreign keys of this table in form of array(key name=>key data)
     * 
     * @return WDB\Structure\ForeignKeyData[]
     */
    public function getForeignKeys();
    
    // </editor-fold>
    
    /**
     * loads record validators for the table
     * 
     * @param \WDB\Event\Event $event
     */
    public function fetchValidators(WDB\Event\Event $event);
    
    // <editor-fold defaultstate="collapsed" desc="Table configuration">
    /**
     * Returns annotations object with loaded annotations for this table.
     *
     * @return \WDB\Annotation\Reflection
     */
    public function getAnnotations();
    
    /**
     * Returns true if this table shoud be listed in schema webui table list.
     * 
     * @return bool
     */
    public function isListedInSchemaAdmin();
    
    /**
     * Returns true if this table is accessible by webui
     * 
     * @return bool
     */
    public function isWebUIAccessible();
    // </editor-fold>
}
