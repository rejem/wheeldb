<?php
namespace WDB\Event;
/**
 * Interface for all object that can be attached as listeners to WDB\Event\Event instance.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iEventListener
{
    /**
     * called when listened event is raised.
     * @param mixed $arg1,... arguments that were passed to Event::raise
     * @return mixed
     */
    public function raise();    
}