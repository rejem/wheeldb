<?php
namespace WDB\Event;

/**
 * Event listener implementation that raises specified callback method.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class CallbackListener implements iEventListener
{
    /**@var callback*/
    protected $callback;
    
    /**
     *
     * @param callback
     */
    public function __construct($callback)
    {
        $this->callback = $callback;
    }
    
    public function raise()
    {
        call_user_func_array($this->callback, func_get_args());
    }
}
