<?php
namespace WDB\Structure;
/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property \WDB\Query\iSelectResult $data
 * @property int $count
 */
class TableWrapperCache extends Structure
{
    public function __construct()
    {
        parent::__construct();
        $this->purge();
    }
    
    public function purge()
    {
        $this->data = NULL;
        $this->count = NULL;
    }
}