<?php
namespace WDB\Structure;

/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $table table name
 * @property-read string $schema schema name
 * @property-read string $connection connection unique WDB name
 */
class TableLocator extends Structure{}