<?php
namespace WDB\Structure;
use WDB;

/**
 * datasource filter rule.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property ColumnIdentifier $column
 * @property-read mixed $value
 */
class FilterRule extends Structure{
    public static function create(WDB\Query\Element\ColumnIdentifier $column, $value)
    {
        return new self(array('column'=>$column, 'value'=>$value));
    }
}
