<?php
namespace WDB\Structure;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $name
 * @property-read string $schema
 * @property-read string $table
 * @property-read string[] $columns
 * @property-read string $refSchema
 * @property-read string $refTable
 * @property-read string[] $refColumns
 * @property-read int $onUpdate
 * @property-read int $onDelete
 */
class ForeignKeyData extends Structure
{
    const CASCADE = 1;
    const SETNULL = 2;
    const RESTRICT = 3;
}
