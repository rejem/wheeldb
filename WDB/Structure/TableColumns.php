<?php
namespace WDB\Structure;

/**
 * table columns information
 * 
 * structure for interchange between database driver and analyzer
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read \WDB\Analyzer\Column[] $columns list of column data
 * @property-read string[][] $unique list of unique keys (elements are also arrays because
 * unique key can contain multiple columns)
 * @property-read string[] $primary list of columns of primary key
 * @property-read ForeignKeyData[] $foreign list of foreign key definitions
 */
class TableColumns extends Structure{}
