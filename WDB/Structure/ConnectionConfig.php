<?php
namespace WDB\Structure;

/**
 * database connection information
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $driver
 * @property-read string $host
 * @property-read string $user
 * @property-read string $password
 * @property-read string $schema
 * @property-read string $charset
 * @property-read int $port
 * @property-read int $loglevel
 * @property-read string|bool|null $model - string: model name; false: do not use model; null: same as schema name if present
 * 
 * @property-read string $ociService oracle specific property
 * @property-read string $ociServerType oracle 11 specific property
 * @property-read string $ociInstanceName oracle 11 specific property
 */
class ConnectionConfig extends Structure
{
    /**
     *
     * @param string driver name
     * @param string dbserver hostname
     * @param string user name
     * @param string user password
     * @param string schema name
     * @param string|NULL|bool model name; NULL for same as schema name (if found), FALSE for not using model file
     * @param string charset identifier
     * @param int port
     * @param int loglevel - enumeration of WDB\Query\Log::LOG_*
     * @return ConnectionConfig 
     */
    public static function create($driver, $host, $user = NULL, $password = NULL, $schema = NULL, $model = NULL, $charset = NULL, $port = NULL, $loglevel = NULL)
    {
        return new ConnectionConfig(array(
            'driver'=>$driver,
            'host'=>$host,
            'user'=>$user,
            'password'=>$password,
            'schema'=>$schema,
            'model'=>$model,
            'charset'=>$charset,
            'port'=>$port,
            'loglevel'=>$loglevel));
    }
}
