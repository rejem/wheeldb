<?php
namespace WDB\Structure;

/**
 * table column meta information
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property mixed $default default value
 * @property-read bool $nullable can have null value
 * @property-read bool $isAutoColumn set to true by db driver if the column is
 * filled automatically / so it should not be edited manually by user
 * @property-read bool $isAutoIncrement true if the column is mysql-like auto increment column
 * or emulated behaviour on other platforms like Oracle
 * @property-read string $comment textual comment of the column (may contain additional meta information)
 * @property-read int $charMaxLength maximum length in case of character data
 * @property-read string $integerMin
 * @property-read string $integerMax
 * @property-read int $precision integer precision (number of valid digits)
 * @property-read int $scale integer scale (number of decimal places in case of decimal datatype)
 * @property-read bool $signed true when column type is a signed integer (not defined when it is not an integer)
 * @property-read string $extra extra database configuration
 * @property-read string $columnType full database data type
 */
class ColumnMetaInfo extends Structure{}
