<?php
namespace WDB\Ajax;
use WDB,
        WDB\Validation\ColumnRules;

/**
 * AJAX arguments:
 * rule     : WDB\Validation\Rules::enumeration_constant
 * value    : validated value
 * argument : optional argument passed to the validator (i.e. validating regular expression)
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnValidator implements iHandler
{
    /**
     *
     * @return array
     * @throws WDB\Exception\WDBException
     */
    public function handle()
    {
        if (!isset($_REQUEST['rule'])) throw new WDB\Exception\WDBException('missing "rule" parameter.');
        if ($_REQUEST['rule'] == ColumnRules::PATTERN)
        {
            //@ is intentionally here - suppress warnings when passed regular expression is not valid
            return array('success'=>@preg_match($_REQUEST['argument'], $_REQUEST['value']));
        }
        elseif ($_REQUEST['rule'] == ColumnRules::CALLBACK)
        {
            return array('success'=>TRUE); //TODO maybe one day... will be tricky to use wisely
        }
        else
        {
            throw new WDB\Exception\WDBException('Invalid rule type.'.$_REQUEST['rule']);
        }
    }
}
