<?php
namespace WDB\Ajax;

/**
 * AJAX request handler interface.
 * Used by public/ajax.php, which looks for class named "WDB\Ajax\{$_REQUEST['method']}" implementing this interface.
 * Instantiates it, calls handle() and sends JSON encoded return value to output.
 *
 * Arguments to the handler are retrieved directly from _REQUEST array. Each validator class should have its arguments specified
 * in class's doc comment.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iHandler
{
    /**
     * @return mixed (most commonly array, it will be JSON encoded and sent to output)
     */
    public function handle();
}
