<?php
namespace WDB;

/**
 * Locale language file access static class.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Lang
{
    /** @var (string|array)[] language string multi-dimensional */
    private static $langdata = NULL;
    
    /**
     *
     * @param string language string dot-separated path, i.e. foo.bar.baz returns $langdata['foo']['bar']['baz']
     * @return string
     * @throws Exception\BadArgument
     * @throws Exception\LangFileNotFound
     * @throws Exception\ConfigKeyMissing
     */
    public static function s($ident)
    {
        if (self::$langdata === NULL)
        {
            $f = Config::read('langdir', TRUE).Config::read('lang', TRUE)
                    .'.php';
            if (!file_exists($f)) throw new Exception\LangFileNotFound 
                ('Language file for language '.Config::read('lang').' not found.');
            self::$langdata = require $f;
        }
        
        $x = Utils\Arrays::dotPath(self::$langdata, $ident);
        
        if (is_array($x)) throw new Exception\BadArgument
            ("$ident is a group of language string, WDB\Lang::s() accepts only
                single language string identifier");
        return $x;
    }
}
