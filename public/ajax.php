<?php
require_once __DIR__.'/../loader.php';
$class = '\\WDB\\Ajax\\'.$_REQUEST['method'];

if (class_exists($class))
{
    $handler = new $class();
    if ($handler instanceof WDB\Ajax\iHandler)
    {
        try
        {
            $result = $handler->handle();
        }
        catch (Exception $ex)
        {
            $result = array(
                'requestError'=>'Ajax handler thrown an exception of class '.get_class($ex).': '.$ex->getMessage(),
            ); 
        }
    }
    else
    {
        $result = array(
            'requestError'=>'Corresponding ajax handler class does not implement \WDB\Ajax\iHandler interface.',
        );
    }
}
else
{
    $result = array(
        'requestError'=>'Corresponding ajax handler class not found.',
    );
}
echo json_encode($result);