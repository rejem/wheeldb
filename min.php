<?php namespace WDB\Ajax{
/**
 * AJAX request handler interface.
 * Used by public/ajax.php, which looks for class named "WDB\Ajax\{$_REQUEST['method']}" implementing this interface.
 * Instantiates it, calls handle() and sends JSON encoded return value to output.
 *
 * Arguments to the handler are retrieved directly from _REQUEST array. Each validator class should have its arguments specified
 * in class's doc comment.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iHandler
{
/**
     * @return mixed (most commonly array, it will be JSON encoded and sent to output)
     */
public function handle();
}
}namespace WDB\Ajax{
use WDB;
/**
 * Server-side form validator verifying that a record identified by _REQUEST['record'] won't violate
 * any unique constraint in a table _REQUEST['table']
 * 
 * Arguments:
 * table   : db table locator string (@see WDB\Structure\TableLocator)
 * record  : original record string key (or none if new record)
 * keys    : array of all unique keys Key name=>Key data
 * keys[k][implicit] : list of names of columns which are part of the key, but are NOT being currently edited. Their value is not
 *                     known from the form, it must be re-read from the database.
 * keys[k][explicit] : associative array of (column name)=>value pairs of all columns in the key that ARE being currently edited.
 * 
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class UniqueValidator implements iHandler
{
public function handle()
{
$table = WDB\Wrapper\TableFactory::fromLocator(new WDB\Structure\TableLocator($_REQUEST['table']));
$columns = $table->getColumns();
if (empty($_REQUEST['record']) || (isset($_REQUEST['recordIsNull']) && $_REQUEST['recordIsNull'] == 1))
{
$record = NULL;
}
else
{
$record = $table->getRecordByStringPK($_REQUEST['record']);
}
$violated = array();
foreach ($_REQUEST['keys'] as $keyIdent=>$key)
{
$searchKey = array();
if (isset($key['explicit']))
{
foreach ($key['explicit'] as $k=>$v)
{
$searchKey[$k] = isset($columns[$k]) ? $columns[$k]->setFieldValue($v) : $v;
}
}
else
{
continue;  }
if (isset($key['implicit']))
{
if ($record !== NULL)
{
foreach ($key['implicit'] as $k)
{
$searchKey[$k] = $record[$k];
}
}
else
{
foreach ($key['implicit'] as $k)
{
$searchKey[$k] = $table->columns[$k]->getDefault();
}
}
}
 $q = $table->getDatasource()->filter($searchKey);
if ($record !== NULL)
{
$q = $q->not($record->getIdentificationKey());
}
if ($q->count())
{
reset($searchKey);
if (count($searchKey) > 1)
{
$titles = array();
foreach (array_keys($searchKey) as $col)
{
$titles[] = $columns[$col]->getTitle();
}
$msg = str_replace('%fieldlist', implode(', ', $titles), WDB\Lang::s('validator.messages.uniqueGroupViolated'));
}
else
{
$msg = str_replace('%name', $columns[key($searchKey)]->getTitle(), WDB\Lang::s('validator.messages.uniqueViolated'));
}
$violated[] = array('key'=>$keyIdent, 'message'=>$msg);
}
}
return array('success'=>count($violated) == 0, 'violated'=>$violated);
}
}
}namespace WDB\Analyzer{
use WDB;
/**
 * Interface for database column information.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iColumn
{
 /**
     * Returns name identifier of this column
     *
     * @return string
     */
public function getName();
/**
     * Returns ColumnIdentifier class for this column
     *
     * @return \WDB\Query\Element\ColumnIdentifier
     */
public function getIdentifier();
/**
     * Returns human-readable title of this column (it is
     * advised to return identifier name when no title is present)
     *
     * @return string
     */
public function getTitle();
/**
     * returns true when this column is filled automatically by database (auto_increment, current_timestamp etc)
     * 
     * @return bool
     */
public function isAutoColumn();
/**
     * Returns default value for this column
     * 
     * @return \WDB\Datatype\iDatatype
     */
public function getDefault();
/**
     * Returns true if this column is nullable.
     * 
     * @return bool
     */
public function isNullable();
/**
     * retrieves annotations obejct applied to this column
     * 
     * @return Annotation\Reflection
     */
public function getAnnotations();

 /**
     * Returns name of wrapper class for this column or null if none specified
     * (wrapper factory then should use default one)
     * 
     * @return string|null
     * 
     * @throws WDB\Exception\ClassNotFound
     * @see \WDB\Wapper\iColumn
     * 
     */
public function getWrapperClass();
/**
     * Returns name of webui class for this column or null if none specified
     * (webui factory then should use default one)
     * 
     * @return string|null
     * 
     * @throws WDB\Exception\ClassNotFound
     * @see \WDB\WebUI\iColumn
     */
public function getWebUIClass();


/**
     * gets bit pattern integer representation of display modes configuration loaded by
     * analyzer.
     * 
     * If $original parameter is present, display modes not explicitly configured
     * by analyzer are perserved and only these explicitly configured are set
     * in return value.
     * 
     * @param int $original
     * @return int
     * @see \WDB\WebUI\Column::$display
     */
public function getWebUIDisplayMode($original = null);

 /**
     * Returns table analyzer this column is bound to
     *
     * @return iTable
     */
public function getTable();
/**
     * Returns identifier names of foreign keys this column is part of
     *
     * @return array
     */
public function getForeignKeys();

/**
     * Configures Rules object to meet some database constraints(size of numeric types,
     * length of textual types etc)
     * 
     * @param \WDB\Validation\ColumnRules $rules
     */
public function fetchValidationRules(WDB\Validation\ColumnRules $rules);

/**
     * Converts PHP value to SQL query string part using an SQL driver.
     * 
     * @param mixed
     * @return WDB\Query\Element\Datatype\iDatatype
     */
public function valueToDatatype($value);
/**
     * Sets Record's Field value with an appropriate type from an user value
     * 
     * @param mixed
     * @return mixed
     */
public function setFieldValue($value);
/**
     * Get string representation of a value in this column
     * 
     * @param mixed value
     * @return string
     */
public function getStringValue($value);
}}namespace WDB\Analyzer{
use WDB;
/**
 * Interface for database schemas.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iSchema
{
/**
     * Returns name identifier of schema
     * 
     * @return string
     */
public function getName();
/**
     * Returns array of all table entities (tables, views) in schema
     * 
     * @return iTable[]
     */
public function getTables();
/**
     * Returns database this schema originates from.
     * 
     * @return WDB\Database
     */
public function getDatabase();
/**
     * Returns model configuration
     * 
     * @return array
     */
public function getModel();
}}namespace WDB\Analyzer{
use WDB;
/**
 * Interface for database table information.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iTable
{
 /**
     * Returns name of this table
     * 
     * @return string
     */
public function getName();
/**
     * creates table identifier object for this table.
     * 
     * @return WDB\Query\Element\TableIdentifier
     */
public function getIdentifier();
/**
     * creates table identifier object for this table.
     * 
     * @return string
     */
public function getTitle();

 /**
     * Returns name of wrapper class for this table or null if none specified
     * (wrapper factory then should use default one)
     *
     * @return string
     * @see \WDB\Wrapper\iTable
     */
public function getWrapperClass();
/**
     * Returns name of wrapper class for records in this table or null if none specified
     * (wrapper factory then should use default one)
     *
     * @return string
     * @see \WDB\Wrapper\iRecord
     */
public function getRecordWrapperClass();
/**
     * Returns name of webui class for this table or null if none specified
     * (webui factory then should use default one)
     *
     * @return string
     * @see \WDB\WebUI\iTable
     */
public function getWebUIClass();
/**
     * Returns true if records in this table can display detail in WebUI.
     * 
     * @return bool
     */
public function isUIDetail();
/**
     * Returns true if records in this table can be edited in WebUI.
     * 
     * @return bool
     */
public function isUIEdit();
/**
     * Returns true if records in this table can be deleted in WebUI.
     * 
     * @return bool
     */
public function isUIDelete();

 /**
     * Returns table schema object this table belongs to
     *
     * @return iSchema
     */
public function getSchema();
/**
     * Returns columns of this table
     *
     * @return iColumn[]
     */
public function getColumns();
/**
     * Returns names of columns in this table that form a primary key
     *
     * @return array
     */
public function getPrimaryKey();
/**
     * Returns unique keys - arrays of names of columns in this table that form
     * an unique key. Keys of master array are names of unique key constraints.
     * 
     *
     * @return array
     */
public function getUniqueKeys();
/**
     * Returns foreign keys of this table in form of array(key name=>key data)
     * 
     * @return WDB\Structure\ForeignKeyData[]
     */
public function getForeignKeys();

/**
     * loads record validators for the table
     * 
     * @param \WDB\Event\Event $event
     */
public function fetchValidators(WDB\Event\Event $event);
 /**
     * Returns annotations object with loaded annotations for this table.
     *
     * @return \WDB\Annotation\Reflection
     */
public function getAnnotations();
/**
     * Returns true if this table shoud be listed in schema webui table list.
     * 
     * @return bool
     */
public function isListedInSchemaAdmin();
/**
     * Returns true if this table is accessible by webui
     * 
     * @return bool
     */
public function isWebUIAccessible();
}
}
namespace WDB\Annotation{
use WDB;
/**
 * Annotation reader.
 * 
 * Can read annotations from a string or any php reflection class supporting getDocComment().
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @packade WDB
 * @property-read string $text
 */
class Reflection
{
protected $doc = '';
protected $text = '';
protected $tags = array();
protected function __construct(){}
/**
     * Factory method - creates annotation reflection from an object supporting getDocComment().
     *
     * @param object any (reflection) object supporting getDocComment() method
     * @return Reflection
     * 
     * @throws WDB\Exception\BadArgument
     */
public static function fromReflObject($obj)
{
return Reflection::_fromReflObject($obj, __CLASS__);
}
protected static function _fromReflObject($obj, $class)
{
if (!method_exists($obj, 'getDocComment'))
{
throw new WDB\Exception\BadArgument("Cannot obtain doc comment from object without support of getDocComment() method.");
}
$ar = new $class();
$ar->parse($obj->getDocComment());
return $ar;
}
/**
     * Factory method - creates annotation reflection from raw string.
     *
     * @param string annotation comment
     * @return self
     */
public static function fromString($str)
{
return Reflection::_fromString($str, __CLASS__);
}
protected static function _fromString($str, $class)
{
$ar = new $class();
$ar->parse($str);
return $ar;
}
/**
     * Merge current object with additional annotations. Only input string supported.
     *
     * @param string $str
     */
public function merge($str)
{
$this->parse($str);
}
/**
     * Merge current object with NeonWheel configuration
     *
     * @param array $neonWheel
     */
public function mergeNeonWheel($neonWheel, $key = NULL)
{
if ($key != NULL)
{
$key = explode('.', $key);
foreach ($key as $keypart)
{
if (!isset($neonWheel[$keypart])) return;
$neonWheel = $neonWheel[$keypart];
}
}
foreach ($neonWheel as $tagname=>$tagdata)
{
if (is_array($tagdata)) continue;  if (strlen($tagname) > 0 && $tagname{0} == '@')
{
$tagname = substr($tagname, 1);
}
$tagname = strtolower($tagname);
$parsed = $this->parseTag($tagname, $tagdata);
if (!isset($this->tags[$tagname]))
{
$this->tags[$tagname] = array();
}
$this->tags[$tagname][] = $parsed;
}
}
/**
     * Parse string into this object - fills doc, tags and text properties.
     *
     * @param string comment block
     */
protected function parse($comment)
{
$this->doc .= $comment;
 $stripped = preg_replace('~^/\\*\\*(.*)\\*/$~sm', '\\1', $comment);
 $lines = explode("\n", str_replace(array("\r\n", "\r"), "\n", $stripped));
$parsed = NULL;
foreach ($lines as $line)
{
 $line = trim(preg_replace('~^\\s*\\*\\s*(.*?)\\s*~', '\\1', trim($line)));
 if ($line == '')
continue;
if (preg_match('~^@(?<name>[0-9a-z_-]+)(\\s(?<data>.*))?~i', $line, $matches))  {
do
{
$tagname = strtolower($matches['name']);
$tagdata = isset($matches['data']) ? $matches['data'] : NULL;
if ($p = preg_match('~^(?<tagdata>.*?)\\s@(?<name>[0-9a-z_-]+)(\\s(?<data>.*))?~i', $tagdata, $matches))  {
$tagdata = $matches['tagdata'];
}
if (!isset($this->tags[$tagname]))
{
$this->tags[$tagname] = array();
}
$parsed = $this->parseTag($tagname, $tagdata);
$this->tags[$tagname][] = $parsed;
} while ($p);
} else  {
if ($parsed !== NULL)
{
$parsed->appendLine($line);
} else
{
$this->text .= $line;
}
}
}
}
/**
     *
     * @param string $name
     * @param type $data
     * @return Property
     */
protected function parseTag(&$name, $data)
{
return new Data($data);
}
/**
     * returns array of annotations with specified key.
     *
     * @param string $key
     * @return Data[]
     */
public function __get($key)
{
$key = strtolower($key);
if ($key == 'text') return $this->text;
if (isset($this->tags[$key])) return $this->tags[$key];
return array();
}
/**
     * verifies if annotation key exist.
     * 
     * @param string $key
     * @return bool
     */
public function __isset($key)
{
return isset($this->tags[strtolower($key)]);
}
/**
     * returns first annotation value with the specified key.
     *
     * @param string $key
     * @return Data|NULL
     */
public function first($key)
{
if (count($this->$key) == 0) return null;
return $this->{$key}[0];
}
/**
     * returns first annotation value with the specified key.
     *
     * @param string $key
     * @return Data|NULL
     */
public function last($key)
{
if (count($this->$key) == 0) return null;
return WDB\Utils\Arrays::last($this->$key);
}
}}namespace WDB{
/**
 * Base object for all WDB framework objects.
 * Features:
 * WDB magic properties = Auto getter/setter caller
 * If a property "xxx" is accessed, class inheriting BaseObject will access
 * it through getXxx() / setXxx($value) methods if defined.
 * If the property has only getXxx, any attempt to write to it will
 * throw an Exception\ReadOnlyProperty;
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
abstract class BaseObject
{
/**
     * Magic getter. tries to invoke public getXxx() on non-existent xxx method.
     * 
     * @param string name
     * @return type
     */
public function __get($name)
{
$m = 'get'. ucfirst($name);
if ($this->doIHavePublicMethod($m))
{
return $this->$m();
}
throw new Exception\NotExistingProperty($this, $name);
}
/**
     * Magic getter. tries to invoke public setXxx($value) on non-existent xxx method.
     * 
     * @param string name
     * @param string value 
     * @throws WDB\Exception\ReadOnlyProperty
     * @throws WDB\Exception\NotExistingProperty
     */
public function __set($name, $value)
{
$m = 'set'. ucfirst($name);
if ($this->doIHavePublicMethod($m))
{
$this->$m($value);
}
elseif (method_exists($this, 'get'. ucfirst($name)))
{
throw new Exception\ReadOnlyProperty($this, $name);
}
else
{
throw new Exception\NotExistingProperty($this, $name);
}
}
/**
     * Returns true if this object has public method of specified name.
     *
     * @param string $methodName
     * @return bool
     */
private function doIHavePublicMethod($methodName)
{
if (method_exists($this, $methodName))
{
$r = new \ReflectionMethod($this, $methodName);
if ($r->isPublic())
{
return true;
}
}
return false;
}
/**
     * Property is considered to be set if it has an existing getter.
     *
     * @param string name
     * @return bool
     */
public function __isset($name)
{
$m = 'get'. ucfirst($name);
return method_exists($this, $m);
}
/**
     * Unset cannot be called on magic getter/setter shortcuts.
     *
     * @param string name
     */
public function __unset($name)
{
throw new \LogicException("cannot unset an object property.");
}
/**
     * Fills public or magic properties from associative array 
     * 
     * @param array $data
     * @return void
     * @throws WDB\Exception\BadArgument
     */
public function _initPropertiesFromArray($data)
{
if ($data === NULL) return;
if (!is_array($data)) throw new WDB\Exception\BadArgument("default object initializer must be an array");
foreach ($data as $key=>$val)
{
if (property_exists($this, $key))
{
$r = new \ReflectionProperty($this, $key);
if ($r->isPublic())
{
$this->$key = $val;
continue;
}
}
$this->__set($key, $val);
}
}
/**
     * Determines if a property exists on this object. In contrast to __isset, this returns true even on write-only
     * magic properties (setXxx only)
     *
     * @param string $name
     * @return bool
     */
public function _propertyExists($name)
{
return method_exists($this, 'get'.ucfirst($name)) || method_exists($this, 'set'.ucfirst($name));
}
}
}namespace WDB{
/**
 * Framework configuration reader class.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Config
{
private static $data = NULL;

public static $source = NULL;
/**
     * auto-load configuration file
     */
private static function init()
{
if (self::$data === NULL)
{
self::$data = require self::$source;
}
}
/**
     * Checks whether the specified configuration key exists
     * 
     * @param string $key key to verify
     * @return boolean
     */
public static function exists($key)
{
self::init();
try
{
Utils\Arrays::dotPath(self::$data, $key);
return true;
}
catch (Exception\BadArgument $ex)
{
return false;
}
}
/**
     * Returns configuration value for the specified key. Supports dot separated multi-dimensional array key
     * (@see Utils\Arrays::dotPath)
     * 
     * @param string configuration key to read
     * @return mixed
     * @throws Exception\ConfigKeyMissing
     */
public static function read($key, $mandatory = FALSE)
{
self::init();
try
{
return Utils\Arrays::dotPath(self::$data, $key);
}
catch (Exception\BadArgument $ex)
{
if ($mandatory) throw new Exception\ConfigKeyMissing("Mandatory configuration key $key not found.", 0, $ex);
}
return NULL;
}
}
Config::$source = __DIR__.DIRECTORY_SEPARATOR.'../config.php';}namespace WDB{
use SQLDriver,
WDB\Exception;
/**
 * Single database connection (database driver encapsulation class).
 * @property-read string $name connection name identifier
 * @property-read \WDB\Structure\ConnectionConfig $connectionConfig connection configuration
 * @property-read \WDB\SQLDriver\iSQLDriver $driver sql database driver
 * @property-read Analyzer\Schema $schema current active schema
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Database extends BaseObject
{
/** @var iSQLDriver $driver*/
private $driver;
/** @var \WDB\Structure\ConnectionConfig[] */
private static $registeredConnections = array();
/** @var \WDB\Structure\ConnectionConfig */
private $connectionConfig;
/** @var boolean $connected */
private $connected = FALSE;
/** @var Database[] $connections */
private static $connections;

private $schemaCache;
/** @var string $currentSchema current schema name */
private $currentSchema;
/** @var string $name wdb connection persistent name*/
private $name;
/** @var Query\Log */
private $log;
/** @var int */
private $transactionLevel;
/** @var bool */
private $mustRollback = FALSE;
const BU_REPLACE = 1;
const BU_EXCEPTION = 2;
public static $brokenUTFMode = self::BU_REPLACE;
public static function validateBrokenUTF($value)
{
if (!mb_check_encoding($value, 'utf8'))
{
switch (self::$brokenUTFMode)
{
case self::BU_EXCEPTION:
throw new Exception\InvalidUTFString($value);
case self::BU_REPLACE:
$value = preg_replace('~[^(\x00-\x7F)]~','?', $value);
break;
default:
throw new Exception\InvalidBrokenUTFSetting($value);
}
}
return $value;
}
public function getLog()
{
return $this->log;
}
/**
     *
     * @param string|Structure\ConnectionConfig name of connection or connection configuration
     * @throws Exception\ConfigKeyMissing when a named connection is not found
     */
private function __construct($connection)
{
$cname = null;
 if (!$connection instanceof Structure\ConnectionConfig)
{
if (isset(self::$registeredConnections[$connection]))
{
$conn = self::$registeredConnections[$connection];
}
elseif (Config::exists("connections.$connection"))
{
$conn = Config::read("connections.$connection");
if (!isset($conn['user'])) $conn['user'] = NULL;
if (!isset($conn['password'])) $conn['password'] = NULL;
if (!isset($conn['schema'])) $conn['schema'] = NULL;
if (!isset($conn['charset'])) $conn['charset'] = NULL;
if (!isset($conn['port'])) $conn['port'] = NULL;
$conn = new Structure\ConnectionConfig($conn);
}
else
{
throw new Exception\ConfigKeyMissing("connection with name '$connection' not found");
}
$cname = $connection;
}
else
{
if (isset($conn['name']))
{
$cname = $conn['name'];
}
}
 if (!isset($conn->driver) || !isset($conn->host))
throw new Exception\ConfigInsufficient("driver and host are required fields for database connection.");
 if ($cname === NULL)
{
$cname = 'NN-'.$conn->host.'-'.$conn->user.'-'.$conn->schema.'-'.$conn->port;
}
 if (isset(self::$connections[$cname]))
{
for ($i = 0; isset(self::$conns[$cname.$i]); ++$i){}
$cname .= $i;
}
 $driver_class = '\\WDB\\SQLDriver\\'.$conn->driver;
if (!class_exists($driver_class))
{
throw new Exception\DriverNotFound("driver '{$conn->driver}' not found");
}
$this->driver = new $driver_class();
$this->connectionConfig = $conn;
$this->schemaCache = new Analyzer\SchemaCache($this);
$this->currentSchema = $this->connectionConfig->schema;
self::$connections[$cname] = $this;
$this->name = $cname;
$this->log = new Query\Log();
if (isset($conn->loglevel))
{
$this->log->level = $conn->loglevel;
}
$this->transactionLevel = 0;
}
/**
     * Registers a database configuration under specified name for the current execution.
     *
     * @param string $name identifier
     * @param WDB\Structure\ConnectionConfig $config configuration
     *
     * @throws Exception\DuplicateName
     */
public static function registerConnection($name, $config)
{
 if (isset(self::$registeredConnections[$name]))
{
throw new Exception\DuplicateName("Connection with name $name already exists");
}
self::$registeredConnections[$name] = $config;
}
/**
     * Unregisters a database configuration.
     *
     * @param string identifier
     */
public static function unregisterConnection($name)
{
if (isset(self::$registeredConnections[$name])) unset (self::$registeredConnections[$name]);
}
/**
     * Singleton container for database connection.
     * returns instance of \WDB\Database connected with a database
     * (NULL returns connection named 'default' in WDB configuration)
     *
     * @param string database connection identifier
     * @return WDB\Database
     */
public static function getInstance($name)
{
if (!isset(self::$connections[$name]))
{
self::$connections[$name] = new self($name);
}
return self::$connections[$name];
}
public static function getDefault()
{
return self::getInstance('default');
}
/**
     * connection name identifier
     *
     * @return string
     */
public function getName()
{
return $this->name;
}
/**
     * database driver
     *
     * @return \WDB\SQLDriver\iSQLDriver
     */
public function getDriver()
{
$this->lazyConnect();
return $this->driver;
}
/**
     * checks if this object is connected to database and connects if it is not
     */
protected function lazyConnect()
{
if (!$this->connected)
{
$this->connect();
$this->connected = TRUE;
}
}
/**
     * connects to the database. this is not needed to call because database will
     * connect automatically with the first query, only if you want to intentionally create connection at some point.
     */
public function connect()
{
$this->driver->connect($this->connectionConfig);
}
/**
     * disconnect from the database. Not needed to, PHP garbage collector will do the job
     */
public function disconnect()
{
$this->driver->disconnect();
$this->connected = FALSE;
}
/**
     * performs query on a database.
     *
     * @param Query\Query $query
     * @return Result\iQueryResult
     */
public function query(Query\Query $query)
{
$this->lazyConnect();
$query->database = $this;
try
{
  $query = clone $query;
$query->setDatabase($this);
$result = $this->driver->query($query);
$this->log->log($result);
return $result;
}
catch (Exception\QueryError $ex)
{
$this->log->logError($query, $ex);
throw $ex;
}
}
/**
     * performs query on a database.
     *
     * @param string
     * @param bool if true, multiple queries are allowed in one call
     * @return mixed
     */
public function queryRaw($queryStr, $multi = FALSE)
{
$this->lazyConnect();
$query = new Query\Raw($queryStr);
$query->database = $this;
try
{
$result = $this->driver->queryRaw($queryStr, $multi);
$this->log->log($result);
return $result;
}
catch (Exception\QueryError $ex)
{
$this->log->logError($query, $ex);
throw $ex;
}
}
/**
     * returns list of tables in currently selected schema
     *
     * @return Analyzer\Table[]
     */
public function getTables()
{
return $this->schemaCache->getTables();
}
/**
     * get connection configuration information
     * @return Structure\ConnectionConfig
     */
public function getConnectionConfig()
{
return $this->connectionConfig;
}
/**
     * Switches connection to another table schema.
     *
     * @param string|Analyzer\Schema schema to switch
     */
public function setSchema($schema)
{
if ($schema instanceof Analyzer\Schema)
$schema = $schema->name;
$this->driver->changeSchema($schema);
$this->currentSchema = $schema;
}
/**
     * returns a schema in this database
     *
     * @param string|NULL schema identifier - null for current
     * @return WDB\Analyzer\iSchema
     * @throws WDB\Exception\SchemaNotFound
     */
public function getSchema($name = NULL)
{
if ($name === NULL)
{
return $this->schemaCache->getSchema($this->currentSchema);
}
else
{
return $this->schemaCache->getSchema($name);
}
}
/**
     *
     * @param Query\Element\TableIdentifier table identifier
     * @return WDB\Analyzer\iTable
     * @throws WDB\Exception\SchemaNotFound
     * @throws WDB\Exception\TableNotFound
     */
public function getTable(Query\Element\TableIdentifier $identifier)
{
$tables = $this->getSchema($identifier->schema)->getTables();
if (!isset($tables[$identifier->table])) throw new Exception\TableNotFound ("Table {$identifier->schema}.{$identifier->table} not found");
return $tables[$identifier->table];
}
public function startTransaction()
{
if (++$this->transactionLevel == 1)
{
$this->getDriver()->startTransaction();
}
}
public function commit()
{
if ($this->transactionLevel == 0)
{
throw new Exception\InvalidOperation("No running transaction to commit");
}
if (--$this->transactionLevel == 0)
{
if ($this->mustRollback) throw new Exception\InvalidOperation("Insidious transaction requested rollback, unable to commit");
$this->getDriver()->commit();
}
}
public function commitable()
{
return $this->transactionLevel > 0 && !$this->mustRollback;
}
public function rollback()
{
if ($this->transactionLevel == 0)
{
throw new Exception\InvalidOperation("No running transaction to rollback");
}
if (--$this->transactionLevel == 0)
{
$this->getDriver()->rollback();
$this->mustRollback = FALSE;
}
else
{
$this->mustRollback = TRUE;
}
}
}
}namespace WDB\Event{
/**
 * Interface for all object that can be attached as listeners to WDB\Event\Event instance.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iEventListener
{
/**
     * called when listened event is raised.
     * @param mixed $arg1,... arguments that were passed to Event::raise
     * @return mixed
     */
public function raise();
}}namespace WDB\Exception{
use WDB;
/**
 * WDB Framework exceptions
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
/** generic framework exception */
class WDBException extends \Exception{}
/** a method is not supported by a database driver */
class DriverNotFound extends WDBException{}
/** a method is not supported by a database driver */
class UnsupportedByDriver extends WDBException{}
/** this method has not been implemented yet. */
class Unimplemented extends WDBException{}
/** argument of wrong type or data was passed */
class BadArgument extends WDBException{}
/** a mandatory configuration key in config.php is missing */
class ConfigKeyMissing extends WDBException{}
/** insufficient information was passed to perform attempted action */
class ConfigInsufficient extends WDBException{}
/** insufficient information was passed to perform attempted action */
class LangFileNotFound extends WDBException{}
/** a mandatory configuration key in config.php is missing */
class DuplicateName extends WDBException{}
/** trying to write a read-only wdb magic property */
class PropertyException extends WDBException{
public function __construct($object,$property,$message)
{
parent::__construct("Property $property of class ".get_class($object).' '.$message);
}
}
/** trying to write a read-only structure property */
class ReadOnlyProperty extends PropertyException{
public function __construct($object,$property)
{
parent::__construct($object,$property,' is read only');
}
}
/** trying to read a write-only structure property */
class WriteOnlyProperty extends PropertyException{
public function __construct($object,$property)
{
parent::__construct($object,$property,' is write only');
}
}
/** trying to access property that do not exist in a structure */
class NotExistingProperty extends PropertyException{
public function __construct($object,$property)
{
parent::__construct($object,$property,' does not exist');
}
}
/** database connection failure */
class ConnectionFailed extends WDBException{}
/** invalid characters in an sql identifier */
class InvalidIdentifier extends WDBException{}
/** requested an element of a collection that is out of its index bounds */
class OutOfBounds extends WDBException{}
class QueryError extends WDBException{

protected $query;
public function getQuery() { return $this->query; }

protected $databaseMessage;
public function getDatabaseMessage() { return $this->databaseMessage; }

protected $schema;
public function getSchema() { return $this->schema; }

protected $table;
public function getTable() { return $this->table; }

protected $column;
public function getColumn() { return $this->table; }

protected $constraint;
public function getConstraint() { return $this->constraint; }

protected $value;
public function getValue() { return $this->value; }
public function __construct($message, $query)
{
$this->query = $query;
$this->databaseMessage = $message;
parent::__construct($query."\n\n".$message);
}
public function logInfo()
{
$classSimpleName = \WDB\Utils\Arrays::last(explode('\\', get_class($this)));
return array(
'class'=>$classSimpleName,
'message'=>$this->databaseMessage,
'schema'=>$this->schema,
'table'=>$this->table,
'column'=>$this->column,
'constraint'=>$this->constraint,
'value'=>$this->value,
);
}
}
abstract class QueryConstraintError extends QueryError
{
public function __construct($message, $query, $constraintName)
{
parent::__construct($message, $query);
$this->constraint = $constraintName;
}
}
abstract class QueryColumnError extends QueryError
{
public function __construct($message, $query, $columnName)
{
parent::__construct($message, $query);
$this->column = $columnName;
}
}
abstract class QueryExpressionError extends QueryError
{
public function __construct($message, $query, $expr)
{
parent::__construct($message, $query);
$this->value = $expr;
}
}
class QueryDuplicateKeyEntry extends QueryConstraintError
{
public function __construct($message, $query, $entry, $keyNumber)
{
parent::__construct($message, $query, $keyNumber);
$this->value = $entry;
}
}
class QueryUniqueViolation extends QueryConstraintError {}
class QueryForeignChildViolation extends QueryConstraintError {}
class QueryForeignParentViolation extends QueryConstraintError {}
class QueryNotNullColumn extends QueryColumnError {}
class QueryDataTooLong extends QueryColumnError {}
class QueryWrongFieldWithGroup extends QueryExpressionError {}
class QueryWrongGroupField extends QueryExpressionError {}
class QuerySyntaxError extends QueryError {}
class QueryDivisionByZero extends QueryError {}
/** thrown when trying to retrieve non-existing data (i.e. information about non-existing table, database etc.) */
class NotFound extends WDBException{}
class ColumnNotFound extends WDBException{}
class UnknownColumnType extends WDBException{}
class InvalidOperation extends WDBException{}
class ValidationFailed extends WDBException
{
protected $errors;
public function __construct($errors)
{
$this->errors = $errors;
if (count($this->errors) == 0) $this->message = 'unknown error';
else
{
if (is_array($this->errors[0]))
{
$r = $this->errors[0]['message'];
}
else
{
$r = $this->errors[0];
}
if (count($this->errors) > 1)
{
$r .= ' '.sprintf(WDB\Lang::s('validator.excmore'), (count($this->errors)-1));
}
$this->message = $r;
}
}
public function getAllErrors()
{
return $this->errors;
}
}
class ClassNotFound extends WDBException{}
class ValueFormatException extends WDBException{}
class BadEnumeration extends WDBException {}
class KeyException extends WDBException {}
class SchemaNotFound extends WDBException {}
class TableNotFound extends WDBException {}
class InvalidBrokenUTFSetting extends WDBException {}
class InvalidUTFString extends WDBException {}
class GtoParseError extends WDBException {}
class GtoUnknownToken extends GtoParseError {}
class MalformedSerializedData extends WDBException{}
class ParseError extends WDBException{}
class CompileError extends WDBException{}
class InvalidModel extends WDBException{}}
namespace WDB\GTO{
use WDB,
WDB\Exception;
/**
 * Grammar To Object language definition class interface.
 * 
 * @author Richard Ejem
 * @package WDB
 */
interface iLanguage
{
/**
     * Get language keywords (literals taken each as token of the same name)
     * 
     * @return string
     */
public function getKeywords();
/**
     * Get array of regex patterns to parse tokens.
     * 
     * The format of the array should be (token name)=>(regex pattern).
     * Regex patterns are PCRE compatible, but without leading and trailing delimiter.
     * Used PCRE modifiers to these REGEX by default: "Axis"
     * You can override default modifiers by definig $pcreModifiers property to your
     * iLanguage implementing class.
     * 
     * Tokens are looked for in the same order as they are in the returned.
     * So, if you have a token "ABCDE" and all other combinations of letters represent
     * one another type of token ([A-Z]+), place the regex matching ABCDE before [A-Z].
     * 
     * @return array
     */
public function getTokenPatterns();
/**
     * Return list of token names which can be thrown away (like insignificant whitespaces).
     * 
     * @return array
     */
public function getTossTokens();
/**
     * Get grammar definition.
     * 
     * @return Grammar
     */
public function getGrammar();
/**
     * Transform Start nonterminal derivation tree into an object.
     * 
     * @param array $args
     * @return object
     */
public function objectFromStart($args);
}}namespace WDB\GTO{
use WDB,
WDB\Exception;
/**
 * Query parser. Uses WDB SQL dialect.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @method object parse(string $source) parse source string into an object
 * @method static object parse(string|iLanguage $language, string $source) parse source string into an object
 */
final class Parser {

private $language;

private $g;

private $t;

private $i;
private static $parsers = array();
public function __call($name, $arguments) {
return call_user_func_array(array($this, '_'.$name), $arguments);
}
public static function __callStatic($name, $arguments) {
return call_user_func_array(array('self', 's_'.$name), $arguments);
}
/**
     *
     * @param string|iLanguage $language
     * @param string $source
     * @return Query
     */
private static function s_parse($language, $source, $symbol = 'Start')
{
$class = is_string($language) ? $language : get_class($language);
if (!isset(self::$parsers[$class]))
{
self::$parsers[$class] = new self($language);
}
return self::$parsers[$class]->parse($source, $symbol);
}
public function __construct($language) {
if (is_string($language)) {
$language = new $language;  }
elseif (!$language instanceof iLanguage)
{
throw new Exception\BadArgument("language must be a class name or iLanguage instance");
}
$this->language = $language;
}
private function _parse($source, $symbol = 'Start')
{
$tokens = $this->tokenize($source);
$tree = $this->llParse($tokens, $symbol);
if (!method_exists($this->language, $parseMethod='objectFrom'.$symbol))
{
throw new Exception\InvalidOperation("Language ".get_class($this->language)." cannot directly parse symbol $symbol/");
}
return $this->language->$parseMethod($tree[$symbol]);
}
public function debug($source)
{
$tokens = $this->tokenize($source);
$this->dumpTokens($tokens);
$tree = $this->llParse($tokens);
$this->dumpTree($tree);
}
private function dumpTokens($tokens)
{
foreach ($tokens as $token)
{
echo $token->type.': '.$token->content."\n";
}
}
private function dumpTree($tree, $indent = 0)
{
foreach ($tree as $key=>$val)
{
echo str_repeat('|  ', $indent);
if ($val instanceof Token)
{
echo "$key:{$val->type}={$val->content}\n";
}
elseif ($val === NULL)
{
echo "$key:\xCE\xBB\n";
}
elseif (is_array($val))
{
echo "$key ->\n";
$this->dumpTree($val, $indent+1);
}
}
}
private function is_terminal($s)
{
return strlen($s) > 0 && ctype_lower($s{0});
}
private function resolveAlias($symbol)
{
if (isset($this->language->getGrammar()->aliases[$symbol])) return $this->language->getGrammar()->aliases[$symbol];
return $symbol;
}
private function llParse(array $tokens, $symbol = 'Start')
{
$g = $this->language->getGrammar();
$stack = array($symbol);
$ptree = array();
$stackLevels = array(&$ptree);
$stackLevelSizes = array(1);
$upper = NULL;
while (count($stack) > 0)
{
$t = count($tokens) ? $tokens[0]->type : '$END';
$stackTop = $this->resolveAlias($stack[0]);
$stackTopAlias = $stack[0];
$reachedBottom = FALSE;
if ($this->is_terminal($stackTop))
{
if ($t != $stackTop)
{
 throw new Exception\ParseError("Unexpected token: $t({$tokens[0]->content}), expected {$stack[0]}");
}
else
{
$stackLevels[0][$stackTopAlias] = array_shift($tokens);  --$stackLevelSizes[0];
$reachedBottom = TRUE;
array_shift($stack);  }
}
else
{
$rule = NULL;
if (!isset($g->parsingTable[$stackTop]) || !isset($g->parsingTable[$stackTop][$t]))
{
if (isset($g->lambdaRules[$stackTop])) {
$rule = $g->lambdaRules[$stackTop];
}
else {
throw new Exception\ParseError("Unexpected token: $t({$tokens[0]->content}), expected one of: ".implode(', ', array_keys($g->parsingTable[$stackTop])));
}
}
--$stackLevelSizes[0];
$nt = array_shift($stack);  if ($rule === NULL)
{
$rule = $g->parsingTable[$stackTop][$t];
}
$postfix = '';
if (isset($stackLevels[0][$stackTopAlias]))
{
$postfix = 0;
while(isset($stackLevels[0][$stackTopAlias.$postfix])) ++$postfix;
}
if (count ($g->rules[$rule]->rewriteTo) > 0)
{
array_unshift($stackLevelSizes, count($g->rules[$rule]->rewriteTo));
$stackLevels[0][$stackTopAlias.$postfix] = array();
array_unshift($stackLevels, NULL);
$stackLevels[0] = &$stackLevels[1][$stackTopAlias.$postfix];
$stack = array_merge($g->rules[$rule]->rewriteTo, $stack);  }
else
{
$stackLevels[0][$stackTopAlias.$postfix] = NULL;
$reachedBottom = TRUE;
}
}
while ($reachedBottom && count($stackLevels) > 1 && $stackLevelSizes[0] == 0)
{
array_shift($stackLevelSizes);
array_shift($stackLevels);
}
}
if (count($tokens) > 0)
{
throw new Exception\ParseError("Unexpected token: {$tokens[0]->type}, expected end of input");
}
return $ptree;
}
private function tokenize($input)
{
$input = str_replace("\r", '', $input);
$tokens = array();
$offset = 0;
while ($offset < strlen($input))
{
$token = NULL;
if (preg_match('~(?:'.$this->language->getKeywords().')~Ai', $input, $m, 0, $offset))
{
$token = Token::create(strtolower(str_replace(' ', '_', $m[0])), $m[0]);
}
else
{
foreach ($this->language->getTokenPatterns() as $tokenType=>$pattern)
{
if (preg_match('~'.str_replace('~', '\\~', $pattern).'~Axis', $input, $m, 0, $offset))
{
$token = Token::create($tokenType, $m[0]);
break;
}
}
}
if ($token === NULL)
{
throw new Exception\GtoUnknownToken("Unknown token at offset $offset of input $input");
}
if (strlen($m[0]) == 0)
{
throw new Exception\GtoUnknownToken("Internal tokenizer error - matched empty token at offset $offset of input $input");
}
$offset += strlen($m[0]);
if (!in_array($token->type, $this->language->getTossTokens()))
{
$tokens[] = $token;
}
}
return $tokens;
}
}}namespace WDB\GTO{
use WDB,
WDB\Exception,
WDB\Query,
WDB\Query\Element;
class WsqlLanguage implements iLanguage
{
private static $grammar = NULL;
public static function parse($source, $symbol = 'Start')
{
return Parser::parse(__CLASS__, $source, $symbol);
}
public function getKeywords()
{
return 'SELECT|INSERT|UPDATE|DELETE|REPLACE|IGNORE|INTO|FROM|LEFT|RIGHT|OUTER|INNER|NATURAL|JOIN|USING|WHERE|IS|NOT|NULL'.
'|GROUP BY|HAVING|LIMIT|ORDER BY|DESC|ASC|UNION|ALL|DISTINCT|AS|ON|DUPLICATE|KEY|VALUES|ODKU|SET|AND|'.
'OR|NOT|XOR|NULL';
}
public static function quoteIdentifier($identifier)
{
if ($identifier instanceof Element\ColumnIdentifier)
{
$i = '';
if ($identifier->schema)
{
$i .= self::quoteIdentifier($identifier->schema).'.';
}
if ($identifier->table)
{
$i .= self::quoteIdentifier($identifier->table).'.';
}
return $i.self::quoteIdentifier($identifier->column);
}
return '`'.str_replace('`', '``', $identifier).'`';
}
public static function quoteLiteral($literal)
{
if ($literal instanceof \DateTime)
{
return '\''.$literal->format('Y-m-d H:i:s').'\'';
}
return '\''.str_replace(array('\'', '\\'), array('\\\'', '\\\\'), $literal).'\'';
}
public function getTossTokens()
{
return array('whitespace', 'comment');
}
public function getTokenPatterns()
{
$identifier = '(?:
                [a-z_][a-z0-9_]* #unquoted identifier
                |
                `(?:[^`]|``)*` #` quoted identifier
                |
                "(?:[^`]|``)*" #" quoted identifier
            )';
return array(
'number'=>'[0-9]+(?:\\.[0-9]+)?',  'literal'=>"\'(?:\\\\.|[^'\\\\])*'", 'identifier'=>$identifier,
'left_bracket'=>'\\(',
'right_bracket'=>'\\)',
'dot'=>'\\.',
'comma'=>',',
'star'=>'\\*',
'expr_operator'=>'[-+/]',
'equals'=>'=',
'comparator'=>'(?:>=|>|<=|<|!=)',
'comment'=>'--.*?(?:\n|$)',
'whitespace'=>'\\s+',
'raw_code'=>'{(?:[^}]|}})*}',
'at'=>'@',
);
}
public function getGrammar()
{
if (self::$grammar === NULL)
{
self::$grammar = new Grammar(file_get_contents(WDB\Config::read('grammarsdir').'wsql.llg'));
}
return self::$grammar;
}
public function objectFromStart($args)
{
if (isset($args['Select']))
{
return $this->objectFromSelect($args['Select']);
}
elseif (isset($args['Insert']))
{
return $this->objectFromInsert($args['Insert']);
}
elseif (isset($args['Update']))
{
return $this->objectFromUpdate($args['Update']);
}
elseif (isset($args['Delete']))
{
return $this->objectFromDelete($args['Delete']);
}
else
{
throw new Exception\CompileError("Unknown parse tree structure.");
}
}
public function objectFromSelect($args)
{
$fields = array();
$clist = $args;
while (isset($clist['ColumnList']))
{
$clist = $clist['ColumnList'];
$fields[] = new Element\SelectField($this->objectFromExpression($clist['Expression']), $clist['Alias'] ? $clist['Alias']['identifier']->content : NULL);
$clist = $clist['FollowingColumns'];
}
$select = new Query\Select(
$fields,
$this->objectFromTableSource($args['TableSource']),
$this->objectFromWhere($args['Where']),
$this->objectFromOrderBy($args['OrderBy']),
$this->objectFromGroupBy($args['GroupBy']),
$this->objectFromWhere($args['Having'])
);
$this->setLimitAndOffset($select, $args['IsLimit']);
$select->setDistinct((bool)$args['IsDistinct']);
if ($args['IsUnion'])
{
$select->setUnion($this->objectFromSelect($args['IsUnion']['Select']), !isset($args['IsUnion']['UnionType']['distinct']));
}
return $select;
}
public function objectFromInsert($args)
{
$insert = new Query\Insert(
$this->objectFromTableIdentifier($args['TableIdentifier']),
$this->objectFromInsertClause($args['InsertClause'])
);
if ($args['Ignore'])
{
$insert->setMode(Query\Insert::IGNORE);
}
elseif($args['ODKU'])
{
if (isset($args['InsertMode']['replace']))
{
throw new Exception\CompileError('Replace query cannot contain ON DUPLICATE KEY UPDATE clause.');
}
$insert->setMode(Query\Insert::UPDATE);
$insert->setODKUColumns($this->objectFromSetValues($args['ODKU']['SetValues']));
}
elseif(isset($args['InsertMode']['replace']))
{
$insert->setMode(Query\Insert::REPLACE);
}
else
{
$insert->setMode(Query\Insert::UNIQUE);
}
return $insert;
}
public function objectFromUpdate($args)
{
$update = new Query\Update(
$this->objectFromTableIdentifier($args['TableIdentifier']),
$this->objectFromSetValues($args['SetValues']),
$this->objectFromWhere($args['Where'])
);
$update->setOrder($this->objectFromOrderBy($args['OrderBy']));
$this->setLimitAndOffset($update, $args['IsLimit']);
return $update;
}
public function objectFromDelete($args)
{
$delete = new Query\Delete(
$this->objectFromTableIdentifier($args['TableIdentifier']),
$this->objectFromWhere($args['Where'])
);
$delete->setOrder($this->objectFromOrderBy($args['OrderBy']));
$this->setLimitAndOffset($delete, $args['IsLimit']);
return $delete;
}
public function objectFromInsertClause($args)
{
if (isset($args['set']))
{
return $this->objectFromSetValues($args['SetValues']);
}
else
{
$columnIds = array();
$idList = $args['IdentifierList'];
do
{
$columnIds[] = $idList['identifier']->content;
$idList = $idList['NextIdentifier'] ? $idList['NextIdentifier']['IdentifierList'] : FALSE;
} while ($idList);
$exprLists = $args['ExpressionLists'];
if ($exprLists['NextExpressionLists'])
{
throw new Exception\Unimplemented('Multiple row insert is not yet implemented.');
}
$exprList = $exprLists['ExpressionList'];
$columns = array();
foreach ($columnIds as $columnId)
{
if (!$exprList) throw new Exception\CompileError('Column count and value count mismatch');
$columns[$columnId] = $this->objectFromExpression($exprList['Expression']);
$exprList = $exprList['NextExpression'] ? $exprList['NextExpression']['ExpressionList'] : FALSE;
}
if ($exprList) throw new Exception\CompileError('Column count and value count mismatch');
return $columns;
}
}
public function objectFromSetValues($args)
{
$values = array();
do {
$values[$args['identifier']->content] = $this->objectFromExpression($args['Expression']);
$args = $args['NextSetValues'] ? $args['NextSetValues']['SetValues'] : FALSE;
} while ($args);
return $values;
}
public function objectFromTableSource($args)
{
$ts = array();
do {
$ts[] = $this->objectFromSingleTableSource($args['SingleTableSource']);
if ($args['Join'])
{
$ts[] = array(
'type'=>end($args['Join']['JoinType'])->type,
'natural'=>(bool)$args['Join']['JoinNatural'],
'condition' => $args['Join']['JoinCondition']
);
$args = $args['Join'];
}
else
{
break;
}
} while (TRUE);
$result = $ts[0];
for ($i = 1; $i < count($ts); $i += 2)
{
$op = $ts[$i];
switch ($op['type'])
{
case 'left':
$type = Element\Join::LEFT;
break;
case 'right':
$type = Element\Join::RIGHT;
break;
case 'outer':
$type = Element\Join::OUTER;
break;
default:
$type = Element\Join::INNER;
}
if ($op['natural'])
{
$condition = Element\Join::NATURAL;
}
elseif ($op['condition']['using'])
{
$idlist = $op['condition'];
$list = array();
do
{
$list[] = self::unquoteIdentifier($idlist['IdentifierList']['identifier']->content);
$idlist = $idlist['IdentifierList']['NextIdentifier'];
}while ($idlist);
$condition = new Element\JoinUsing($list);
}
else
{
$condition = $this->objectFromCondition($op['condition']['Condition']);
}
$result = new Element\Join($result, $ts[$i+1], $condition, $type);
}
return $result;
}
public function objectFromSingleTableSource($args)
{
if (isset($args['TableIdentifier']))
{
$ti = $this->objectFromTableIdentifier($args['TableIdentifier']);
if ($args['TableAlias'])
{
return Element\AliasedTableSource::create($ti, $args['TableAlias']['identifier']->content);
}
else
{
return $ti;
}
}
if (isset($args['Select']))
{
return Element\AliasedTableSource::create($this->objectFromSelect($args['Select']), $args['identifier']->content);
}
throw new Exception\CompileError();
}
public function objectFromTableIdentifier($args)
{
$id = array(self::unquoteIdentifier($args['identifier']->content), NULL);
if ($args['TDotId'])
{
array_unshift($id, self::unquoteIdentifier($args['TDotId']['identifier']->content));
}
return new Element\TableIdentifier(
$id[0],
$id[1]
);
}
public static function unquoteIdentifier($identifier)
{
if (strlen($identifier) >= 2 && ($identifier[0] == '"' || $identifier[0] == '`'))
{
$identifier = str_replace($identifier[0].$identifier[0], $identifier[0], substr($identifier, 1, strlen($identifier)-2));
}
return $identifier;
}
public static function unquoteLiteral($literal)
{
return preg_replace('~(?<!\\\\)\\\\(\\\\\\\\)*\'~', '\\1\'', substr($literal, 1, strlen($literal)-2));
}
public function objectFromWhere($args)
{
if ($args === NULL) return NULL;
return $this->objectFromCondition($args['Condition']);
}
public function objectFromCondition($args)
{
return $this->objectFromExpression($args['Expression']);
}
public function objectFromExpression($args)
{
$expressions = array();
do {
$expressions[] = $this->objectFromSingleExpression($args['SingleExpression']);
if ($args['Operator'])
{
$expressions[] = end($args['Operator']['OperatorT'])->content;
$args = $args['Operator']['Expression'];
}
else
{
break;
}
} while (TRUE);
$expressions = $this->getInfixOps($expressions, array('*', '/'));
$expressions = $this->getInfixOps($expressions, array('+', '-'));
$expressions = $this->getInfixOps($expressions, array('>', '<', '>=', '<=', '=', '!='));
$expressions = $this->getInfixOps($expressions, array('AND', 'OR', 'XOR'));
if (count($expressions) > 1) throw new Exception\CompileError('Invalid operator.');
return $expressions[0];
}
private function getInfixOps($expressions, array $ops)
{
$result = array();
for ($i = 1; $i < count($expressions); $i += 2)
{
if (in_array($expressions[$i], $ops))
{
$chained = FALSE;
if ($expressions[$i-1] instanceof Element\InfixOperator && $expressions[$i-1]->getOperator() == $expressions[$i])
{
 try {
$expressions[$i+1] = $expressions[$i-1]->addExpression($expressions[$i+1]);
$chained = TRUE;
}
 catch (Exception\InvalidOperation $ex) {}
}
if (!$chained)
{
 $expressions[$i+1] = Element\InfixOperator::create(array($expressions[$i-1], $expressions[$i+1]), $expressions[$i]);
}
}
else
{
$result[] = $expressions[$i-1];
$result[] = $expressions[$i];
}
}
$result[] = end($expressions);
return $result;
}
public function objectFromSingleExpression($args)
{
if (isset($args['Literal']))
{
return $this->objectFromLiteral($args['Literal']);
}
if (isset($args['not']))
{
return Element\LogicOperator::lNot($this->objectFromExpression($args['Condition']));
}
if (isset($args['ColumnIdentifierOrFunction']))
{
if (isset($args['ColumnIdentifierOrFunction']['IoF']['left_bracket']) || $args['ColumnIdentifierOrFunction']['IsWDB'])
{
 if (isset($args['ColumnIdentifierOrFunction']['IdentifierOrAll']['star']))
{
throw new CompileError('Syntax error: function name cannot be a wildcard.');
}
$funcArgs = array();
$argNT = $args['ColumnIdentifierOrFunction']['IoF']['HasArgs'];
while ($argNT)
{
$funcArgs[] = $this->objectFromExpression($argNT['ExpressionList']['Expression']);
$argNT = $argNT['ExpressionList']['NextExpression'];
}
return new Element\DBFunction(
$args['ColumnIdentifierOrFunction']['IdentifierOrAll']['identifier']->content,
$funcArgs,
!$args['ColumnIdentifierOrFunction']['IsWDB']
);
}
else
{
 $args['ColumnIdentifierOrFunction']['DotId2'] = $args['ColumnIdentifierOrFunction']['IoF']['DotId2'];
return $this->objectFromColumnIdentifier($args['ColumnIdentifierOrFunction']);
}
}
if (isset($args['left_bracket']))
{
return $this->objectFromExpression($args['Expression']);
}
if (isset($args['Select']))
{
return $this->objectFromSelect($args['Select']);
}
throw new Exception\CompileError("Unknown Condition rewrite rule used.");
}
public function objectFromLiteral($args)
{
if (isset($args['number']))
{
if (strpos($args['number']->content, '.') === FALSE)
return new Element\Datatype\Integer ($args['number']->content);
else
return new Element\Datatype\Float ($args['number']->content);
}
if (isset($args['literal']))
{
return new Element\Datatype\String(self::unquoteLiteral($args['literal']->content));
}
if (isset($args['null']))
{
return new Element\Datatype\TNull;
}
}
public function objectFromColumnIdentifier_getElm($arg)
{
if (isset($arg['identifier']))
{
return self::unquoteIdentifier($arg['identifier']->content);
}
else
{
return TRUE;
}
}
public function objectFromColumnIdentifier($args)
{
$id = array($this->objectFromColumnIdentifier_getElm($args['IdentifierOrAll']), NULL, NULL);
if ($args['DotId2'])
{
array_unshift($id, $this->objectFromColumnIdentifier_getElm($args['DotId2']['IdentifierOrAll']));
if ($args['DotId2']['DotId'])
{
array_unshift($id, $this->objectFromColumnIdentifier_getElm($args['DotId2']['DotId']['IdentifierOrAll']));
}
}
if ($id[1] === TRUE || $id[2] === TRUE) throw new Exception\CompileError("Invalid column identifier");
return Element\ColumnIdentifier::create(
$id[0],
$id[1],
$id[2]
);
}
public function objectFromGroupBy($args)
{
$result = array();
while ($args)
{
$result[] = $this->objectFromExpression($args['GroupByExpr']['Expression']);
$args = $args['GroupByExpr']['NextGroupByEx'];
}
return $result;
}
private function setLimitAndOffset(Query\SUDQuery $query, $args)
{
if (!$args) return;
$args = $args['Limit'];
if ($args['LimitCount'])
{
$query->setLimit($args['LimitCount']['number']->content);
$query->setOffset($args['number']->content);
}
else
{
$query->setLimit($args['number']->content);
$query->setOffset(0);
}
}
public function objectFromOrderBy($args)
{
$orders = array();
if (!$args) return $orders;
$args = $args['OrderByExpr'];
while(TRUE)
{
$orders[] = new Element\OrderRule(
$this->objectFromExpression($args['Expression']),
isset($args['Dir']['desc']) ? Element\OrderRule::DESC : Element\OrderRule::ASC
);
if ($args['NextOrderByExpr'])
{
$args = $args['NextOrderByExpr']['OrderByExpr'];
}
else
{
return $orders;
}
}
}
}}namespace WDB{
/**
 * Objects implementing this interface can provide sorted and filtered data.
 * 
 * Result of configured filters is retrieved via fetch() method as
 * a WDB\iRowCollection object.
 * Filters are configured by other iDatasource methods.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iDatasource extends
\Countable
{
/**
     * Filters only records with specified column values.
     * Supports fluent interface.
     * 
     * @param array|FilterRule $condition,... associative array with [column name]=>[column value] or FilterRule value
     * @return iDatasource
     */
public function filter($condition);
/**
     * Filters only records not having specified column values. (negation to filter())
     * Supports fluent interface.
     * 
     * @param array associative array with [column name]=>[column value]
     * @return iDatasource
     */
public function not(array $condition);
/**
     * Sorts the result by a key.
     * @param string key
     * @param bool (true is ascending, false is descending)
     * @param bool $prepend prepend or append this rule
     * @return iDatasource
     */
public function sort($key, $asc = TRUE, $prepend = TRUE);
/**
     * Sets selection to subset of (page*pageSize, pagesize).
     * @param int zero-based page number
     * @return iDatasource fluent interface
     */
public function page($number);
/**
     * returns selected page number.
     * 
     * @return int
     */
public function getPage();
/**
     * Returns only a subset starting with $offset and of length $count.
     * 
     * @param int offset
     * @param int count
     * @return iDatasource
     */
public function subset($offset, $count);
/**
     * configures page size supplied to page().
     * 
     * @param int page number
     * @return iDatasource
     */
public function pageSize($number);
/**
     * returns page size supplied to page().
     * 
     * @return int
     */
public function getPageSize();
/**
     * Restricts list of retrieved columns to these specified.
     * 
     * @param string $column1,... list of columns to be retrieved
     * @return iDatasource
     */
public function select();
/**
     * get only distinct rows
     * 
     * @return iDatasource
     */
public function distinct($distinct = TRUE);
/**
     * get filtered data through this datasource object
     * 
     * @return WDB\iRowCollection
     */
public function fetch();
}}namespace WDB{
/**
 * Traversable collection of table rows (basically two-dimensional array),
 * generally used for browsing table data.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iRowCollection extends \Countable, \ArrayAccess, \Iterator
{
/**
     * returns value of first column of first returned row
     * 
     * useful for queries for a signle value, i.e. count of table rows
     * 
     * @return mixed */
public function singleValue();
/**
     * returns first returned row
     * 
     * @return WDB\Query\SelectedRow
     */
public function singleRow();
/**
     * Returns array of all contained rows
     * 
     * @return WDB\Query\SelectedRow[]
     */
public function allRows();
}}
namespace WDB{
/**
 * Objects implementing this interface can supply associative array of options.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iSelectOptions
{
/**
     * Returns associative array containing options for select box, checkbox/radio button list etc.
     * 
     * @return array
     */
public function getOptions();
}
}namespace WDB{
/**
 * Locale language file access static class.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Lang
{
/** @var (string|array)[] language string multi-dimensional */
private static $langdata = NULL;
/**
     *
     * @param string language string dot-separated path, i.e. foo.bar.baz returns $langdata['foo']['bar']['baz']
     * @return string
     * @throws Exception\BadArgument
     * @throws Exception\LangFileNotFound
     * @throws Exception\ConfigKeyMissing
     */
public static function s($ident)
{
if (self::$langdata === NULL)
{
$f = Config::read('langdir', TRUE).Config::read('lang', TRUE)
.'.php';
if (!file_exists($f)) throw new Exception\LangFileNotFound
('Language file for language '.Config::read('lang').' not found.');
self::$langdata = require $f;
}
$x = Utils\Arrays::dotPath(self::$langdata, $ident);
if (is_array($x)) throw new Exception\BadArgument
("$ident is a group of language string, WDB\Lang::s() accepts only
                single language string identifier");
return $x;
}
}
}namespace WDB\Query{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class CachedSelect extends \WDB\BaseObject implements \Countable
{

protected $select;

protected $origSelect;

protected $cache;
/**
     * @param Select
     */
public function __construct(Select $select = NULL)
{
$this->origSelect = $select === NULL ? NULL : clone $select;
$this->select = $select;
$this->cache = new SelectCache();
}
/**
     * Changes internal select query object
     *
     * @param Select
     */
public function setSelect(Select $select)
{
$this->select = $select;
}
/**
     * Returns internal select query object
     *
     * @return Select
     */
public function getSelect()
{
return $this->select;
}
/**
     * shortcut to getResult()
     * 
     * @return int|NULL
     * @throws WDB\Exception\InvalidOperation
     */
public function __invoke()
{
return $this->getResult();
}
/**
     * Get cached select result if already queried for data or bound select object was not changed,
     * otherwise queries for data and caches result
     *
     * @return iSelectResult|NULL
     * @throws WDB\Exception\InvalidOperation
     */
public function getResult()
{
if (!$this->select instanceof Select) throw new WDB\Exception\InvalidOperation("getting data from a cached select without setting select query");
if (serialize($this->select) != serialize($this->origSelect))
{
$this->cache->result = $this->select->run();
$this->cache->count = NULL;
$this->origSelect = clone $this->select;
}
elseif ($this->cache->result === NULL)
{
$this->cache->result = $this->select->run();
}
return $this->cache->result;
}
/**
     * Get count of records in cached select result if already queried for data or bound select object was not changed,
     * otherwise queries for data and caches result
     *
     * @return int|NULL
     * @throws WDB\Exception\InvalidOperation
     */
public function getCount()
{
if (!$this->select instanceof Select) throw new WDB\Exception\InvalidOperation("getting data from a cached select without setting select query");
if (serialize($this->select) != serialize($this->origSelect))
{
$this->cache->count = $this->select->count();
$this->cache->result = NULL;
$this->origSelect = clone $this->select;
}
elseif ($this->cache->count === NULL)
{
$this->cache->count = $this->select->count();
}
return $this->cache->count;
}

/**
     * synonym for getCount() for \Countable interface
     *
     * @return int|NULL
     * @throws WDB\Exception\InvalidOperation
     */
public function count()
{
return $this->getCount();
}
}
}namespace WDB\Query\Element{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iElement
{
/**
     * Converts iElement object to a database query part.
     * 
     * @param WDB\SQLDriver\iSQLDriver driver for conversion
     * @return string raw database query part
     */
public function toSQL(WDB\SQLDriver\iSQLDriver $driver);
}}namespace WDB\Query\Element{
/**
 * Element representing an expression resulting in a value
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iExpression extends iElement
{
}
}namespace WDB\Query\Element{
use WDB,
WDB\Exception;
/**
 * Query logic operator
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
abstract class InfixOperator extends WDB\BaseObject implements iElement
{
protected $op;
protected $expressions;
/**
     * Chain more expressions to this operator. not supported by unary operators
     *
     * @param iExpression|array $expr1,...
     * @return self
     * @throws Exception\InvalidOperation for descendant classes that cannot be chained
     */
public function addExpression($expr1)
{
$this->expressions = array_merge($this->expressions, WDB\Utils\Arrays::linearize(func_get_args()));
return $this;
}
public function getOperator()
{
return $this->op;
}
public function getExpressions()
{
return $this->expressions;
}
public static function create($expressions, $op)
{
if (in_array($op, ExprOperator::$supportedOps))
{
return new ExprOperator($expressions, $op);
}
if (in_array($op, LogicOperator::$supportedOps))
{
return new LogicOperator($expressions, $op);
}
if (in_array($op, Compare::$supportedOps))
{
if (count($expressions) != 2)
{
throw new Exception\BadArgument("Compare operators must have exactly two operands.");
}
return new Compare($expressions[0], $expressions[1], $op);
}
throw new Exception\BadArgument("Invalid operator: $op");
}
public function __construct($expressions, $op)
{
$this->expressions = WDB\Utils\Arrays::linearize($expressions);
$this->op = $op;
}
}
}namespace WDB\Query\Element{
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iTableSource extends iElement
{
/**
     *
     * @param string $alias
     * @return AliasedTableSource
     */
public function alias($alias);
}}namespace WDB\Query\Element{
use WDB,
WDB\Exception;
/**
 * Table join holding two table sources.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package
 */
final class Join extends \WDB\BaseObject implements iTableSource
{
const INNER = 1;
const OUTER = 2;
const LEFT = 3;
const RIGHT = 4;

const CARTESIAN = 1;

const NATURAL = 2;

private $a;

private $b;

private $condition;

private $type;
/**
     *
     * @param iTableSource|string left side of join operator
     * @param iTableSource|string right side of join operator
     * @param iCondition|JoinUsing|int condition or using clause (int is enumeration of Join::[CARTESIAN|NATURAL])
     * @param int type (Enumeration of Join::[INNER|OUTER|LEFT|RIGHT])
     */
public function __construct($a, $b, $condition = self::NATURAL, $type = self::INNER)
{
$this->setTableA($a);
$this->setTableB($b);
$this->setCondition($condition);
$this->setType($type);
}
/**
     * Set join type (Enumeration of Join::[INNER|OUTER|LEFT|RIGHT])
     *
     * @param int type
     * @return Join fluent interface
     */
public function setType($type)
{
$this->type = min(4, max(1, intval($type)));
return $this;
}
/**
     * Get join type (Enumeration of Join::[INNER|OUTER|LEFT|RIGHT])
     *
     * @return int
     */
public function getType()
{
return $this->type;
}
/**
     * Set left side of join operator
     *
     * @param iTableSource|string
     * @return Join  fluent interface
     */
public function setTableA($a)
{
if (is_string($a))
{
$a = new TableIdentifier($a);
}
elseif (!$a instanceof iTableSource)
{
throw new Exception\BadArgument("Table source must be a string or an iTableSource object");
}
$this->a = $a;
return $this;
}
/**
     * Get left side of join operator
     *
     * @return iTableSource
     */
public function getTableA()
{
return $this->a;
}
/**
     * Set right side of join operator
     *
     * @param iTableSource|string
     * @return Join  fluent interface
     */
public function setTableB($b)
{
if (is_string($b))
{
$b = new TableIdentifier($b);
}
elseif (!$b instanceof iTableSource)
{
throw new Exception\BadArgument("Table source must be a string or an iTableSource object");
}
$this->b = $b;
return $this;
}
/**
     * Get right side of join operator
     *
     * @return iTableSource
     */
public function getTableB()
{
return $this->b;
}
/**
     * Set join condition/using/natural/cartesian
     *
     * @param iCondition|JoinUsing|int condition or using clause (int is enumeration of Join::[CARTESIAN|NATURAL])
     * @return Join fluent interface
     */
public function setCondition($condition)
{
if (!$condition instanceof iCondition && !$condition instanceof JoinUsing && (!is_int($condition) || $condition < 1 || $condition > 2))
{
throw new Exception\BadArgument("join condition must be one of iCondition, JoinUsing or [1,2]");
}
$this->condition = $condition;
return $this;
}
/**
     * Get join condition/using/natural/cartesian
     *
     * @return iCondition|JoinUsing|int condition or using clause (int is enumeration of Join::[CARTESIAN|NATURAL])
     */
public function getCondition()
{
return $this->condition;
}
/**
     * Factory method preparing USING clause.
     *
     * @param string|ColumnIdentifier $col1,...
     * @return JoinUsing
     */
public static function using()
{
return new JoinUsing(func_get_args());
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_tableJoin($this);
}
public function alias($alias)
{
return AliasedTableSource::create($this, $alias);
}
}
}namespace WDB\Query\Element{
use WDB;
/**
 * ORDER BY single rule.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property iExpression $expression
 * @property bool $ascending
 * @property bool $descending
 */
class OrderRule extends WDB\BaseObject
{
const ASC = TRUE;
const DESC = FALSE;

protected $expr;
protected $ascending;
public function __construct($expr, $dir = self::ASC)
{
$this->expr = $expr;
$this->ascending = (bool)$dir;
}

public function setExpression(iExpression $expr)
{
$this->expr = $expr;
}

public function getExpression()
{
return $this->expr;
}

public function setDescending($descending)
{
$this->ascending = !$ascending;
}

public function getDescending()
{
return !$this->ascending;
}

public function setAscending($ascending)
{
$this->ascending = (bool)$ascending;
}

public function getAscending()
{
return $this->ascending;
}
/**
     * set ordering direction to ascending.
     * 
     * @return OrderRule fluent interface
     */
public function asc()
{
$this->setAscending(TRUE);
return $this;
}
/**
     * set ordering direction to descending.
     * 
     * @return OrderRule fluent interface
     */
public function desc()
{
$this->setAscending(FALSE);
return $this;
}
}}namespace WDB\Query\Element{
use WDB;
/**
 * Field selected by SELECT consiting of expression and optionally alias
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property iExpression $expression
 * @property string alias
 */
class SelectField extends WDB\BaseObject{

protected $expr;

protected $alias;
public function __construct($expr, $alias = NULL)
{
$this->setExpression($expr);
$this->alias = $alias;
}

public function setExpression($expr)
{
if ($expr instanceof \WDB\Query\Element\iExpression)
{
$this->expr = $expr;
}
else
{
$this->expr = ColumnIdentifier::create($expr);
}
}

public function getColumnIdentifier()
{
if ($this->expr instanceof ColumnIdentifier)
return $this->expr;
return NULL;
}

public function getExpression()
{
return $this->expr;
}

public function setAlias($alias)
{
$this->alias = $alias === NULL ? NULL : (string)$alias;
}

public function getAlias()
{
return $this->alias;
}
/**
     * Return identifier by which this column is identified in result set (alias if present, or column name if not)
     *
     * @return string
     */
public function getIdentifier()
{
if ($this->getAlias()) return $this->alias;
if ($this->getColumnIdentifier()) return $this->getColumnIdentifier()->column;
return NULL;  }
}}namespace WDB\Query{
/**
 * Result of WDB\Database::query execution
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read Query $query 
 */
interface iQueryResult
{
}
}namespace WDB\Query{
use WDB;
/**
 * Result of Select query execution.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iSelectResult extends iQueryResult, WDB\iRowCollection
{
/**
     * returns foreign key columns in result.
     * 
     * @return WDB\Structure\ForeignKey[]
     * @throws WDB\Exception\InvalidOperation if database is not bound so SelectResult cannot search for foreign key data.
     */
public function getForeignKeys();
/**
     * iSelectResult may use the database object to look for foreign keys on the original table.
     * 
     * @param WDB\Database database object the result was performed on
     */
public function bindDatabase (WDB\Database $db);
}
}namespace WDB\Query{
use WDB;
/**
 * Query logging class used by WDB\Database.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Log extends WDB\BaseObject
{

const LOG_ALL = 3;

const LOG_WRITE = 2;

const LOG_ERRONEOUS = 1;

const LOG_NOTHING = 0;

private $logDB = NULL;
/**
     * Defines user ID for current session. This is not automatically configured by framework.
     * 
     * @var int|NULL*/
private $idUser = NULL;
/**
     * Enumeration Log::LOG_*
     *
     * @var int
     */
private $level;
/**
     * Unique value generated once per PHP script execution.
     * 
     * Written to log with each query to help determine which queries were ran during one script execution.
     *
     * @var string
     */
private static $requestID = NULL;
/**
     * Default value for $idUser for each instance.
     *
     * @var int|NULL
     */
public static $id_user = NULL;
/**
     * Default value for $level for each instance.
     * 
     * Enumeration Log::LOG_*
     *
     * @var int
     */
public static $default_level = self::LOG_ERRONEOUS;
/**
     * Creates new LOG with default level and idUser.
     */
public function __construct()
{
$this->level = self::$default_level;
$this->idUser = self::$id_user;
}
/**
     * get user ID for current instance.
     *
     * @return int|NULL
     */
public function getIdUser()
{
return $this->idUser;
}
/**
     * set user id for current instance. Application can supply any integer to Log class as "user ID", which will be
     * saved with all logged queries to easily trace which application user did an action that invoked the query.
     * User logic is NOT maintained by WDB, only this ID is supplied from application.
     *
     * @param int|NULL user ID
     */
public function setIdUser($value)
{
$this->idUser = ($value === NULL ? NULL : intval($value));
}
/**
     * Gets log level. Enumeration of Log::LOG_*
     * 
     * LOG_ALL = Log each query. Useful for debugging, not production - may significantly reduce performance/page loading time
     * LOG_WRITE = Log only write queries. Acceptable for production of highly-dependable systems or occasional change tracing.
     * LOG_ERRONEOUS = Log only queries that result into an error. Recommended production value, no erroneous query
     *      should happen on production.
     * LOG_NOTHING = Do not log anything.
     *
     * @return int
     */
public function getLevel()
{
return $this->level;
}
/**
     * Set log level.
     *
     * @param int log level, enumeration of Log::LOG_*
     */
public function setLevel($value)
{
$this->level = max(self::LOG_NOTHING, min(self::LOG_ALL, $value));
}
/**
     * Log successful query.
     *
     * @param iQueryResult query result
     */
public function log(iQueryResult $q)
{
if ($this->level < self::LOG_WRITE || !$this->connect()) return;
if ($q->query instanceof Select)
{
if ($this->level < self::LOG_ALL) return;
}
elseif ($q->query instanceof Insert)
{
if ($this->level < self::LOG_WRITE) return;
}
elseif ($q->query instanceof Update)
{
if ($this->level < self::LOG_WRITE) return;
}
elseif ($q->query instanceof Delete)
{
if ($this->level < self::LOG_WRITE) return;
}
else
{
if ($this->level < self::LOG_ALL) return;
}
$this->logQuery($q->query, true);
}
/**
     * Logs query that's running threw an exception.
     *
     * @param WDB\Query\Query source query
     * @param WDB\Exception\QueryError exception thrown during execution
     */
public function logError(WDB\Query\Query $query, WDB\Exception\QueryError $ex)
{
if ($this->level < self::LOG_ERRONEOUS || !$this->connect()) return;
$log = $this->logQuery($query, false);
$data = array_merge($ex->logInfo(), array(
'id_query'=>$log->insertId,
));
$query = new Insert('query_errors', $data);
$query->run($this->logDB);
}
/**
     * Inserts basic record to a querylog table.
     *
     * @param Query
     * @param bool
     */
private function logQuery(Query $query, $success)
{
if ($query instanceof Select)
{
$type = 'select';
}
elseif ($query instanceof Insert)
{
$type = 'insert';
}
elseif ($query instanceof Update)
{
$type = 'update';
}
elseif ($query instanceof Delete)
{
$type = 'delete';
}
else
{
$type = 'other';
}
$sid = session_id();
if ($sid == '') $sid = NULL;
$query = new Insert('query_log', $q=array(
'id_user'=>$this->idUser,
'session'=>$sid,
'request'=>self::requestID(),
'query'=>$query->__toString(),
'type'=>$type,
'success'=>$success ? 1 : 0,
));
return $query->run($this->logDB);
}
/**
     * generate/get one unique ID per php script execution.
     *
     * @return string
     */
private static function requestID()
{
if (self::$requestID === NULL)
{
self::$requestID = sha1(uniqid(NULL, TRUE).$_SERVER['REMOTE_ADDR'].$_SERVER['REMOTE_PORT'].$_SERVER['HTTP_USER_AGENT']);
}
return self::$requestID;
}
/**
     * Tries to connect to log database.
     *
     * @return bool true if database is successfully connected.
     */
private function connect()
{
if ($this->logDB === NULL)
{
try
{
$this->logDB = WDB\Database::getInstance('log');
$this->logDB->log->level = self::LOG_NOTHING;
}
catch (WDB\Exception\ConfigInsufficient $ex)
{
$this->logDB = FALSE;
return FALSE;
}
}
elseif ($this->logDB === FALSE)
{
return FALSE;
}
return TRUE;
}
}
}namespace WDB\Query{
use WDB;
/**
 * Query structure base class.
 *
 * Query structure is developed for object-style building SQL queries (change any part of the query anytime!
 * not just wrap query in another like some database libraries do).
 * This base class contains structure parts which are common for all query types:
 * - table
 * - bound database
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property Element\iTableSource $table
 */
abstract class Query extends \WDB\BaseObject
{

protected $table;

protected $database;
public function __construct()
{
$this->table = new Element\Dual();
}
 /**
     * Get current table source.
     *
     * @return Element\iTableSource
     */
public function getTable()
{
return $this->table;
}
/**
     * Set table source.
     *
     * @param Element\iTableSource|string|null table name or source object (or null for no-table queries)
     * @return Query fluent interface
     * @throws WDB\Exception\BadArgument
     */
public function setTable($what)
{
if ($what === NULL) $what = new Element\Dual();
if (! $what instanceof Element\iTableSource) $what = new Element\TableIdentifier($what);
$this->table = $what;
return $this;
}

 /**
     * Bind database to this query. If query is ran without specifying database at run-time, this database is used.
     * If either it is not set, default database is used.
     *
     * @param WDB\Database|NULL database
     * @return Query fluent interface
     */
public function setDatabase(WDB\Database $db = NULL)
{
$this->database = $db;
return $this;
}
/**
     * Get currently bound database.
     *
     * @return WDB\Database
     */
public function getDatabase()
{
return $this->database;
}
/**
     * Executes the query.
     *
     * @param WDB\Database database to run the query on. If not specified, database bound to query is used. If
     *      either it is not specified, default WDB database is used.
     * @return iQueryResult
     */
public function run(WDB\Database $database = NULL)
{
return $this->getUsedDatabase($database)->query($this);
}
/**
     * Get database to run the queries on.
     *
     * Finds first non-null WDB\Database in set:
     * - $db argument
     * - $this->database
     * - WDB\Database::getDefault().
     *
     * @param WDB\Database|NULL database connection
     * @return WDB\Database
     */
protected function getUsedDatabase(WDB\Database $db = NULL)
{
if ($db !== NULL)
{
return $db;
}
elseif ($this->database !== NULL)
{
return $this->database;
}
else
{
return WDB\Database::getDefault();
}
}

/**
     * Prints query string translated by database driver from this object and terminates script.
     *
     * @param bool if true, query string is returned instead of printing and script is not terminated.
     * @param WDB\Database|NULL database connection object containing translation driver
     */
public function debug($return = FALSE, WDB\Database $db = NULL)
{
if ($return) return $this->__toString($db);
echo $this->__toString($db);
die();
}
/**
     * Translate this object to query string by bound database driver.
     *
     * @return string
     */
public function __toString()
{
return $this->getUsedDatabase(NULL)->driver->queryString($this);
}
}}namespace WDB\Query{
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class Raw extends Query
{
private $query;
public function __construct($query)
{
$this->query = $query;
}
public function __toString()
{
return $this->query;
}
}
}namespace WDB\Query{
use WDB,
ArrayAccess,
Iterator,
Countable;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read \WDB\Query\Select $query
 * @property-read array $array
 */
class SelectedRow extends WDB\BaseObject implements ArrayAccess, Iterator, Countable
{

private $result;

private $row;

private $colNumberMap;

public function getResult()
{
return $this->result;
}

public function getQuery()
{
return $this->result->query;
}
/**
     *
     * @param iSelectResult result object
     * @param array row data
     */
public function __construct(iSelectResult $result, array $row)
{
$this->row = $row;
$this->result = $result;
$this->colNumberMap = $this->getColNumberMapping($row);
}
/**
     * Create column number mapping for an associative array.
     * 
     * When a column is accessed through number, the corresponding key
     * is mappedd by this array.
     *
     * @param array associative array from fetch_assoc
     * @return array mapping in form of "row number"=>"row name"
     */
private function getColNumberMapping(array $row)
{
return array_keys($row);
}
/**
     * Get the result as an associative array.
     *
     * @return array
     */
public function getArray()
{
return $this->row;
}
/**
     * Retrieves a query to retrieve record bound to this row by a particular foreign key.
     *
     * @param string $constraint name of FK constraint (higher priority) or column
     * @return SelectedRow
     * 
     * @throws WDB\Exception\QueryException
     */
public function foreign($constraint)
{
if (!isset($this->result->foreignKeys[$constraint]))
{
$found = FALSE;
foreach ($this->result->foreignKeys as $cname=>$key)
{
if (count($key->columns) == 1 && $key->columns[0] == $constraint)
{
$constraint = $cname;
$found = TRUE;
break;
}
}
if (!$found) throw new WDB\Exception\KeyException ("foreign key constraint or column $constraint not found");
}
$foreignKey = $this->result->foreignKeys[$constraint];
$filter = array();
foreach ($foreignKey->refColumns as $colId=>$colName)
{
$filter[$colName] = $this[$foreignKey->columns[$colId]];
}
$query = new Select(Element\ColumnIdentifier::ALL_COLUMNS, new Element\TableIdentifier($foreignKey->refTable, $foreignKey->refSchema));
$query->filter($filter);
return $query->run($this->getQuery()->getDatabase())->singleRow();
}
 /**
     * Returns count of fields in this row.
     *
     * @return int
     */
public function count()
{
return count($this->row);
}

 /**
     * Checks if column with such name or number exists.
     *
     * @param string|int column name or number
     * @return bool
     */
public function offsetExists($offset)
{
return array_key_exists($offset, $this->row) || isset($this->colNumberMap[$offset]);
}
/**
     * Return value of the specified column.
     *
     * @param string|int column name or number
     * @return mixed
     */
public function offsetGet($offset)
{
if (!$this->offsetExists($offset)) throw new \WDB\Exception\OutOfBounds("offset $offset does not exist");
return array_key_exists($offset, $this->row) ? $this->row[$offset] : $this->row[$this->colNumberMap[$offset]];
}
/**
     * Set value of the specified column.
     *
     * @param string|int column name or number
     * @param mixed
     */
public function offsetSet($offset, $value)
{
if (isset($this->colNumberMap[$offset]))
{
$this->row[$this->colNumberMap[$offset]] = $value;
}
else
{
$this->row[$offset] = $value;
}
}
/**
     * Nullate specified column.
     *
     * @param string|int column name or number
     */
public function offsetUnset($offset)
{
$this->offsetSet($offset, NULL);
  }

 /**
     * Return value on the position of internal pointer.
     *
     * @return mixed
     */
public function current()
{
return \current($this->row);
}
/**
     * Return column name on the position of internal pointer.
     *
     * @return string
     */
public function key()
{
return \key($this->row);
}
/**
     * Increase internal pointer and return value on the new position
     *
     * @return mixed
     */
public function next()
{
return \next($this->row);
}
/**
     * Reset the internal pointer
     */
public function rewind()
{
return \reset($this->row);
}
/**
     * Return true if current position of internal pointer is valid (not after last element)
     *
     * @return bool
     */
public function valid()
{
$key = key($this->row);
return ($key !== NULL && $key !== FALSE);
}

/**
     * dedicated for debugging.
     * 
     * simply prints out row data.
     * 
     * @param bool html output (plaintext otherwise)
     * @param bool print (return otherwise)
     */
public function dump($html = true, $print = true)
{
if ($html)
{
$data = "<table>\n";
foreach ($this->row as $key=>$val)
{
$data .= '<tr><th>'.htmlspecialchars($key).'</th><td>'.htmlspecialchars(var_export($val, true))."</td></tr>\n";
}
$data .= "</table>\n";
}
else
{
$data = '';
foreach ($this->row as $key=>$val)
{
$data .= $key.': '.var_export($val, true)."\n";
}
}
if ($print)
{
echo $data;
return '';
}
else
{
return $data;
}
}
}}namespace WDB\Query{
use WDB;
/**
 * Query structure base class.
 *
 * Query structure is developed for object-style building SQL queries (change any part of the query anytime!
 * not just wrap query in another like some database libraries do).
 * This base class contains structure parts which are common for select, update and delete:
 * - where condition
 * - order clause
 * - data count limit and offset
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property Element\iCondition $where
 * @property Element\OrderRule[] $order
 * @property int $limit
 * @property int $offset
 */
abstract class SUDQuery extends Query
{

protected $where = NULL;

protected $order = array();

protected $limit = 0;

protected $offset = 0;

/**
     * Add WHERE condition. Joined to current condition with AND operator.
     *
     * @param Element\iCondition condition
     * @return Query fluent interface.
     */
public function addCondition(Element\iCondition $condition)
{
if ($this->where === NULL)
{
$this->where = $condition;
}
else
{
$this->where = Element\LogicOperator::lAnd($this->where, $condition);
}
return $this;
}
/**
     * Get current WHERE condition.
     *
     * @return Element\iCondition
     */
public function getWhere()
{
return $this->where;
}
/**
     * Set WHERE condition. Previous condition is rewritten.
     *
     * @param Element\iCondition|NULL condition or NULL for no condition
     * @return Query fluent interface
     */
public function setWhere(Element\iCondition $condition = NULL)
{
$this->where = $condition;
return $this;
}

 /**
     * Add order rule to the end of rules list.
     *
     * It will be the last rule applied - only row groups equal from previous ordering will be sorted.
     *
     * @param Element\OrderRule order rule
     * @return Query fluent interface
     */
public function appendOrder(Element\OrderRule $order)
{
$this->order[] = $order;
return $this;
}
/**
     * Add order rule to the beginning of rules list.
     *
     * It will be the first rule applied - all rows will be sorted by this rule first,
     * then equal groups with following rules.
     *
     * @param Element\OrderRule order rule
     * @return Query fluent interface
     */
public function prependOrder(Element\OrderRule $order)
{
array_unshift($this->order, $order);
return $this;
}
/**
     * Get array of all order rules.
     *
     * @return Element\OrderRule[]
     */
public function getOrder()
{
return $this->order;
}
/**
     * Set all order rules as array (previous rules are rewritten)
     *
     * @param Element\OrderRule[] array of rules
     * @return Query  fluent interface
     */
public function setOrder(array $orders)
{
$this->order = array();
if (is_array($orders))
{
foreach ($orders as $order)
{
$this->appendOrder($order);
}
}
else
{
$this->appendOrder($orders);
}
return $this;
}

 /**
     * Set maximum limit how many records will be retrieved in the query. Default is 0 which means no limit.
     *
     * @param int limit
     * @return Query fluent interface
     */
public function setLimit($value)
{
$this->limit = intval($value);
return $this;
}
/**
     * Get record limit.
     *
     * @return int
     */
public function getLimit()
{
return $this->limit;
}
/**
     * Set from which offset in full result set we will start returning rows. Default is 0 (beginning)
     *
     * @param int
     * @return Query fluent interface
     */
public function setOffset($value)
{
$this->offset = intval($value);
return $this;
}
/**
     * Get from which offset in full result set we will start returning rows.
     *
     * @return int
     */
public function getOffset()
{
return $this->offset;
}

public function filter($condition)
{
if (!is_array($condition)) $condition = func_get_args();
foreach ($condition as $key=>$val)
{
if ($val instanceof WDB\Structure\FilterRule)
{
$column = Element\ColumnIdentifier::parse($val->column);
$val = $val->value;
}
else
{
$column = Element\ColumnIdentifier::parse($key);
}
if (!$val instanceof Element\Datatype\iDatatype)
{
$val = Element\Datatype\AbstractType::createDatatype($val);
}
$this->addCondition(Element\Compare::Equals($column, $val));
}
return $this;
}
public function not(array $condition)
{
foreach ($condition as $key=>$val)
{
$this->addCondition(Element\Compare::NEquals(Element\ColumnIdentifier::parse($key), WDB\Query\Element\Datatype\AbstractType::createDatatype($val)));
}
return $this;
}
}}namespace WDB\Query{
use WDB,
WDB\Exception;
/**
 * SQL UPDATE query builder
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property array $columns colname=>value
 */
class Update extends SUDQuery
{

protected $columns;
/**
     *
     * @param Element\TableIdentifier|string table to update records in
     * @param array column name=>value pairs
     * @param array|Element\iCondition $cond
     */
public function __construct($table = NULL, $columns = array(), $where = NULL)
{
$this->setTable($table);
$this->setColumns($columns);
if ($where instanceof Element\iCondition || $where === NULL)
{
$this->setWhere($where);
}
elseif (is_array($where))
{
$this->filter($where);
}
else
{
throw new Exception\BadArgument("Unknown where argument type (use iCondition or array): ".print_r($where, true));
}
}
 /**
     * Add a column name=>value part to the query.
     *
     * @param string column name
     * @param mixed value
     * @return Update fluent interface
     */
public function addColumn($column, $value = NULL)
{
$this->columns[$column] = $value;
return $this;
}
/**
     * Get all column=>value pairs in the query.
     *
     * @return array
     */
public function getColumns()
{
return $this->columns;
}
/**
     * Set all column=>value pairs in the query.
     *
     * @param array column name=>value pairs
     * @return Update fluent interface
     */
public function setColumns($columns)
{
$this->columns = $columns;
return $this;
}
}}namespace WDB\SQLDriver{
use WDB,
WDB\Query\Element;
/**
 * WDB SQL driver interface
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iSQLDriver
{
/**
     * connects to a database server.
     *
     * @param WDB\Structure\ConnectionConfig $connection
     * @throws WDB\Exception\ConnectionFailed
     */
public function connect(WDB\Structure\ConnectionConfig $connection);
/**
     * disconnect from a database server.
     */
public function disconnect();
/**
     * set database connection session character set
     * @param string $charset charset identifier
     */
public function setCharset($charset);
/**
     * get database connection session character set
     * @return string $charset charset identifier
     */
public function getCharset();
/**
     * @param string $schema
     */
public function changeSchema($schema);
/**
     * @param WDB\Query\Query
     * @return iQueryResult
     */
public function query(WDB\Query\Query $query);
/**
     * Sends a raw, platform-dependent query string.
     *
     * @param string
     * @param bool if true, multiple queries are allowed in one call
     * @return iQueryResult
     */
public function queryRaw($query, $multi = FALSE);
/**
     * @param WDB\Query\Select
     * @return iSelectResult
     */
public function select(WDB\Query\Select $query);
/**
     * @param WDB\Query\Insert
     * @return WriteResult
     */
public function insert(WDB\Query\Insert $query);
/**
     * @param WDB\Query\Update
     * @return WriteResult
     */
public function update(WDB\Query\Update $query);
/**
     * @param WDB\Query\Delete
     * @return WriteResult
     */
public function delete(WDB\Query\Delete $query);
/**
     * Compile and return query string
     *
     * @param WDB\Query\Query $q
     * @return string
     */
public function queryString(WDB\Query\Query $q);
/**
     * @param WDB\Analyzer\Schema $schema
     * @return WDB\Analyzer\TableView[] */
public function getTablesViews(WDB\Analyzer\Schema $schema);
/**
     * @param WDB\Analyzer\TableView $table
     * @return WDB\Structure\TableColumns */
public function getTableColumns(WDB\Analyzer\TableView $table);
/**
     * @param string schema name
     * @return bool
     */
public function schemaExists($schema);
public function startTransaction();
public function commit();
public function rollback();
/**
     * @param string function name
     * @param string[] arguments
     * @param bool by default, WDB uses own function names for cross-database compatibility. If native is true,
     *  database function with specified name is directly referenced and no WDB internal naming is used.
     *
     */
public function tosql_function($name, $args, $native = FALSE);
/**
     * @param Element\iExpression
     * @param Element\iExpression
     * @param Element\Compare
     * @return string
     */
public function tosql_compare($expr1, $expr2, $operator);
/**
     * @param Element\iExpression[]
     * @param Element\LogicOperator
     * @return string
     */
public function tosql_logic($expressions, $operator);
/**
     * @param Element\ColumnIdentifier
     * @return string
     */
public function tosql_column(Element\ColumnIdentifier $identifier);
/**
     * @param Element\TableIdentifier
     * @return string
     */
public function tosql_table(Element\TableIdentifier $table);
/**
     * @param Element\Join
     * @return string
     */
public function tosql_tableJoin(Element\Join $join);
/**
     * @param Element\AliasedTableSource
     * @return string
     */
public function tosql_aliasedTableSource(Element\AliasedTableSource $ts);
/**
     * @param Element\JoinUsing
     * @return string
     */
public function tosql_joinUsingClause(Element\JoinUsing $using);
/**
     * escape and quote literal string
     *
     * @param string
     * @return string
     */
public function tosql_string($value);
/**
     * escape and quote identifier
     *
     * @param string
     * @return string
     */
public function tosql_identifier($value);
/**
     * @param int|float|double|string
     * @return string
     */
public function tosql_number($value);
/**
     * @param \DateTime|NULL $value
     * @param bool $date
     * @param bool $time
     * @return string
     */
public function tosql_datetime(\DateTime $value = NULL, $date, $time);
/**
     * @param string|NULL
     * @return string
     */
public function tosql_enum($value);
/**
     * @param array|NULL
     * @return string
     */
public function tosql_set($value);
/**
     * Returns select parsed as sub-query (usually in parentheses)
     *
     * @param Select
     * @return string
     */
public function tosql_subselect(WDB\Query\Select $select);
/**
     * "Dual" table source - dummy table with one column and one row
     *
     * @return string
     */
public function tosql_dual();
/**
     * @param \DateInterval
     * @return string
     */
public function tosql_time(\DateInterval $value = NULL);
/**
     * NULL value literal
     *
     * @return string
     */
public function tosql_null();
}}namespace WDB\SQLDriver\Result{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class MySQL_SelectResult extends WDB\BaseObject implements WDB\Query\iSelectResult
{

private $resultObject;

private $query;

private $database = NULL;

public function getDatabase()
{
return $this->database;
}

public function getQuery()
{
return $this->query;
}

private $offset;

private $fields;
/**
     * @param WDB\Query\Select original select query
     * @param \mysqli_result mysqli result object
     */
public function __construct(WDB\Query\Select $query, \mysqli_result $resultObject)
{
$this->resultObject = $resultObject;
$this->query = $query;
$this->offset = 0;
$fields = $this->resultObject->fetch_fields();
$this->fields = array();
foreach ($fields as $field)
{
$this->fields[$field->name] = $field;
}
}
 public function bindDatabase (WDB\Database $db)
{
$this->database = $db;
}
public function singleValue()
{
return isset($this[0]) && isset($this[0][0]) ? $this[0][0] : NULL;
}
public function singleRow()
{
return $this[0];
}
public function allRows()
{
$result = array();
foreach ($this as $row)
{
$result[] = $row;
}
return $result;
}
public function getForeignKeys()
{
$foreignKeys = array();
if ($this->database === NULL) throw new Exception\InvalidOperation("select result has not bound database connection.");
$schema = $this->database->getSchema();
$fields = array();
 foreach ($this->fields as $field)
{
if ($field->orgtable && !isset($fields[$field->orgtable]))
{
$fields[$field->orgtable] = array();
}
if ($field->orgname) $fields[$field->orgtable][$field->orgname] = $field->name;
}
 foreach ($this->fields as $field)
{
if (isset($schema->tables[$field->orgtable]) && isset($schema->tables[$field->orgtable]->columns[$field->orgname]))
{
$fk = $schema->tables[$field->orgtable]->columns[$field->orgname]->foreignKeys;
foreach ($fk as $fkey)
{
  $success = true;
foreach ($fkey->columns as $column)
{
if (!isset($fields[$field->orgtable][$column]))
{
$success = false;
break;
}
}
if ($success)
{
$foreignKeys[$fkey->name] = $fkey;
}
}
}
}
return $foreignKeys;
}

 public function count()
{
return $this->resultObject->num_rows;
}

 public function offsetExists($offset)
{
return is_numeric($offset) && $offset < $this->resultObject->num_rows;
}
/**
     *
     * @param int offset
     * @return WDB\Query\SelectedRow 
     * @throws Exception\OutOfBounds on non-existent offset
     */
public function offsetGet($offset)
{
if (!$this->offsetExists($offset)) throw new Exception\OutOfBounds("offset $offset does not exist");
$this->resultObject->data_seek($offset);
$return = $this->fetchResult();
$this->resultObject->data_seek($this->offset);
return new WDB\Query\SelectedRow($this, $return);
}
/**
     * Invalid operation on database result.
     *
     * @param int offset
     * @parma mixed value
     * @throws Exception\InvalidOperation
     */
public function offsetSet($offset, $value)
{
throw new Exception\InvalidOperation("database result is read only");
}
/**
     * Invalid operation on database result.
     *
     * @param int offset
     * @throws Exception\InvalidOperation
     */
public function offsetUnset($offset)
{
throw new Exception\InvalidOperation("database result is read only");
}

 public function current()
{
$data = $this->fetchResult();
$result = new WDB\Query\SelectedRow($this, $data);
$this->resultObject->data_seek($this->offset);
return $result;
}
public function key()
{
return $this->offset;
}
public function next()
{
++$this->offset;
return new WDB\Query\SelectedRow($this, $this->fetchResult());
}
public function rewind()
{
$this->offset = 0;
$this->resultObject->data_seek(0);
}
public function valid()
{
return ($this->offset >= 0 && $this->offset < $this->count());
}

private function fetchResult()
{
$row = $this->resultObject->fetch_assoc();
foreach ($row as $key=>&$val)
{
if ($val === NULL) continue;
if ($this->fields[$key]->flags & 2048)  {
if ($val === '')
{
$val = array();
}
else
{
$val = explode(',', $val);
}
}
if (in_array($this->fields[$key]->type, array(7,10,12)))  {
$val = new \DateTime($val);
}
if ($this->fields[$key]->type == 11)  {
$val = new \DateInterval("P0000-00-00T".$val);
}
}
unset($val);  return $row;
}
}}namespace WDB\SQLDriver\Result{
use WDB,
WDB\Exception;

/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Oracle_SelectResult extends WDB\BaseObject implements WDB\Query\iSelectResult
{

private $resultObject;

private $query;

private $database = NULL;

public function getDatabase()
{
return $this->database;
}

public function getQuery()
{
return $this->query;
}

private $offset;

private $fields;
/**
     * @param WDB\Query\Select original select query
     * @param \mysqli_result mysqli result object
     */
public function __construct(WDB\Query\Select $query, $resultObject)
{
$this->resultObject = $resultObject;
$this->query = $query;
$this->offset = 0;
$this->fields = array();
for ($i = 0; $i < oci_num_fields($resultObject); ++$i)
{
$this->fields[oci_field_name($resultObject, $i+1)] = array(
'type'=>oci_field_type($resultObject, $i+1),
'typeRaw'=> oci_field_type_raw($resultObject, $i+1),
);
}
}
 public function bindDatabase (WDB\Database $db)
{
$this->database = $db;
}
public function singleValue()
{
$this->rewind();
if (!$this->valid()) return NULL;
$row = $this->current();
return $row[0];
}
public function singleRow()
{
$this->rewind();
if (!$this->valid()) return NULL;
return $this->current();
}
public function allRows()
{
$result = array();
foreach ($this as $row)
{
$result[] = $row;
}
return $result;
}
 public function getForeignKeys()
{

throw new Exception\Unimplemented();
$foreignKeys = array();
if ($this->database === NULL) throw new Exception\InvalidOperation("select result has not bound database connection.");
$schema = $this->database->schema;
$fields = array();
 foreach ($this->fields as $field)
{
if ($field->orgtable && !isset($fields[$field->orgtable]))
{
$fields[$field->orgtable] = array();
}
if ($field->orgname) $fields[$field->orgtable][$field->orgname] = $field->name;
}
 foreach ($this->fields as $field)
{
if (isset($schema->tables[$field->orgtable]) && isset($schema->tables[$field->orgtable]->columns[$field->orgname]))
{
$fk = $schema->tables[$field->orgtable]->columns[$field->orgname]->foreignKeys;
foreach ($fk as $fkey)
{
  $success = true;
foreach ($fkey->columns as $column)
{
if (!isset($fields[$field->orgtable][$column]))
{
$success = false;
break;
}
}
if ($success)
{
$foreignKeys[$fkey->name] = $fkey;
}
}
}
}
return $foreignKeys;
}

 public function count()
{
$qCount = clone $this->query;
$qCount->setFields(WDB\Query\Element\DBFunction::countRows());
return $qCount->run()->singleValue();
}

 public function offsetExists($offset)
{
return is_numeric($offset) && $offset < $this->count();
}
/**
     *
     * @param int offset
     * @return WDB\Query\SelectedRow 
     * @throws Exception\OutOfBounds on non-existent offset
     */
public function offsetGet($offset)
{
throw new Exception\UnsupportedByDriver("Oracle driver does not support direct offset access, use iterator instead");
}
/**
     * Invalid operation on database result.
     *
     * @param int offset
     * @parma mixed value
     * @throws Exception\InvalidOperation
     */
public function offsetSet($offset, $value)
{
throw new Exception\InvalidOperation("database result is read only");
}
/**
     * Invalid operation on database result.
     *
     * @param int offset
     * @throws Exception\InvalidOperation
     */
public function offsetUnset($offset)
{
throw new Exception\InvalidOperation("database result is read only");
}

 public function current()
{
if ($this->lastResult === NULL)
{
return $this->next();
}
if ($this->lastResult === FALSE)
{
return FALSE;
}
$result = new WDB\Query\SelectedRow($this, $this->lastResult);
return $result;
}
public function key()
{
return $this->offset;
}
public function next()
{
++$this->offset;
$result = $this->fetchResult();
if ($result !== FALSE)
{
return new WDB\Query\SelectedRow($this, $result);
}
else
{
return FALSE;
}
}
public function rewind()
{
if ($this->offset != 0)
{
$this->offset = 0;
oci_execute($this->resultObject);
$this->lastResult = NULL;
}
}
public function valid()
{
return ($this->offset >= 0 && $this->current() != FALSE);
}

private $lastResult = NULL;
private function fetchResult()
{
$row = oci_fetch_assoc($this->resultObject);
if ($row === FALSE)
{
$this->lastResult = FALSE;
}
else
{
foreach ($row as $key=>&$val)
{
if ($val === NULL) continue;
if (in_array($this->fields[$key]['typeRaw'], array(12,187)))  {
$val = new \DateTime($val);
}
if (is_object($val) && get_class($val) == 'OCI-Lob')
{
$val = $val->load();
}
}
unset($val);  $this->lastResult = $row;
}
return $this->lastResult;
}
}}
namespace WDB\Structure{
use WDB,
WDB\Exception,
WDB\Exception\MalformedSerializedData;
/**
 * Base class for creating PHP structure. Properties defined by annotations can be accessed
 * as normal PHP properties like $object->propName.
 * 
 * Properties are defined via "property[-read|-write] type $name [description]" annotations.
 * Read-only properties can be set only in constructor and cannot be re-written later.
 * Write-only properties can be only written by external code.
 * Structure descendants can access properties without restriction through $data array.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
abstract class Structure extends \WDB\BaseObject
{
/** @var array structure properties data */
protected $data;
/** @var WDB\Annotation\Property[] structure properties description */
protected $properties;
/** @var bool indicates that object is still being constructed so read only variables are writeable */
private $constructing = TRUE;
/**
     *
     * @return string serialized version of this object
     */
public function serialize()
{
$s = $this->serializeArray($this->data);
return $s;
}
protected function _serialize($indent)
{
$s = $this->serializeArray($this->data, $indent);
return $s;
}
private function serializeArray($arr, $indent = 0)
{
$s = '';
foreach ($arr as $key=>$val)
{
if (is_string($val))
{
$export = $this->serializeString($val);
}
elseif (is_int($val) || is_float($val))
{
$export = $val;
}
elseif (is_bool($val))
{
$export = $val ? 'T' : 'F';
}
elseif (is_null($val))
{
$export = 'N';
}
elseif (is_array($val))
{
$ser = $this->serializeArray($val, $indent+1);
$export = 'a'.strlen($ser).":\n".$ser;
}
elseif (is_object($val))
{
if ($val instanceof Structure)
{
$ser = $val->_serialize($indent+1);
$export = 's'.strlen($ser).':'.$this->serializeString(get_class($val)).":\n".$ser;
}
else
{
$ser = serialize($val);
$export = 'o'.strlen($ser).':'.$ser;
}
}
elseif (is_resource($val))
{
$ser = serialize($val);
$export = 'r'.strlen($ser).':'.$ser;
}
else
{
throw new WDB\Exception\InvalidOperation("Cannot serialize an unknown type: ".gettype($val));
}
$s .= str_repeat(' ', $indent).(is_string($key) ? $this->serializeString($key) : $key).'='.$export."\n";
}
return $s;
}
private function serializeString($str)
{
return "'".str_replace(array('\\', "'"), array('\\\\', "\\'"), $str)."'";
}
protected function _unserialize($content)
{
return $this->unserializeArray($content);
}
private function unserializeArray($content)
{
$offset = 0;
$values = array();
while (true)
{
while ($offset < strlen($content) && ($content{$offset} == ' ' || $content{$offset} == "\n")) ++$offset;  if ($offset >= strlen($content)) break;
$key = $this->unserializeValue($content, $offset);
if ($content{$offset} != '=') throw new MalformedSerializedData ("= expected.");
++$offset;
$values[$key] = $this->unserializeValue($content, $offset);
}
return $values;
}
private function unserializeValue($content, &$offset)
{
switch ($content{$offset})  {
case "'":
if (!preg_match("~\'(?<str>(?:\\\\.|[^'\\\\])*)'~As", $content, $matches, NULL, $offset))
{
throw new MalformedSerializedData("malformed string at offset $offset");
}
$offset += strlen($matches['str'])+2;
return str_replace(array('\\\\', "\\'"), array('\\', "'"), $matches['str']);
case 'T':
++$offset;
return TRUE;
case 'F':
++$offset;
return FALSE;
case 'N':
++$offset;
return NULL;
case 'a':
++$offset;
$len = $this->_unserGetLen($content, $offset);
$this->_unserSemiColon($content, $offset);
$u = $this->unserializeArray(substr($content, $offset, $len));
$offset += $len;
return $u;
case 's':
++$offset;
$len = $this->_unserGetLen($content, $offset);
$this->_unserSemiColon($content, $offset);
$className = $this->unserializeValue($content, $offset);
$this->_unserSemiColon($content, $offset);
$c = new $className(substr($content, $offset, $len));
$offset += $len;
return $c;
case 'o':
case 'r':
++$offset;
$len = $this->_unserGetLen($content, $offset);
$this->_unserSemiColon($content, $offset);
$u = unserialize(substr($content, $offset, $len));
$offset += $len;
return $u;
default:
if (!preg_match("~[+-]?[0-9.,e]+~A", $content, $matches, NULL, $offset))
{
throw new MalformedSerializedData("unknown token at offset $offset: $content");
}
$offset += strlen($matches[0]);
return $matches[0];
}
}
private function _unserSemiColon($content, &$offset)
{
if ($content{$offset} != ':') throw new MalformedSerializedData ("Expected colon at offset $offset");
++$offset;
}
private function _unserGetLen($content, &$offset)
{
if (!preg_match('~[0-9]+~A', $content, $m, NULL, $offset)) throw new MalformedSerializedData("unknown");
$offset += strlen($m[0]);
return $m[0];
}
public function copy($values = array())
{
$copy = clone $this;
$copy->fetchArrayValues($values);
return $copy;
}
/**
     *
     * @param array|string array of key=>value pairs to initialize structure properties
     * or serialized version of property object
     */
public function __construct($values = array())
{
$this->data = array();
 $this->properties = array();
$queue = array();
$refl = new \ReflectionClass($this);
array_push($queue, $refl);
foreach ($refl->getInterfaces() as $interface)
{
array_push($queue, $interface);
}
do
{
$reflClass = array_pop($queue);
$refl = WDB\Annotation\PhpDocReflection::fromReflObject($reflClass);
if (!$reflClass->isInterface() || isset($refl->wdbStructureAnnotatiton) || isset($refl->wsa))
{
foreach ($refl->property as $prop)
{
if (!method_exists($this, 'get' . ucfirst($prop->propName))  && !method_exists($this, 'set' . ucfirst($prop->propName))
&& !isset($this->properties[$prop->propName]))  {
$this->properties[$prop->propName] = $prop;
}
}
}
if ($reflClass->isInterface())
{
foreach ($reflClass->getInterfaces() as $interface)
{
array_push($queue, $interface);
}
}
if ($reflClass->getParentClass() && $reflClass->getParentClass()->getName() !== __CLASS__)
{
array_push($queue, $reflClass->getParentClass());
}
} while ($queue);
$this->fetchArrayValues($values);
}
private function fetchArrayValues($values = array())
{
$this->constructing = TRUE;
if (!is_array($values) && ($v = $this->_unserialize($values)) !== FALSE)
{
$values = $v;
}
if (is_array($values))
 {
 foreach (array_keys($values) as $key)
{
if (!$this->_propertyExists($key))
unset($values[$key]);
}
 parent::_initPropertiesFromArray($values);
}
else
 {
throw new WDB\Exception\BadArgument("Structure initializer must be an array");
}
$this->constructing = FALSE;
}
/**
     * Get specified property value.
     *
     * @param string
     * @return mixed
     * @throws WDB\Exception\NotExistingProperty
     * @throws WDB\Exception\WriteOnlyProperty
     */
public function __get($name)
{
if (!$this->_propertyExists($name))
throw new WDB\Exception\NotExistingProperty($this, $name);
if (parent::_propertyExists($name))
{
return parent::__get($name);
}
if (!$this->properties[$name]->read)
throw new WDB\Exception\WriteOnlyProperty($this, $name);
if (!isset($this->data[$name]))
return NULL;
return $this->data[$name];
}
/**
     * Set specified property value.
     *
     * @param string
     * @param mixed
     * @throws WDB\Exception\NotExistingProperty
     * @throws WDB\Exception\ReadOnlyProperty
     */
public function __set($name, $value)
{
if (!$this->_propertyExists($name))
throw new WDB\Exception\NotExistingProperty($this, $name);
if (parent::_propertyExists($name))
{
parent::__set($name, $value);
}
if (!$this->constructing && !$this->properties[$name]->write)
throw new WDB\Exception\ReadOnlyProperty($this, $name);
$this->data[$name] = $value;
}
/**
     * Determines if property is set and is not null.
     *
     * @param string
     * @return bool
     */
public function __isset($name)
{
return $this->_propertyExists($name) && $this->__get($name) !== NULL;
}
/**
     * Always throws logic exception - cannot unset structure property.
     *
     * @param string
     * @throws \LogicException
     */
public function __unset($name)
{
throw new \LogicException("Structure property cannot be unset");
}
/**
     * Determines whether property exists (even if it is NULL unlike __isset()) in Structure or BaseObject logic.
     *
     * @param string
     * @return bool
     */
public function _propertyExists($name)
{
return parent::_propertyExists($name) || isset($this->properties[$name]);
}
/**
     *
     * @return string human-readable list of properties and their values
     */
public function dump()
{
$dump = '';
foreach ($this->properties as $key=>$val)
{
$dump .= "$key => ".$this->dumpVar($this->data[$key])."\n";
}
return $dump;
}
/**
     * Sets property in extending class which is read only for public
     *
     * @param string
     * @param mixed
     */
protected function _setReadonly($name, $value)
{
if (!$this->_propertyExists($name))
throw new WDB\Exception\NotExistingProperty($this, $name);
$this->data[$name] = $value;
}
/**
     * Prints debug information about one property.
     *
     * @param Structure $var
     * @return Structure 
     */
private function dumpVar($var)
{
if ($var instanceof Structure)
{
return 'structure:'.$var->dump();
}
if (is_bool($var)) return $var ? 'bool:TRUE' : 'bool:FALSE';
if (is_null($var)) return 'NULL';
if (is_string($var)) return 'string:'.$var;
if (is_numeric($var)) return 'number:'.$var;
return WDB\Utils\System::debug($var, TRUE, FALSE);
}
}
}namespace WDB\Structure{
/**
 * table columns information
 * 
 * structure for interchange between database driver and analyzer
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read \WDB\Analyzer\Column[] $columns list of column data
 * @property-read string[][] $unique list of unique keys (elements are also arrays because
 * unique key can contain multiple columns)
 * @property-read string[] $primary list of columns of primary key
 * @property-read ForeignKeyData[] $foreign list of foreign key definitions
 */
class TableColumns extends Structure{}
}namespace WDB\Structure{
/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $table table name
 * @property-read string $schema schema name
 * @property-read string $connection connection unique WDB name
 */
class TableLocator extends Structure{}}namespace WDB\Structure{
/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property \WDB\Query\iSelectResult $data
 * @property int $count
 */
class TableWrapperCache extends Structure
{
public function __construct()
{
parent::__construct();
$this->purge();
}
public function purge()
{
$this->data = NULL;
$this->count = NULL;
}
}}namespace WDB\Utils{
use WDB,
ArrayAccess;
class Arrays
{
/**
     * Converts multi-dimensional array to one-dimensional.
     *
     * @param array
     * @return array
     */
public static function linearize($arr)
{
$result = array();
foreach ($arr as $elm)
{
if (is_array($elm))
{
$result = array_merge($result, self::linearize($elm));
}
else
{
$result[] = $elm;
}
}
return $result;
}
/**
     * Returns last element of an array (or false on empty array). Similar to PHP end() function,
     * which raises "Indirect modification of overloaded property" notice for retrieving
     * element of an overloaded property of array type.
     *
     * @param array $arr
     * @return mixed
     */
public static function last(array $arr)
{
if (count($arr) === 0) return FALSE;
$keys = array_keys($arr);
return $arr[$keys[count($keys)-1]];
}
/**
     * Returns first element of an array (or false on empty array). Similar to PHP reset(),
     * then current() function, which raises "Indirect modification of overloaded property"
     * notice for retrieving element of an overloaded property of array type.
     *
     * @param array $arr
     * @return mixed
     */
public static function first(array $arr)
{
if (count($arr) === 0) return FALSE;
$keys = array_keys($arr);
return $arr[$keys[0]];
}
/**
     * Returns true when $arr is an array or instance of class implementing \ArrayAccess
     *
     * @param mixed $arr
     * @return bool
     */
public static function isArrayAccessible($arr)
{
return is_array($arr) || $arr instanceof ArrayAccess;
}
/**
     * Returns array value from a multidimensional array with keys in $key separated by dot.
     * i.e. $array, 'config.foo.bar' returns $array['config']['foo']['bar']
     *
     * @param array input array
     * @param string dot separated key
     * @return mixed array value in the position defined by key
     *
     * @throws WDB\Exception\BadArgument
     */
public static function dotPath(array $array, $key)
{
$path = explode('.', $key);
$x = $array;
foreach ($path as $p)
{
if (!isset($x[$p])) throw new WDB\Exception\BadArgument("Key $key not found");
$x = $x[$p];
}
return $x;
}
/**
     * Remove an element from an array.
     *
     * @param array source array
     * @param mixed value to remove
     * @param bool remove all occurences (otherwise only first occurence is removed)
     */
public static function remove(array &$array, $value, $all = TRUE)
{
foreach ($array as $key=>$val)
{
if ($val === $value)
{
unset($array[$key]);
if (!$all) return;
}
}
}
/**
     * merges arrays with maintaining keys
     *
     * @param array $array1,...
     * @return array
     */
public static function assocMerge()
{
$result = array();
foreach (func_get_args() as $arr)
{
if ($arr)
{
foreach ($arr as $key=>$val)
{
$result[$key] = $val;
}
}
}
return $result;
}
/**
     * Returns elements from first array that are not present in any other array.
     * In comparison with php's array_diff, this method compares values with typed comparison,
     * not casting them to strings, so it is better useable for objects.
     *
     * @param array $array1
     * @param array $array2,...
     * @return array
     */
public static function diff(array $array1, array $array2)
{
$arrays = func_get_args();
array_shift($arrays);
foreach ($array1 as $key=>$val)
{
foreach ($arrays as $arrayn)
{
if (in_array($val, $arrayn, TRUE))
{
unset($array1[$key]);
break;
}
}
}
return $array1;
}
}
}namespace WDB\Utils{
/**
 * Can call method or callable object on demand. Arguments can be prepared earlier before call.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Caller
{

private $receiver;

public $arguments;
/**
     * Prepares the callback
     *
     * @param callback
     * @param mixed $arg,... arguments to callback
     */
public function __construct($receiver)
{
$x = func_get_args();
array_shift($x);
$this->receiver = $receiver;
$this->arguments = $x;
}
/**
     * PHP magic method to directly call object as a function.
     *
     * @return mixed
     */
public function __invoke()
{
return $this->call();
}
/**
     * Calls the callback.
     * 
     * @return mixed
     */
public function call()
{
return call_user_func_array($this->receiver, $this->arguments);
}
}
}namespace WDB\Utils{
use WDB;
class Strings
{
/**
     * Serializes array of strings as a comma separated list (escaping only commas). Produces more human-readable output
     * than generic PHP serialize() method. Does not preserve array keys (they need to be supplied to unserializeCommaSeparatedList)
     *
     * @param string[] array to serialize
     * @return string
     */
public static function serializeCommaSeparatedList(array $data)
{
$result = ''; $first = true;
foreach ($data as $val)
{
if ($first) $first = false;
else $result .= ',';
$result .= str_replace(array(',', '!'), array('!,', '!!'), $val);
}
return $result;
}
/**
     * Unserializes comma separated list produced by serializeCommaSeparatedList()
     *
     * @param type $data
     * @param array $keys
     * @return type 
     * @see serializeCommaSeparatedList
     * 
     * @throws Exception\WDBException
     */
public static function unserializeCommaSeparatedList($data, array $keys)
{
$result = array();
reset($keys);
do
{
if (preg_match('~^(?P<head>.*?(?<!\\!)(!!)*),(?P<tail>.*)$~', $data, $matches))
{
$data = $matches['tail'];
$val = $matches['head'];
}
else
{
$val = $data;
$data = false;
}
$key = each($keys);
if ($key === false) throw new WDB\Exception\WDBException("number of keys and unserialized values mismatch");
$result[$key[1]] = str_replace(array('!,', '!!'), array(',','!'), $val);
}
while ($data !== false);
if (each($keys)) throw new WDB\Exception\WDBException("number of keys and unserialized values mismatch");
return $result;
}
/**
     * Executes preg_match and returns a matched subpattern chosen by these rules:
     * - a named subpattern "read" if present, or
     * - first subpattern if present, or
     * - whole matched string if no subpattern is present
     * 
     * @param string PCRE compatible regular expression
     * @param string string to be matched
     * @return string matched content
     */
public static function pregRead($regex, $string)
{
if (!preg_match($regex, $string, $m)) return FALSE;
if (isset($m['read'])) return $regex['read'];
if (isset($m[1])) return $m[1];
return $m[0];
}
/**
     * Escapes a string to be a valid PHP variable name. Useful for PHP GET/POST
     * requests where the key must be a valid PHP variable name.
     * 
     * @param string
     * @return string
     */
public static function escapeVarName($str)
{
if ($str == 'this')  {
return '_74his';
}
if ($str == '')
{
return '_';
}
return preg_replace_callback("~[^a-z\x7f-\xff]~i", function($match) { return '_'.str_pad(dechex(ord($match[0])), 2, '0', STR_PAD_LEFT); }, $str{0})
.preg_replace_callback("~[^a-z0-9\x7f-\xff]~i", function($match) { return '_'.str_pad(dechex(ord($match[0])), 2, '0', STR_PAD_LEFT); }, substr($str, 1));
}
/**
     * Unescapes a string encoded by escapeVarName method.
     * 
     * @param string
     * @return string
     */
public static function unescapeVarName($str)
{
return self::unescapeWES($str);
}
/**
     * Escapes all special characters 
     * 
     * @param string
     * @return string
     */
public static function escapeWes($str, $regexChars = '~[^a-z0-9]~i')
{
return preg_replace_callback($regexChars, function($match) { return '_'.str_pad(dechex(ord($match[0])), 2, '0', STR_PAD_LEFT); }, $str);
}
/**
     * Unescapes a string escaped by any WDB escaping system method.
     * WDB escaping system replaces unwanted special characters(bytes) with '_xx' sequence,
     * where xx is a hexadecimal number from the original byte.
     *
     * @param string
     * @return string
     */
public static function unescapeWes($str)
{
if ($str == '_')
{
return '';
}
return preg_replace_callback("~_([a-f0-9]{2})~i", function($match) { return chr(hexdec($match[1])); }, $str);
}
}
}namespace WDB\Utils{
use WDB;
class System
{
/**
     * Redirects to a specified URL and terminates script.
     *
     * @param string|WebUI\Uri target url (should be fully qualified URL as HTTP
     *  header location requests
     * @param int HTTP code (301, 302...)
     */
public static function redirect($url, $code = NULL)
{
if ($code != NULL)
{
header('HTTP/1.1 '.intval($code).' Redirect');
}
header('location: '.$url);
die();
}
/**
     * Tries to find a class with multiple possible prefixes. Internal WDB utility,
     * used i.e. when user supplies own class for WDB interface (i.e. wrapper).
     * The class can be located in a particular namespace (WDB\Wrapper\User)
     * or just without it, having the exact passed name.
     *
     * @param string sought class name
     * @param string|array possible prefixes
     * @param bool allowed plain name with no prefix
     * @return string found class full name
     */
public static function findClass($className, $prefixes, $noPrefix = TRUE)
{
if (!is_array($prefixes))
{
$prefixes = array($prefixes);
}
foreach ($prefixes as $prefix)
{
if (!is_string($prefix)) continue;
$tClass = $prefix.$className;
if (class_exists($tClass))
{
return $tClass;
}
}
if ($noPrefix && class_exists($className))
{
return $className;
}
throw new WDB\Exception\ClassNotFound("class $className not found even with possible prefixes");
}
/**
     * Return is false:
     * -    Prints debug information about a variable and terminates script.
     * -    Does not do anything when config key 'debug' is not set to true.
     * Return is true:
     *      Returns the debug information as a string.
     * Supply function _wdb_external_debugger if you want to use an extra debugger
     *
     * @param mixed
     * @param bool
     * @param bool
     * @return string
     */
public static function debug($val, $return = FALSE, $externalDebugger = TRUE)
{
if ($return || WDB\Config::read('debug'))
{
if ($return) ob_start();
if ($externalDebugger && function_exists('_wdb_external_debugger'))
{
_wdb_external_debugger($val);
}
else
{
var_dump($val);
}
if ($return)
{
return ob_get_clean();
}
else
{
die();
}
}
}
/**
     * Returns first non-null argument (like MySQL COALESCE() function) or NULL if all arguments are NULL
     *
     * @param $value1,...
     * @return mixed
     */
public static function coalesce()
{
foreach (func_get_args() as $arg)
{
if ($arg !== NULL) return $arg;
}
return NULL;
}
/**
     * Creates DateTime object from either timestamp or string representation or just passes-through DateTime object.
     *
     * @param string|int|\DateTime $value
     * @return \DateTime
     */
public static function createDateTime($value)
{
if ($value instanceof \DateTime) return $value;
if (is_null($value)) return NULL;
if (is_numeric($value)) return \DateTime::createFromFormat ('U', $value);
return new \DateTime($value);
}
}
}namespace WDB\Validation{
use WDB;
/**
 * 
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iValidator extends WDB\Event\iEventListener
{
/**
     * Gets validation rules data, i.e. for javascript clone
     * 
     * @param \WDB\Wrapper\iRecord $record
     * @param mixed $data structural data to add validation rules etc.
     */
public function fetchValidatorData(WDB\Wrapper\iRecord $record, &$data);

/**
     * PHP5.3 doesn't allow re-definition of interface method
     * arguments, but you should always expect an iRecord argument
     * with validator event.
     */
}}namespace WDB\Validation{
use WDB,
WDB\Exception;
/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class UniqueConstraint implements iValidator
{

protected $uniqueKey;

protected $name;
 public function raise(WDB\Wrapper\iRecord $record = NULL)  {
if ($record === NULL) throw new Exception\BadArgument("Validator event raise() method needs"
. " WDB\Wrapper\iRecord as a raise argument");
if (!method_exists($record->getTable(), 'getDatasource'))
{
throw new Exception\WDBException('record`s table does not have a datasource');
}
$key = array();
foreach ($this->uniqueKey as $col)
{
if (is_array($col))
{
$recName = $col['recName'];
$ci = WDB\GTO\WsqlLanguage::quoteIdentifier($col['ColumnIdentifier']);
}
else
{
$recName = $ci = $col;
}
$key[$ci] = $record[$recName];
if ($key[$ci] === NULL) return true;  }
$utest = $record->getTable()->getDatasource()->filter($key);
if (!$record->isNew())
{
$utest = $utest->not(method_exists($record, 'getUniqueIdentificationKey') ? $record->getUniqueIdentificationKey() : $record->getIdentificationKey());
}
if ($utest->count())
{
$columns = $record->getTable()->getColumns();
if (count($this->uniqueKey) == 1)
{
$record->getField($recName)->addValidationError($message=str_replace('%name', $columns[$recName]->getTitle(), WDB\Lang::s('validator.messages.uniqueViolated')));
$record->addValidationErrors(array(array('message'=>$message, 'column'=>$columns[$recName])));
}
else
{
$titles = array();
foreach ($this->uniqueKey as $col)
{
if (is_array($col)) $col = $col['recName'];
$titles[] = $columns[$col]->getTitle();
}
$record->addValidationErrors(array(str_replace('%fieldlist', implode(', ', $titles), WDB\Lang::s('validator.messages.uniqueGroupViolated'))));
}
return false;
}
else
{
return true;
}
}

 /**
     * @throws WDB\Exception\WDBException
     */
public function fetchValidatorData(WDB\Wrapper\iRecord $record, &$data)
{
if (!isset($data['UniqueConstraints']))
{
$data['UniqueConstraints'] = array();
}
if (isset($data['UniqueConstraints'][$this->name])) throw new Exception\WDBException("Duplicate constraint name: {$this->name}");
$data['UniqueConstraints'][$this->name] = $this->uniqueKey;
}

/**
     * @param string constraint name
     * @param string|array column or group of columns to be unique
     */
public function __construct($name, $key)
{
if (is_string($key)) $key = array($key);
if (!is_array($key)) throw new Exception\BadArgument('$key must be sting or array');
$this->uniqueKey = $key;
$this->name = $name;
}
}
}namespace WDB\WebUI{
use WDB;
/**
 * Factory for creating standard WebUI Column objects for column wrappers.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnFactory
{
/**
     *
     * @param WDB\Wrapper\iColumn
     * @param iTable
     * @return iColumn
     */
public static function create(WDB\Wrapper\iColumn $columnWrapper, iTable $tableWebUI)
{
$tClass = $tableWebUI->getColumnWebUIClass($columnWrapper->getName());
if ($tClass === NULL)
{
$class = '\\WDB\\WebUI\\ColumnText';
}
else
{
$class = WDB\Utils\System::findClass($tClass, WDB\Config::read('userWebUIClassPrefix'));
}
return new $class($columnWrapper, $tableWebUI);
}
}
}namespace WDB\WebUI{
use WDB;
/**
 * HTML node builder.
 * 
 * Provides easy, object-oriented building of HTML code.
 * HTML attributes are retrieved via ArrayAccess,
 * inner content is accessed through ->content property.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @property array $content list of inner nodes(text nodes or elements)
 */
class HTMLNode extends WDB\BaseObject implements \ArrayAccess
{

protected $name;

protected $attributes;

protected $content;

private $contentArrayDelegate;

protected $xhtml;
/**
     *
     * @param string node name
     * @param array node attribudes
     * @param string|array inner content
     */
public function __construct($name, $attributes=array(), $content = NULL, $xhtml = true)
{
$this->name = $name;
$this->attributes = $attributes;
$this->setContent($content);
$this->xhtml = (bool)$xhtml;
$this->contentArrayDelegate = new WDB\Utils\ArrayDelegate($this->content);
}
/**
     * Set __toString output to HTML.
     *
     * @return string
     */
public function setHtml()
{
$this->xhtml = false;
}
/**
     * Set __toString output to XHTML.
     *
     * @return string
     */
public function setXhtml()
{
$this->xhtml = true;
}
/**
     * Return HTML representation of this node
     *
     * @return string
     */
public function html()
{
$node = clone $this;
$node->setHtml();
return $node->__toString();
}
/**
     * Return XHTML representation of this node
     *
     * @return string
     */
public function xhtml()
{
$node = clone $this;
$node->setXhtml();
return $node->__toString();
}
/**
     * Get node content. Returned as ArrayDelegate object which represents a reference to an array,
     * so changes to the returned array will be projected to the node.
     *
     * @return WDB\Utils\ArrayDelegate
     */
public function &getContent()
{
return $this->contentArrayDelegate;
}
/**
     * Set node inner content.
     * 
     * @param string|array Pass list of strings/elements or a string. String represents text node.
     */
public function setContent($content)
{
if ($content === NULL) $content = array();
elseif (!is_array($content))
{
if ($content instanceof WDB\BaseCollection)
{
$content = $content->toArray();
}
else
{
$content = array($content);
}
}
$this->content = $content;
}
/**
     * Build a HTML string representing this node.
     *
     * @return string
     */
public function __toString()
{
$tag = '<'.htmlspecialchars($this->name);
foreach ($this->attributes as $key=>$val)
{
$tag .= ' '.htmlspecialchars($key).'="'.htmlspecialchars($val).'"';
}
if (count($this->content) == 0)
{
$tag .= ' />';
return $tag;
}
$tag .= '>';
foreach ($this->content as $val)
{
if ($val instanceof HTMLNode)
{
$tag .= $val->__toString();
}
else
{
$tag .= htmlspecialchars($val);
}
}
$tag .= '</'.htmlspecialchars($this->name).'>';
return $tag;
}
/**
     * Removes css class.
     * 
     * @param string CSS class
     * @return HTMLNode fluent interface
     */
public function removeClass($class)
{
if (!empty($this->attributes['class']))
{
$classes = explode(' ', $this->attributes['class']);
WDB\Utils\Arrays::remove($classes, $class);
$this->attributes['class'] = implode(' ', $classes);
}
return $this;
}
/**
     * Add css class.
     * 
     * @param string CSS class
     * @return HTMLNode fluent interface
     */
public function addClass($class)
{
if (empty($this->attributes['class']))
{
$this->attributes['class'] = $class;
}
else
{
$this->attributes['class'] .= ' '.$class;
}
return $this;
}
 public function offsetExists($offset)
{
return isset($this->attributes[$offset]);
}
public function offsetGet($offset)
{
if (!$this->offsetExists($offset)) return NULL;
return $this->attributes[$offset];
}
public function offsetSet($offset, $value)
{
$this->attributes[$offset] = $value;
}
public function offsetUnset($offset)
{
unset($this->attributes[$offset]);
}
}
}namespace WDB\WebUI{
use WDB;
/**
  * column WebUI.
  *
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iColumn
{

const DISPLAY_LIST = 1;

const DISPLAY_DETAIL = 2;

const DISPLAY_EDIT = 12;

const DISPLAY_EDIT_READONLY = 4;

const DISPLAY_AUTOCOLUMN_DEFAULT = 0;

const DISPLAY_ALL = 15;
 /**
     * Returns name identifier of this column
     *
     * @return string
     */
public function getName();
/**
     * Returns human-readable title of this column
     *
     * @return string
     */
public function getTitle();
/**
     * Returns html code representing human-readable title of this column
     * (for table headings etc)
     *
     * @return string
     */
public function getTitleHTML();
/**
     * returns table this column is bound to
     *
     * @return iTable
     */
public function getTable();

 /**
     * add display mode(s) to this column
     *
     * @param int $mode mode bitmask
     */
public function addDisplayMode($mode);
/**
     * remove display mode(s) from this column
     *
     * @param int $mode mode bitmask
     */
public function removeDisplayMode($mode);
/**
     * sets display modes mask for this column
     *
     * @param int $mode mode bitmask
     */
public function setDisplayModes($mode);
/**
     * returns display modes mask for this column
     *
     * @return int mode bitmask
     */
public function getDisplayModes();
/**
     * Returns whether this column has to be displayed in record list
     *
     * @return bool
     */
public function isDisplayedList();
/**
     * Returns whether this column has to be displayed in record editor
     * as an editable item
     *
     * @return bool
     */
public function isDisplayedEdit();
/**
     * Returns whether this column has to be displayed in record editor
     * (check isDisplayedEdit() for whether it also has to be editable)
     *
     * @return bool
     */
public function isDisplayedEditRead();
/**
     * Returns whether this column has to be displayed in record detail
     *
     * @return bool
     */
public function isDisplayedDetail();
/**
     * Returns whether this column has to be displayed in specified mode
     * (or one of modes if more modes are set)
     *
     * @return bool
     */
public function isDisplayed($mode);

 /**
     * Returns whether this column can have a null value or not
     */
public function isNullable();
/**
     * Returns validation rules for this column
     *
     * @return \WDB\Validation\Rules
     */
public function getRules();

 /**
     * Returns html code representing this field's simplified value to display i.e. in record listing
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
public function getSimpleHTML(WDB\Wrapper\iRecord $record);
/**
     * Returns html code representing this field's value to display in record detail
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
public function getDetailedHTML(WDB\Wrapper\iRecord $record);
/**
     * Returns html code representing this field's datatype and value to display in record editor
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
public function getEditingHTML(WDB\Wrapper\iRecord $record);
/**
     * Returns html code displayed in separate column for setting NULL value.
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
public function getEditingHTML_nullator(WDB\Wrapper\iRecord $record);
/**
     * Returns html code representing this column's status (i.e. validation error)
     *
     * @param WDB\Wrapper\iRecord
     * @return string
     */
public function getHTMLEditStatus(WDB\Wrapper\iRecord $record);
/**
     * Gets CSS classes of the surrounding HTML element.
     *
     * @param WDB\Wrapper\iRecord $record
     * @return array
     */
public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record);

/**
     * Reads and parses value of this column from request
     *
     * @param WDB\Wrapper\iRecord
     * @param string|array input string or array value
     * @return mixed parsed value
     */
public function parseInputValue(WDB\Wrapper\iRecord $record, $value);
/**
     * Returns true if this column can be initialized with a default value,
     * i.e. a database column which can be null or has a default non-null vaue.
     *
     * @return bool
     */
public function isDefaultInitializable();
}}namespace WDB\WebUI{
use WDB;
/**
  * Orderable column WebUI.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iFilterable
{
/**
     * Determines whether records can be filtered by this column
     * 
     * @return bool
     */
public function isFilterable();
/**
     * @return bool
     */
public function isToggledFilter();
/**
     * @return array associative key=>title array of options
     */
public function getFilterOptions();
/**
     * @return string representation of wrapper layer value
     */
public function getFilterStringValue($val);
/**
     * @return string
     */
public function getFilteredValue();
}}namespace WDB\WebUI{
use WDB;
/**
  * Orderable column WebUI.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iOrderable
{
/**
     * Determines whether records can be ordered by this column
     * 
     * @return bool
     */
public function isOrderable();
/**
     * @param bool $asc
     * @return bool
     */
public function isToggledOrder($asc = TRUE);
}}namespace WDB\WebUI{
use WDB;
/**
 * Input request to webui. Specifies view to display and modifications to data model.
 * Usually and by default, it is implemented by HttpRequest class for requests coming from HTTP,
 * but there may be implemented other request suppliers and bound to WebUI schema/tables.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iRequest
{
/**
     * reads current state identifier (list, edit etc...)
     * 
     * @return string
     */
public function getRequestedState();
/**
     * get http/html identifier for an html field
     * 
     * @param string $name
     * @return string
     */
public function getSaveFieldName($name);
/**
     * reads particular value from input
     * 
     * @return string
     */
public function getEdited($key);
/**
     * reads row data from input
     * 
     * @return array associative array colname=>value
     */
public function getAllEdited();
/**
     * get http link to a different state of the component
     * 
     * @param int $state 
     * @param string|bool|null $selectedRecordKey
     *  string for existing record
     *  bool(TRUE) for new record
     *  null for no record pointout
     * @return Uri
     */
public function getStateLink($state, $selectedRecordKey = NULL);
/**
     * get key for selected record to display/edit
     * @return string
     */
public function getSelectedRecordKey();
/**
     * get name of html hidden input indicating that an edit form was sent
     * @return string
     */
public function getEditControlName();
/**
     * determines whether an edit form was submitted
     * @return bool
     */
public function isEditSubmitted();
/**
     * determines whether a new record is selected (instead of existing record selected by getSelectedRecordKey)
     * @return bool
     */
public function isNewRecord();
/**
     * get http link to a script deleting specified record
     * 
     * @return Uri
     */
public function getDeleteLink($recordKey);
/**
     * redirects after succesfully saving a record
     */
public function redirectSuccessfulSave($recordKey = NULL);
/**
     * determines whether there is request to delete a record
     * 
     * @return bool
     */
public function isDeleteRequest();
/**
     * gets string key of a record requested to be deleted
     * 
     * @return string
     */
public function getDeleteKey();
/**
     * get uri parameter name for record listing page number
     * 
     * @return string
     */
public function getPageParamName();
/**
     * Get current page number (zero-based, -1 difference from web information)
     * 
     * @return int
     */
public function getCurrentPage();
/**
     * Configure datasource object for a table with requested page, ordering, filtering etc.
     * 
     * @param WDB\Wrapper\iTable
     * @return WDB\iDatasource configured instance
     */
public function configureDatasource(WDB\Wrapper\iTable $table);
/**
     * link to get sorted table ordered by passed column identifier
     * 
     * @param string column identifier
     * @param bool ascending
     * @return Uri
     */
public function orderLink($column, $asc = TRUE);
/**
     * link to filter table by a column
     * 
     * @param string column identifier
     * @param mixed value
     * @return Uri
     */
public function filterLink($column, $value);
/**
     *
     * @param string $table
     * @return Uri 
     */
public function getSelectTableLink($table);
/**
     * @return string
     */
public function getSelectedTable();
/**
     * @return string
     */
public function getOrderColumn();
/**
     * @return bool
     */
public function isOrderAsc();
/**
     * @return array
     */
public function getFilters();
}}namespace WDB\WebUI{
use WDB;
/**
 * Interface for webui tables.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iTable extends \Iterator, \Countable, \ArrayAccess
{
 /** Constant for addColumn $after argument - add column as first in the table */
const FIRST = TRUE;
/** Constant for addColumn $after argument - add column as last in the table */
const LAST = FALSE;
/** State of UI - brief list of list all records */
const STATE_LIST = 'list';
/** State of UI - detailed output of one record */
const STATE_DETAIL = 'detail';
/** State of UI - edit interface for a record */
const STATE_EDIT = 'edit';
/** State of UI - default one */
const STATE_DEFAULT = self::STATE_LIST;
/** State of UI - auto-detect from context */
const STATE_AUTO = NULL;

 /**
     * Returns name of this table
     * 
     * @return string
     */
public function getName();
/**
     * Returns human-readable title of this table.
     * 
     * @return string
     */
public function getTitle();
/**
     * Returns a table locator if this table has its base in database or NULL otherwise.
     * 
     * @return WDB\Structure\TableLocator
     */
public function getTableLocator();

/**
     * Retrieves record selected by current http request
     *
     * @return iRecord
     */
public function getSelectedRecord();
 /**
     * Returns Request object bound to this table
     *
     * @return iRequest
     */
public function getRequest();
/**
     * Sets Request object for this table
     * 
     * @param iRequest $rqi
     */
public function setRequest(iRequest $rqi);
/**
     * Perform events based on http request, especially those which should be
     * performed before commiting headers
     */
public function handleRequest();

 /**
     * Configures template manager class
     * 
     * @param iTemplateAdapter $manager
     */
public function setTemplateAdapterClass(iTemplateAdapter $manager);
/**
     * Echoes HTML code of selected state on output.
     * 
     * @param int state
     */
public function show($state = self::STATE_AUTO);
/**
     * Prepares HTML code of selected state and returns it as string
     *
     * @param int $state
     * @return string
     */
public function generateHTML($state = self::STATE_AUTO);

 /**
     * Returns all columns in this table.
     * 
     * @return \WDB\WebUI\iColumn[]
     */
public function getColumns();
/**
     * browses all columns in this table and creates a unique name identifier (for a new one)
     */
public function createUniqueColumnName();
/**
     * Adds a column to a specified position. 
     * 
     * @param iColumn
     * @param string|bool after which column it should be added; TRUE = first, FALSE = last
     * @return iTable Fluent interface - returns itself
     */
public function addColumn(iColumn $column, $after = self::LAST);
/**
     * sets the order of displayed columns. i.e. $table->order('name','address','phone');
     * 
     * @param string $col1,...
     * @return iTable
     */
public function order();

 /**
     * Event fired after all fields of input row are parsed and loaded into selected record
     * 
     * @return WDB\Event\Event
     */
public function getOnValidateInput();
/**
     * Fetches input from iRequest to the record if requested record editation
     * 
     * @param WDB\Wrapper\iRecord
     */
public function parseInput(WDB\Wrapper\iRecord $record);

/**
     * @return \WDB\iDatasource
     */
public function getDatasource();
/**
     * Returns true if this table shoud be listed in schema webui table list.
     * 
     * @return bool
     */
public function isListedInSchemaAdmin();
/**
     * Returns true if this table is accessible by webui
     * 
     * @return bool
     */
public function isWebUIAccessible();
/**
     * Checks a table for non-null non-writable column paradox.
     * The issue is that if a column has a NOT NULL constraint, no default value
     * and is not writable by WDB WebUI, new records cannot be created with such table
     * (because there is neither valid default value for the column nor user
     * can specify it).
     * 
     * @return bool
     */
public function isNewRecordCapable();
/**
     * returns data of all bound validators to this table's record to pass to javascript validator clone
     *
     * @param WDB\Wrapper\iRecord
     * @return array
     */
public function getValidatorData(WDB\Wrapper\iRecord $record);
/**
     * Get class for column webui for a column identified by name
     * 
     * @param string column name
     * @return string|NULL
     */
public function getColumnWebUIClass($columnName);
/**
     * Get HTML code representing table edit status (i.e. validation errors)
     * 
     * @return string
     */
public function getHTMLEditStatus();
}}namespace WDB\WebUI{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iTemplateAdapter
{
/**
     * @param string template identifier
     */
public function __construct($templateId);
/**
     * @param string template identifier
     * @return self
     */
public function setTemplate($templateId);
/**
     * @return string
     */
public function getTemplate();
/**
     * Gets template variable with magic getter
     * 
     * @param string
     */
public function __get($key);
/**
     * Sets template variable with magic setter
     * 
     * @param string
     * @param mixed
     */
public function __set($key, $value);
/**
     * returns parsed template as string
     * 
     * @param string override configured template identifier
     * @return string
     */
public function parse($templateId = NULL);
/**
     * prints parsed template to output
     * 
     * @param string override configured template identifier
     */
public function show($templateId = NULL);
}}namespace WDB\WebUI{
use WDB;
/**
 * Schema browser WebUIds
 * 
 * This WebUI class provides an interface for browsing tables, selecting one and then forwards requests
 * to the selected table. This basic implementation is connected directly to Analyzer\iSchema
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Schema extends WDB\BaseObject
{

protected $schemaAnalyzer;

protected $template;

protected $templateAdapterClass = '\\WDB\\WebUI\\SimpleTemplateAdapter';

protected $templateSet = 'default';

protected $selectedTable = NULL;

protected $request;

protected $registeredTables = array();
 /**
     * Creates WebUI for schema analyzer.
     *
     * @param WDB\Analyzer\Schema schema analyzer
     * @return self 
     */
public function __construct(WDB\Analyzer\Schema $schema)
{
$this->schemaAnalyzer = $schema;
$this->template = new $this->templateAdapterClass($this->templateSet.'.schema');
$this->template->schema = $this;
$this->request = new HttpRequest($this->name);
if (($selTable = $this->request->getSelectedTable()) !== NULL)
{
$this->selectTable($selTable);
}
}
/**
     * Creates Schema webUI for specified schema in database
     *
     * @param string schema name
     * @param WDB\Database database connection where the schema is located
     * @return Schema
     */
public static function forSchemaName($name, WDB\Database $database = NULL)
{
if ($database == NULL) $database = WDB\Database::getDefault();
return new self($database->getSchema($name));
}
/**
     * Creates Schema webUI for currently selected schema in database
     *
     * @param WDB\Database database
     * @return Schema
     */
public static function forCurrentSchema(WDB\Database $database = NULL)
{
return self::forSchemaName(NULL, $database);
}

 /**
     * Registers WebUI iTable object which does not represent a table stored in database
     * so it is not accessible by Analyzer layer. If a table with given name is already
     * registered, it will be replaced with the table given to this method.
     *
     * @param iTable the table
     * @param string name identifier
     */
public function registerTable(iTable $table, $name)
{
$this->registeredTables[$name] = $table;
}
/**
     * Returns all tables registered by registerTable method.
     *
     * @return iTable[]
     */
public function getRegisteredTables()
{
return $this->registeredTables();
}
/**
     * Get all tables listed in schema admin
     *
     * @return Structure\TableLink[]
     */
public function getTableSchemaList()
{
$result = array();
foreach (array_merge($this->schemaAnalyzer->getTables(), $this->registeredTables) as $table)
{
if ($table->isListedInSchemaAdmin())
{
$result[] = new Structure\TableLink(array(
'name'=>$table->getName(),
'title'=>$table->getTitle(),
));
}
}
return $result;
}
/**
     * Get currently selected table
     * 
     * @return iTable
     */
public function getSelectedTable()
{
return $this->selectedTable;
}
/**
     * Sets ant initializes currently selected table. It is automatically done in constructor from iRequest.
     * 
     * @param string table name
     */
protected function selectTable($tableName)
{
if ($tableName !== NULL)
{
$this->selectedTable = TableFactory::fromName($tableName, $this->schemaAnalyzer->getName(), $this->schemaAnalyzer->getDatabase());
if (!$this->selectedTable->isWebUIAccessible())
{
$this->selectedTable = NULL;
return null;
}
$this->selectedTable->setRequest($this->request);
$this->selectedTable->handleRequest();
}
else
{
$this->selectedTable = NULL;
}
}

/**
     * Get schema name
     *
     * @return string
     */
public function getName()
{
return $this->schemaAnalyzer->name;
}
/**
     * Returns current request sent to this Schema object.
     *
     * @return iRequest
     */
public function getRequest()
{
return $this->request;
}

/**
     * Generates WebUI HTML and returns it as a string
     *
     * @return string
     */
public function generateHTML()
{
return $this->template->parse();
}
/**
     * Generates WebUI HTML directly to output
     *
     * @return string
     */
public function show()
{
return $this->template->show();
}
}
}namespace WDB\WebUI{
use WDB;

class SimpleTemplateAdapter extends WDB\BaseObject implements iTemplateAdapter
{

protected $data;

protected $template;
public function __construct($templateId)
{
$this->template = $templateId;
$this->data = array();
}
public function setTemplate($templateId)
{
$this->template = $templateId;
}
public function getTemplate()
{
return $this->template;
}
public function __get($key)
{
return isset($this->data[$key]) ? $this->data[$key] : NULL;
}
public function __set($key, $value)
{
$this->data[$key] = $value;
}
public function parse($templateId = NULL)
{
ob_start();
$this->show($templateId);
return ob_get_clean();
}
public function show($templateId = NULL)
{
if ($templateId === NULL) $templateId = $this->template;
require WDB\Config::read('templatesdir').str_replace('.', DIRECTORY_SEPARATOR, $templateId).'.php';
}
}}namespace WDB\WebUI\Structure{
use WDB;
/**
 * table column meta information
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $name table name
 * @property-read string $title human-readable table title
 */
class TableLink extends WDB\Structure\Structure{}
}namespace WDB\WebUI{
use WDB,
WDB\Exception;
/**
  * WebUI for a table wrapper.
  *
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class Table extends WDB\BaseObject implements iTable
{
/** @var iTable[] register all instances here for common calling handleRequestAll */
protected static $instances = array();
/** @var string[] specifies order of columns in output for OrderedIterator*/
protected $columnOrder = array();

protected $templateSet = 'default';
/** @var \WDB\Wrapper\Table*/
protected $tableWrapper;
/** @var iRequest */
protected $request;
/** @var bool this is set to TRUE after calling handleRequest to prevent performing actions twice.*/
protected $doneEvents = FALSE;

protected $templateAdapterClass = '\\WDB\\WebUI\\SimpleTemplateAdapter';

protected $paginatorTemplate = NULL;

protected $selectedRecord = NULL;

protected $columns;
/**
     * Raised with \WDB\Wrapper\iRecord as argument
     *
     * @var \WDB\Event\Event*/
protected $onValidateInput;
/**
     * @var int records shown per page
     * @todo allow customization
     */
protected $recordsPerPage = 10;
/**
     * Names of column WebUI classes for this table in form of {column name}=>{class name}
     *
     * @var string[]
     */
protected $columnWebUIClasses;
/**
     * @var boolean
     */
protected $newRecordCapable;
/**
     * @var array
     */
protected $validationErrors;
 /**
     * Register iTable instance to call handleRequest on all tables with ::handleRequestAll().
     *
     * Table class is automatically registered in constructor, you can also register
     * your own iTable implementations here.
     *
     * @param iTable
     */
public static function registerInstance(iTable $t)
{
self::$instances[] = $t;
}
/**
     * call handleRequest on all registered instances of iTable.
     */
public static function handleRequestAll()
{
foreach (self::$instances as $instance)
{
$instance->handleRequest();
}
}


public function getName()
{
return $this->tableWrapper->getName();
}
public function getTitle()
{
return $this->tableWrapper->getTitle();
}
public function getTableLocator()
{
if (method_exists($this->tableWrapper, 'getTableLocator'))
{
return $this->tableWrapper->getTableLocator();
}
else
{
return NULL;
}
}
public function getSelectedRecord()
{
if (!$this->doneEvents) $this->handleRequest();
return $this->selectedRecord;
}
public function getRequest()
{
if (!$this->request instanceof iRequest)
{
$this->request = new HttpRequest($this->tableWrapper->name);
}
return $this->request;
}
public function setRequest(iRequest $rq)
{
$this->request = $rq;
}
public function handleRequest()
{
if ($this->doneEvents) return;
$this->doneEvents = TRUE;
if ($this->getRequest()->getSelectedRecordKey() !== NULL)
{
$this->selectedRecord = $this->tableWrapper->getRecordByStringPK($this->getRequest()->getSelectedRecordKey());
}
elseif ($this->getRequest()->isNewRecord())
{
$this->selectedRecord = $this->tableWrapper->newRecord();
}
if ($this->selectedRecord && $this->getRequest()->isEditSubmitted())
{
$this->parseInput($this->selectedRecord);
try
{
if ($this->selectedRecord->save())
{
$this->getRequest()->redirectSuccessfulSave();
}
}
catch (Exception\ValidationFailed $ex)
{
}
}
if ($this->getRequest()->isDeleteRequest())
{
try
{
$rec = $this->tableWrapper->getRecordByStringPK($this->getRequest()->getDeleteKey());
$rec->delete();
}
catch (Exception\NotFound $ex)
{}
$this->getRequest()->redirectSuccessfulSave();
}
}
protected function prepareTemplate($state = iTable::STATE_AUTO)
{
if ($state == iTable::STATE_AUTO)
{
$state = $this->getRequest()->getRequestedState();
}
$tgen = new $this->templateAdapterClass($this->templateSet.'.'.$state);
$tgen->table = $this;
if ($state == iTable::STATE_LIST)
{
$tgen->records = $this->tableWrapper->elevateRecords($this->getDatasource()->fetch());
}
return $tgen;
}
public function setTemplateAdapterClass(iTemplateAdapter $manager)
{
$this->templateAdapterClass = $manager;
}
/**
     *
     * @return WDB\WebUI\iTemplateAdapter
     */
public function getPaginatorTemplate()
{
 if ($this->paginatorTemplate === NULL)
{
$this->paginatorTemplate = new $this->templateAdapterClass($this->templateSet.'.paginator');
$this->paginatorTemplate->around = 3;  $this->paginatorTemplate->uri = $this->getRequest()->getStateLink(\WDB\WebUI\iTable::STATE_LIST);
$this->paginatorTemplate->uriPageParam = $this->getRequest()->pageParamName;
}
 if ($this->recordsPerPage > 0)
{
$pages = ceil(max(1, $this->count()) / $this->recordsPerPage);
}
else
{
$pages = 1;
}
$cpage = max(1, min($pages, $this->getRequest()->getCurrentPage()+1));
$this->paginatorTemplate->currentPage = $cpage;
$this->paginatorTemplate->pages = $pages;
return $this->paginatorTemplate;
}
public function show($state = iTable::STATE_AUTO)
{
if (!$this->doneEvents) $this->handleRequest();
$this->prepareTemplate($state)->show();
}
public function generateHTML($state = iTable::STATE_AUTO)
{
if (!$this->doneEvents) $this->handleRequest();
return $this->prepareTemplate($state)->parse();
}
public function getColumns()
{
return new WDB\Utils\OrderedIterator($this->columns, $this->columnOrder);
}
public function createUniqueColumnName()
{
$maxid = 0;
foreach ($this->columns as $column)
{
if (preg_match('~unnamed([0-9]+)~', $column->name, $m))
{
if ($m[1] > $maxid) $maxid = $m[1];
}
}
return 'unnamed'.($maxid+1);
}
public function addColumn(iColumn $column, $after = iTable::LAST)
{
$this->columns[$column->name] = $column;
if ($after === iTable::LAST)
{
$this->columnOrder[] = $column->name;
}
elseif ($after === iTable::FIRST)
{
array_unshift($this->columnOrder, $column->name);
}
else
{
$offset = array_search($after, $this->columnOrder, TRUE);
if ($offset === FALSE)
{
 $this->columnOrder[] = $column->name;
}
else
{
 array_splice($this->columnOrder, $offset+1, 0, $column->name);
}
}
}
public function order()
{
$notOrdered = array_diff($this->columnOrder, func_get_args());
$this->columnOrder = array();
foreach (func_get_args() as $arg)
{
$cname = $this->tableWrapper->getColumnCName($arg);
if (isset($this->columns[$cname]))
{
$this->columnOrder[] = $cname;
}
}
$this->columnOrder = array_merge($this->columnOrder, $notOrdered);
return $this;
}
public function getOnValidateInput()
{
return $this->onValidateInput;
}
public function parseInput(WDB\Wrapper\iRecord $record)
{
foreach ($this->columns as $column)
{
if ($column->isDisplayedEdit())
{
$column->parseInputValue($record, $this->getRequest()->getEdited($column->getName()));
}
}
$this->onValidateInput->raise($record);
}
public function getDatasource()
{
$ds = $this->getRequest()->configureDatasource($this->tableWrapper);
$ds->pageSize(10);  return $ds;
}
public function isListedInSchemaAdmin()
{
return $this->tableWrapper->isListedInSchemaAdmin();
}
public function isWebUIAccessible()
{
return $this->tableWrapper->isWebUIAccessible();
}
public function getValidatorData(WDB\Wrapper\iRecord $record)
{
$data = array(
'columns'=>$this->getValidatorColumns(),
'tableLocator'=>$this->getTableLocator()->serialize(),
'key'=>$record->getStringKey(),
);
foreach ($record->getValidators() as $validator)
{
if ($validator instanceof WDB\Validation\iValidator)
{
$validator->fetchValidatorData($record, $data);
}
}
return $data;
}
public function getHTMLEditStatus()
{
if ($this->selectedRecord !== NULL)
{
return '<div class="wdb_tableValidationErrors">'.implode('<br>', $this->selectedRecord->getGlobalValidationErrors()).'</div>';
}
else
{
return '';
}
}

public function __construct(WDB\Wrapper\Table $table)
{
self::registerInstance($this);
$this->tableWrapper = $table;
$this->newRecordCapable = true;
foreach ($table->columns as $column)
{
$this->columns[$column->name] = ColumnFactory::create($column, $this);
$this->columnOrder[] = $column->name;
}
$this->onValidateInput = new WDB\Event\Event($this);
}
protected function getValidatorColumns()
{
$columns = array();
foreach ($this->columns as $column)
{
if (!$column->isDisplayedEdit()) continue;
$columns[$column->name] = array(
'nullable'=>$column->isNullable(),
'title'=>$column->title
);
$columns[$column->name]['elementName']=$this->getRequest()->getSaveFieldName($column->getName()).'[value]';
if ($column->isNullable())
{
$columns[$column->name]['nullCheckerName']=$this->getRequest()->getSaveFieldName($column->getName()).'[notnull]';
}
}
return $columns;
}
public function getColumnWebUIClass($columnName)
{
if (isset($this->columnWebUIClasses[$columnName])) return $this->columnWebUIClasses[$columnName];
$columns = $this->tableWrapper->getColumns();
if (isset($columns[$columnName]))
{
return $columns[$columnName]->getWebUIClass();
}
return NULL;
}
public function isNewRecordCapable()
{
foreach ($this->columns as $column)
{
if (!$column->isDefaultInitializable() && !$column->isDisplayedEdit())
{
return false;
}
}
return true;
}
 public function count() { return $this->tableWrapper->count(); }
public function offsetExists($offset) { return $this->tableWrapper->offsetExists($offset);}
public function offsetGet($offset) { return $this->tableWrapper->offsetGet($offset);}
public function offsetSet($offset, $value) { return $this->tableWrapper->offsetSet($offset, $value);}
public function offsetUnset($offset) { return $this->tableWrapper->offsetUnset($offset);}
public function current() { return $this->tableWrapper->current();}
public function key() { return $this->tableWrapper->key();}
public function next() { return $this->tableWrapper->next();}
public function rewind() { return $this->tableWrapper->rewind();}
public function valid() { return $this->tableWrapper->valid();}
}}namespace WDB\WebUI{
use WDB;
/**
 * Factory methods for creating WebUI tables
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class TableFactory
{
/**
     *
     * @param \WDB\Wrapper\iTable
     * @return \WDB\WebUI\Table
     */
public static function fromWrapper(WDB\Wrapper\table $table)
{
$tClass = $table->getWebUIClass();
if ($tClass === NULL)
{
$class = '\\WDB\\WebUI\\Table';
}
else
{
$class = WDB\Utils\System::findClass($tClass, WDB\Config::read('userWebUIClassPrefix'));
}
return new $class($table);
}
/**
     *
     * @param \WDB\Analyzer\iTable
     * @return \WDB\WebUI\Table
     */
public static function fromAnalyzer(WDB\Analyzer\iTable $table)
{
$wrapper = WDB\Wrapper\TableFactory::fromAnalyzer($table);
return self::fromWrapper($wrapper);
}
/**
     *
     * @param string
     * @param string schema name
     * @param WDB\Database database connection
     * @return \WDB\WebUI\iTable
     */
public static function fromName($table, $schema = NULL, WDB\Database $database = NULL)
{
$wrapper = WDB\Wrapper\TableFactory::fromName($table, $schema, $database);
return self::fromWrapper($wrapper);
}
}
}namespace WDB\WebUI{
use WDB;
/**
 * class for handling URIs with PHP query strings
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Uri {

public $params = array(); 

public $fragment = ''; 

public $path; 

public $abspath_prefix; 

public $host;

public $protocol;

public $port;

private $urlmode;

public $title;
/**
     * @param string $root URI root (absolute or relative), may also include query - will be automatically parsed
     * @param array $params query parameters
     * @param string $fragment URI fragment after #
     */
public function __construct($root = './', $params = array(), $fragment = NULL) {
$this->parseUri($root,$params,$fragment);
}
/**
     * loads URI from a string.
     *
     * @param string $root raw uri
     * @param array $params
     * @param string $fragment
     * @return \WDB\WebUI\Uri
     */
public function parseUri($root = './', $params = array(), $fragment = NULL)
{
if ($root == '') $root = '.';
$root = $this->parseQuery($root);
$this->addParams($params);
if ($fragment != NULL)
{
$this->fragment = $fragment;
}
$data = explode('://',$root,2);
 if (count($data) > 1)
{
$this->urlmode = 3;
 $this->protocol = strtolower(preg_replace('~[^a-z0-9]~i', '', $data[0]));
 preg_match('~^[a-z0-9-.]+~i', $data[1], $matches);
$this->host = $matches[0];
$data = substr($data[1], strlen($this->host));
 if ($data[0] == ':')
{
$data = substr($data, 1);
preg_match('~^[0-9]+~i', $data, $matches);
$this->port = $matches[0];
$data = substr($data, strlen($this->port));
}
else
{
$this->port = NULL;
}
$root = $data;
}
else
{
$this->protocol = (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') ? 'https' : 'http';
$this->host = $_SERVER['HTTP_HOST'];
$this->port = NULL;
}
$this->path = $root;
if ($this->path[0] == '/')
{  if (!$this->urlmode) $this->urlmode = 2;
$this->abspath_prefix = '';
}
else
{  if (!$this->urlmode) $this->urlmode = 1;
 $this->abspath_prefix = preg_replace('~[^/]*$~', '', $_SERVER['PHP_SELF']);
}
return $this;
}
/**
     * @param string $key
     * @param string $value
     * @return \WDB\WebUI\Uri
     */
public function addParam($key, $value) {
$this->_addParam($key, $value, $this->params);
return $this;
}
private function _addParam($key, $value, &$container)
{
$ct = &$container;
while (preg_match('~([^[]*)\\[(.*?)](.*)~', $key, $matches))
{
if (strlen($matches[3]) > 0 && $matches[3]{0} != '[') $matches[3] = '';  $key=$matches[2].$matches[3];
if (!isset($container[$matches[1]])) $container[$matches[1]] = array();
 $ctx = &$ct[$matches[1]];
unset($ct);
$ct = &$ctx;
}
if (WDB\Utils\Arrays::isArrayAccessible($value))
{
$this->_addParams(array($key => $value), $ct);
}
else
{
$ct[$key] = $value;
}
}
/**
     * @param string $key
     * @return string
     */
public function getParam($key) {
return isset($this->params[$key]) ? $this->params[$key] : NULL;
}
private function _addParams($params, &$container)
{
foreach ($params as $key=>$value)
{
if (WDB\Utils\Arrays::isArrayAccessible($value))
{
if (!isset($this->params[$key])) $this->params[$key] = array();
$this->_addParams($value, $container[$key]);
}
else
{
$this->_addParam($key, $value, $container);
}
}
}
/**
     * @param array $params associative parameter array
     * @return \WDB\WebUI\Uri
     */
public function addParams($params) {
$this->_addParams($params, $this->params);
return $this;
}
/**
     * @param string|array $key parameter or list of parameters
     * @return \WDB\WebUI\Uri
     */
public function removeParams($key) {
if (is_array($key))
{
foreach ($key as $k) $this->removeParams($k);
}
else
{
if (isset($this->params[$key])) unset($this->params[$key]);
}
return $this;
}
/**
     *
     * @param array $keys list of allowed keys
     * @return \WDB\WebUI\Uri
     */
public function restrictParams($keys) {
if (!is_array($keys))
{  $keys = func_get_args();
}
foreach ($this->params as $key=>$val)
{
if (!in_array($key,$keys)) unset($this->params[$key]);
}
return $this;
}
/**
     * Clones object to fetch relative URIs. Returns self to enable chaining.
     *
     * @return \WDB\WebUI\Uri
     */
public function relative()
{
$copy = clone $this;
$copy->urlmode = 1;
return $copy;
}
/**
     * Configures object to fetch absolute resource path (host and protocol not included). Returns self to enable chaining.
     * 
     * @return \WDB\WebUI\Uri
     */
public function absolute_path()
{
$copy = clone $this;
$copy->urlmode = 2;
return $copy;
}
/**
     * Configures object to fetch full absolute URIs. Returns self to enable chaining.
     *
     * @return \WDB\WebUI\Uri
     */
public function absolute()
{
$copy = clone $this;
$copy->urlmode = 3;
return $copy;
}
private $fetch_params_firstpass;
private function fetch_params(array $params, $prefix = '')
{
$link = '';
foreach ($params as $key=>$val) {
if ($val === NULL) continue;
$full_key = $prefix == '' ? urlencode($key) : $prefix.'['.urlencode($key).']';
$full_key = str_replace(array('%5B', '%5D'), array('[', ']'), $full_key);
if (is_array($val))
{
$link .= $this->fetch_params($val, $full_key);
}
else
{
if ($this->fetch_params_firstpass) {
$this->fetch_params_firstpass = FALSE;
} else {
$link .= '&';
}
$link .= $full_key.'='.urlencode($val);
}
}
return $link;
}
/**
     * @param array $params additional parameters
     * @param string $fragment
     * @param bool $deleteParams fetch uri with cleaned all parameters
     * @return void
     */
public function redirect($params = array(), $fragment = NULL, $deleteParams = FALSE)
{
header('location: '.$this->absolute()->fetch($params, $fragment, $deleteParams));
die();
}
/**
     * @param array $params additional parameters
     * @param string $fragment
     * @param bool $deleteParams fetch uri with cleaned all parameters
     * @return string
     */
public function fetch($params = array(), $fragment = NULL, $deleteParams = FALSE) {  $oldparams = $this->params;
if ($deleteParams) $this->params = array();
$this->addParams($params);
switch ($this->urlmode)
{
case 2:
$link = $this->abspath_prefix;
break;
case 3:
$link = $this->protocol.'://'.$this->host;
if ($this->port && !
(($this->protocol == 'http' && $this->port == 80) ||
($this->protocol == 'https' && $this->port == 443)
))
{
$link .= ':'.$this->port;
}
$link .= $this->abspath_prefix;
break;
case 1:
default:
$link = '';
break;
}
$link .= $this->path.((count($this->params) > 0) ? '?' : '');
$this->fetch_params_firstpass = TRUE;
$link .= $this->fetch_params($this->params);
if ($fragment !== NULL) {
if ($fragment != '')
{
$link .= '#'.urlencode($fragment);
}
} elseif (!empty($this->fragment)) {
$link .= '#'.urlencode($this->fragment);
}
$this->params = $oldparams;
return $link;
}
public function __toString() {
return $this->fetch();
}
/**
     * parses query and fragment part of uri
     *
     * @param string $uri raw uri
     * @return \WDB\WebUI\Uri
     */
private function parseQuery($uri) {
$this->params = array();
$this->fragment = NULL;
$query = NULL;
$fragmentPosition = strpos($uri, '#');
if ($fragmentPosition === FALSE) {  $queryPosition = strpos($uri, '?');
if ($queryPosition === FALSE) {  $root = $uri;  } else {  list($root, $query) = explode('?', $uri);  }
} else {  list($root, $this->fragment) = explode('#', $uri);  $queryPosition = strpos($root, '?');
if ($queryPosition !== FALSE) {  list($root, $query) = explode('?', $root);  }
}
if ($query)
{
$query = explode('&', $query);
foreach ($query as $param) {
$kv = explode('=', $param);
if ($kv[0] == '') continue;
if (count($kv) > 1) {
$this->addParam(urldecode($kv[0]), urldecode($kv[1]));
} else {
$this->addParam(urldecode($kv[0]), TRUE);
}
}
}
return $root;
}
}}namespace WDB\Wrapper{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnFactory
{
/**
     * Creates a column wrapper
     * 
     * @param WDB\Analyzer\Column
     * @param iTable
     * @return iColumn
     */
public static function create(WDB\Analyzer\Column $columnAnalyzer, iTable $tableWrapper)
{
$tClass= $tableWrapper->getColumnWrapperClass($columnAnalyzer->name);
if ($tClass === NULL)
{
$class = '\\WDB\\Wrapper\\Column';
}
else
{
$class = WDB\Utils\System::findClass($tClass, WDB\Config::read('userWrappersClassPrefix'));
}
return new $class($tableWrapper, $columnAnalyzer);
}
/**
     * Creates a column wrapper for a foreign key
     * 
     * @param WDB\Analyzer\Column
     * @param iTable
     * @return ColumnForeign
     */
public static function createForeign(WDB\Structure\ForeignKeyData $key, iTable $table, array $analyzerColumns)
{
 $c = new ColumnForeign($key, $table, $analyzerColumns);
return $c;
}
}
}namespace WDB\Wrapper{
use WDB;
/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Field extends WDB\BaseObject
{

private $column;

private $originalValue;

private $newValue;
private $written = FALSE;
private $validationErrors = array();
/**
     *
     * @param iColumn column this field belongs to
     * @param bool is in new record
     * @param mixed field value
     */
public function __construct(iColumn $column, $isNew, $value)
{
$this->column = $column;
if ($isNew)
{
$this->originalValue = $column->getDefault();
$this->newValue = $value;
$this->written = TRUE;
}
else
{
$this->originalValue = $value;
$this->newValue = FALSE;
}
}
/**
     * Adds a validation error on this field.
     *
     * @param string error message
     */
public function addValidationError($message)
{
$this->validationErrors[] = $message;
}
/**
     * Erases all bound validation errors.
     */
public function resetValidationErrors()
{
$this->validationErrors = array();
}
public function getValidationErrors()
{
return $this->validationErrors;
}
/**
     * Called when owner record is saved - updates original value to current value (Resets the changed state)
     *
     */
public function saved()
{
if ($this->written)
{
$this->originalValue = $this->newValue;
}
$this->written = FALSE;
}
/**
     * Sets the value of the field.
     *
     * @param mixed the value
     * @return self fluent interface
     */
public function setValue($value)
{
$this->newValue = $value;
$this->written = TRUE;
return $this;
}
/**
     * Gets the value of the field.
     *
     * @return mixed
     */
public function getValue()
{
return $this->written ? $this->newValue : $this->originalValue;
}
public function getStringValue()
{
return $this->column->getStringValue($this->getValue());
}
/**
     * Returns true if the value has been changed since load (differs from the original value)
     *
     * @return bool
     */
public function isChanged()
{
if (!$this->written) return false;
if (is_object($this->newValue) && method_exists($this->newValue, 'equals'))
{
return $this->newValue->equals($this->originalValue);
} else
{
return $this->newValue != $this->originalValue;
}
}
/**
     * Returns true if the value has been written since load (may be equal to original value)
     *
     * @return bool
     */
public function isWritten()
{
return $this->written;
}
/**
     * Returns the original value of this column as it was on load
     *
     * @return mixed
     */
public function getOriginalValue()
{
return $this->originalValue;
}
/**
     * Adds a value to the array which represents the row which will be saved to database.
     *
     * @param array the row
     */
public function saveToRow(array &$row)
{
$this->column->saveToRow($row, $this->value);
}
/**
     *
     * @return iColumn
     */
public function getColumn()
{
return $this->column;
}
}}namespace WDB\Wrapper{
use WDB;
/**
 * Interface for table wrapper's column.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @todo eliminate loadField and createField
 */
interface iColumn
{
 /**
     * Returns name identifier for this column
     *
     * @return string
     */
public function getName();
/**
     * Returns human-readable title for this column
     *
     * @return type 
     */
public function getTitle();

 /**
     * Returns default value for fields in this column
     *
     * @return mixed
     */
public function getDefault();
/**
     * Returns true when this column is filled automatically (i.e. by database,
     * auto_increment, current_timestamp etc)
     * 
     * @return bool
     */
public function isAutoColumn();
/**
     * Returns true when this column is filled automatically (i.e. by database,
     * auto_increment, current_timestamp etc)
     * 
     * @return bool
     */
public function isAutoIncrement();
/**
     * Returns true when fields in this column can have a null value
     *
     * @return bool
     */
public function isNullable();

 /**
     * Returns validation rules defined for this column
     *
     * @return WDB\Validation\Rules
     */
public function getRules();
/**
     * Adds validation rule to this column
     * 
     * @param int rule (enumeration of WDB\Validation\ColumnRules::*
     * @param string error message
     * @param mixed validator argument
     * @return self fluent interface
     */
public function rule($rule, $message = NULL, $argument = NULL);

 /**
     * Returns name of webui class for this object (or null if default should be used)
     *
     * @return string
     */
public function getWebUIClass();
/**
     * Configures webui display mode
     * 
     * @param int $original
     * @return int
     * @see WDB\Analyzer\iColumn::getWebUIDisplayMode
     */
public function getWebUIDisplayMode($original = NULL);

/**
     * Returns table object this column belongs to
     * 
     * @return iTable
     */
public function getTable();
/**
     * Converts PHP value to SQL query string part using an SQL driver.
     * 
     * @param mixed
     * @return WDB\Query\Element\Datatype\iDatatype
     */
public function valueToDatatype($value);
/**
     * Returns true when both values are equal in context of this column.
     * 
     * @param mixed
     * @param mixed
     * @return bool
     */
public function valuesEqual($first, $second);
/**
     * Create Field object to provide it to Record.
     * 
     * @param bool is new record
     * @param mixed value
     * @param WDB\Query\SelectedRow whole record row
     * @return Field
     */
public function createField($isNew, $value, WDB\Query\SelectedRow $row = NULL);
/**
     * Adds a value from Field to the array which represents the row which will be saved to database.
     * 
     * @param array the row
     * @param mixed the value
     */
public function saveToRow(array &$row, $value);
/**
     * Raise the OnSaved event after a record with this column is saved.
     * 
     * @param WDB\Wrapper\iRecord
     */
public function raiseOnSaved(WDB\Wrapper\iRecord $record);
/**
     * Get the OnSaved event control class
     * 
     * @return WDB\Event\Event
     */
public function getOnSaved();
/**
     * Returns true if a datasource can be ordered by this column
     * 
     * @return bool
     */
public function isOrderable();
/**
     * May transform field value stored inside iRecord Field into a complex object or just pass it through.
     * 
     * @param mixed $value value from Field
     * @return mixed transformed value
     */
public function getFieldValue($value);
/**
     * get string representation for this column's value.
     */
public function getStringValue($value);
/**
     * May transform complex value passed by user to store inside iRecord Field or just pass it through.
     * 
     * @param mixed $value value from user
     * @return mixed value to store to Field
     */
public function setFieldValue($value);
/**
     * Configure datasource to be sorted by this column.
     * 
     * @param WDB\iDatasource $ds
     * @param bool ascending
     * @param bool prepend order rule to other ds rules
     */
public function sortDatasource(WDB\iDatasource $datasource, $asc = TRUE, $prepend = TRUE);
}}namespace WDB\Wrapper{
/**
 * Column with base data stored in database under WDB\Database.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iDBColumn extends iColumn
{
/**
     * Adds fields to the array argument to be read from database when loading table with this column.
     * 
     * @param WDB\Query\Element\SelectField[]
     */
public function fetchReadFields(&$fields);
}}namespace WDB\Wrapper{
use WDB;
/**
  * Orderable column WebUI.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iFilterable
{
/**
     * Determines whether records can be filtered by this column
     * 
     * @return bool
     */
public function isFilterable();
/**
     * @param mixed $value
     * @return bool
     */
public function filterDatasource(WDB\iDatasource $datasource, $value);
}}namespace WDB\Wrapper{
use WDB;
/**
  * Orderable column WebUI.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
interface iOrderable
{
/**
     * Returns true if a datasource can be ordered by this column
     * 
     * @return bool
     */
public function isOrderable();
/**
     * Configures datasource to order by this column
     * 
     * @param WDB\iDatasource $datasource
     * @param bool $asc ascending if true, descending if false
     * @param bool $append if false, the order rule is prepended.
     */
public function sortDatasource(WDB\iDatasource $datasource, $asc = true, $prepend = true);
}}namespace WDB\Wrapper{
use WDB;
/**
 * Table record wrapping class. Contains exactly one selected row of a table.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iRecord extends \ArrayAccess, \Countable, \Iterator
{
/** when updating, write only columns that were changed (value is not equal to the original) */
const WM_CHANGED = 1;
/**  when updating, write only columns that were written (value may be equal to the original) */
const WM_WRITTEN = 2;
/** when updating, force write all columns */
const WM_ALL = 3;
/**
     * get table owning this record
     *
     * @return iTable
     */
public function getTable();
/**
     * Get the Field object for the specified column.
     *
     * @param string column name
     * @return Field
     */
public function getField($name);
 /**
     * Returns all validators bound to this record
     *
     * @return WDB\Validation\iValidator[]
     */
public function getValidators();
/**
     * get validation error messages
     *
     * @return string[]
     */
public function getValidationErrors();
/**
     * Add validation errors to validation errors container
     *
     * @param string[]
     */
public function addValidationErrors($errors);
/**
     * binds a validator object
     *
     * @param WDB\Validation\iValidator
     */
public function addValidator(WDB\Validation\iValidator $validator);
/**
     * Removes bound validator
     *
     * @param WDB\Validation\iValidator
     */
public function removeValidator(WDB\Validation\iValidator $validator);
/**
     * Returns whether to validate this record or not upon save
     *
     * @return bool
     */
public function isValidatedUponSave();
/**
     * Configures whether to validate this record or not upon save
     *
     * @param bool
     */
public function setValidateUponSave($value);
/**
     * validates current record
     * @throws WDB\Exception\ValidationFailed
     */
public function validate();


/**
     * Saves this record to database.
     *
     * @return bool
     */
public function save();
/**
     * Removes row corresponding to this record from the database.
     *
     * @return bool
     */
public function delete();
/**
     * Returns true when a value of any column was changed since last load/save
     * (always true for all columns of new row).
     *
     * @return bool
     */
public function changed();
/**
     * Returns true when a value of any column was written since last load/save.
     * In compare with changed() method, this returns true even when newly assigned value
     * is equal to original value.
     *
     * @return bool
     */
public function written();
/**
     * Set column write mode when updating
     *
     * @param int enumeration of iRecord::WM_*
     */
public function setWriteMode($mode);
/**
     * Get column write mode when updating
     *
     * @return int enumeration of iRecord::WM_*
     */
public function getWriteMode();
/**
     * Returns true if this is a new record, not persisted yet in database.
     *
     * @return bool
     */
public function isNew();

 /**
     * Returns key=>values of columns which are uniquely identifying the record. It should be a table primary key or a reasonable substitution.
     *
     * @return array
     */
public function getIdentificationKey();
/**
     * returns a string identifier of a row built from primary key (should be as
     * simple as possible)
     * @return string
     */
public function getStringKey();

 /**
     * Determines whether this record has an accessible detail view.
     * A record must have valid primary key to be accessible in detail view.
     *
     * @return bool
     */
public function isUIDetail();
/**
     * Determines whether this record has an accessible edit view.
     * A record must have valid primary key to be accessible in edit view.
     *
     * @return bool
     */
public function isUIEdit();
/**
     * Determines whether this record can be deleted from UI.
     * A record must have valid primary key to be deleted in UI.
     *
     * @return bool
     */
public function isUIDelete();
}}namespace WDB\Wrapper{
use WDB;
/**
 * Table wrapper class. Provides information about its columns and allows to retrieve contained records.
 * May or may not be reflecting a database table - this allows programmers to add virtual tables which are implemented
 * another way than binding data to database and combine them with database tables all under WDB API.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iTable extends \Iterator, \Countable, \ArrayAccess
{
 /**
     * Returns name of this table
     *
     * @return string
     */
public function getName();
/**
     * Returns human-readable title of this table.
     *
     * @return string
     */
public function getTitle();

 /**
     * Returns serialized form of a key to string.
     *
     * @param array $k
     * @return string
     */
public function serializeKey(array $k);

public function unserializeKey($data, array $keys);

 /**
     * get record object identified by a key (which is associative array: columnname=>value) or a single column key value
     *
     * @param array|mixed $key
     * @return iRecord
     */
public function getRecordByKey($key);
/**
     * Returns record matching passed key or throws NotFound if not found.
     *
     * @param string record key
     * @return iRecord
     * @throws WDB\Exception\NotFound
     */
public function getRecordByStringPK($key);
/**
     * Get table's primary key as an array of column names or list of columns that emulate a primary key.
     *
     * @return array
     */
public function getPrimaryKey();
/**
     * Get table's primary key as an array of arrays of column names.
     *
     * @return array[]
     */
public function getUniqueKeys();
/**
     * fetches query result into a record object.
     *
     * @param WDB\Query\SelectedRow $row
     * @return WDB\Wrapper\iRecord
     */
public function fetchRecord(WDB\Query\SelectedRow $row);
/**
     * creates a new iRecord object bound to this table
     * @param array $data
     * @param int $mode
     * @return iRecord
     */
public function newRecord($data = array(), $mode = WDB\Query\Insert::UNIQUE);
/**
     * Elevates collection of select result rows to collection of iRecords of this table.
     *
     * @param WDB\Query\SelectedRow[] select result rows
     * @return iRecord[]
     */
public function elevateRecords($rows);

 /**
     *
     * @param string column identifier name
     * @return string|NULL column wrapper class name or use default class
     */
public function getColumnWrapperClass($columnName);
/**
     *
     * @param string column identifier name
     * @return string|NULL column webui class name or use default class
     */
public function getColumnWebUIClass($columnName);
/**
     * Returns class name for webui class of this table (or null when default shoud be used)
     *
     * @return string
     */
public function getWebUIClass();
/**
     * Returns class name for wrapper of records in this table (or null when default shoud be used)
     *
     * @return string
     */
public function getRecordWrapperClass();

 /**
     * @return iColumn[]
     */
public function getColumns();
/**
     * Allows columns to have aliases to be accessed through.
     * i.e. column which is (part of a) foreign key is not represented
     * by a standard column object, but a foreign column object
     * and the original column name is aliased to the foreign key name.
     *
     * @param string alias
     * @return string canonical name
     */
public function getColumnCName($alias);
/**
     * Adds a column wrapper to this table.
     *
     * @param iColumn
     * @param string|null column identifier. if null, $column->name is used.
     */
public function addColumn(iColumn $column, $name = NULL);

 /**
     * Returns true if this table shoud be listed in schema webUI table list.
     *
     * @return bool
     */
public function isListedInSchemaAdmin();
/**
     * Returns true if this table is accessible by webUI
     *
     * @return bool
     */
public function isWebUIAccessible();
/**
     * Returns true if records in this table can display detail in WebUI.
     *
     * @return bool
     */
public function isUIDetail();
/**
     * Returns true if records in this table can be edited in WebUI.
     *
     * @return bool
     */
public function isUIEdit();
/**
     * Returns true if records in this table can be deleted in WebUI.
     *
     * @return bool
     */
public function isUIDelete();

/**
     * loads record validators for the table
     *
     * @param WDB\Event\Event
     */
public function fetchValidators(WDB\Event\Event $event);
}}
namespace WDB\Wrapper{
use WDB,
WDB\Exception;
/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Record extends WDB\BaseObject implements iRecord
{
/** @var \WDB\Wrapper\iDBTable */
protected $tableWrapper;
/** @var \WDB\Wrapper\Field[] */
protected $fields;
/** @var bool */
protected $validateUponSave;
/** @var bool */
protected $isNew;
/** @var int enumeration of WDB\Query\Insert::MODE_* */
protected $ODKUmode;
/** @var WDB\Event\Event */
protected $validateEvent;
/** @var array */
protected $validationErrors;
/** @var array global validation errors not bound to any column*/
protected $globalValidationErrors = NULL;
/** @var WDB\Validation\ColumnValidator */
protected $columnValidator = NULL;
/** @var int enumeration of iRecord::WM_* */
protected $writeMode = iRecord::WM_WRITTEN;
 public function getTable()
{
return $this->tableWrapper;
}
public function getValidators()
{
return $this->validateEvent->toArray();
}
public function getValidationErrors()
{
return $this->validationErrors;
}
public function getGlobalValidationErrors()
{
if ($this->globalValidationErrors === NULL)
{
$this->globalValidationErrors = array();
if (is_array($this->validationErrors))
{
foreach ($this->validationErrors as $error)
{
if (is_string($error))
{
$this->globalValidationErrors[] = $error;
}
}
}
}
return $this->globalValidationErrors;
}
public function addValidationErrors($errors)
{
$this->validationErrors = array_merge($this->validationErrors, $errors);
}
public function addValidator(WDB\Validation\iValidator $validator)
{
$this->validateEvent->addListener($validator);
}
public function removeValidator(WDB\Validation\iValidator $validator)
{
return $this->validateEvent->removeListener($validator);
}
public function isValidatedUponSave()
{
return $this->validateUponSave;
}
public function setValidateUponSave($value)
{
$this->validateUponSave = (bool) $value;
}
public function validate()
{
$this->validationErrors = array();
$this->globalValidationErrors = NULL;
foreach ($this->fields as $field)
{
$field->resetValidationErrors();
}
 return !in_array(FALSE, $this->validateEvent->raise($this), TRUE);
}
/**
     *
     * @return bool
     */
public function save()
{
if ($this->validateUponSave && !$this->validate())
{
throw new Exception\ValidationFailed($this->validationErrors);
}
if ($this->isNew)
{
$result = $this->saveNew();
$this->isNew = FALSE;
} else
{
$result = $this->saveUpdate();
}
foreach ($this->fields as $field)
{
$field->saved();
}
foreach ($this->getTable()->getColumns() as $column)
{
$column->raiseOnSaved($this);
}
return $result;
}
public function delete()
{
if ($this->isNew)
{
throw new Exception\InvalidOperation("cannot delete newly created row.");
}
$query = new WDB\Query\Delete($this->tableWrapper->tableAnalyzer->identifier, $this->keyMatchCondition());
 $this->isNew = TRUE;
return $query->run($this->tableWrapper->database)->success;
}
public function changed()
{
foreach ($this as $fields)
{
if ($field->changed)
return TRUE;
}
return FALSE;
}
public function written()
{
foreach ($this as $field)
{
if ($field->written)
return TRUE;
}
return FALSE;
}
public function getIdentificationKey()
{
if ($this->isNew()) return NULL;
$val = array();
foreach ($this->tableWrapper->getPrimaryKey() as $part)
{
$val[$part] = $this->fields[$part]->getOriginalValue();
}
return $val;
}
public function getStringKey()
{
if ($this->isNew()) return NULL;
$key = $this->getIdentificationKey();
if (count($key) > 1)
{
return $this->tableWrapper->serializeKey($key);
} else
{
return current($key);
}
}
public function isUIDetail()
{
return $this->tableWrapper->isUIDetail();
}
public function isUIEdit()
{
return $this->tableWrapper->isUIEdit();
}
public function isUIDelete()
{
return $this->tableWrapper->isUIDelete();
}
public function isNew()
{
return $this->isNew;
}
public function setWriteMode($mode)
{
$this->writeMode = (int) $mode;
}
public function getWriteMode()
{
return $this->writeMode;
}

/**
     * Get the Column validator for this record (special common validator for column rules)
     *
     * @return WDB\Validation\ColumnValidator
     */
public function getColumnValidator()
{
return $this->columnValidator;
}

public function __construct(Structure\RecordInitializer $ri)
{
$this->tableWrapper = $ri->table;
if (!$this->tableWrapper instanceof iDBTable)
{
throw new Exception\BadArgument("DB Record class needs a table implementing iDBTable. iTable interface" .
"is insufficient.");
}
if ($ri instanceof Structure\NewRecordInitializer)
{
$this->constructNew($ri);
} elseif ($ri instanceof Structure\ExistingRecordInitializer)
{
$this->constructExisting($ri);
} else
{
throw new Exception\BadArgument('Usupported record initializer argument: ' . getClass($ri));
}
$this->validateEvent = new WDB\Event\Event();
$this->addValidator($this->columnValidator = new WDB\Validation\ColumnValidator());
$this->table->fetchValidators($this->validateEvent);
$this->setValidateUponSave(TRUE);
}
private function constructNew(Structure\NewRecordInitializer $ri)
{
$this->isNew = TRUE;
$this->ODKUmode = intval($ri->insertMode);
$this->fields = array();
foreach ($this->tableWrapper->columns as $column)
{
$value = isset($ri->data[$column->name]) ? $ri->data[$column->name] : $column->default;
$this->fields[$column->name] = $column->createField(true, $value);
}
}
private function constructExisting(Structure\ExistingRecordInitializer $ri)
{
$this->isNew = FALSE;
$this->fields = array();
foreach ($this->tableWrapper->columns as $column)
{
$value = isset($ri->row[$column->name]) ? $ri->row[$column->name] : NULL;
$this->fields[$column->name] = $column->createField(false, $value, $ri->row);
}
}

/**
     *
     * @return bool
     */
protected function saveNew()
{
$row = array();
foreach ($this->fields as $field)
{
if ($field->isWritten())
{
$field->saveToRow($row);
}
}
$query = new WDB\Query\Insert($this->tableWrapper->getTableAnalyzer()->getIdentifier(), $row, $this->ODKUmode);
$result = $query->run($this->tableWrapper->getDatabase());
if (!$result->success) return FALSE;
$this->isNew = FALSE;
if ($result->insertId)
{
foreach ($this->fields as $field)
{

if ($field->getColumn()->isAutoIncrement())
{
$field->setValue($result->insertId);
}
}
}
return TRUE;
}
/**
     *
     * @return bool
     */
protected function saveUpdate()
{
$upd = array();
foreach ($this->fields as $field)
{
if ($this->writeMode == iRecord::WM_WRITTEN && $field->isWritten() ||
$this->writeMode == iRecord::WM_CHANGED && $field->isChanged() ||
$this->writeMode == iRecord::WM_ALL)
{
$field->saveToRow($upd);
}
}
$query = new WDB\Query\Update($this->tableWrapper->getTableAnalyzer()->getIdentifier(), $upd);
$query->where = $this->keyMatchCondition();
return $query->run($this->tableWrapper->getDatabase())->success;
}
/**
     * get a where condition to identify current record in a table by primary key if available, unique key if PK not available
     * or all columns if no key available.
     *
     * @return \WDB\Query\Element\iCondition
     */
protected function keyMatchCondition()
{
$compare = array();
$columns = $this->tableWrapper->getColumns();
$ikey = $this->getIdentificationKey();
foreach ($ikey as $key=>$val)
{
$compare[] = WDB\Query\Element\Compare::Equals(
WDB\Query\Element\ColumnIdentifier::create($key), $columns[$key]->valueToDatatype($val)
);
}
return WDB\Query\Element\LogicOperator::lAnd($compare);
}
  public function fieldExists($name)
{
return isset($this->fields[$name]);
}
public function getFieldValue($name)
{
if (!$this->fieldExists($name))
{
throw new Exception\ColumnNotFound($name);
}
$columns = $this->tableWrapper->getColumns();
return $columns[$name]->getFieldValue($this->fields[$name]->getValue());
}
public function setFieldValue($name, $value)
{
if (!$this->fieldExists($name))
{
throw new Exception\ColumnNotFound($name);
}
$columns = $this->tableWrapper->getColumns();
$this->fields[$name]->setValue($columns[$name]->setFieldValue($value));
}
/**
     *
     * @param string column name
     * @return Field
     */
public function getField($name)
{
if (!$this->fieldExists($name))
{
throw new Exception\ColumnNotFound($name);
}
return $this->fields[$name];
}
   public function offsetExists($offset)
{
return $this->fieldExists($offset);
}
public function offsetGet($offset)
{
return $this->getFieldValue($offset);
}
public function offsetSet($offset, $value)
{
$this->setFieldValue($offset, $value);
}
public function offsetUnset($offset)
{
$this->setFieldValue($offset, NULL);
}
   public function count()
{
return count($this->fields);
}
   public function current()
{
return current($this->fields)->getValue();
}
public function key()
{
return key($this->fields);
}
public function next()
{
$a = next($this->fields);
if (is_object($a))
{
return $a->getValue();
} else
{
return FALSE;  }
}
public function rewind()
{
return reset($this->fields);
}
public function valid()
{
$key = key($this->fields);
return ($key !== NULL && $key !== FALSE);
}
}}namespace WDB\Wrapper{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class RecordFactory
{
public static function load(iTable $table, WDB\Query\SelectedRow $row)
{
$class = self::getClass($table);
return new $class(new Structure\ExistingRecordInitializer(
array(
'table'=>$table,
'row'=>$row,
)
));
}
private static function getClass(iTable $table)
{
$tClass = $table->getRecordWrapperClass();
if ($tClass === NULL)
{
return '\\WDB\\Wrapper\\Record';
}
else
{
return WDB\Utils\System::findClass($tClass, WDB\Config::read('userWrappersClassPrefix'));
}
}
public static function create(iTable $table, array $data = array(), $mode = WDB\Query\Insert::UNIQUE)
{
$class = self::getClass($table);
return new $class(new Structure\NewRecordInitializer(
array(
'table'=>$table,
'data'=>$data,
'insertMode'=>$mode,
)
));
}
}
}namespace WDB\Wrapper\Structure{
use WDB;
/**
 * @property-read WDB\Wrapper\iTable $table
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
abstract class RecordInitializer extends WDB\Structure\Structure {}
}namespace WDB\Wrapper\Structure{
use WDB;
/**
 * @property-read int $id
 * @property-read \DateTime $from
 * @property-read \DateTime $to
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class TimeSpan extends WDB\Structure\Structure
{

private $table;
/**
     *
     * @param WDB\Wrapper\TimeDependentTable $table
     * @param int $id
     * @param \DateTime $from
     * @param \DateTime $to
     */
public static function create(WDB\Wrapper\TimeDependentTable $table, $id, \DateTime $from = NULL, \DateTime $to = NULL)
{
$result = new self(array('id'=>$id, 'from'=>$from, 'to'=>$to));
$result->table = $table;
return $result;
}
/**
     * get the record referenced by this timespan.
     * @return WDB\Wrapper\iRecord
     */
public function getTheRecord()
{
return $this->table->getRecordByTimeId($this->id);
}
}
}namespace WDB\Wrapper{
use WDB,
WDB\Exception;
/**
 * Static class for creating iTable objects.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class TableFactory
{
/**
     * Creates table wrapper from table analyzer.
     *
     * @param WDB\Analyzer\Table $table
     * @return iTable
     */
public static function fromAnalyzer(WDB\Analyzer\iTable $table)
{
$tClass = $table->getWrapperClass();
if ($tClass === NULL)
{
$tClass = '\\WDB\\Wrapper\\Table';
}
else
{
$tClass = WDB\Utils\System::findClass($tClass, WDB\Config::read('userWrappersClassPrefix'));
}
return new $tClass($table);
}
/**
     * Creates table wrapper from table name in database.
     *
     * @param string table name
     * @param string|null schema name
     * @param WDB\Database|null source database
     * @return iTable 
     * 
     * @throws Exception\NotFound
     */
public static function fromName($table, $schema = NULL, WDB\Database $database = NULL)
{
if ($database === NULL)
{
$database = WDB\Database::getDefault();
}
if ($schema === NULL)
{
$schema = $database->getSchema();
}
else
{
$schema = $database->getSchema($schema);
}
if (!isset($schema->tables[$table]))
{
throw new Exception\NotFound("Table $table not found.");
}
$analyzer = $schema->tables[$table];
return self::fromAnalyzer($analyzer);
}
/**
     * Creates table wrapper from table locator (table name, table schema and a named database connection).
     *
     * @param WDB\Structure\TableLocator
     * @return iTable
     */
public static function fromLocator(WDB\Structure\TableLocator $locator)
{
return self::fromName($locator->table, $locator->schema, WDB\Database::getInstance($locator->connection));
}
}
}
namespace WDB\Wrapper{
use WDB,
WDB\Exception,
WDB\Query\Element;
/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class TimeDependentRecord extends Record
{

protected $isNewTimespan = FALSE;

protected $fromGlobal;

protected $fromGlobalOriginal;

protected $toGlobal;

protected $toGlobalOriginal;

private $doNotUpdateGlobalTimeRanges = FALSE;
 public function __construct(Structure\RecordInitializer $ri)
{
parent::__construct($ri);
$this->fromGlobal = $this->fromGlobalOriginal = $ri->row[TimeDependentTable::FROM_COLUMN_GLOBAL_ALIAS];
$this->toGlobal = $this->toGlobalOriginal = $ri->row[TimeDependentTable::TO_COLUMN_GLOBAL_ALIAS];
$this->isNewTimespan = $this->isNew;
}
public function delete()
{
if ($this->isNew)
{
throw new Exception\InvalidOperation("cannot delete newly created row.");
}
$queryMaster = new WDB\Query\Delete($this->tableWrapper->getTableAnalyzer()->getIdentifier(), $this->keyMatchConditionMasterTable());
$queryTd = new WDB\Query\Delete($this->tableWrapper->getTimeTableAnalyzer()->getIdentifier(), $this->keyMatchConditionMasterTable());
 $this->isNew = TRUE;
return $queryMaster->run($this->tableWrapper->database)->success && $queryTd->run($this->tableWrapper->database)->success;
}
public function getUniqueIdentificationKey()
{
return $this->getMasterIdentificationKey();
}
public function getMasterIdentificationKey()
{
if ($this->isNew()) return NULL;
$val = array();
foreach ($this->tableWrapper->getMasterPrimaryKey() as $part)
{
$val[$part] = $this->fields[$part]->getOriginalValue();
}
return $val;
}
public function getIdentificationKey()
{
if ($this->isNew()) return NULL;
return array(WDB\GTO\WsqlLanguage::quoteIdentifier(Element\ColumnIdentifier::create(TimeDependentTable::ID_COLUMN,
$this->tableWrapper->getTimeTableAnalyzer()->getName(),
$this->tableWrapper->getTimeTableAnalyzer()->getSchema()->getName()))=> $this->fields[TimeDependentTable::ID_COLUMN]->getOriginalValue());
}

/**
     *
     * @return WDB\Query\WriteResult
     */
protected function saveNew()
{
$rowMaster = array();
$rowTd = array();
foreach ($this->fields as $field)
{
if ($field->isWritten())
{
if (in_array($field->getColumn(), $this->tableWrapper->getMasterColumns()))
{
$field->saveToRow($rowMaster);
}
if (in_array($field->getColumn()->getName(), $this->tableWrapper->getPrimaryKey())
|| in_array($field->getColumn(), $this->tableWrapper->getTimeDependentColumns()))
{
$field->saveToRow($rowTd);
}
}
}
$queryMaster = new WDB\Query\Insert($this->tableWrapper->getTableAnalyzer()->getIdentifier(), $rowMaster, $this->ODKUmode);
$this->tableWrapper->getDatabase()->startTransaction();
$resultM = $queryMaster->run($this->tableWrapper->getDatabase());
if ($resultM->insertId)
{
foreach ($this->tableWrapper->getMasterColumns() as $column)
{

if ($column->isAutoIncrement())
{
$this->getField($column->getName())->setValue($resultM->insertId)->saveToRow($rowTd);
}
}
}
$queryTd = new WDB\Query\Insert($this->tableWrapper->getTimeTableAnalyzer()->getIdentifier(), $rowTd, WDB\Query\Insert::UNIQUE);
$resultT = $queryTd->run($this->tableWrapper->getDatabase());
if (!$resultM->success || !$resultT->success || !$this->updateGlobalTimeRanges())
{
$this->tableWrapper->getDatabase()->rollback();
return FALSE;
}
$this->tableWrapper->getDatabase()->commit();
$this->isNew = $this->isNewTimespan = FALSE;
$this->getField('id~td')->setValue($resultT->insertId);
return TRUE;
}
/**
     *
     * @return WDB\Query\WriteResult
     */
protected function saveUpdate()
{
$timeTableName = $this->tableWrapper->getTimeTableAnalyzer()->getIdentifier();
$rowMaster = array();
$rowTd = array();
foreach ($this->fields as $field)
{
$isFromTd = (in_array($field->getColumn()->getName(), $this->tableWrapper->getMasterPrimaryKey())
|| in_array($field->getColumn(), $this->tableWrapper->getTimeDependentColumns()))
&& $field->getColumn()->getName() !== TimeDependentTable::ID_COLUMN;
$isFromMaster = in_array($field->getColumn(), $this->tableWrapper->getMasterColumns());
if ($this->writeMode == iRecord::WM_WRITTEN && $field->isWritten() ||
$this->writeMode == iRecord::WM_CHANGED && $field->isChanged() ||
$this->writeMode == iRecord::WM_ALL ||
$isFromTd && $this->isNewTimespan)
{
if ($isFromMaster)
{
$field->saveToRow($rowMaster);
}
if ($isFromTd)
{
$field->saveToRow($rowTd);
}
}
}
$queryMaster = new WDB\Query\Update($this->tableWrapper->getTableAnalyzer()->getIdentifier(), $rowMaster);
$queryMaster->where = $this->keyMatchConditionMasterTable();
if ($this->isNewTimespan)
{
$queryTd = new WDB\Query\Insert($timeTableName, $rowTd, WDB\Query\Insert::UNIQUE);
}
else
{
$queryTd = new WDB\Query\Update($timeTableName, $rowTd);
$queryTd->where = $this->keyMatchConditionTdTable();
}
$this->tableWrapper->getDatabase()->startTransaction();
$resultM = $queryMaster->run($this->tableWrapper->getDatabase());
$resultT = $queryTd->run($this->tableWrapper->getDatabase());
if ($this->isNewTimespan() && $resultT->success)
{
$this->getField(TimeDependentTable::ID_COLUMN)->setValue($resultT->insertId);
}
if ($resultM->success
&& $resultT->success
&& ($this->doNotUpdateGlobalTimeRanges ||$this->updateGlobalTimeRanges()))
{
$this->tableWrapper->getDatabase()->commit();
return TRUE;
}
else
{
$this->tableWrapper->getDatabase()->rollback();
return FALSE;
}
}
 private function deleteCoveredTimespans()
{
$cond = new Element\LogicOperator(
Element\Compare::NEquals(
Element\ColumnIdentifier::create(TimeDependentTable::ID_COLUMN),
Element\Datatype\AbstractType::createDatatype($this[TimeDependentTable::ID_COLUMN])
),
Element\LogicOperator::L_AND);
 $from = $this[TimeDependentTable::FROM_COLUMN];
$fromOrig = $this->getField(TimeDependentTable::FROM_COLUMN)->getOriginalValue();
if ($from !== NULL && ($fromOrig !== NULL || $this->isNewTimespan))
{
$delThreshold = $this[TimeDependentTable::FROM_COLUMN];
if (!$this->isNewTimespan)
{
$delThreshold = min($from, $fromOrig);
}
$cond->addExpression(Element\Compare::Gte(
Element\ColumnIdentifier::create(TimeDependentTable::FROM_COLUMN),
Element\Datatype\AbstractType::createDatatype($delThreshold)
));
}
$to = $this[TimeDependentTable::TO_COLUMN];
$toOrig = $this->getField(TimeDependentTable::TO_COLUMN)->getOriginalValue();
if ($to !== NULL && ($toOrig !== NULL || $this->isNewTimespan))
{
$delThreshold = $this[TimeDependentTable::TO_COLUMN];
if (!$this->isNewTimespan)
{
$delThreshold = max($to, $toOrig);
}
$cond->addExpression(Element\Compare::Lte(
Element\ColumnIdentifier::create(TimeDependentTable::TO_COLUMN),
Element\Datatype\AbstractType::createDatatype($delThreshold)
));
}
 if ($this->fromGlobal !== NULL || $this->toGlobal !== NULL)
{
$cond = new Element\LogicOperator(array($cond), Element\LogicOperator::L_OR);
if ($this->fromGlobal !== NULL)
{
$cond->addExpression(Element\Compare::Lte(
Element\ColumnIdentifier::create(TimeDependentTable::TO_COLUMN),
Element\Datatype\AbstractType::createDatatype($this->fromGlobal)
));
}
if ($this->toGlobal !== NULL)
{
$cond->addExpression(Element\Compare::Gte(
Element\ColumnIdentifier::create(TimeDependentTable::FROM_COLUMN),
Element\Datatype\AbstractType::createDatatype($this->toGlobal)
));
}
}
$timeTableName = $this->tableWrapper->getTimeTableAnalyzer()->getIdentifier();
$q=new WDB\Query\Delete($timeTableName, $cond);
$q->filter($this->getMasterIdentificationKey())->not($this->getIdentificationKey());
return $this->tableWrapper->getDatabase()->query($q)->success;
}
private function fitNeighbourTimespans()
{
$timeTableName = $this->tableWrapper->getTimeTableAnalyzer()->getIdentifier();
if ( ( $this->getField(TimeDependentTable::FROM_COLUMN)->isChanged()
&& $this->getField(TimeDependentTable::FROM_COLUMN)->getOriginalValue() !== NULL
&& $this[TimeDependentTable::FROM_COLUMN] !== NULL
) || $this->isNewTimespan)
{
$toCol = Element\ColumnIdentifier::create(TimeDependentTable::TO_COLUMN);
$getNearestLowerTime = new WDB\Query\Select(
array(
new Element\DBFunction('MAX', array($toCol)),
$toCol),
$timeTableName
);
$getNearestLowerTime->addCondition(Element\Compare::Lte($toCol,
new Element\Datatype\DateTime($this[TimeDependentTable::FROM_COLUMN])));
$getNearestLowerTime->filter($this->getMasterIdentificationKey());
$nearestLowerTime = $getNearestLowerTime->run($this->tableWrapper->getDatabase());
if ($nearestLowerTime->count())
{
$q = new WDB\Query\Update($timeTableName,
array(TimeDependentTable::TO_COLUMN=>$this[TimeDependentTable::FROM_COLUMN]),
Element\Compare::Equals(
Element\ColumnIdentifier::create(TimeDependentTable::TO_COLUMN),
new Element\Datatype\DateTime($nearestLowerTime->singleValue())
)
);
if (!$q->run($this->tableWrapper->getDatabase())->success) return FALSE;
}
}
if ( ( $this->getField(TimeDependentTable::TO_COLUMN)->isChanged()
&& $this->getField(TimeDependentTable::TO_COLUMN)->getOriginalValue() !== NULL
&& $this[TimeDependentTable::TO_COLUMN] !== NULL
) || $this->isNewTimespan)
{
$fromCol = Element\ColumnIdentifier::create(TimeDependentTable::FROM_COLUMN);
$getNearestGreaterTime = new WDB\Query\Select(
array(
new Element\DBFunction('MIN', array($fromCol)),
$fromCol),
$timeTableName
);
$getNearestGreaterTime->addCondition(Element\Compare::Gte($fromCol,
new Element\Datatype\DateTime($this[TimeDependentTable::TO_COLUMN])));
$getNearestGreaterTime->filter($this->getMasterIdentificationKey());
$nearestGreaterTime = $getNearestGreaterTime->run($this->tableWrapper->getDatabase());
if ($nearestGreaterTime->count())
{
$q = new WDB\Query\Update($timeTableName,
array(TimeDependentTable::FROM_COLUMN=>$this[TimeDependentTable::TO_COLUMN]),
Element\Compare::Equals(
Element\ColumnIdentifier::create(TimeDependentTable::FROM_COLUMN),
new Element\Datatype\DateTime($nearestGreaterTime->singleValue())
)
);
if (!$q->run($this->tableWrapper->getDatabase())->success) return FALSE;
}
}
return TRUE;
}
private function getSurroundingRecord()
{

if ($this[TimeDependentTable::FROM_COLUMN] === NULL || $this[TimeDependentTable::TO_COLUMN] === NULL) return NULL;
$tt = WDB\GTO\WsqlLanguage::quoteIdentifier($this->tableWrapper->getTimeTableAnalyzer()->getSchema()->getName()).'.'.
WDB\GTO\WsqlLanguage::quoteIdentifier($this->tableWrapper->getTimeTableAnalyzer()->getName());
$from = WDB\GTO\WsqlLanguage::quoteLiteral($this[TimeDependentTable::FROM_COLUMN]);
$to = WDB\GTO\WsqlLanguage::quoteLiteral($this[TimeDependentTable::TO_COLUMN]);
$fc = WDB\GTO\WsqlLanguage::quoteIdentifier(TimeDependentTable::FROM_COLUMN);
$tc = WDB\GTO\WsqlLanguage::quoteIdentifier(TimeDependentTable::TO_COLUMN);
$rec = $this->table->getAllTimesDatasource()
->filter($this->getMasterIdentificationKey())
->addCondition(WDB\GTO\WsqlLanguage::parse("($tt.$fc < $from OR $tt.$fc = NULL) AND ($tt.$tc > $to  OR $tt.$tc = NULL)", 'Condition'))
->run($this->tableWrapper->getDatabase());
if($rec->count())
{
$r = $this->tableWrapper->elevateRecords(array($rec->singleRow()));
return $r[0];
}
else
{
return NULL;
}
}
private function splitSurroundingRecord(TimeDependentRecord $surroundingRecord)
{
$originalToColumn = $surroundingRecord[TimeDependentTable::TO_COLUMN];
$surroundingRecord[TimeDependentTable::TO_COLUMN] = $this[TimeDependentTable::FROM_COLUMN];
$surroundingRecord->doNotUpdateGlobalTimeRanges = TRUE;
$surroundingRecord->validateUponSave = FALSE;
if (!$surroundingRecord->save()) return FALSE;
$surroundingRecord->makeNewTimespan($this[TimeDependentTable::TO_COLUMN], $originalToColumn);
if (!$surroundingRecord->save()) return FALSE;
return TRUE;
}
private function applyGlobalBoundary($originalValue, $newValue, $columnName, $dbfuncName)
{
if ($originalValue !== $newValue)
{
$timeTableName = $this->tableWrapper->getTimeTableAnalyzer()->getIdentifier();
$colId = Element\ColumnIdentifier::create($columnName);
$getTimeBoundary = new WDB\Query\Select(
array (
new Element\SelectField(new Element\DBFunction($dbfuncName, array($colId)), 'bnd'),
new Element\SelectField(new Element\DBFunction('SUM', array(Element\Compare::Equals($colId, new Element\Datatype\TNull()))), 'hasNull')
),
$timeTableName);
$getTimeBoundary->filter($this->getMasterIdentificationKey());
$bndTime = $getTimeBoundary->run()->singleRow();
if ($bndTime['hasNull'])
{
$lt = new Element\Datatype\TNull();
}
else
{
$lt = new Element\Datatype\DateTime($bndTime['bnd']);
}
 $q = new WDB\Query\Update($timeTableName,
array($columnName=>$newValue),
Element\Compare::Equals(
Element\ColumnIdentifier::create($columnName),
$lt
)
);
if (!$q->run($this->tableWrapper->getDatabase())->success) return FALSE;
}
return TRUE;
}
private function applyGlobalRange()
{
return $this->applyGlobalBoundary($this->fromGlobalOriginal, $this->fromGlobal, TimeDependentTable::FROM_COLUMN, 'MIN')
&& $this->applyGlobalBoundary($this->toGlobalOriginal, $this->toGlobal, TimeDependentTable::TO_COLUMN, 'MAX');
}
private function updateGlobalTimeRanges()
{
$timespanChanged = $this->isNewTimespan
|| $this->getField(TimeDependentTable::FROM_COLUMN)->isChanged()
|| $this->getField(TimeDependentTable::TO_COLUMN)->isChanged();
$globalChanged = $this->fromGlobal !== $this->fromGlobalOriginal || $this->toGlobal !== $this->toGlobalOriginal;
if ($timespanChanged || $globalChanged)
{
if (!$this->deleteCoveredTimespans()) return FALSE;
}
if ($timespanChanged)
{
if (($surroundingRecord = $this->getSurroundingRecord()) !== NULL)
{
if (!$this->splitSurroundingRecord($surroundingRecord)) return FALSE;
}
else
{
if (!$this->fitNeighbourTimespans()) return FALSE;
}
}
if ($globalChanged)
{
if(!$this->applyGlobalRange()) return FALSE;
}
return TRUE;
}

protected function keyMatchCondition()
{
return $this->keyMatchConditionTdTable();
}
/**
     * get a where condition to identify current record in a master table.
     *
     * @return \WDB\Query\Element\iCondition
     */
protected function keyMatchConditionMasterTable()
{
$ikey = array();
foreach ($this->tableWrapper->getMasterPrimaryKey() as $part)
{
$ikey[$part] = $this->fields[$part]->getOriginalValue();
}
return $this->matchConditionForKey($ikey);
}
/**
     * get a where condition to identify current record in a time dependency support table.
     *
     * @return \WDB\Query\Element\iCondition
     */
protected function keyMatchConditionTdTable()
{
return $this->matchConditionForKey(array(TimeDependentTable::ID_COLUMN=>$this[TimeDependentTable::ID_COLUMN]));
}
protected function matchConditionForKey($key)
{
$compare = array();
$columns = $this->tableWrapper->getColumns();
foreach ($key as $k=>$val)
{
$compare[] = WDB\Query\Element\Compare::Equals(
WDB\Query\Element\ColumnIdentifier::create($k), $columns[$k]->valueToDatatype($val)
);
}
return WDB\Query\Element\LogicOperator::lAnd($compare);
}
 protected function isChangedTimespan()
{
return $this->getField(TimeDependentTable::FROM_COLUMN)->isChanged()
|| $this->getField(TimeDependentTable::TO_COLUMN)->isChanged();
}
protected function isNewTimespan()
{
return $this->isNew || $this->isNewTimespan;
}
/**
     * Left boundary of this record's timespan
     *
     * @return \DateTime|NULL null means -infinity
     */
public function getFrom()
{
return $this[TimeDependentTable::FROM_COLUMN];
}
/**
     * Right boundary of this record's timespan
     *
     * @return \DateTime|NULL null means +infinity
     */
public function getTo()
{
return $this[TimeDependentTable::TO_COLUMN];
}
/**
     * Set left boundary of this record's timespan
     *
     * @param int|\DateTime|string|NULL $value
     * @return self fluent interface
     */
public function setFrom($value)
{
$this[TimeDependentTable::FROM_COLUMN] = WDB\Utils\System::createDateTime($value);
return $this;
}
/**
     * Set right boundary of this record's timespan
     *
     * @param int|\DateTime|string|NULL $value
     * @return self fluent interface
     */
public function setTo($value)
{
$this[TimeDependentTable::TO_COLUMN] = WDB\Utils\System::createDateTime($value);
return $this;
}
/**
     *
     * @return \DateTime|null
     */
public function getFromGlobal()
{
return $this->fromGlobal;
}
/**
     *
     * @return \DateTime|null
     */
public function getToGlobal()
{
return $this->toGlobal;
}
/**
     * Set global left boundary of this record's validity (first timespan's begin)
     *
     * @param int|\DateTime\string\NULL|bool $value - like setFrom, false means do not modify
     * @return self fluent interface
     */
public function setGlobalFrom($value)
{
if ($value === FALSE) $this->fromGlobal = $this->fromGlobalOriginal;
else $this->fromGlobal = WDB\Utils\System::createDateTime($value);
return $this;
}
/**
     * Set global right boundary of this record's validity (last timespan's end)
     *
     * @param int|\DateTime\string\NULL|bool $value - like setTo, false means do not modify
     * @return self fluent interface
     */
public function setGlobalTo($value)
{
if ($value === FALSE) $this->toGlobal = $this->toGlobalOriginal;
else $this->toGlobal = WDB\Utils\System::createDateTime($value);
return $this;
}
/**
     * Set both boundaries of this record's timespan
     *
     * @param int|\DateTime\string\NULL $from
     * @param int|\DateTime\string\NULL $to
     * @return self fluent interface
     */
public function setTimespan($from, $to)
{
return $this->setFrom($from)->setTo($to);
}
/**
     * Set both global boundaries of this record
     *
     * @param int|\DateTime\string\NULL $from
     * @param int|\DateTime\string\NULL $to
     * @return self fluent interface
     */
public function setGlobalTimespan($from, $to)
{
return $this->setGlobalFrom($from)->setGlobalTo($to);
}
/**
     * Specifies that this record will be saved as new timespan (not overwrite original timespan).
     *
     * You can also optionally specify timespan range with this method.
     *
     * @param int|\DateTime|bool|null $from
     *      int or DateTime: define date range, use specified date
     *      null: define date range, use NULL (-infinity)
     *      false: do not define date range (or translated to NULL if TO is defined)
     * @param int|\DateTime|bool|null $to like $from
     * @return self fluent interface
     */
public function makeNewTimespan($from = FALSE, $to = FALSE)
{
$this->isNewTimespan = TRUE;
if ($from !== FALSE) $this->setFrom ($from);
if ($to !== FALSE) $this->setTo($to);
return $this;
}
}}namespace WDB\Ajax{
use WDB,
WDB\Validation\ColumnRules;
/**
 * AJAX arguments:
 * rule     : WDB\Validation\Rules::enumeration_constant
 * value    : validated value
 * argument : optional argument passed to the validator (i.e. validating regular expression)
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnValidator implements iHandler
{
/**
     *
     * @return array
     * @throws WDB\Exception\WDBException
     */
public function handle()
{
if (!isset($_REQUEST['rule'])) throw new WDB\Exception\WDBException('missing "rule" parameter.');
if ($_REQUEST['rule'] == ColumnRules::PATTERN)
{
 return array('success'=>@preg_match($_REQUEST['argument'], $_REQUEST['value']));
}
elseif ($_REQUEST['rule'] == ColumnRules::CALLBACK)
{
return array('success'=>TRUE);  }
else
{
throw new WDB\Exception\WDBException('Invalid rule type.'.$_REQUEST['rule']);
}
}
}
}namespace WDB\Analyzer{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Column extends WDB\BaseObject implements iColumn
{

protected $name;

protected $tableAnalyzer;

protected $meta;

protected $foreign = array();

protected $datatype = 'String';

protected $annotations;
/**
     * Returns true if this column is an AUTO_INCREMENT. iColumn orders only to specify is
     *
     * @return bool
     */
public function isAutoIncrement()
{
return $this->meta->isAutoIncrement;
}
public function __construct($name, TableView $table, WDB\Structure\ColumnMetaInfo $meta)
{
$this->name = $name;
$this->tableAnalyzer = $table;
$this->meta = $meta;
$this->annotations = WDB\Annotation\Reflection::fromString($meta->comment);
$this->annotations->mergeNeonWheel($table->getSchema()->getModel(), $table->getName().'.'.$this->name);
if ($this->wrapperClass !== NULL)
{
$refl = new \ReflectionClass($this->wrapperClass);
$this->annotations->merge($refl->getDocComment());
}
}
 public function isAutoColumn()
{
return isset($this->annotations->auto) || $this->meta->isAutoColumn;
}
public function getWrapperClass()
{
if (isset($this->annotations->wrapper))
{
return WDB\Utils\System::findClass($this->annotations->last('wrapper')->text, WDB\Config::read('userWrappersClassPrefix'));
}
return NULL;
}
public function getWebUIClass()
{
$userWCP = WDB\Config::read('userWebUIClassPrefix');
if (isset($this->annotations->webui))
{
$class = $this->annotations->last('webui')->getText();
if (class_exists($userWCP.$class)) return $userWCP.$class;
if (class_exists($class)) return $class;
throw new Exception\ClassNotFound("WebUI class {$class} for column {$this->name} not found (set by db comment annotation)");
}
return NULL;
}
public function getWebUIDisplayMode($original = NULL)
{
$display = ($original === NULL ? ~0 : $original);
$this->parseModes($display, $this->table->getDisplayDefault());
$this->parseModes($display, $this->annotations->display);
return $display;
}
public function getAnnotations()
{
return $this->annotations;
}
public function getDefault()
{
return $this->meta->default;
}
public function getIdentifier()
{
return WDB\Query\Element\ColumnIdentifier::create(
$this->name,
$this->tableAnalyzer->getName(),
$this->tableAnalyzer->getSchema()->getName());
}
public function getName()
{
return $this->name;
}
public function getTitle()
{
return isset($this->annotations->title) ? $this->annotations->last('title')->getText() : $this->name;
}
public function getTable()
{
return $this->tableAnalyzer;
}
public function isNullable()
{
return $this->meta->nullable;
}
public function getForeignKeys()
{
if (!$this->tableAnalyzer instanceof Table) return array();
$keys = array();
foreach ($this->foreign as $f)
{
$keys[] = $this->tableAnalyzer->foreignKeys[$f];
}
return $keys;
}
public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
 }
/**
     * highly recommended to override in children to create datatype of the column's actual data type.
     * @param mixed
     * @return WDB\Query\Element\Datatype\iDatatype
     */
public function valueToDatatype($value)
{
return WDB\Query\Element\Datatype\AbstractType::createDatatype($value);
}
public function setFieldValue($value)
{
return $value;
}
public function getStringValue($value)
{
return $value;
}

/**
     * tells that this column is (part of) foreign key with identifier $name
     *
     * @param string foreign key identifier
     */
public function addForeignKey($name)
{
$this->foreign[] = $name;
}
/**
     * Updates display mode bitmask with configuration string in form of ([+|-]{list|detail|edit|editread|editwrite} )*
     *
     * @param int original display mode bitmask
     * @param array modes to mask
     * @return int updated display mode bitmask
     * @throws Exception\BadArgument
     */
private function parseModes(&$original, array $new)
{
foreach ($new as $modes)
{
$mode = explode(' ', $modes);
foreach($mode as $m)
{
if ($m[0] == '-')
{
$add = 0;
$mode = substr($m, 1);
}
elseif ($m[0] == '+')
{
$add = 1;
$mode = substr($m, 1);
}
else
{
$add = 1;
$mode = $m;
}
switch (strtolower($mode))
{
case 'list':
$mask = WDB\WebUI\iColumn::DISPLAY_LIST;
break;
case 'detail':
$mask = WDB\WebUI\iColumn::DISPLAY_DETAIL;
break;
case 'edit':
$mask = WDB\WebUI\iColumn::DISPLAY_EDIT;
break;
case 'editread':
$mask = WDB\WebUI\iColumn::DISPLAY_EDIT_READONLY;
break;
case 'editwrite':
$mask = WDB\WebUI\iColumn::DISPLAY_EDIT &~ WDB\WebUI\iColumn::DISPLAY_EDIT_READONLY;
break;
default:
throw new Exception\BadArgument("unknown displayMode $mode");
}
if ($add)
{
$original |= $mask;
}
else
{
$original &= ~$mask;
}
}
}
}
}}namespace WDB\Analyzer{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 */
class ColumnChar extends Column
{
 public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
parent::fetchValidationRules($rules);
if ($this->meta->charMaxLength !== NULL)
{
$rules->maxLength($this->meta->charMaxLength);
}
}
public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\String($value);
}
public function getWebUIClass()
{
return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnText');
}

/**
     * Get maximum character length.
     *
     * @return int
     */
public function getMaxLength()
{
return $this->meta->charMaxLength;
}
}
}namespace WDB\Analyzer{
use WDB;
/**
 * Decimal datatype column.
 * Note: the number range is defined by scale and precision, there is no other limitation.
 * 
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnDecimal extends Column
{
 public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\Float($value);
}
public function getWebUIClass()
{
return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnNumeric');
}
public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
parent::fetchValidationRules($rules);
$rules->float();
if (!$this->meta->nullable && !$this->meta->isAutoColumn)
{
$rules->required();
}
}
}
}namespace WDB\Analyzer{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnEnum extends Column implements WDB\iSelectOptions
{

protected $dictionary;
/**
     *
     * @param string column name
     * @param TableView table object
     * @param WDB\Structure\ColumnMetaInfo column information
     * @param array key=>value pairs of options
     */
public function __construct($name, TableView $table, WDB\Structure\ColumnMetaInfo $meta, array $dictionary)
{
parent::__construct($name, $table, $meta);
$this->dictionary = array();
foreach ($dictionary as $val)
{
$this->dictionary[$val]=$val;
}
}
/**
     * Returns true if specified key exists in options dictionary
     *
     * @param string key
     * @return bool
     */
public function keyExists($value)
{
return in_array($value, $this->dictionary);
}
 public function getOptions()
{
return $this->dictionary;
}


public function getWrapperClass()
{
$c = parent::getWrapperClass();
if ($c === NULL) $c = '\\WDB\\Wrapper\\ColumnSelect';
return $c;
}
public function getWebUIClass()
{
return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnSelect');
}
public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\Enum($value);
}
public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
parent::fetchValidationRules($rules);
if (!$this->isNullable())
{
$rules->required();
}
}
}
}namespace WDB\Analyzer{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnFloat extends Column
{
private $double;
/**
     *
     * @param string column name
     * @param TableView table object
     * @param \WDB\Structure\ColumnMetaInfo column information
     * @param bool determines if it is a double-precision float
     */
public function __construct($name, TableView $table, WDB\Structure\ColumnMetaInfo $meta, $double_precision)
{
parent::__construct($name, $table, $meta);
$this->double = (bool)$double_precision;
}
public function getDouble()
{
return $this->double;
}
 public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\Float($value);
}
public function getWebUIClass()
{
return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnNumeric');
}
public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
parent::fetchValidationRules($rules);
$rules->float();
if (!$this->meta->nullable && !$this->meta->isAutoColumn)
{
$rules->required();
}
}
}
}namespace WDB\Analyzer{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnInteger extends Column
{
private $min;
private $max;
/**
     *
     * @param string column name
     * @param TableView table object
     * @param WDB\Structure\ColumnMetaInfo column information
     * @param string[] range - $range[0] specifies minimum of the number, $range[1] maximum.
     */
public function __construct($name, TableView $table, WDB\Structure\ColumnMetaInfo $meta, $range = NULL)
{
parent::__construct($name, $table, $meta);
if ($range !== NULL)
{
$this->min = (string)$range[0];
$this->max = (string)$range[1];
if (!$meta->signed)
{
$this->max = bcadd($this->max, -$this->min);
$this->min = '0';
}
}
else
{
$this->min = $this->max = NULL;
}
}
/**
     * Return count of valid digits in number
     *
     * @return int
     */
public function getPrecision()
{
return $this->meta->precision;
}
/**
     * Return number scale. Shifts the number in database  XXX*10^<scale>
     *
     * @return int
     */
public function getScale()
{
return $this->meta->scale;
}
/**
     * Returns true if column's values are signed (can be negative)
     *
     * @return bool
     */
public function getSigned()
{
return $this->meta->signed;
}
/**
     * Returns maximum integer value determined by database datatype (not a user constraint)
     * 
     * Represented by string because database value range can be (and often is) greater than PHP integer range
     * 
     * @return string
     */
public function getMaxValue()
{
return $this->max;
}
/**
     * Returns maximum integer value determined by database datatype (not a user constraint)
     * 
     * Represented by string because database value range can be (and often is) greater than PHP integer range
     * 
     * @return string
     */
public function getMinValue()
{
return $this->min;
}
 public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\Integer($value);
}
public function getWebUIClass()
{
return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnInteger');
}
public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
parent::fetchValidationRules($rules);
$rules->integer();
if ($this->min !== NULL)
{
$rules->minValue($this->min);
}
if ($this->max !== NULL)
{
$rules->maxValue($this->max);
}
if (!$this->isNullable() && !$this->isAutoColumn())
{
$rules->required();
}
}
}
}namespace WDB\Analyzer{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnSet extends Column implements WDB\iSelectOptions
{
 public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\Set($value);
}
public function getWebUIClass()
{
$c = parent::getWrapperClass();
if ($c === NULL) $c = '\\WDB\\WebUI\\ColumnMultiChoice';
return $c;
}
public function getWrapperClass()
{
$c = parent::getWrapperClass();
if ($c === NULL) $c = '\\WDB\\Wrapper\\ColumnSelect';
return $c;
}


protected $dictionary;
/**
     *
     * @param string column name
     * @param TableView table object
     * @param WDB\Structure\ColumnMetaInfo column information
     * @param array key=>value pairs of options
     */
public function __construct($name, TableView $table, WDB\Structure\ColumnMetaInfo $meta, array $dictionary)
{
parent::__construct($name, $table, $meta);
$this->dictionary = array();
foreach ($dictionary as $val)
{
$this->dictionary[$val]=$val;
}
}
/**
     * Returns true if specified key exists in options dictionary
     *
     * @param string key
     * @return bool
     */
public function keyExists($value)
{
return in_array($value, $this->dictionary);
}
 public function getOptions()
{
return $this->dictionary;
}
}
}namespace WDB\Analyzer{
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB;
 */
abstract class ColumnTimeInfo extends Column
{
protected $stringFormat = '';
protected $_wdbType = 'DateTime';
/**
     * indicates whether this column type contains information about time.
     * @return bool
     */
abstract public function hasTime();
/**
     * indicates whether this column type contains information about date.
     * @return bool
     */
abstract public function hasDate();
protected static $datePattern = '\\s*(?:
               (?:0?[1-9]|[1-2][0-9]|3[0-1]) # 31 days
               \\s*[.-/]\\s*
               (?:1|3|5|7|8|10|12)
               |
               (?:0?[1-9]|[1-2][0-9]|30) # 30 days
               \\s*[.-/]\\s*
               (?:4|6|9|11)
               |
               (?:0?[1-9]|1[0-9]|2[0-8]) # 29 days (we will not check for existence of 29 february and also two rare history cases of 30 feb)
               \\s*[.-/]\\s*
               2)
               \\s*[.-/]\\s*
            [0-9]{4}\\s*';
protected static $timePattern = '\\s*
                            (?:[0-1]?[0-9]|2[0-3]) #hour
                            \\s*[.-/:]\\s*
                            [0-5][0-9] #minute
                            (\\s*[.-/:]\\s*[0-5][0-9])? #second (obligatory)
                            \\s*
                             ';
public function getStringValue($value)
{
return $value instanceof \DateTime ? $value->format($this->stringFormat) : (string)$value;
}
public function setFieldValue($value)
{
if ($value === NULL) return NULL;
try
{
$ret = $value instanceof \DateTime ? $value : new \DateTime($value);
$ret->_wdbType = $this->_wdbType;
return $ret;
}
catch (\Exception $ex)
{
return $value;
}
}
}}namespace WDB\Analyzer{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnTimestamp extends ColumnTimeInfo
{

private $onupd_curts;
/**
     *
     * @param string column name
     * @param TableView table object
     * @param WDB\Structure\ColumnMetaInfo meta info
     * @param bool determines if on update current_timestamp is set
     */
public function __construct($name, iTable $table, WDB\Structure\ColumnMetaInfo $meta, $onupd_curts = FALSE)
{
parent::__construct($name, $table, $meta);
$this->onupd_curts = (bool)$onupd_curts;
}
/**
     * Returns true if this column has ON UPDATE CURRENT_TIMESTAMP property (supported by i.e. MySQL, can be emulated on others)
     *
     * @return bool
     */
public function isOnUpdateCurrentTimestamp()
{
return $this->onupd_curts;
}
 public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\DateTime($value);
}
public function getWebUIClass()
{
return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnDateTime');
}

 public function hasTime() { return TRUE; }
public function hasDate() { return TRUE; }
}}namespace WDB\Analyzer{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>

 * @property-read string $name
 * @property-read iTable[] $tables
 */
class Schema extends WDB\BaseObject implements iSchema
{

protected $database;

protected $name;

protected $tables;

protected $model;
private $dbName;
/**
     *
     * @param string schema name
     * @param WDB\Database connection
     * @throws Exception\SchemaNotFound
     */
public function __construct($name, WDB\Database $database)
{
if (!$database->getDriver()->schemaExists($name))
{
throw new Exception\SchemaNotFound("Schema `$name` not found");
}
$this->name = $name;
$this->database = $database;
$this->dbName = $database->getName();
$this->model = $this->initModel();
$this->tables = $database->getDriver()->getTablesViews($this);
foreach ($this->tables as $table)
{
$table->onSchemaConstructed();
}
}
private function initModel()
{
$modelName = $this->database->getConnectionConfig()->model;
if ($modelName === FALSE) return;  $modelDir = WDB\Config::read('modeldir');
if ($modelName === NULL)
{
$lookPaths = array($modelDir.DIRECTORY_SEPARATOR.$this->name.'.nw');
}
else
{
$lookPaths = array(
$modelName,
$modelName.'.nw',
$modelDir.DIRECTORY_SEPARATOR.$modelName,
$modelDir.DIRECTORY_SEPARATOR.$modelName.'.nw',
);
}
$model = NULL;
foreach ($lookPaths as $lookPath)
{
if (file_exists($lookPath))
{
$model = \NeonWheel::decode(file_get_contents($lookPath));
break;
}
}
if ($model === NULL)
{
if ($modelName !== NULL)
{
throw new WDB\Exception\ConfigInsufficient ("Model $modelName for schema ".$this->schema->getName()." was specified but not found");
}
}
return $model;
}
 /**
     * schema name
     *
     * @return string
     */
public function getName()
{
return $this->name;
}
/**
     * array of all tables in schema
     *
     * @return iTable[]
     */
public function getTables()
{
return $this->tables;
}
public function getModel()
{
return $this->model;
}
public function getDatabase()
{
return $this->database;
}

public function __sleep ()
{
return array('name', 'tables', 'dbName');
}
public function __wakeup()
{
$this->database = WDB\Database::getInstance($this->dbName);
}
}}namespace WDB\Analyzer{
use WDB;
/**
 * WDB\Analyzer\Schema repository.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class SchemaCache extends WDB\BaseObject
{
/** @var \WDB\Database $c database connection for this analyzer */
private $c;
/** @var \WDB\Analyzer\Schema[] $schemata */
private $schemata;
public function __construct(WDB\Database $connection)
{
$this->c = $connection;
}
protected function dbident($schema)
{
return WDB\Utils\Strings::escapeWes($schema).'%'
.WDB\Utils\Strings::escapeWes($this->c->connectionConfig->host).'%'
.WDB\Utils\Strings::escapeWes($this->c->connectionConfig->user).'%'
.substr(sha1('##'.$this->c->connectionConfig->password),0,6).'%'
.$this->c->connectionConfig->port;
}
/**
     *
     * @param string $schema
     * @return Schema
     * 
     * @throws WDB\Exception\SchemaNotFound
     */
public function getSchema($schema)
{
if (!isset($this->schemata[$schema]))
{
$schemaDir = WDB\Config::read('datadir').'schemata/';
$file = $schemaDir.$this->dbident($schema).'.db';
if (file_exists($file))
{
$this->schemata[$schema] = unserialize(file_get_contents($file));
}
else
{
$this->schemata[$schema] = new WDB\Analyzer\Schema($schema, $this->c);
if (!is_dir($schemaDir))
{
mkdir($schemaDir);
}
file_put_contents($file, serialize($this->schemata[$schema]), LOCK_EX);
}
}
return $this->schemata[$schema];
}
}
}namespace WDB\Analyzer{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 */
class TableView extends WDB\BaseObject implements iTable
{
/** @var string*/
protected $name;
/** @var iSchema*/
protected $schema;
/** @var \WDB\Structure\TableColumns $tableColumns */
protected $tableColumns;
/** @var WDB\Annotation\Reflection */
protected $annotations;
/**
     *
     * @param string table name
     * @param string table comment (search for annotations)
     * @param WDB\Analyzer\Schema table schema
     * @param WDB\SQLDriver\iSQLDriver SQL driver to get table structure
     */
public function __construct($tableName, $tableComment, WDB\Analyzer\iSchema $schema, WDB\SQLDriver\iSQLDriver $driver)
{
$this->name = $tableName;
$this->schema = $schema;
$this->tableColumns = $driver->getTableColumns($this);
$this->annotations = WDB\Annotation\Reflection::fromString($tableComment);
$this->annotations->mergeNeonWheel($schema->getModel(), $this->name);
if (($c = $this->getWrapperClass()) !== NULL)
{
$refl = new \ReflectionClass($c);
$this->annotations->merge($refl->getDocComment());
}
}
public function onSchemaConstructed()
{
}
 public function getName()
{
return $this->name;
}
public function getIdentifier()
{
return new WDB\Query\Element\TableIdentifier($this->name, $this->schema->name);
}
public function getTitle()
{
return isset($this->annotations->title) ? $this->annotations->last('title')->text : $this->name;
}
public function getWrapperClass()
{
if (isset($this->annotations->wrapper))
{
return WDB\Utils\System::findClass($this->annotations->last('wrapper')->text, WDB\Config::read('userWrappersClassPrefix'));
}
return NULL;
}
public function getRecordWrapperClass()
{
if (isset($this->annotations->recordWrapper))
{
return WDB\Utils\System::findClass($this->annotations->last('recordWrapper')->text, WDB\Config::read('userWrappersClassPrefix'));
}
return NULL;
}
public function getWebUIClass()
{
if (isset($this->annotations->webUI))
{
return $this->annotations->last('webUI')->text;
}
return NULL;
}
public function getSchema()
{
return $this->schema;
}
public function getColumns()
{
return $this->tableColumns->columns;
}
public function getPrimaryKey()
{
$key = $this->tableColumns->primary;
 if (count($key) == 0)
{
$ukeys = $this->getUniqueKeys();
foreach ($ukeys as $ukey)
{
if (count($ukey) > 0)
{
 foreach ($ukey as $keyitem)
{
if ($this->tableColumns->columns[$keyitem]->isNullable())
continue;
}
$key = $ukey;
break;
}
}
 if (count($key) == 0)
{
foreach ($this->tableColumns->columns as $column)
{
$key[] = $column->getName();
}
}
}
return $key;
}
public function getUniqueKeys()
{
return $this->tableColumns->unique;
}
public function getForeignKeys()
{
return array();
}
public function fetchValidators(WDB\Event\Event $event)
{
foreach ($this->tableColumns->unique as $name=>$ukey)
{
if ($ukey != $this->tableColumns->primary)
{
foreach($ukey as &$val)
{
$val = array('recName'=>$val, 'ColumnIdentifier'=> WDB\Query\Element\ColumnIdentifier::create($val, $this->name, $this->schema->getName()));
}
unset($val);
$event->addListener(new WDB\Validation\UniqueConstraint($name, $ukey));
}
}
}
public function getDisplayDefault()
{
$a = array();
foreach ($this->annotations->displayDefault as $val)
{
$a[] = $val->getText();
}
return $a;
}
public function getAnnotations()
{
return $this->annotations;
}
public function isListedInSchemaAdmin()
{
return $this->isWebUIAccessible() && !isset($this->annotations->nodisplay);
}
public function isWebUIAccessible()
{
return !isset($this->annotations->noaccess);
}
public function isUIDetail()
{
return true;
}
public function isUIEdit()
{
return true;
}
public function isUIDelete()
{
return true;
}
}
}namespace WDB\Analyzer{
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class View extends TableView
{
public function getDisplayDefault()
{
$a = parent::getDisplayDefault();
array_unshift($a, "-edit");
return $a;
}
public function isUIEdit()
{
return false;
}
public function isUIDelete()
{
return false;
}
}
}namespace WDB\Annotation{
use WDB;
/**
 * General single annotation information (text only)
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @property-read string $text 
 */
class Data extends \WDB\BaseObject
{
protected $text = '';
public function __construct($data)
{
$this->text = $data;
}
public function getText()
{
return $this->text;
}
public function setText($value)
{
$this->text = $value;
}
public function appendLine($text)
{
$this->text .= PHP_EOL;
$this->text .= $text;
}
public function __toString()
{
return $this->text;
}
}}
namespace WDB\Annotation{
/**
 * Annotation reader for PhpDoc.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class PhpDocReflection extends Reflection
{
/**
     * Factory method - creates annotation phpdoc reflection from an object supporting getDocComment().
     *
     * @param object any (reflection) object supporting getDocComment() method
     * @return PhpDocReflection
     * 
     * @throws WDB\Exception\BadArgument
     */
public static function fromReflObject($obj)
{
return Reflection::_fromReflObject($obj, __CLASS__);
}

protected function parseTag(&$name, $data)
{
if (in_array($name, array('property', 'property-read', 'property-write')))
{
$read = ($name != 'property-write');
$write = ($name != 'property-read');
$name = 'property';
return new Property($data, $read, $write);
} else
{
return parent::parseTag($name, $data);
}
}
}}namespace WDB\Annotation{
use WDB;
/**
 * Annotation information about PHP property.
 * Parsed annotations: property, property-read, property-write
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $propName
 * @property-read string $propType
 * @property-read bool $read
 * @property-read bool $write
 */
class Property extends Data
{
protected $propname = '';
protected $proptype;
protected $read;
protected $write;
public function __construct($data, $read, $write)
{
$data = preg_split('~\\s+~', $data, 3);
$this->read = (bool) $read;
$this->write = (bool) $write;
$this->proptype = $data[0];
if (count($data) > 1)
$this->propname = substr($data[1], 1);
if (count($data) > 2)
parent::__construct($data[2]);
}
public function getPropName()
{
return $this->propname;
}
public function getPropType()
{
return $this->proptype;
}
public function getRead()
{
return $this->read;
}
public function getWrite()
{
return $this->write;
}
}}namespace WDB{
/**
 * BaseObject extension with collection behavior
 * 
 * Iterator, ArrayAccess and Countable implementation on internal array
 * or object implementing any of these interfaces.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
abstract class BaseCollection extends BaseObject implements \Iterator, \ArrayAccess, \Countable
{
const WRITE_ALL = 1;
const WRITE_NOT_UNSET = 2;
const WRITE_EXISTING = 3;
const WRITE_FORBIDDEN = 4;
/** @var array|object $contents */
protected $contents = array();
protected $writeMode = self::WRITE_ALL;
/**
     * @param string interface name
     * @throws Exception\InvalidOperation
     */
private function requireInterface($name)
{
if (!is_array($this->contents) && !$this->contents instanceof $name) throw new Exception\InvalidOperation("Provided collection does not implement $name interface");
}
 public function count()
{
$this->requireInterface('\\Countable');
return count($this->contents);
}

 public function current()
{
return $this->outValue($this->_current(), $this->key());
}
private function _current()
{
$this->requireInterface('\\Iterator');
if (is_array($this->contents))
{
return current($this->contents);
}
else
{
return $this->contents->current();
}
}
public function key()
{
$this->requireInterface('\\Iterator');
if (is_array($this->contents))
{
return key($this->contents);
}
else
{
return $this->contents->key();
}
}
public function next()
{
$val = $this->_next();
return $this->outValue($val, $this->key());
}
private function _next()
{
$this->requireInterface('\\Iterator');
if (is_array($this->contents))
{
return next($this->contents);
}
else
{
return $this->contents->next();
}
}
public function rewind()
{
$this->requireInterface('\\Iterator');
if (is_array($this->contents))
{
return reset($this->contents);
}
else
{
return $this->contents->rewind();
}
}
public function valid()
{
$this->requireInterface('\\Iterator');
if (is_array($this->contents))
{
$key = key($this->contents);
return ($key !== NULL && $key !== FALSE);
}
else
{
return $this->contents->valid();
}
}

 public function offsetExists($offset)
{
return isset($this->contents[$offset]);
}
public function offsetGet($offset)
{
return $this->outValue($this->contents[$offset], $offset);
}
public function offsetSet($offset, $value)
{
if ($this->writeMode == self::WRITE_FORBIDDEN ||
$this->writeMode == self::WRITE_EXISTING && !isset($this->contents[$offset]))
{
throw new Exception\InvalidOperation("Cannot write key $offset to the collection");
}
if ($offset === NULL)
{
$this->contents[] = $this->processValue($value, $offset);
}
else
{
$this->contents[$offset] = $this->processValue($value, $offset);
}
}
public function offsetUnset($offset)
{
if ($this->writeMode == self::WRITE_FORBIDDEN)
{
throw new Exception\InvalidOperation("Cannot unset properties from the collection");
}
if ($this->writeMode == self::WRITE_ALL)
{
unset($this->contents[$offset]);
}
else
{
$this->contents[$offset] = NULL;
}
}

/**
     * Get this object's php array.
     *
     * @return array
     */
public function toArray()
{
return $this->contents;
}
/**
     * Processes value before inserting into collection. Dedicated for overriding in child classes
     *
     * @param mixed $value
     * @param mixed $offset
     * @return mixed
     */
protected function processValue($value, $offset)
{
return $value;
}
/**
     * Processes value before returning. Dedicated for overriding in child classes
     *
     * @param mixed $value
     * @param mixed $offset
     * @return mixed
     */
protected function outValue($value, $offset)
{
return $value;
}
}
}namespace WDB\Event{
/**
 * Event listener implementation that raises specified callback method.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class CallbackListener implements iEventListener
{

protected $callback;
/**
     *
     * @param callback
     */
public function __construct($callback)
{
$this->callback = $callback;
}
public function raise()
{
call_user_func_array($this->callback, func_get_args());
}
}
}
namespace WDB\Event{
/** 
 * event listening and raising class
 * invokes all attached iEventListener objects on raise() call
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Event extends \WDB\BaseCollection
{
/**
     * Calls all attached event listeners.
     *
     * @param mixed $arg1,... arguments passed to listeners
     * @return array of results of listener calls
     */
public function raise()
{
$returned_values = array();
$arguments = func_get_args();
foreach ($this as $listener)
{
$returned_values[] = call_user_func_array(array($listener, 'raise'), $arguments);
}
return $returned_values;
}
/**
     * Attaches a listener.
     *
     * @param iEventListener
     * @return bool false if listener object is already attached (will not be attached twice)
     */
public function addListener(iEventListener $listener)
{
if (!in_array($listener, $this->contents))
{
$this[] = $listener;
return TRUE;
}
return FALSE;
}
/**
     * Detaches a listener
     *
     * @param iEventListener
     * @return true if listener was found and detached
     */
public function removeListener(iEventListener $listener)
{
$k = array_search($listener, $this->contents);
if ($k !== FALSE)
{
unset($this[$k]);
return TRUE;
}
return FALSE;
}
}}namespace WDB\GTO{
use WDB,
WDB\Exception;
/**
 * Grammar representation object
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read array $rules
 * @property-read array $parsingTable
 * @property-read array $lambdaRules
 * @property-read array $aliases
 */
class Grammar extends WDB\Structure\Structure
{
public static function create($rules, $parsingTable, $lambdaRules, $aliases)
{
return new self(array('rules'=>$rules, 'parsingTable'=>$parsingTable, 'lambdaRules'=>$lambdaRules, 'aliases'=>$aliases));
}
}}namespace WDB\GTO{
use WDB,
WDB\Exception;
/**
 * Grammar rule representation object
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $nonTerminal
 * @property-read array $rewriteTo
 */
class Rule extends WDB\Structure\Structure
{
public static function create($nonTerminal, $rewriteTo)
{
return new self (array('nonTerminal'=>$nonTerminal, 'rewriteTo'=>$rewriteTo));
}
}}namespace WDB\GTO{
use WDB,
WDB\Exception;
/**
 * @property-read string $type
 * @property-read string $content
 * 
 * @author Richard Ejem
 * @package WDB
 */
class Token extends WDB\Structure\Structure {
public static function create($type, $content)
{
return new self(array('type'=>$type, 'content'=>$content));
}
}}namespace WDB\Query{
use WDB,
WDB\Exception;
/**
 * SQL DELETE query builder
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Delete extends SUDQuery
{
/**
     * @param Element\iTableSource|string|NULL target table
     * @param Element\iCondition condition
     */
public function __construct($table = NULL, $cond = array())
{
$this->setTable($table);
if ($cond instanceof Element\iCondition)
{
$this->setWhere($cond);
}
elseif (is_array($cond))
{
$this->filter($cond);
}
else
{
throw new Exception\BadArgument("Unknown where argument type (use iCondition or array): ".print_r($where, true));
}
}
}}namespace WDB\Query\Element{
use WDB,
WDB\Exception;
/**
 * Table source with an alias.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read iTableSource $tableSource
 * @property-read string $alias
 */
class AliasedTableSource extends WDB\Structure\Structure implements iTableSource
{
/**
     * @param iTableSource|WDB\Query\Select table source
     * @param string alias
     * @throws WDB\Exception\BadArgument
     */
public static function create($tableSource, $alias)
{
if (!$tableSource instanceof WDB\Query\Select && !$tableSource instanceof iTableSource)
{
throw new Exception\BadArgument("table source must be Select or iTableSource");
}
return new self (array('tableSource'=>$tableSource, 'alias'=>$alias));
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_aliasedTableSource($this);
}
public function alias($alias)
{
return AliasedTableSource::create($this->tableSource, $alias);
}
}
}namespace WDB\Query\Element{
use WDB,
WDB\Exception;
/**
 * Database column identifier. May be fully qualified (schema.table.column), table-level qualified (table.column)
 * or simple name (column only)
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property string $schema
 * @property string $table
 * @property string $column
 */
class ColumnIdentifier extends WDB\Structure\Structure implements iExpression
{
const ALL_COLUMNS = TRUE;
/**
     * Factory method creating column identifier from string in form of schema.table.column.
     *
     *
     * @param string|ColumnIdentifier dot-separated name or already a column identifier
     * @return ColumnIdentifier
     */
public static function parse($str)
{
if ($str instanceof ColumnIdentifier) return $str;
$ids = array_reverse(explode('.', $str));
foreach ($ids as &$id)
{
$id = WDB\GTO\WsqlLanguage::unquoteIdentifier($id);
} unset($id);
return new self(array('column'=>$ids[0], 'table'=>count($ids)>1?$ids[1]:NULL, 'schema'=>count($ids)>2?$ids[2]:NULL));
}
/**
     * Creates column identifier from identifier parts.
     *
     * @param string|TRUE column name or TRUE for all columns (*)
     * @param string|NULL table name
     * @param string|NULL schema name
     * @return ColumnIdentifier
     */
public static function create($column, $table = NULL, $schema = NULL)
{
if (!is_string($column) && $column !== TRUE) throw new Exception\BadArgument('Column name must be a string or TRUE, '.gettype($column).' given.');
if (!is_string($table) && !is_null($table)) throw new Exception\BadArgument('Table name must be a string.');
if (!is_string($schema) && !is_null($schema)) throw new Exception\BadArgument('Schema name must be a string.');
return new self(array('column'=>$column, 'table'=>$table, 'schema'=>$schema));
}
    
private static $allColumns = NULL;
public static function allColumns()
{
if (self::$allColumns === NULL)
{
self::$allColumns = self::create(self::ALL_COLUMNS);
}
return self::$allColumns;
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_column($this);
}
}
}namespace WDB\Query\Element\Datatype{
use WDB;
/**
 * Encapsulation class for various database data types. Used for object query builder.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iDatatype extends WDB\Query\Element\iExpression
{
/**
     * creates the datatype object and sets its value from input php value (including NULL corresponding to database NULL value)
     * 
     * @param mixed $value
     */
public function __construct($value);
/**
     * Returns PHP native value of this object
     * 
     * @return mixed
     */
public function getValue();
/**
     * Checks whether the value is set to null
     * 
     * @return bool
     */
public function isNULL();
}}namespace WDB\Query\Element{
use WDB,
WDB\Exception;
/**
 * Database function element.
 * 
 * WDB supports two type of function elements. See constructor argument "custom" documentation for details.
 * While normal DBFunction object represent a function on WDB level, which is translated to a
 * platform-dependent function, custom function means a platform-native function, which is converted
 * directly to SQL with specified name and arguments.
 * WDB function support may slightly vary over database drivers, but there is a list of functions
 * generally supported:
 * CURRENT_SCHEMA()
 * COUNT([column_name])
 * VALUES(c)
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class DBFunction extends WDB\BaseObject implements iExpression
{

private $name;

private $args;

private $custom;
/**
     * Factory - current schema function
     *
     * @return DBFunction
     */
public static function currentSchema()
{
return new DBFunction('CURRENT_SCHEMA');
}
/**
     * Factory - VALUES(c) function for mysql ON DUPLICATE KEY UPDATE clause (emulated on other databases)
     * 
     * @param string column name
     * @return DBFunction 
     */
public static function Values($column)
{
if (!$column instanceof iExpression)
{
$column = new ColumnIdentifier(array('column'=>$column));
}
return new DBFunction('VALUES', array($column));
}
/**
     * Factory- count of records of specified columns
     * 
     * @param ColumnIdentifier|string column
     * @return DBFunction 
     * @throws Exception\BadArgument
     */
public static function countRows($column = NULL)
{
if ($column === NULL)
{
$column = ColumnIdentifier::allColumns();
}
else
{
if (!$column instanceof ColumnIdentifier)
{
if (is_string($column))
{
$column = new ColumnIdentifier(array('column'=>$column));
}
else
{
throw new Exception\BadArgument();
}
}
}
return new DBFunction('COUNT', array($column));
}
/**
     *
     * @param string function name
     * @param iExpression[] arguments
     * @param bool custom - keeping it false means that function name is under WDB
     * standard and may be translated to different function name by the database driver.
     * Setting to true tells the driver to use exactly this function name.
     * It is because there are too many database functions so there are few commons
     * translated by the driver for maximum compatibility, but custom function name
     * grants an easy way to use database-specific or less common functions.
     */
public function __construct($f, $args = array(), $custom = FALSE)
{
$this->custom = $custom;
$this->args = $args;
$this->name = $f;
}
public function toSQL(\WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_function($this->name, $this->args, $this->custom);
}
}
}namespace WDB\Query\Element{
use WDB;
/**
 * Dual table - "dual" on Oracle, none on MySQL. Table consisting of one dummy column
 * and one dummy row, useful for table-independent queries.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Dual implements iTableSource
{
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_dual();
}
public function alias($alias)
{
return AliasedTableSource::create($this, $alias);
}
}
}namespace WDB\Query\Element{
use WDB;
/**
 * Query expression binary infix operator
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ExprOperator extends InfixOperator implements iExpression
{
const PLUS = '+';
const MINUS = '-';
const TIMES = '*';
const DIVIDE = '/';
protected static $supportedOps = array(self::PLUS, self::MINUS, self::TIMES, self::DIVIDE);
/**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
public static function plus()
{
return new self(func_get_args(), self::PLUS);
}
/**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
public static function minus()
{
return new self(func_get_args(), self::MINUS);
}
/**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
public static function times()
{
return new self(func_get_args(), self::TIMES);
}
/**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
public static function divide()
{
return new self(func_get_args(), self::DIVIDE);
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_exprOp($this->expressions, $this->op);
}
}
}namespace WDB\Query\Element{
/**
 * Condition (compare or logical join of more compares, result is true or false)
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iCondition extends iElement
{
}
}namespace WDB\Query\Element{
use WDB,
WDB\Exception;
/**
 * Join USING clause
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class JoinUsing extends WDB\BaseCollection implements iElement
{
/**
     * check element type before inserting into collection.
     */
protected function processValue($value, $offset)
{
if (is_string($value))
{
$value = ColumnIdentifier::parse($value);
}
elseif (!$value instanceof ColumnIdentifier)
{
throw new Exception\BadArgument("Join USING clause can contain only column identifiers (ColumnIdentifier|string)!");
}
return $value;
}
public static function create()
{
return new self(func_get_args());
}
public function __construct()
{
foreach (WDB\Utils\Arrays::linearize(func_get_args()) as $arg)
{
$this[] = $arg;
}
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_joinUsingClause($this);
}
}
}namespace WDB\Query\Element{
use WDB;
/**
 * Query logic operator
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class LogicOperator extends InfixOperator implements iCondition
{
const L_AND = 'AND';
const L_OR = 'OR';
const L_NOT = 'NOT';
const L_XOR = 'XOR';
protected static $supportedOps = array(self::L_AND, self::L_OR, self::L_NOT, self::L_XOR);
/**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
public static function lAnd()
{
return new self(func_get_args(), self::L_AND);
}
/**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
public static function lOr()
{
return new self(func_get_args(), self::L_OR);
}
/**
     *
     * @param iExpression|array $expr1,...
     * @return self
     */
public static function lXor()
{
return new self(func_get_args(), self::L_XOR);
}
/**
     *
     * @param iExpression $expr
     * @return self
     */
public static function lNot($expr)
{
return new self(array($expr), self::L_NOT);
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_logic($this->expressions, $this->op);
}
}
}namespace WDB\Query\Element{
use WDB;
/**
 * Raw database-native query part. Not translated by driver, inserted to query as is.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class Raw implements iTableSource, iCondition
{

private $string;

public function __construct($string)
{
$this->string = $string;
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $this->string;
}
public function alias($alias)
{
return AliasedTableSource::create($this, $alias);
}
}
}namespace WDB\Query\Element{
use WDB,
WDB\Exception;
/**
 * Table identifier consiting of table name and optionally schema name.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read string|NULL $schema
 * @property-read string $table
 */
class TableIdentifier extends WDB\Structure\Structure implements iTableSource
{
/**
     * @param string table name
     * @param string|NULL schema name
     * @throws WDB\Exception\BadArgument
     */
public function __construct($table, $schema = NULL)
{
if (!is_string($table)) throw new Exception\BadArgument("table name must be string.");
if ($schema !== NULL && !is_string($schema)) throw new Exception\BadArgument("schema name must be string or NULL.");
parent::__construct(array('table'=>$table, 'schema'=>$schema));
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_table($this);
}
public function alias($alias)
{
return AliasedTableSource::create($this, $alias);
}
}
}namespace WDB\Query{
use WDB,
WDB\Exception;
/**
 * SQL INSERT query builder
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property array $columns colname=>value
 * @property Element\TableIdentifier $table
 * @property int $mode duplicity resolving mode (see class constants MODE_*)
 * @property array $ODKUcolumns update columns for ON DUPLICATE KEY UPDATE
 * clause if $mode is UPDATE
 */
class Insert extends Query
{

protected $duplicityMode;

protected $ODKUcolumns;
  
/** use plain INSERT INTO - trigger sql error on duplicating unique values; default behavior */
const UNIQUE = 1;
/** use REPLACE INTO - replace existing row when saving a new row on duplicating unique values */
const REPLACE = 2;
/** use INSERT IGNORE INTO - throw away a new row on duplicating unique values. Use with caution - can lead
     * to unexpected results on tables with multiple unique constraints */
const IGNORE = 3;
/** use ON DUPLICATE KEY UPDATE clause - update existing row on duplicating unique values. Use with caution - can lead
     * to unexpected results on tables with multiple unique constraints */
const UPDATE = 4;
/**
     *
     * @param Element\iTableSource|string target table
     * @param array column name=>value pairs
     * @param int insert mode (Insert::MODE_*)
     */
public function __construct($table = NULL, $columns = array(), $mode = self::UNIQUE)
{
if ($table !== NULL)
{
$this->setTable($table);
$this->setColumns($columns);
if ($mode == self::UPDATE)
{
$this->ODKUsameAsInsert(TRUE);
}
}
$this->duplicityMode = $mode;
}
 /**
     * Add a column name=>value part to the query.
     *
     * @param string column name
     * @param mixed value
     * @return Insert fluent interface
     */
public function addColumn($column, $value = NULL)
{
$this->columns[$column] = $value;
return $this;
}
/**
     * Get all column=>value pairs in the query.
     *
     * @return array
     */
public function getColumns()
{
return $this->columns;
}
/**
     * Set all column=>value pairs in the query.
     *
     * @param array column name=>value pairs
     * @return Insert fluent interface
     */
public function setColumns($columns)
{
$this->columns = $columns;
return $this;
}


/**
     * Get current duplicity solving mode.
     *
     * @return int (Enumeration of Insert::MODE_*)
     */
public function getMode()
{
return $this->duplicityMode;
}
/**
     * Set current duplicity solving mode.
     *
     * @param int Enumeration of Insert::MODE_*
     * @return Insert fluent interface
     */
public function setMode($mode)
{
$this->duplicityMode = min(4, max(1, $mode));
return $this;
}
/**
     * Get column=>value pairs in ON DUPLICATE KEY UPDATE clause.
     *
     * @return Element\iExpression[] column=>value pairs
     */
public function getODKUColumns()
{
return $this->ODKUcolumns;
}
/**
     * Set column=>value pairs in ON DUPLICATE KEY UPDATE clause.
     *
     * @param Element\iExpression[] column=>value pairs
     * @return Insert fluent interface
     */
public function setODKUColumns($columns)
{
$this->ODKUcolumns = $columns;
return $this;
}
/**
     * Create ODKU Columns array that assigns the values to values of the inserting row
     * i.e. ON DUPLICATE KEY UPDATE foo=VALUES(foo), bar=VALUES(bar) in MySQL dialect
     * 
     * @param bool if true, generated columns are immediately bound
     * to ODKU clause for the query
     * @return array ODKU Columns
     */
public function ODKUsameAsInsert($setNow = FALSE)
{
$odku = array();
foreach (array_keys($this->columns) as $c)
{
$odku[$c] = Element\DBFunction::Values($c);
}
if ($setNow)
{
$this->ODKUcolumns = $odku;
}
return $odku;
}
}}namespace WDB\Query{
use WDB;
/**
 * Result of raw query execution.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read Query $query
 * @property-read bool $success
 * @property-read mixed $nativeResult value returned by platform sql driver
 */
class RawResult extends WDB\Structure\Structure implements iQueryResult
{
}
}namespace WDB\Query{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property Element\SelectField[] $fields
 * @property Element\iExpression[] $group
 * @property Element\iCondition $having
 */
final class Select extends SUDQuery implements WDB\iDatasource, Element\iExpression, Element\iTableSource
{

private $fields;

private $union = NULL;

private $unionAll = TRUE;

private $group = array();

private $having = NULL;
 
private $page;

private $pageSize;

private $distinct = FALSE;
/**
     * Create the select query class.
     *
     * @param Element\SelectField[]|bool fields to select or TRUE if all columns should be selected
     * @param Element\iTableSource source table
     * @param Element\iCondition selection condition
     * @param Element\OrderRule[] ordering rules
     * @param Element\iExpression[] GROUP BY clause expressions
     * @param Element\iCondition $having HAVING condition
     * @param int maximum number of retrieved records
     * @param int start retrieving at offset
     */
public function __construct($fields, $table, $where = NULL, $order = array(), $group = array(), $having = NULL, $limit = 0, $offset = 0)
{
parent::__construct();
$this->fields = array();
$this->setFields($fields);
$this->setTable($table);
$this->setWhere($where);
if (is_array($order))
{
foreach ($order as $o)
{
$this->appendOrder($o);
}
}
else
{
$this->appendOrder($o);
}
if (is_array($group))
{
foreach ($group as $g)
{
$this->addGroup($g);
}
}
else
{
$this->addGroup($g);
}
$this->setHaving($having);
$this->setLimit($limit);
$this->setOffset($limit);
}
public function join($table, $condition = Element\Join::NATURAL, $type = Element\Join::INNER)
{
$this->table = new Element\Join($this->table, $table, $condition, $type);
return $this;
}
public function ljoin($table, $condition = Element\Join::NATURAL)
{
return $this->join($table, $condition, Element\Join::LEFT);
}
public function rjoin($table, $condition = Element\Join::NATURAL)
{
return $this->join($table, $condition, Element\Join::RIGHT);
}
public function ojoin($table, $condition = Element\Join::NATURAL)
{
return $this->join($table, $condition, Element\Join::OUTER);
}
 /**
     * Add field to select field list.
     *
     * @param Element\iExpression|Element\SelectField
     * @return Query
     */
public function addField($field)
{
if (is_string($field))
{
$field = Element\ColumnIdentifier::parse($field);
}
if ($field === Element\ColumnIdentifier::ALL_COLUMNS)
{
$field = Element\ColumnIdentifier::allColumns();
}
if ($field instanceof Element\iExpression)
{
$field = new Element\SelectField($field);
}
if (!$field instanceof Element\SelectField)
{
throw new Exception\BadArgument(__METHOD__.'accepts only Element\SelectField or Element\iExpression');
}
$this->fields[] = $field;
return $this;
}
public function removeField($name)
{
foreach ($this->fields as $key=>$field)
{
if ($field->identifier == $name)
{
unset($this->fields[$key]);
}
}
return $this;
}
public function getFields()
{
return $this->fields;
}
public function setFields($fields)
{
$this->fields = array();
if (is_array($fields))
{
foreach ($fields as $field)
{
$this->addField($field);
}
}
else
{
$this->addField($fields);
}
return $this;
}

 /**
     * Add GROUP BY expression.
     *
     * @param Element\iExpression $group
     * @return Query fluent interface
     */
public function addGroup(Element\iExpression $group)
{
$this->group[] = $group;
return $this;
}
/**
     * Get all GROUP BY expressions.
     *
     * @return Element\iExpression[]
     */
public function getGroup()
{
return $this->group;
}
/**
     * add GROUP BY condition built from key==val associative array, like filter() for WHERE condition.
     *
     * @param array $condition
     * @return Select fluent interface
     */
public function filterGroup($condition)
{
if (!is_array($condition)) $condition = func_get_args();
foreach ($condition as $val)
{
$column = Element\ColumnIdentifier::create($val);
$this->addGroup($column);
}
return $this;
}
/**
     * Set array of all GROUP BY expressions (previous expressions are rewritten).
     *
     * @param Element\iExpression[] array of expressions
     * @return Select fluent interface
     */
public function setGroup(array $fields)
{
$this->group = array();
if (is_array($fields))
{
foreach ($fields as $group)
{
$this->addGroup($group);
}
}
else
{
$this->addGroup($fields);
}
return $this;
}

 /**
     * Set HAVING condition (applied after GROUP BY and column aggregation functions)
     *
     * @param Element\iCondition|NULL having condition
     * @return Query fluent interface
     */
public function setHaving(Element\iCondition $having = NULL)
{
$this->having = $having;
return $this;
}
/**
     * Get current HAVING condition.
     *
     * @return Element\iCondition|NULL
     */
public function getHaving()
{
return $this->having;
}
public function filterHaving($condition)
{
if (!is_array($condition)) $condition = func_get_args();
foreach ($condition as $key=>$val)
{
if ($val instanceof WDB\Structure\FilterRule)
{
$column = Element\ColumnIdentifier::parse($val->column);
$val = $val->value;
}
else
{
$column = Element\ColumnIdentifier::parse($key);
}
if (!$val instanceof Element\Datatype\iDatatype)
{
$val = Element\Datatype\AbstractType::createDatatype($val);
}
$this->addHavingCondition(Element\Compare::Equals($column, $val));
}
return $this;
}
public function notHaving(array $condition)
{
foreach ($condition as $key=>$val)
{
$this->addHavingCondition(Element\Compare::NEquals(Element\ColumnIdentifier::parse($key), WDB\Query\Element\Datatype\AbstractType::createDatatype($val)));
}
return $this;
}
public function addHavingCondition(Element\iCondition $condition)
{
if ($this->having === NULL)
{
$this->having = $condition;
}
else
{
$this->having = Element\LogicOperator::lAnd($this->having, $condition);
}
return $this;
}

 /**
     * Appends an union after this query.
     *
     * Unions in WDB are built the way that you append one select query to another with this method.
     *
     * @param WDB\Query\Select select to append after this with union
     * @param bool If true, union returns all records returned in both selects (UNION ALL clause). If false,
     *  returns only distinct rows (UNION DISTINCT clause)
     */
public function setUnion(WDB\Query\Select $unionTo, $all = TRUE)
{
$this->union = $unionTo;
$this->unionAll = $all;
}
/**
     * Get current Select object bound to this one with union (or NULL if no select is bound)
     *
     * @return WDB\Query\Select|NULL
     */
public function getUnion()
{
return $this->union;
}
/**
     * Returns true if current union is configured as UNION ALL (false means DISTINCT)
     *
     * @return bool
     */
public function getUnionAll()
{
return $this->unionAll;
}
/**
     * Remove select appended to this one with union.
     */
public function removeUnion()
{
$this->union = NULL;
}

 /**
     * Return count of rows returned when executing this query.
     *
     * @param WDB\Database database connection
     * @return int
     */
public function count($database = NULL)
{
$c = clone $this;
$c->setFields(new Element\SelectField(Element\DBFunction::countRows()));
$c->limit = NULL;
$c->offset = NULL;
return $c->run($database)->singleValue();
}


/**
     * update configuration of limit and offset when page or pagesize changes
     */
protected function updatePage()
{
$this->limit = $this->pageSize;
$this->offset = $this->page * $this->pageSize;
}
public function page($number)
{
$this->page = $number;
$this->updatePage();
return $this;
}
public function getPage()
{
return $this->page;
}
public function select()
{
$fields = WDB\Utils\Arrays::linearize(func_get_args());
foreach ($this->fields as $field)
{
if ($field->identifier === NULL) continue;
if (!in_array($field->identifier, $fields))
{
$this->removeField($field->identifier);
}
else
{
unset($fields[array_search($field->identifier, $fields)]);
}
}
if (count($fields) > 0)
{
throw new Exception\BadArgument('Column(s) '.implode(', ', $fields).' not found in query');
}
return $this;
}
public function getPageSize()
{
return $this->pageSize;
}
public function pageSize($number)
{
$this->pageSize = $number;
$this->updatePage();
return $this;
}
public function sort($key, $asc = true, $prepend = true)
{
$this->prependOrder(new Element\OrderRule(
Element\ColumnIdentifier::parse($key), $asc ? Element\OrderRule::ASC : Element\OrderRule::DESC));
return $this;
}
public function subset($offset, $count)
{
$this->limit = $count;
$this->offset = $offset;
return $this;
}
public function fetch()
{
return $this->run();
}
public function distinct($distinct = TRUE)
{
$this->setDistinct($distinct);
return $this;
}

 public function getDistinct()
{
return $this->distinct;
}
public function setDistinct($distinct)
{
$this->distinct = (bool)$distinct;
}

/**
     * Perform this select on a databse.
     *
     * @param WDB\Database|NULL
     * @return iSelectResult
     * @throws Exception\InvalidOperation
     */
public function run(WDB\Database $database = NULL)
{
if (count($this->fields) == 0) throw new Exception\InvalidOperation
('cannot execute select query with no column');
return parent::run($database);
}
 public function toSQL(\WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_subselect($this);
}

public function alias($alias)
{
return AliasedTableSource::create($this, $alias);
}
}
}namespace WDB\Query{
/**
 * @property WDB\Query\iSelectResult|NULL $result
 * @property int|NULL $count
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class SelectCache extends \WDB\Structure\Structure
{
}
}namespace WDB\Query{
/**
 * Result of write query execution (Insert, Update, Delete).
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read int $affectedRows
 * @property-read int $insertId
 * @property-read Query $query
 * @property-read bool $success
 */
class WriteResult extends \WDB\Structure\Structure implements iQueryResult
{
}
}namespace WDB\SQLDriver{
use WDB,
WDB\Exception,
WDB\Query\Query,
WDB\Query\Insert,
WDB\Query\Delete,
WDB\Query\Select,
WDB\Query\Update,
WDB\Query\Element;
/**
 * Base iSQLDriver implementation for common issues.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
abstract class BaseSQL implements iSQLDriver
{
public function query(Query $query)
{
if ($query instanceof Select)
{
$q = $this->select($query);
}
elseif ($query instanceof Delete)
{
$q = $this->delete($query);
}
elseif ($query instanceof Insert)
{
$q = $this->insert($query);
}
elseif ($query instanceof Update)
{
$q = $this->update($query);
}
else
{
throw new WDB\Exception\BadArgument("unknown select class ".get_class($query));
}
return $q;
}
protected $functionChangeNames = array('CURRENT_SCHEMA'=>'DATABASE');
protected $functionPassthrough = array('COUNT','VALUES','MAX','MIN','SUM');
/**
     *
     * @param string name
     * @param iElement[] args
     * @param bool native
     * @return string
     * @throws WDB\Exception\BadArgument
     */
public function tosql_function($name, $args, $native = FALSE)
{
if (!$native)
{
 if (in_array($name, $this->functionPassthrough))
{
 }
elseif (isset($this->functionChangeNames[$name]))
{
$name = $this->functionChangeNames[$name];
}
else
{
throw new Exception\Unimplemented("function $name is not supported by current database driver: ".get_class($this));
}
}
$out = $this->escape_identifier($name).'(';
$first = TRUE;
foreach ($args as $arg)
{
if (!$arg instanceof Element\iElement)
{
throw new WDB\Exception\BadArgument("every sql function argument must be iElement, ".gettype($arg).'('.(is_object($arg) ? get_class($arg) : (string)$arg).') given');
}
if ($first)
{
$first = FALSE;
}
else
{
$out .= ', ';
}
$out .= $arg->toSQL($this);
}
$out .= ')';
return $out;
}
public function tosql_compare($expr1, $expr2, $operator)
{
if($expr2 instanceof Element\Datatype\TNull)
{
$operator = ($operator == '=') ? 'IS' : 'IS NOT';
return '('.$expr1->toSQL($this).") $operator ".$expr2->toSQL($this);
}
elseif ($expr1 instanceof Element\Datatype\TNull)
{
$operator = ($operator == '=') ? 'IS' : 'IS NOT';
return '('.$expr2->toSQL($this).") $operator ".$expr1->toSQL($this);
}
return '('.$expr1->toSQL($this).") $operator (".$expr2->toSQL($this).')';
}
public function tosql_logic($expressions, $operator)
{
$first_expr = array_shift($expressions);
if (\strtoupper($operator) == 'NOT')
{
return 'NOT ('.$first_expr->toSQL($this).')';
}
else
{
$q = '('.$first_expr->toSQL($this).')';
foreach ($expressions as $expr)
{
$q .= " $operator (".$expr->toSQL($this).')';
}
return $q;
}
}
public function tosql_exprOp($expressions, $operator)
{
$first_expr = array_shift($expressions);
$q = '('.$first_expr->toSQL($this).')';
foreach ($expressions as $expr)
{
$q .= " $operator (".$expr->toSQL($this).')';
}
return $q;
}
public function tosql_column(Element\ColumnIdentifier $identifier)
{
$r = '';
if ($identifier->schema)
{
$r .= $this->tosql_identifier($identifier->schema).'.';
}
if ($identifier->table)
{
$r .= $this->tosql_identifier($identifier->table).'.';
}
if ($identifier->column === Element\ColumnIdentifier::ALL_COLUMNS)
{
return $r.'*';
}
else
{
return $r.$this->tosql_identifier($identifier->column);
}
}
public function tosql_identifier($identifier)
{
return $this->quote_identifier($this->escape_identifier($identifier));
}
public function tosql_null()
{
return 'NULL';
}
public function tosql_string($value)
{
if ($value === NULL) return 'NULL';
return $this->quote_string($this->escape_string($value));
}
public function tosql_enum($value)
{
if ($value === NULL) return 'NULL';
return $this->tosql_string($value);
}
public function tosql_set($value)
{
if ($value === NULL) return 'NULL';
return $this->tosql_string(implode(',', $value));
     }
public function tosql_number($value)
{
if ($value === NULL) return 'NULL';
return intval($value);
}
public function tosql_table(Element\TableIdentifier $table)
{
if ($table->schema !== NULL)
{
$tbl = $this->tosql_identifier($table->schema).'.';
}
else
{
$tbl = '';
}
$tbl .= $this->tosql_identifier($table->table);
return $tbl;
}
public function tosql_tableJoin(Element\Join $join)
{
$ta = $join->getTableA()->toSQL($this);
$tb = $join->getTableB()->toSQL($this);
switch ($join->getType())
{
case Element\Join::INNER:
$clause = 'JOIN';
break;
case Element\Join::LEFT:
$clause = 'LEFT JOIN';
break;
case Element\Join::RIGHT:
$clause = 'RIGHT JOIN';
break;
case Element\Join::OUTER:
throw new \WDB\Exception\Unimplemented("This database does not support full outer join and wdb simulation is not implemented yet.");
}
if ($join->getCondition() === Element\Join::NATURAL)
{
$clause = "NATURAL $clause";
}
if ($join->getCondition() instanceof Element\iCondition)
{
$condition = ' ON '.$join->getCondition()->toSQL($this);
}
elseif ($join->getCondition() instanceof Element\JoinUsing)
{
$condition = ' USING ('.$join->getCondition()->toSQL($this).')';
}
else
{
$condition = '';
}
return sprintf('%s %s %s %s', $ta, $clause, $tb, $condition);
}
public function tosql_joinUsingClause(Element\JoinUsing $using)
{
$first = true;
$result = '';
foreach ($using as $u)
{
if ($first) $first = false;
else $result .= ', ';
$result .= $u->toSQL($this);
}
return $result;
}
public function queryString(\WDB\Query\Query $q)
{
if ($q instanceof Select)
{
$str = $this->tosql_select($q);
}
elseif ($q instanceof Delete)
{
$str = $this->tosql_delete($q);
}
elseif ($q instanceof Insert)
{
$str = $this->tosql_insert($q);
}
elseif ($q instanceof Update)
{
$str = $this->tosql_update($q);
}
else
{
throw new \WDB\Exception\BadArgument("unknown select class ".get_class($q));
}
return $str;
}
public function tosql_subselect(Select $select)
{
return '('.$this->tosql_select($select).')';
}
public function tosql_aliasedTableSource(Element\AliasedTableSource $ts)
{
if ($ts->tableSource instanceof Select)
{
return sprintf('(%s) %s', $this->tosql_select($ts->tableSource), $this->tosql_identifier($ts->alias));
}
else
{
return sprintf('%s %s', $this->tosql_tableSource($ts->tableSource), $this->tosql_identifier($ts->alias));
}
}
public function tosql_dual()
{
return 'dual';
}
/**
     *
     * @param iExpression
     * @return type
     */
protected function tosql_tableSource(Element\iTableSource $ts)
{
return $ts->toSQL($this);
}
/**
     *
     * @param Element\OrderRule[]
     * @return string
     * @throws WDB\Exception\BadArgument
     */
protected function tosql_order($rules)
{
$first = TRUE;
$q = '';
foreach ($rules as $rule)
{
$first == TRUE ? $first = FALSE : $q .= ", ";  if ($rule instanceof Element\OrderRule)
{
$q .= $rule->expression->toSQL($this);
if ($rule->descending) $q .= ' DESC';
}
else
{
throw new WDB\Exception\BadArgument("order rule list must be array of OrderRule");
}
}
return $q;
}
/**
     * Convert array to update assign list string.
     *
     * @param WDB\Analyzer\Table
     * @param array
     * @return string
     */
protected function tosql_assignList(WDB\Analyzer\Table $table = NULL, $items = array())
{
$first = TRUE;
$q = '';
foreach ($items as $column=>$value)
{
$first == TRUE ? $first = FALSE : $q .= ", ";  if ($value instanceof Element\iExpression)
{
$val = $value->toSQL($this);
}
else
{
if ($value === NULL)
{
$val = 'NULL';
}
else
{
if ($table !== NULL)
{
if (!isset($table->columns[$column])) throw new \WDB\Exception\NotFound("column $column not found in {$table->name}");
$val = $table->columns[$column]->valueToDatatype($value)->toSQL($this);
}
else
{
$val = WDB\Query\Element\Datatype\AbstractType::createDatatype($value)->toSQL($this);
}
}
}
$q .= $this->tosql_identifier($column).'='.$val;
}
return $q;
}
protected function tosql_selectFieldList($list)
{
$first = TRUE;
$q = '';
foreach ($list as $field)
{
$first == TRUE ? $first = FALSE : $q .= ", ";  if ($field instanceof Element\SelectField)
{
$q .= $field->expression->toSQL($this);
if ($field->alias) $q .= " AS ".$this->tosql_identifier($field->alias);
}
elseif ($field instanceof Element\iExpression)
{
$q .= $field->toSQL($this);
}
else
{
throw new WDB\Exception\BadArgument("field list must be array of iExpression or SelectField");
}
}
return $q;
}
protected function tosql_select(Select $select)
{
if (count($select->getFields()) == 0)
{
throw new Exception\InvalidOperation("Select query must have at least one field to select, none given");
}
$q = "SELECT ";
if ($select->getDistinct())
{
$q .= "DISTINCT ";
}
$q .= $this->tosql_selectFieldList($select->fields);
if ($select->table)
{
$q .= "\nFROM ".$this->tosql_tableSource($select->table);
}
if ($select->where)
{
$q .= "\nWHERE ".$select->where->toSQL($this);
}
if ($select->group)
{
$q .= "\nGROUP BY ".$this->tosql_selectFieldList($select->group);
}
if ($select->having)
{
$q .= "\nHAVING ".$select->having->toSQL($this);
}
if ($select->order)
{
$q .= "\nORDER BY ".$this->tosql_order($select->order);
}
if ($select->limit || $select->offset)
{
$limit = $select->limit ? $select->limit : '18446744073709551615';  $q .= "\nLIMIT ".$limit;
if ($select->offset) $q .= ' OFFSET '.$select->offset;
}
if ($select->union !== NULL)
{
$q .= "\n UNION";
if ($select->unionAll)
{
$q .= ' ALL';
}
$q .= "\n".$select->union->toSQL($this);
}
return $q;
}
protected function tosql_delete(\WDB\Query\Delete $delete)
{
$q = 'DELETE FROM ';
$q .= $delete->table->toSQL($this);
if ($delete->where)
{
$q .= "\nWHERE ".$delete->where->toSQL($this);
}
if ($delete->order)
{
$q .= "\nORDER BY ".$this->tosql_order($delete->order);
}
if ($delete->limit)
{
$q .= "\nLIMIT ".$delete->limit;
}
if ($delete->offset)
{
throw new Exception\UnsupportedByDriver("Delete command does not support offsets.");
}
return $q;
}
protected function tosql_update(\WDB\Query\Update $update)
{
$q = 'UPDATE ';
$q .= $update->table->toSQL($this);
$q .= ' SET ';
$q .= $this->tosql_assignList($update->getDatabase() ? $update->getDatabase()->getTable($update->table) : NULL, $update->columns);
if ($update->where)
{
$q .= "\nWHERE ".$update->where->toSQL($this);
}
if ($update->order)
{
$q .= "\nORDER BY ".$this->tosql_order($update->order);
}
if ($update->limit)
{
$q .= "\nLIMIT ".$update->limit;
}
return $q;
}
protected $integer_ranges = array(
'tinyint'=>array('-128', '127'),
'smallint'=>array('-32768', '32767'),
'mediumint'=>array('-8388608', '8388607'),
'int'=>array('-2147483648', '2147483647'),
'bigint'=>array('-9223372036854775808', '9223372036854775807'),
);
/**
     * Parses SQL string representing ENUM/SET identifiers into an array
     *
     * @param string $set
     * @return array
     */
protected static function parseSet($set)
{
for ($start = 1; $start <= strlen($set) && $set{$start-1} != "'"; ++$start) {}
$dictionary = array();
while ($start < strlen($set))
{
$end = $start;
do
{
if ($set{$end} == "'")
{
if ($end < strlen($set)-1 && $set{$end+1} == "'")
{
$end += 2;
}
else
{
break;
}
}
else
{
++$end;
}
} while ($end < strlen($set));
$dictionary[] = str_replace("''", "'", substr($set, $start, $end-$start));
for ($start = $end+2; $start <= strlen($set) && $set{$start-1} != "'"; ++$start) {}
}
return $dictionary;
}
protected function fkeyAction($keyword)
{
switch ($keyword)
{
case 'CASCADE':
return WDB\Structure\ForeignKeyData::CASCADE;
case 'SET NULL':
return WDB\Structure\ForeignKeyData::SETNULL;
default:
return WDB\Structure\ForeignKeyData::RESTRICT;
}
}
public function commit()
{
$this->queryRaw("COMMIT");;
}
public function rollback()
{
$this->queryRaw("ROLLBACK");
}
}}namespace WDB\SQLDriver{
use WDB;
use WDB\Analyzer;
use WDB\Query\Element;
/**
 * MySQL driver implementation
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read Analyzer\View[] $views
 * @property-read Analyzer\TableView[] $tablesViews
 */
final class MySQL extends BaseSQL implements iSQLDriver
{
/** @var mysqli $c*/
private $c;
private $lastQueryString = '';
public function changeSchema($schema)
{
if (!$this->c->select_db((string)$schema))
{
throw $this->analyzeLastError();
}
}
public function getTablesViews(WDB\Analyzer\Schema $schema)
{
$table_list = $this->c->query($q="SELECT TABLE_NAME AS name, TABLE_TYPE AS type, TABLE_COMMENT AS comment FROM `information_schema`.`TABLES` WHERE TABLE_SCHEMA = ".$this->tosql_string($schema->name));
$tables = array();
while ($table = $table_list->fetch_assoc())
{
if ($table['type'] == 'BASE TABLE')
{
$tables[$table['name']] = new Analyzer\Table($table['name'], $table['comment'], $schema, $this);
}
elseif ($table['type'] == 'VIEW')
{
$tables[$table['name']] = new Analyzer\View($table['name'], $table['comment'], $schema, $this);
}
}
return $tables;
}
public function getTableColumns(Analyzer\TableView $table)
{
$ts = $this->quote_string($this->escape_string($table->schema->getName()));
$tn = $this->quote_string($this->escape_string($table->name));
 $fk = $this->getForeignForTable($table);
 $foreign = array();
foreach ($fk as $name=>$key)
{
$cols = $refCols = array();
foreach ($key as $row)
{
$cols[] = $row['column'];
$refCols[] = $row['refColumn'];
}
$foreign[$name] = new WDB\Structure\ForeignKeyData(array(
'name'=>$name,
'schema'=>$table->schema->getName(),
'table'=>$table->name,
'columns'=>$cols,
'refSchema'=>$key[0]['refSchema'],
'refTable'=>$key[0]['refTable'],
'refColumns'=>$refCols,
'onUpdate'=>$this->fkeyAction($key[0]['onUpdate']),
'onDelete'=>$this->fkeyAction($key[0]['onDelete']),
));
}
$info = $this->c->query("SELECT * FROM information_schema.columns WHERE table_schema=$ts AND table_name=$tn ORDER BY ordinal_position");
$columns = array();
 while ($r = $info->fetch_assoc())
{
$colInfo = array(
'default'=>$r['COLUMN_DEFAULT'],
'nullable'=>($r['IS_NULLABLE'] == 'YES'),
'charMaxLength'=>$r['CHARACTER_MAXIMUM_LENGTH'],
'precision'=>$r['NUMERIC_PRECISION'],
'scale'=>$r['NUMERIC_SCALE'],
'extra'=>$r['EXTRA'],
'comment'=>$r['COLUMN_COMMENT'],
'signed'=>$this->signedType($r['COLUMN_TYPE']),
'columnType'=>$r['COLUMN_TYPE'],
'isAutoColumn'=>$r['EXTRA'] != NULL,  'isAutoIncrement'=>strtolower($r['EXTRA']) == 'auto_increment',  );
if (isset($this->integer_ranges[$r['DATA_TYPE']]))
{
$min = $this->integer_ranges[$r['DATA_TYPE']][0];
$max = $this->integer_ranges[$r['DATA_TYPE']][1];
if ($colInfo['signed'])
{
$max = bcadd($max, -$min);
$min = '0';
}
$colInfo['integerMin'] = $min;
$colInfo['integerMax'] = $max;
}
$columns[$r['COLUMN_NAME']] = $this->columnForType($r['DATA_TYPE'], $r['COLUMN_NAME'], $table, new WDB\Structure\ColumnMetaInfo($colInfo));
}
 $infokeys = $this->c->query("SHOW INDEX FROM ".$this->quote_identifier($table->schema->getName()).'.'.$this->quote_identifier($table->name));
$primary = array();
$unique = array();
while ($r = $infokeys->fetch_assoc())
{
if (!$r['Non_unique'])
{
if (!isset($unique[$r['Key_name']]))
{
$unique[$r['Key_name']] = array();
}
$unique[$r['Key_name']][] = $r['Column_name'];
}
if ($r['Key_name'] == 'PRIMARY')
{
$primary[] = $r['Column_name'];
}
}
return new WDB\Structure\TableColumns(array('columns'=>$columns, 'unique'=>$unique, 'primary'=>$primary, 'foreign'=>$foreign));
}
public function schemaExists($schema)
{
$schema = $this->c->query(
sprintf("SELECT SCHEMA_NAME FROM information_schema.schemata WHERE SCHEMA_NAME='%s'",
$this->c->escape_string($schema)));
return ($schema->num_rows > 0);
}
public function queryRaw($query, $multi = FALSE)
{
if ($multi)
{
$result = $this->c->multi_query($query);
while ($this->c->more_results()) {$this->c->next_result();}
}
else
{
$result = $this->driver_query($query);
if (!$result)
{
throw $this->analyzeLastError();
}
}
return new WDB\Query\RawResult(array('query'=>new WDB\Query\Raw($query), 'result'=>(bool)$result, 'nativeResult'=>$result));
}
public function select(WDB\Query\Select $select)
{
$q=$this->tosql_select($select);
$result = $this->driver_query($q);
if (!$result)
{
throw $this->analyzeLastError();
}
return new Result\MySQL_SelectResult($select, $result);
}
public function insert(WDB\Query\Insert $insert)
{
$q=$this->tosql_insert($insert);
$result = $this->driver_query($q);
if (!$result)
{
throw $this->analyzeLastError();
}
return new WDB\Query\WriteResult(array('affectedRows'=>$this->c->affected_rows, 'insertId'=>$this->c->insert_id, 'query'=>$insert, 'success'=>(bool)$result));
}
public function update(WDB\Query\Update $update)
{
if (count($update->getColumns()) == 0)
{
 return new WDB\Query\WriteResult(array('affectedRows'=>0, 'insertId'=>NULL, 'query'=>$update, 'success'=>true));
}
$q=$this->tosql_update($update);
$result = $this->driver_query($q);
if (!$result)
{
throw $this->analyzeLastError();
}
return new WDB\Query\WriteResult(array('affectedRows'=>$this->c->affected_rows, 'insertId'=>NULL, 'query'=>$update, 'success'=>(bool)$result));
}
public function delete(WDB\Query\Delete $delete)
{
$q=$this->tosql_delete($delete);
$result = $this->driver_query($q);
if (!$result)
{
throw $this->analyzeLastError();
}
return new WDB\Query\WriteResult(array('affectedRows'=>$this->c->affected_rows, 'insertId'=>NULL, 'query'=>$delete, 'success'=>(bool)$result));
}
public function connect(WDB\Structure\ConnectionConfig $connection)
{
$port = ($connection->port === NULL) ? $connection->port : ini_get("mysqli.default_port");
$this->c = @new \mysqli($connection->host,$connection->user,$connection->password,$connection->schema,$port);
$e = mysqli_connect_errno();
if ($e == 1049) throw new WDB\Exception\SchemaNotFound(mysqli_connect_error(), $e);
elseif ($e != 0)
{
throw new WDB\Exception\ConnectionFailed(mysqli_connect_error(), $e);
}
if ($connection->charset !== NULL) $this->setCharset($connection->charset);
}
public function disconnect()
{
$this->c->close();
}
public function getCharset()
{
return $this->c->get_charset()->charset;
}
public function setCharset($charset)
{
$this->c->set_charset($charset);
}
protected function tosql_insert(WDB\Query\Insert $insert)
{
  switch ($insert->mode)
{
case WDB\Query\Insert::IGNORE:
$q = 'INSERT IGNORE INTO ';
break;
case WDB\Query\Insert::REPLACE:
$q = 'REPLACE INTO ';
break;
case WDB\Query\Insert::UNIQUE:
case WDB\Query\Insert::UPDATE:
default:
$q = 'INSERT INTO ';
break;
}
$q .= $insert->table->toSQL($this);
$q .= ' SET ';
$q .= $this->tosql_assignList($insert->getDatabase() ? $insert->getDatabase()->getTable($insert->table) : NULL, $insert->columns);
if ($insert->mode == WDB\Query\Insert::UPDATE)
{
$q .= ' ON DUPLICATE KEY UPDATE ';
$q .= $this->tosql_assignList($insert->getDatabase() ? $insert->getDatabase()->getTable($insert->table) : NULL, $insert->ODKUcolumns);
}
return $q;
}
public function tosql_datetime(\DateTime $value = NULL, $date, $time)
{
if ($value === NULL) return $this->tosql_null();
$datePattern = 'Y-m-d';
$timePattern = 'H:i:s';
if ($date && $time)
{
$d = $value->format("$datePattern $timePattern");
}
elseif ($date)
{
$d = $value->format($datePattern);
}
elseif ($time)
{
$d = $value->format($timePattern);
}
else
{
throw new WDBException("at least one of date or time parameters of iSQLDriver::tosql_datetime() must be TRUE");
}
return $this->quote_string($d);
}
public function tosql_time(\DateInterval $value = NULL)
{
if ($value === NULL) return $this->tosql_null();
$timePattern = 'H:i:s';
return $this->quote_string($value->format($timePattern));
}
private function signedType($type)
{
return preg_match('~\\bunsigned\\b~i', $type);
}
private $foreignKeyCache = NULL;
/**
     * foreign key structure should be read by one query for whole schema because
     * this query is noticeably slow
     *
     * @param Analyzer\TableView $table
     * @return array[][][]
     */
private function getForeignForTable($table)
{
$ts = $this->escape_string($table->schema->getName());
$tn = $this->escape_string($table->name);
if ($this->foreignKeyCache === NULL)
{
$foreign = $this->c->query("SELECT
           u.table_schema AS 'schema',
           u.table_name AS 'table',
           u.constraint_name AS 'name',
           u.column_name AS 'column',
           u.referenced_table_schema AS 'refSchema',
           u.referenced_table_name AS 'refTable',
           u.referenced_column_name AS 'refColumn',
           r.update_rule AS 'onUpdate',
           r.delete_rule AS 'onDelete'
          FROM information_schema.table_constraints AS c
          INNER JOIN information_schema.key_column_usage AS u
          USING( constraint_schema, constraint_name )
          INNER JOIN information_schema.referential_constraints AS r
          USING( constraint_schema, constraint_name )
          WHERE c.constraint_type = 'FOREIGN KEY'
          ORDER BY constraint_name");
 $this->foreignKeyCache = array();
while ($row = $foreign->fetch_assoc())
{
if (!isset($this->foreignKeyCache[$row['schema']]))
$this->foreignKeyCache[$row['schema']] = array();
if (!isset($this->foreignKeyCache[$row['schema']][$row['table']]))
$this->foreignKeyCache[$row['schema']][$row['table']] = array();
if (!isset($this->foreignKeyCache[$row['schema']][$row['table']][$row['name']]))
$this->foreignKeyCache[$row['schema']][$row['table']][$row['name']] = array();
$this->foreignKeyCache[$row['schema']][$row['table']][$row['name']][] = $row;
}
}
if (isset($this->foreignKeyCache[$table->schema->getName()][$table->name]))
{
return $this->foreignKeyCache[$table->schema->getName()][$table->name];
}
else
{
return array();
}
}
private function driver_query($q)
{
$this->lastQueryString = $q;
return $this->c->query($q);
}
private function analyzeLastError()
{
 switch ($this->c->errno)
{
case 1263:
case 1048:
return new WDB\Exception\QueryNotNullColumn($this->c->error, $this->lastQueryString, WDB\Utils\Strings::pregRead('~^column \'([^\']+)\'~', $this->c->error));
case 1055:
return new WDB\Exception\QueryWrongFieldWithGroup($this->c->error, $this->lastQueryString, WDB\Utils\Strings::pregRead('~\'([^\']+)\'$~', $this->c->error));
case 1056:
return new WDB\Exception\QueryWrongGroupField($this->c->error, $this->lastQueryString, WDB\Utils\Strings::pregRead('~^\'([^\']+)\'~', $this->c->error));
case 1062:
return new WDB\Exception\QueryDuplicateKeyEntry($this->c->error, $this->lastQueryString, WDB\Utils\Strings::pregRead('~\'([^\']+)\' for key ~', $this->c->error), WDB\Utils\Strings::pregRead('~\\\'?([^ ]+?)\\\'?$~', $this->c->error));
case 1062:
case 1149:
return new WDB\Exception\QuerySyntaxError($this->c->error, $this->lastQueryString);
case 1149:
return new WDB\Exception\QueryUniqueViolation($this->c->error, $this->lastQueryString, WDB\Utils\Strings::pregRead('~\'([^\']+)\'$~', $this->c->error));
case 1216:
case 1451:
return new WDB\Exception\QueryForeignChildViolation($this->c->error, $this->lastQueryString, WDB\Utils\Strings::pregRead('~\\(([^\\)]+)\\)$~', $this->c->error));
case 1217:
case 1452:
return new WDB\Exception\QueryForeignParentViolation($this->c->error, $this->lastQueryString, WDB\Utils\Strings::pregRead('~\\(([^\\)]+)\\)$~', $this->c->error));
case 1365:
return new WDB\Exception\QueryDivisionByZero($this->c->error, $this->lastQueryString);
case 1406:
return new WDB\Exception\QueryDataTooLong($this->c->error, $this->lastQueryString, WDB\Utils\Strings::pregRead('~ for column \'([^\']+)\'~', $this->c->error), WDB\Utils\Strings::pregRead('~([0-9]+)$~', $this->c->error));
default:
return new WDB\Exception\QueryError($this->c->error, $this->lastQueryString);
}
}
protected function quote_identifier($identifier)
{
return '`'.$this->escape_identifier($identifier).'`';
}
protected function quote_string($string)
{
return "'".$string."'";
}
/**
     * Escape string literal.
     *
     * @param string
     * @return string
     */
protected function escape_string($string)
{
return $this->c->escape_string($string);
}
/**
     * Escape identifier.
     *
     * @param string
     * @return string
     */
protected function escape_identifier($identifier)
{
if (strpos($identifier, "\0") !== FALSE)
{
throw new WDB\Exception\InvalidIdentifier("MySQL identifier cannot contain a null character");
}
$identifier = str_replace("`", '``', $identifier);
return $identifier;
}
/**
     *
     * @param string $datatype
     * @param string $column_name
     * @param \WDB\Analyzer\TableView $table
     * @param \WDB\Structure\ColumnMetaInfo $meta
     * @return \WDB\Analyzer\Column
     */
protected function columnForType($datatype, $column_name, \WDB\Analyzer\TableView $table, WDB\Structure\ColumnMetaInfo $meta)
{
$datatype = strtolower($datatype);
switch ($datatype)
{
case 'char':
case 'varchar':
case 'binary':
case 'varbinary':
case 'tinytext':
case 'text':
case 'blob':
case 'mediumtext':
case 'mediumblob':
case 'longtext':
case 'longblob':
return new \WDB\Analyzer\ColumnChar($column_name, $table, $meta);
case 'tinyint':
case 'smallint':
case 'mediumint':
case 'int':
case 'bigint':
return new \WDB\Analyzer\ColumnInteger($column_name, $table, $meta, $this->integer_ranges[$datatype]);
case 'float':
case 'double':
return new \WDB\Analyzer\ColumnFloat($column_name, $table, $meta, $datatype == 'double');
case 'decimal':
return new \WDB\Analyzer\ColumnDecimal($column_name, $table, $meta);
case 'date':
return new \WDB\Analyzer\ColumnDate($column_name, $table, $meta);
case 'datetime':
return new \WDB\Analyzer\ColumnDateTime($column_name, $table, $meta);
case 'time':
return new \WDB\Analyzer\ColumnTime($column_name, $table, $meta);
case 'timestamp':
if (strtolower($meta->default) == 'current_timestamp') $meta->default = 'now';
return new \WDB\Analyzer\ColumnTimestamp($column_name, $table, $meta, preg_match('~\\bon update current_timestamp\\b~i', $meta->extra));
case 'enum':
return new \WDB\Analyzer\ColumnEnum($column_name, $table, $meta, $this->parseSet($meta->columnType));
case 'set':
return new \WDB\Analyzer\ColumnSet($column_name, $table, $meta, $this->parseSet($meta->columnType));
default:
return new \WDB\Analyzer\Column($column_name, $table, $meta);
}
}
public function startTransaction()
{
$this->queryRaw("START TRANSACTION");
}
}}namespace WDB\SQLDriver{
use WDB;
use WDB\Analyzer;
use WDB\Query\Element;
use WDB\Exception;
/**
 * Oracle SQL driver implementation
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 * @property-read Analyzer\View[] $views
 * @property-read Analyzer\TableView[] $tablesViews
 */
final class Oracle extends BaseSQL implements iSQLDriver
{
/** @var resource oracle connection*/
private $c;
private $lastQueryString = '';
private $lastStmt = null;
private $charset;
public function changeSchema($schema)
{
throw new Exception\UnsupportedByDriver("Oracle database does not support table schemas.");
}
public function schemaExists($schema)
{
if ($schema === NULL)
{
return true;
}
throw new Exception\UnsupportedByDriver("Oracle database does not support table schemas.");
}
private function getConnectionString(\WDB\Structure\ConnectionConfig $connection)
{
$str = $connection->host;
if ($connection->port !== NULL)
{
$str .= ':'.$connection->port;
}
if ($connection->ociService === NULL)
{
$str .= '/xe';
}
else
{
$str .= '/'.$connection->ociService;
}
if ($connection->ociServerType === NULL)
{
$str .= ':'.$connection->ociServerType;
}
if ($connection->ociInstanceName === NULL)
{
$str .= '/'.$connection->ociInstanceName;
}
}
public function connect(WDB\Structure\ConnectionConfig $connection)
{
$port = ($connection->port === NULL) ? $connection->port : ini_get("mysqli.default_port");
$this->c = \oci_connect($connection->user, $connection->password, $this->getConnectionString($connection), $connection->charset);
$this->charset = $connection->charset;
$this->driver_query("alter session set nls_date_format='yyyy-mm-dd hh24:mi:ss'");
$this->driver_query("alter session set nls_timestamp_format='yyyy-mm-dd hh24:mi:ss'");
}
public function disconnect()
{
\oci_close($this->c);
}
public function setCharset($charset)
{
$this->query("SET NAMES '".$this->escape_string($charset)."'");
$this->charset = $charset;
}
public function getCharset()
{
return $this->charset;
}
private function driver_query($q, &$stmt = null)
{
$stmt = oci_parse($this->c, $q);
return @oci_execute($stmt);
}
private function perform_query($q)
{
$result = $this->driver_query($q, $stmt);
$this->lastQueryString = $q;
$this->lastStmt = $stmt;
if ($result)
{
return $stmt;
}
else
{
throw $this->analyzeLastError();
}
}
private function analyzeLastError()
{
 $e = oci_error($this->lastStmt);
switch ($e['code'])
{
case 112:
case 1451:
return new WDB\Exception\QueryNotNullColumn($e['message'], $this->lastQueryString, WDB\Utils\Strings::pregRead('~^\\(([^)]+\\))$~', $e['message']));
case 979:
return new WDB\Exception\QueryWrongGroupField($e['message'], $this->lastQueryString, "");
case 933:
case 971:
case 925:  return new WDB\Exception\QuerySyntaxError($e['message'], $this->lastQueryString);
case 1:
return new WDB\Exception\QueryUniqueViolation($e['message'], $this->lastQueryString, WDB\Utils\Strings::pregRead('~\\(([^)]+)\\)~', $e['message']));
case 2291:
return new WDB\Exception\QueryForeignChildViolation($e['message'], $this->lastQueryString, WDB\Utils\Strings::pregRead('~\\(([^)]+)\\)~', $e['message']));
case 2292:
return new WDB\Exception\QueryForeignParentViolation($e['message'], $this->lastQueryString, WDB\Utils\Strings::pregRead('~\\(([^\\)]+)\\)$~', $e['message']));
case 1476:
return new WDB\Exception\QueryDivisionByZero($e['message'], $this->lastQueryString);
case 12899:
return new WDB\Exception\QueryDataTooLong($e['message'], $this->lastQueryString, WDB\Utils\Strings::pregRead('~ for column (.+)$~', $e['message']), WDB\Utils\Strings::pregRead('~([0-9]+)$~', $e['message']));
default:
return new WDB\Exception\QueryError($e['message'], $this->lastQueryString);
}
}
protected function escape_identifier($identifier)
{
if (strpos($identifier, "\0") !== FALSE || strpos($identifier, '"') !== FALSE)
{
throw new \WDB\Exception\InvalidIdentifier("Oracle identifier cannot contain a null character or double quotes");
}
return $identifier;
}
protected function quote_identifier($identifier)
{
return '"'.$this->escape_identifier($identifier).'"';
}
protected function quote_string($string)
{
return "'".$this->escape_string($string)."'";
}
protected function escape_string($string)
{
return str_replace("'", "''", $string);
}
public function queryRaw($query, $multi = FALSE)
{
if ($multi)
{
$result = true;
foreach ($this->splitMultiQuery($query) as $q)
{
if (!$this->perform_query($q))
$result = false;
}
}
else
{
$result = $this->perform_query($query);
}
return new WDB\Query\RawResult(array('query'=>new WDB\Query\Raw($query), 'result'=>(bool)$result, 'nativeResult'=>$result));
}
public function select(WDB\Query\Select $select)
{
$q=$this->tosql_select($select);
$result = $this->perform_query($q);
$s = clone $select;
return new Result\Oracle_SelectResult($select, $result);
}
public function insert(WDB\Query\Insert $insert)
{
$q=$this->tosql_insert($insert);
$result = $this->perform_query($q);
  return new WDB\Query\WriteResult(array('affectedRows'=>$result ? oci_num_rows($result) : 0, 'insertId'=>NULL, 'query'=>$insert, 'success'=>(bool)$result));
}
public function update(WDB\Query\Update $update)
{
if (count($update->getColumns()) == 0)
{
 return new WDB\Query\WriteResult(array('affectedRows'=>0, 'insertId'=>NULL, 'query'=>$update, 'success'=>true));
}
$q=$this->tosql_update($update);
$result = $this->perform_query($q);
return new WDB\Query\WriteResult(array('affectedRows'=>$result ? oci_num_rows($result) : 0, 'insertId'=>NULL, 'query'=>$update, 'success'=>(bool)$result));
}
public function delete(WDB\Query\Delete $delete)
{
$q=$this->tosql_delete($delete);
$result = $this->perform_query($q);
return new WDB\Query\WriteResult(array('affectedRows'=>$result ? oci_num_rows($result) : 0, 'insertId'=>NULL, 'query'=>$delete));
}
protected function tosql_select(\WDB\Query\Select $select)
{
$q = "SELECT ";
$sql_order = $this->tosql_order($select->order);
if ($select->limit || $select->offset)
{
if ($select->order)
{
$q .= 'ROW_NUMBER() OVER (ORDER BY '.$sql_order.') ';
}
else
{
$q .= 'ROWNUM ';
}
$q .= '"wdb___rownum___", ';
}
$q .= $this->tosql_selectFieldList($select->fields);
if ($select->table)
{
$q .= "\nFROM ".$this->tosql_tableSource($select->table);
}
if ($select->where)
{
$q .= "\nWHERE ".$select->where->toSQL($this);
}
if ($select->group)
{
$q .= "\nGROUP BY ".$this->tosql_selectFieldList($select->group);
}
if ($select->having)
{
$q .= "\nHAVING ".$select->having->toSQL($this);
}
if ($select->order)
{
$q .= "\nORDER BY ".$sql_order;
}
 if ($select->limit || $select->offset)
{
$q = "SELECT * FROM ($q) WHERE ";
if ($select->offset && $select->limit)
{
$q .= '"wdb___rownum___" BETWEEN '.$select->offset." AND ".($select->limit+$select->offset-1);
}
elseif ($select->offset)
{
$q .= '"wdb___rownum___" >= '.$select->offset;
}
else
{
$q .= '"wdb___rownum___" < '.($select->limit+$select->offset);
}
}
if ($select->union !== NULL)
{
$q .= "\n UNION";
if ($select->unionAll)
{
$q .= ' ALL';
}
$q .= "\n".$select->union->toSQL($this);
}
return $q;
}
protected function tosql_insert(WDB\Query\Insert $insert)
{
  switch ($insert->mode)
{
case WDB\Query\Insert::UNIQUE:
return $this->tosql_insert_unique($insert);
case WDB\Query\Insert::IGNORE:
throw new Exception\UnsupportedByDriver("IGNORE insert mode is not yet supported");
case WDB\Query\Insert::REPLACE:
throw new Exception\UnsupportedByDriver("REPLACE insert mode is not yet supported");
case WDB\Query\Insert::UPDATE:
default:
throw new Exception\UnsupportedByDriver("UPDATE insert mode is not yet supported");
}
}
private function tosql_insert_unique(WDB\Query\Insert $insert)
{
$q = 'INSERT INTO '.$insert->table->toSQL($this).' (';
$tables = $insert->database->getSchema()->getTables();
$table = $tables[$insert->table->table];
$first = TRUE;
$v = '';
foreach ($insert->columns as $column=>$value)
{
if ($first)
{
$first = FALSE;
}
else
{
$q .= ', ';
$v .= ', ';
}
if ($value instanceof \WDB\iExpression)
{
$val = $value->toSQL($this);
}
else
{
if ($value === NULL)
{
$val = 'NULL';
}
else
{
if (!isset($table->columns[$column])) throw new \WDB\Exception\NotFound("column $column not found in {$table->name}");
$val = $table->columns[$column]->valueToDatatype($value)->toSQL($this);
}
}
$q .= $this->tosql_identifier($column);
$v .= $val;
}
$q .= ') VALUES ('.$v.')';
return $q;
}
public function tosql_datetime(\DateTime $value = NULL, $date, $time)
{
if ($value === NULL) return $this->tosql_null();
$datePattern = 'Y-m-d';
$timePattern = 'H:i:s';
$datePatternSQL = 'yyyy-mm-dd';
$timePatternSQL = 'hh24:mi:ss';
if ($date && $time)
{
$d = $value->format("$datePattern $timePattern");
$sq = "$datePatternSQL $timePatternSQL";
}
elseif ($date)
{
$d = $value->format($datePattern);
$sq = $datePatternSQL;
}
elseif ($time)
{
$d = $value->format($timePattern);
$sq = $timePatternSQL;
}
else
{
throw new WDBException("at least one of date or time parameters of iSQLDriver::tosql_datetime() must be TRUE");
}
return "to_date('$d', '$sq')";
}
public function tosql_time(\DateInterval $value = NULL)
{
$timePattern = 'H:i:s';
$timePatternSQL = 'hh24:mi:ss';
$d = $value->format($timePattern);
$sq = $timePatternSQL;
return "to_date('$d', '$sq')";
}
private $foreignKeyCache = NULL;
public function getTablesViews(WDB\Analyzer\Schema $schema)
{
$table_list = $this->perform_query("SELECT
                uo.OBJECT_NAME AS \"name\",
                uo.OBJECT_TYPE AS \"type\",
                utc.COMMENTS AS \"comment\"
            FROM user_objects uo
            LEFT JOIN user_tab_comments utc ON uo.OBJECT_NAME=utc.TABLE_NAME
            WHERE (uo.OBJECT_TYPE = 'TABLE' OR uo.OBJECT_TYPE = 'VIEW') AND uo.STATUS != 'INVALID'");
$tables = array();
while ($table = oci_fetch_assoc($table_list))
{
if ($table['type'] == 'TABLE')
{
$tables[$table['name']] = new Analyzer\Table($table['name'], $table['comment'], $schema, $this);
}
elseif ($table['type'] == 'VIEW')
{
$tables[$table['name']] = new Analyzer\View($table['name'], $table['comment'], $schema, $this);
}
}
return $tables;
}
    
public function getTableColumns(Analyzer\TableView $table)
{
$tn = $this->quote_string($this->escape_string($table->getName()));
 $fk = $this->getForeignForTable($table);
 $foreign = array();
foreach ($fk as $name=>$key)
{
$cols = $refCols = array();
foreach ($key as $row)
{
$cols[] = $row['column'];
$refCols[] = $row['refColumn'];
}
$foreign[$name] = new WDB\Structure\ForeignKeyData(array(
'name'=>$name,
'schema'=>NULL,
'table'=>$table->name,
'columns'=>$cols,
'refSchema'=>NULL,
'refTable'=>$key[0]['refTable'],
'refColumns'=>$refCols,
'onDelete'=>$this->fkeyAction($key[0]['onDelete']),
));
}
$info = $this->perform_query("SELECT
                c.COLUMN_NAME,
                c.DATA_DEFAULT,
                c.NULLABLE,
                c.CHAR_LENGTH,
                c.DATA_PRECISION,
                c.DATA_SCALE,
                ct.COMMENTS,
                c.DATA_TYPE
            FROM user_tab_columns c
                LEFT JOIN user_col_comments ct ON c.column_name=ct.column_name AND ct.table_name=".$this->quote_string($table->getName())."
            WHERE c.table_name=".$this->quote_string($table->getName())." ORDER BY column_id");
$columns = array();
 while ($r = oci_fetch_assoc($info))
{
$colInfo = array(
'default'=>$r['DATA_DEFAULT'],
'nullable'=>($r['NULLABLE'] == 'Y'),
'charMaxLength'=>$r['CHAR_LENGTH'] == 0 ? NULL : $r['CHAR_LENGTH'],
'precision'=>$r['DATA_PRECISION'],
'scale'=>$r['DATA_SCALE'],
'extra'=>null,
'comment'=>$r['COMMENTS'],
'signed'=>true,
'columnType'=>$r['DATA_TYPE'],
'isAutoColumn'=>false,  'isAutoIncrement'=>false,  );
$columns[$r['COLUMN_NAME']] = $this->columnForType($r['DATA_TYPE'], $r['COLUMN_NAME'], $table, new WDB\Structure\ColumnMetaInfo($colInfo));
}
 $infokeys = $this->perform_query("
                SELECT
                    cons.CONSTRAINT_TYPE,
                    cons.CONSTRAINT_NAME,
                    cols.COLUMN_NAME
                FROM all_constraints cons
                  JOIN all_cons_columns cols
                    ON cons.constraint_name = cols.constraint_name
                    AND cons.owner = cols.owner
                WHERE
                  cols.table_name=".$this->quote_string($table->name)." AND cols.owner=USER
                      AND (cons.constraint_type='U' OR cons.constraint_type='P')
                ORDER BY cols.table_name, cols.position");
$primary = array();
$unique = array();
while ($r = oci_fetch_assoc($infokeys))
{
if ($r['CONSTRAINT_TYPE'] == 'U')
{
if (!isset($unique[$r['CONSTRAINT_NAME']]))
{
$unique[$r['CONSTRAINT_NAME']] = array();
}
$unique[$r['CONSTRAINT_NAME']][] = $r['COLUMN_NAME'];
}
if ($r['CONSTRAINT_TYPE'] == 'P')
{
$primary[] = $r['COLUMN_NAME'];
}
}
return new WDB\Structure\TableColumns(array('columns'=>$columns, 'unique'=>$unique, 'primary'=>$primary, 'foreign'=>$foreign));
}
/**
     * foreign key structure should be read by one query for whole schema because
     * this query is noticeably slow
     *
     * @param Analyzer\TableView $table
     * @return array[][][]
     */
private function getForeignForTable($table)
{
$this->generateForeignKeyCache();
if (isset($this->foreignKeyCache[$table->name]))
{
return $this->foreignKeyCache[$table->name];
}
else
{
return array();
}
}
private function generateForeignKeyCache()
{
if ($this->foreignKeyCache === NULL)
{
$foreign = $this->perform_query("
SELECT   uc.constraint_name AS \"name\"
,        ucc1.TABLE_NAME AS \"table\"
,        ucc1.column_name AS \"column\"
,        ucc2.TABLE_NAME AS \"refTable\"
,        ucc2.column_name AS \"refColumn\"
,        uc.delete_rule AS \"onDelete\"
FROM     user_constraints uc
JOIN     user_cons_columns ucc1 ON uc.constraint_name = ucc1.constraint_name
JOIN     user_cons_columns ucc2 ON uc.r_constraint_name = ucc2.constraint_name
                                AND ucc1.POSITION = ucc2.POSITION
WHERE    uc.constraint_type = 'R'
ORDER BY ucc1.TABLE_NAME
,        uc.constraint_name");
 $this->foreignKeyCache = array();
while ($row = oci_fetch_assoc($foreign))
{
if (!isset($this->foreignKeyCache[$row['table']]))
$this->foreignKeyCache[$row['table']] = array();
if (!isset($this->foreignKeyCache[$row['table']][$row['name']]))
$this->foreignKeyCache[$row['table']][$row['name']] = array();
$this->foreignKeyCache[$row['table']][$row['name']][] = $row;
}
}
}
protected function columnForType($datatype, $column_name, WDB\Analyzer\TableView $table, WDB\Structure\ColumnMetaInfo $meta)
{
$datatype = strtolower($datatype);
$datatype = preg_replace('~\(.*\)$~', '', $datatype);
switch ($datatype)
{
case 'char':
case 'nchar':
case 'varchar':
case 'varchar2':
case 'nvarchar2':
case 'varbinary':
case 'text':
case 'blob':
case 'clob':
  return new WDB\Analyzer\ColumnChar($column_name, $table, $meta->copy(array('nullable'=>FALSE)));
case 'number':
if ($meta->scale > 0)
{
return new WDB\Analyzer\ColumnDecimal($column_name, $table, $meta);
}
else
{
return new WDB\Analyzer\ColumnInteger($column_name, $table, $meta, NULL);
}
case 'float':
return new WDB\Analyzer\ColumnFloat($column_name, $table, $meta, $datatype == 'double');
case 'date':
if (strtolower(trim($meta->default)) == 'sysdate') $meta->default = 'now';
return new WDB\Analyzer\ColumnDateTime($column_name, $table, $meta);
case 'time':
if (strtolower(trim($meta->default)) == 'sysdate') $meta->default = 'now';
return new WDB\Analyzer\ColumnTime($column_name, $table, $meta);
case 'timestamp':
if (strtolower(trim($meta->default)) == 'sysdate') $meta->default = 'now';
return new WDB\Analyzer\ColumnTimestamp($column_name, $table, $meta, preg_match('~\\bon update current_timestamp\\b~i', $meta->extra));
default:
return new WDB\Analyzer\Column($column_name, $table, $meta);
}
}
private function splitMultiQuery($q)
{
$result = array();
$offset = 0;
  $q = preg_replace('~((?:[^;\"\']|\\"(?:[^"\\\\]|\\.)\\"|\\\'(?:[^\'\\\\]|\\.)\\\')*?)--.*(?:\n|$)~', '\\1', $q);
 $q_orig = $q;
$q = '';
while(preg_match('~((?:[^;\"\']|\\"(?:[^"\\\\]|\\.)\\"|\\\'(?:[^\'\\\\]|\\.)\\\')*?)/\\*.*?\\*/(.*)$~sm', $q_orig, $m))
{
$q .= $m[1].' ';
$q_orig = $m[2];
}
$q .= $q_orig;
  
while (strlen($q) > 0 && preg_match('~^(?<cmd>
                (?: [^;\"\'/b]                        #any character outside string value except of ; / and b
                    |
                    (?<!\\s)b                         #b not being the keyword begin case 1
                    |
                    b(?!egin\\s*)                     #b not being the keyword begin case 2
                    |
                    (["\']).*?(?<!\\\\)(\\\\\\\\)*\2  #string value
                    |
                    (?<!\n)\\s*/\\s*(?!\r?\n)         #slash not on separate line
                    |
                    begin (?: [^/]*                   #any character except of /
                        |
                        (?<!\n)\\s*/\\s*(?!\r?\n)     #slash not on separate line
                    )

                )*
            )(?<tail>.*)$~xsmi', $q, $matches, NULL))
{
if (!preg_match('~^\\s*$~', $matches['cmd']))
{
$result[] = $matches['cmd'];
}
$q = preg_replace('~^[\\s;/]*~', '', $matches['tail']);
}
return $result;
}
public function startTransaction()
{
$this->queryRaw("START TRANSACTION");
}
}}namespace WDB\Structure{
/**
 * table column meta information
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property mixed $default default value
 * @property-read bool $nullable can have null value
 * @property-read bool $isAutoColumn set to true by db driver if the column is
 * filled automatically / so it should not be edited manually by user
 * @property-read bool $isAutoIncrement true if the column is mysql-like auto increment column
 * or emulated behaviour on other platforms like Oracle
 * @property-read string $comment textual comment of the column (may contain additional meta information)
 * @property-read int $charMaxLength maximum length in case of character data
 * @property-read string $integerMin
 * @property-read string $integerMax
 * @property-read int $precision integer precision (number of valid digits)
 * @property-read int $scale integer scale (number of decimal places in case of decimal datatype)
 * @property-read bool $signed true when column type is a signed integer (not defined when it is not an integer)
 * @property-read string $extra extra database configuration
 * @property-read string $columnType full database data type
 */
class ColumnMetaInfo extends Structure{}
}namespace WDB\Structure{
/**
 * database connection information
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $driver
 * @property-read string $host
 * @property-read string $user
 * @property-read string $password
 * @property-read string $schema
 * @property-read string $charset
 * @property-read int $port
 * @property-read int $loglevel
 * @property-read string|bool|null $model - string: model name; false: do not use model; null: same as schema name if present
 * 
 * @property-read string $ociService oracle specific property
 * @property-read string $ociServerType oracle 11 specific property
 * @property-read string $ociInstanceName oracle 11 specific property
 */
class ConnectionConfig extends Structure
{
/**
     *
     * @param string driver name
     * @param string dbserver hostname
     * @param string user name
     * @param string user password
     * @param string schema name
     * @param string|NULL|bool model name; NULL for same as schema name (if found), FALSE for not using model file
     * @param string charset identifier
     * @param int port
     * @param int loglevel - enumeration of WDB\Query\Log::LOG_*
     * @return ConnectionConfig 
     */
public static function create($driver, $host, $user = NULL, $password = NULL, $schema = NULL, $model = NULL, $charset = NULL, $port = NULL, $loglevel = NULL)
{
return new ConnectionConfig(array(
'driver'=>$driver,
'host'=>$host,
'user'=>$user,
'password'=>$password,
'schema'=>$schema,
'model'=>$model,
'charset'=>$charset,
'port'=>$port,
'loglevel'=>$loglevel));
}
}
}namespace WDB\Structure{
use WDB;
/**
 * datasource filter rule.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property ColumnIdentifier $column
 * @property-read mixed $value
 */
class FilterRule extends Structure{
public static function create(WDB\Query\Element\ColumnIdentifier $column, $value)
{
return new self(array('column'=>$column, 'value'=>$value));
}
}
}namespace WDB\Structure{
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read string $name
 * @property-read string $schema
 * @property-read string $table
 * @property-read string[] $columns
 * @property-read string $refSchema
 * @property-read string $refTable
 * @property-read string[] $refColumns
 * @property-read int $onUpdate
 * @property-read int $onDelete
 */
class ForeignKeyData extends Structure
{
const CASCADE = 1;
const SETNULL = 2;
const RESTRICT = 3;
}
}namespace WDB\Utils{
use WDB;
/**
 * Object delegate to pass array by reference. Original php array can be changed through this class's object.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ArrayDelegate extends WDB\BaseCollection
{
public function __construct(array &$source)
{
$this->contents = &$source;
}
}}namespace WDB\Utils{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class OrderedIterator extends WDB\BaseCollection
{
protected $order;
/**
     *
     * @param array values
     * @param array of indexes of the first array in the desired order
     */
public function __construct($array, $order)
{
$this->contents = $array;
$this->order = $order;
}
public function current()
{
return $this->contents[current($this->order)];
}
public function key()
{
return current($this->order);
}
public function next()
{
$k = next($this->order);
if ($this->valid())
{
return $this->contents[$k];
}
else
{
return FALSE;
}
}
public function rewind()
{
reset($this->order);
}
public function valid()
{
$key = key($this->order);
return ($key !== NULL && $key !== FALSE);
}
}
}namespace WDB\Validation{
use WDB,
WDB\Exception;
/**
 * Column validation rules collection.
 * 
 * Holds validation information for ColumnValidator for one column.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read int $type
 * @property-read string $message
 * @property-read mixed $argument
 */
class ColumnRules extends WDB\BaseCollection
{
/** required field; cannot be empty string or NULL, depending on datatype */
const REQUIRED = 1;
/** value must be valid integer */
const INTEGER = 2;
/** value must be valid floating-point number*/
const FLOAT = 3;
/** string value must have a specified minimal character count*/
const MINLENGTH = 4;
/** string value must have a specified maximal character count*/
const MAXLENGTH = 5;
/** numeric value must be equal or greater than specified value*/
const MINVALUE = 6;
/** numeric value must be equal or lower than specified value*/
const MAXVALUE = 7;
/** string value must match specified regular expression (PCRE format) pattern*/
const PATTERN = 8;
/** value of the column must be equal to value of another column*/
const EQUALTO = 9;
/** string value must match built-in e-mail regular expression*/
const EMAIL = -1;
/** string value must match built-in url regular expression*/
const URL = -2;
/** value is validated by a specified callback function*/
const CALLBACK = 10;

protected $columnTitle;
public function __construct($columnTitle)
{
$this->columnTitle = $columnTitle;
}
/**
     * Sets minimal string length.
     *
     * @param int
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function minLength($value, $message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::MINLENGTH, $message, 'minlength', $value);
}
/**
     * Sets maximal string length.
     *
     * @param int
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function maxLength($value, $message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::MAXLENGTH, $message, 'maxlength', $value);
}
/**
     * Sets minimal numeric value
     *
     * @param int
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function minValue($value, $message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::MINVALUE, $message, 'minvalue', $value);
}
/**
     * Sets maximal numeric value
     *
     * @param int
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function maxValue($value, $message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::MAXVALUE, $message, 'maxvalue', $value);
}
/**
     * Sets that this field is required
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function required($message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::REQUIRED, $message, 'required', NULL);
}
/**
     * Sets that this field must be a valid integer
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function integer($message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::INTEGER, $message, 'integer', NULL);
}
/**
     * Sets that this field must be a valid float number
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function float($message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::FLOAT, $message, 'float', NULL);
}
/**
     * Sets that this field must be equal to another column's field
     *
     * @param string column name to compare
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function equals($columnName, $message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::EQUALTO, $message, 'equals', $columnName);
}
/**
     * Sets a callback which will validate this field
     *
     * @param callback
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function callback($callback, $message = NULL)
{
if (!$rule['argument'] instanceof \WDB\Utils\Caller && !is_callable($rule['argument']))
{
throw new Exception\BadArgument("Validation rule with custom callback need the callback to be an instance of \WDB\Utils\Caller");
}
return $this->set(WDB\Validation\ColumnRules::CALLBACK, $message, 'callback', $callback);
}
/**
     * Sets a pcre pattern this field will be validated by.
     *
     * @param string pcre regular expression pattern
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function pattern($pcre_regex, $message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::PATTERN, $message, 'pattern', $pcre_regex);
}
/**
     * Sets that this field must be a valid e-mail address.
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function email($message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::EMAIL, $message, 'email', NULL);
}
/**
     * Sets that this field must be a valid URL.
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function url($message = NULL)
{
return $this->set(WDB\Validation\ColumnRules::URL, $message, 'url', NULL);
}
/**
     * Sets that this field is not required (can be empty or NULL)
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function notRequired()
{
$this->remove(WDB\Validation\ColumnRules::REQUIRED);
}
/**
     * Resets that this field don't have to be integer
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function noInteger()
{
$this->remove(WDB\Validation\ColumnRules::INTEGER);
}
/**
     * Resets that this field don't have to be float
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function noFloat()
{
$this->remove(WDB\Validation\ColumnRules::FLOAT);
}
/**
     * Resets this field's minimal string length
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function noMinLength()
{
$this->remove(WDB\Validation\ColumnRules::MINLENGTH);
}
/**
     * Resets this field's maximal string length
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function noMaxLength()
{
$this->remove(WDB\Validation\ColumnRules::MAXLENGTH);
}
/**
     * Resets this field's minimal numeric value
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function noMinValue()
{
$this->remove(WDB\Validation\ColumnRules::MINVALUE);
}
/**
     * Resets this field's maximal numeric value
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function noMaxValue()
{
$this->remove(WDB\Validation\ColumnRules::MAXVALUE);
}
/**
     * Resets this field's pcre validator
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function noPattern()
{
$this->remove(WDB\Validation\ColumnRules::PATTERN);
}
/**
     * Resets this field's callback validator
     *
     * @param string error message
     * @return ColumnRules fluent interface
     */
public function noCallback()
{
$this->remove(WDB\Validation\ColumnRules::CALLBACK);
}
/**
     *
     * @param int $type rule constant Rule::* or instantiated Rule class
     * @param string $message error message on violated rule
     * @param string $defaultMessage language identifier of string representing default error message
     *  in lang namespace validator.messages
     * @param mixed $argument rule argument (max length etc)
     * @return self
     */
protected function set($type, $message, $defaultMessage, $argument)
{
if ($type < 0)
{
switch ($type)
{
case -1:
$type = 8;
$argument = '~^.*@.*\\..*~';
break;
case -2:
$type = 8;
$argument = '~^([a-z][a-z0-9.+-]*)?://(.*@)?[a-z0-9-]+~';
break;
}
}
if ($message === NULL)
{
$message = WDB\Lang::s('validator.messages.'.$defaultMessage);
}
$this[$type] = array('message'=>$this->parseMessage($message, $argument), 'argument'=>$argument);
return $this;
}
protected function parseMessage($msg, $argument)
{
return str_replace(array('%name', '%arg'),
array($this->columnTitle, $argument),
$msg);
}
protected function remove($type)
{
unset($this[$type]);
return $this;
}
}}namespace WDB\Validation{
use WDB,
WDB\Exception;
/**
 * @property-read ColumnValidationError[] $errors
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
final class ColumnValidator implements iValidator
{

private $errors;
 /**
     * Validate passed record.
     *
     * @param WDB\Wrapper\Record $record
     * @return bool
     * @throws WDB\Exception\BadArgument
     */
public function raise(WDB\Wrapper\iRecord $record = NULL)
{
if ($record === NULL) throw new Exception\BadArgument("Validator event raise() method needs"
. " WDB\Wrapper\iRecord as a raise argument");
$this->errors = array();
foreach ($record->getTable()->getColumns() as $column)
{
$this->validateColumn($column, $record);
}
$record->addValidationErrors($this->errors);
return $this->success();
}

/**
     * Returns true if last validation was successful.
     *
     * @return bool
     */
public function success()
{
return count($this->errors) == 0;
}
 /**
     * Returns list of validation errors occured
     *
     * @return array
     */
public function getErrors()
{
return $this->errors;
}
 public function fetchValidatorData(WDB\Wrapper\iRecord $record, &$data)
{
$columns = $record->getTable()->getColumns();
foreach ($data['columns'] as $key=>&$c)
{
if (isset($columns[$key]) && $columns[$key]->getRules() !== NULL)
{
$c['ColumnValidator'] = $columns[$key]->getRules()->toArray();
}
}
}

/**
     * If condition is false, adds a message to this->errors based on currently validated column and rule.
     *
     * @param bool $condition
     */
private function assert(WDB\Wrapper\iColumn $column, $rule, WDB\Wrapper\Field $field, $condition)
{
if (!$condition)
{
$rules = $column->getRules();
$err = $rules[$rule]['message'];
$this->errors[] = array('message'=>$err, 'column'=>$column);
$field->addValidationError($err);
}
}
/**
     * Validates current column by class private variables (column, field).
     *
     * @throws WDB\Exception\BadArgument
     */
private function validateColumn(WDB\Wrapper\iColumn $column, WDB\Wrapper\iRecord $record)
{
$value = $record[$column->getName()];
$columns = $record->getTable()->getColumns();
$c = $column;
$f = $record->getField($column->getName());
 $isEmpty = (($value === NULL || $value === '') && empty($column->rules[ColumnRules::REQUIRED]));
foreach ($column->rules as $id=>$rule)
{
$r = $id;
$special = FALSE;
if (is_array($value) || is_object($value))
{
$special = TRUE;
if (!in_array($id, array(ColumnRules::REQUIRED, ColumnRules::EQUALTO, ColumnRules::CALLBACK))) continue;
}
switch ($id)
{
case ColumnRules::REQUIRED:
$this->assert($c,$r,$f,($value !== '' && $value !== NULL && $value !== FALSE && $value !== array()));
break;
case ColumnRules::INTEGER:
$this->assert($c,$r,$f,preg_match('~[+-]?[0-9]+~', trim($value)));
break;
case ColumnRules::FLOAT:
$this->assert($c,$r,$f,is_numeric(trim($value)));
break;
case ColumnRules::MINLENGTH:
$this->assert($c,$r,$f,strlen($value) >= $rule['argument']);
break;
case ColumnRules::MAXLENGTH:
$this->assert($c,$r,$f,strlen($value) <= $rule['argument']);
break;
case ColumnRules::MINVALUE:
$this->assert($c,$r,$f,is_numeric($value) && bccomp($value, $rule['argument']) >= 0);
break;
case ColumnRules::MAXVALUE:
$this->assert($c,$r,$f,is_numeric($value) && bccomp($value, $rule['argument']) <= 0);
break;
case ColumnRules::PATTERN:
$this->assert($c,$r,$f,$isEmpty || preg_match($rule['argument'], $value));
break;
case ColumnRules::EQUALTO:
if (!isset($record[$rule['argument']]))
{
throw new Exception\BadArgument("field {$rule['argument']} tested for equality with {$column->getName()} was not found");
}
$this->assert($c,$r,$f,$value == $record[$rule['argument']]);
break;
case ColumnRules::CALLBACK:
if (!$rule['argument'] instanceof WDB\Utils\Caller && !is_callable($rule['argument']))
{
throw new Exception\BadArgument("Validation rule with custom callback need the callback to be an instance of \WDB\Utils\Caller");
}
if ($rule['argument'] instanceof WDB\Utils\Caller)
{
$rule['argument']->arguments['value'] = $value;
$this->assert($c,$r,$f,$isEmpty || $rule['argument']($value));
}
else
{
$this->assert($c,$r,$f,$isEmpty || call_user_func($rule['argument'], $value));
}
break;
default:
throw new Exception\BadArgument("unknown validation rule #$id");
}
}
}
}
}namespace WDB\WebUI{
use WDB;
/**
  * Base iColumn implementation.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
abstract class Column extends WDB\BaseObject implements iColumn, iOrderable, iFilterable
{

protected $columnWrapper;

protected $table;

protected $display;
/**
     *
     * @param WDB\Wrapper\iColumn column wrapper
     * @param iTable table webui
     */
public function __construct(WDB\Wrapper\iColumn $columnWrapper, iTable $tableWebUI)
{
$this->columnWrapper = $columnWrapper;
$this->table = $tableWebUI;
if ($columnWrapper->isAutoColumn())
{
$this->setDisplayModes(iColumn::DISPLAY_AUTOCOLUMN_DEFAULT);
}
else
{
$this->setDisplayModes($this->getDefaultDisplayModes());
}
$this->setDisplayModes($columnWrapper->getWebUIDisplayMode($this->getDisplayModes()));
}
protected function getDefaultDisplayModes()
{
return ~0;
}
public function getName()
{
return $this->columnWrapper->name;
}
public function getTitle()
{
return $this->columnWrapper->title;
}
public function getTitleHTML()
{
return htmlspecialchars($this->columnWrapper->title);
}
public function getTable()
{
return $this->table;
}
public function addDisplayMode($mode)
{
$this->display |= $mode;
}
public function removeDisplayMode($mode)
{
$this->display &= ~$mode;
}
public function getDisplayModes()
{
return $this->display;
}
public function setDisplayModes($mode)
{
$this->display = (int)$mode;
}
public function isDisplayedList()
{
return $this->isDisplayed(iColumn::DISPLAY_LIST);
}
public function isDisplayedEdit()
{
return $this->isDisplayed(iColumn::DISPLAY_EDIT);
}
public function isDisplayedEditRead()
{
return $this->isDisplayed(iColumn::DISPLAY_EDIT_READONLY);
}
public function isDisplayedDetail()
{
return $this->isDisplayed(iColumn::DISPLAY_DETAIL);
}
public function isDisplayed($mode)
{
return ($this->display & $mode) == $mode;
}
public function isNullable()
{
return $this->columnWrapper->isNullable();
}
public function isDefaultInitializable()
{
return ($this->columnWrapper->isAutoColumn() || $this->columnWrapper->getDefault() !== NULL || $this->columnWrapper->isNullable());
}
public function getRules()
{
return $this->columnWrapper->rules;
}
public function parseInputValue(WDB\Wrapper\iRecord $record, $value)
{
if ($this->isNullable())
{
if (!empty($value['notnull']))
{
$v = $this->fetchInputValue(isset($value['value']) ? $value['value'] : '');
}
else
{
$v = NULL;
}
}
else
{
$v = $this->fetchInputValue(isset($value['value']) ? $value['value'] : '');
}
$record->setFieldValue($this->getName(), $v);
}
/**
     * can be called by descendants' parseInputValue() when they have different or no way to express NULL
     * 
     * @param WDB\Wrapper\iRecord $record
     */
protected function parseInputValueNoNullable(WDB\Wrapper\iRecord $record, $value)
{
$record->setFieldValue($this->getName(), $this->fetchInputValue(isset($value['value']) ? $value['value'] : ''));
}
/**
     * Translates string value read from request to a valid value of record wrapper's field.
     *
     * @param string value
     * @return mixed
     */
protected function fetchInputValue($v)
{
return $v;
}
public function getSimpleHTML(WDB\Wrapper\iRecord $record)
{
return $this->getDetailedHTML($record);
}
public function getEditingHTML_nullator(WDB\Wrapper\iRecord $record)
{
if (!$this->isNullable()) return '';
$input_name = $this->table->getRequest()->getSaveFieldName($this->name);
$value = $record[$this->name];
return '<input
            type="checkbox"
            name="'.htmlspecialchars($input_name).'[notnull]"
            value="1" '
.($record[$this->name] !== NULL ? 'checked="checked"' : '').
' onclick="
                var inp = $(\'input[name=&quot;'.str_replace(array('[', ']'), array('\\[', '\\]'), htmlspecialchars($input_name)).'\[value\]&quot;]\');
                if (this.checked) inp.show();else inp.hide();
            "/>';
}
protected function getInputHTMLNode(WDB\Wrapper\iRecord $record)
{
$input_name = $this->table->getRequest()->getSaveFieldName($this->name);
$input = new HTMLNode('input');
$input['type'] = 'text';
$val = $record[$this->name];
$input['name'] = $input_name.'[value]';
if ($this->isNullable())
{
if ($val === NULL)
{
$input['data-wdbCommand'] = 'hide';
}
}
$input['value'] = $val;
return $input;
}
public function getEditingHTML(WDB\Wrapper\iRecord $record)
{
return $this->getInputHTMLNode($record)->__toString();
}
public function getHTMLEditStatus(WDB\Wrapper\iRecord $record)
{
return implode('<br>', $record->getField($this->name)->getValidationErrors());
}
public function getDetailedHTML(WDB\Wrapper\iRecord $record)
{
return htmlspecialchars($record[$this->name]);
}
public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
{
if (count($record->getField($this->name)->getValidationErrors()) > 0)
{
return array('invalid');
}
return array();
}
public function isOrderable()
{
return $this->columnWrapper instanceof WDB\Wrapper\iOrderable && $this->columnWrapper->isOrderable();
}
public function isFilterable()
{
return $this->columnWrapper instanceof WDB\Wrapper\iFilterable && $this->columnWrapper->isFilterable();
}
public function isToggledOrder($asc = TRUE)
{
return $this->table->getRequest()->getOrderColumn() == $this->name
&& $this->table->getRequest()->isOrderAsc() == $asc;
}
public function isToggledFilter()
{
$f = $this->table->getRequest()->getFilters();
return isset($f[$this->name]);
}
public function getFilterOptions()
{
return $this->columnWrapper->getFilterOptions();
}
public function getFilteredValue()
{
$f = $this->table->getRequest()->getFilters();
if (isset($this->filterOptions[$f[$this->name]]))
{
return $this->getFilterStringValue($this->filterOptions[$f[$this->name]]);
}
else
{
return NULL;
}
}
public function getFilterStringValue($val)
{
return (string)$val;
}
}}namespace WDB\WebUI{
use WDB;
/**
  * Boolean column
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnBoolean extends Column
{
public function getDetailedHTML(WDB\Wrapper\iRecord $record)
{
return htmlspecialchars($this->getStringRepresentation($record[$this->name]));
}
private function getStringRepresentation($value)
{
return $value ? WDB\Lang::s('webui.boolean.yes') : WDB\Lang::s('webui.boolean.no');
}
public function getFilterStringValue($val)
{
return $this->getStringRepresentation($val);
}
protected function fetchInputValue($v)
{
return $v ? 1 : 0;
}
public function isNullable()
{
return false;
}
public function getEditingHTML(WDB\Wrapper\iRecord $record)
{
$input_name = $this->table->getRequest()->getSaveFieldName($this->name);
$input = new HTMLNode('input');
$input['type'] = 'checkbox';
$input['name'] = $input_name.'[value]';
$input['value'] = '1';
if ($record[$this->name])
{
$input['checked'] = 'checked';
}
return $input;
}
}}namespace WDB\WebUI{
use WDB,
WDB\Exception;
/**
  * Base multichoice column implementation through checkbox list.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnMultiChoice extends Column implements WDB\iSelectOptions
{
/**
     * @var string
     * @see ColumnSelect::$nullKey
     */
private $nullKey;
/**
     *
     * @param WDB\Wrapper\iColumn $columnWrapper
     * @param iTable $tableWebUI 
     * @throws Exception\BadArgument
     */
public function __construct(WDB\Wrapper\iColumn $columnWrapper, iTable $tableWebUI)
{
if (!$columnWrapper instanceof WDB\iSelectOptions)
{
throw new Exception\BadArgument("column webui under FieldCheckboxes must implement \\WDB\\iSelectOptions");
}
parent::__construct($columnWrapper, $tableWebUI);
$this->nullKey = '=null';
while (array_key_exists($this->nullKey, $this->getOptions()))
{
$this->nullKey .= '=';
}
}
public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
{
return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_multichoice'));
}
public function getDetailedHTML(WDB\Wrapper\iRecord $record)
{
$list = array();
$options = $this->getOptions();
foreach ($record[$this->name] as $item)
{
$list[] = isset($options[$item]) ? $options[$item] : '?';
}
return htmlspecialchars(implode(',', $list));
}
protected function fetchInputValue($v)
{
if (!is_array($v)) return array();  return $v;
}
public function getEditingHTML_nullator(WDB\Wrapper\iRecord $record)
{
$val = $record[$this->name];
if ($this->isNullable())
{
$input_name = $this->table->getRequest()->getSaveFieldName($this->name);
return '<input type="checkbox" name="'.htmlspecialchars($input_name).'[notnull]" value="1" '.($val !== null ? 'checked="checked"' : '').'
                    onclick="var inp = $(\'input[name=&quot;'.str_replace(array('[', ']'), array('\\[', '\\]'), htmlspecialchars($input_name)).'\[value\]\[\]&quot;]\');
                        if (this.checked) inp.removeAttr(\'disabled\'); else inp.attr(\'disabled\', true);"/>';
}
else
{
return '';
}
}
public function getEditingHTML(WDB\Wrapper\iRecord $record)
{
$val = $record[$this->name];
$input_name = $this->table->getRequest()->getSaveFieldName($this->name);
$out = '';
$valuesName = $input_name.'[value][]';
$i = 0;
foreach ($this->getOptions() as $key=>$text)
{
$checkbox = new HTMLNode('input');
$checkbox['type'] = 'checkbox';
$checkbox['name'] = $valuesName;
$checkbox['id'] = str_replace(array('[', ']'), '_', $valuesName).$i;
$checkbox['value'] = $key;
if ($val !== NULL && in_array($key, $val))
{
$checkbox['checked'] = 'checked';
}
if ($this->isNullable() && $val === NULL)
{
$checkbox['disabled'] = 'disabled';
}
$out .= "<div class=\"wdb_multi_elem\">".$checkbox.'<label for="'.$checkbox['id'].'">'.htmlspecialchars($text)."</label></div>\n";
++$i;
}
return $out;
}
public function getOptions()
{
return $this->columnWrapper->getOptions();
}
public function isFilterable()
{
return false;
}
public function isOrderable()
{
return false;
}
}}namespace WDB\WebUI{
use WDB;
/**
 * The "repeat password" column.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnPasswordCheck extends WDB\BaseObject implements WDB\WebUI\iColumn
{

protected $table;

protected $display;

protected $title;

protected $name;

protected $compareTo;
/**
     * @var \WDB\Validation\ColumnRules only for javascript clone. Server-side validation
     * executes on WebUI layer and therefore cannot be validated by a wrapper validator.
     */
protected $rules;
/**
     *
     * @param iTable webui table
     * @param string column title
     * @param string password column name to match
     * @param string column name (or null to auto-generate)
     */
public function __construct(iTable $tableWebUI, $title, $passwordColumn, $name = NULL)
{
$this->table = $tableWebUI;
$this->table->getOnValidateInput()->addListener(
new WDB\Event\CallbackListener(
array($this, 'validateComparePasswords')
)
);
$this->title = $title;
if ($name === NULL || isset($tableWebUI->columns[$name]))
{
$this->name = $tableWebUI->createUniqueColumnName();
}
else
{
$this->name = $name;
}
$this->setDisplayModes(iColumn::DISPLAY_EDIT);
$this->compareTo = $passwordColumn;
$this->rules = new WDB\Validation\ColumnRules($title);
$this->rules->equals($passwordColumn);
}
public function getCompareTo()
{
return $this->compareTo;
}
public function getTable()
{
return $this->table;
}
public function addDisplayMode($mode)
{
$this->display |= $mode;
}
public function removeDisplayMode($mode)
{
$this->display &= ~$mode;
}
public function setDisplayModes($mode)
{
$this->display = (int)$mode;
}
public function getDisplayModes()
{
return $this->display;
}
public function isDisplayedList()
{
return $this->isDisplayed(iColumn::DISPLAY_LIST);
}
public function isDisplayedEdit()
{
return $this->isDisplayed(iColumn::DISPLAY_EDIT);
}
public function isDisplayedEditRead()
{
return $this->isDisplayed(iColumn::DISPLAY_EDIT_READONLY);
}
public function isDisplayedDetail()
{
return $this->isDisplayed(iColumn::DISPLAY_DETAIL);
}
public function isDisplayed($mode)
{
return ($this->display & $mode) != 0;
}
public function getTitleHTML()
{
return htmlspecialchars($this->title);
}
public function getName()
{
return $this->name;
}
public function getTitle()
{
return $this->title;
}
public function isNullable()
{
return FALSE;
}
public function getRules()
{
return $this->rules;
}
public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
{
return array('wdb_field_pwdcheck');
}
public function getEditingHTML(WDB\Wrapper\iRecord $record)
{
$input_name = $this->table->getRequest()->getSaveFieldName($this->name);
$input = new HTMLNode('input');
$input['type'] = 'password';
$input['name'] = $input_name;
$input['value'] = $record[$this->name];
return $input->__toString();
}
public function validateComparePasswords(WDB\Wrapper\iRecord $record)
{
$value = ColumnPassword::hash($this->table->getRequest()->getEdited($this->name));
if ($value != $record[$this->compareTo]) throw new Exception\ValidationFailed
(array(WDB\Lang::s('webui.passwordNotMatch')));
}
public function parseInputValue(WDB\Wrapper\iRecord $record, $value)
{
 }
public function getDetailedHTML(WDB\Wrapper\iRecord $record)
{
return '';
}
public function getEditingHTML_nullator(WDB\Wrapper\iRecord $record)
{
return '';
}
public function getSimpleHTML(WDB\Wrapper\iRecord $record)
{
return '';
}
public function getHTMLEditStatus(WDB\Wrapper\iRecord $record)
{
return implode('<br>', $record->getField($this->name)->getValidationErrors());
}
public function isDefaultInitializable()
{
return TRUE;
}
public function isOrderable()
{
return FALSE;
}
}}namespace WDB\WebUI{
use WDB,
WDB\Exception;
/**
  * Column of a choice select value (i.e. ENUM).
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnSelect extends Column implements WDB\iSelectOptions
{

public $unselectedText;

private $nullKey;

public function getOptions()
{
return $this->columnWrapper->getOptions();
}
/**
     *
     * @param WDB\Wrapper\iColumn
     * @param iTable
     * @throws Exception\BadArgument
     */
public function __construct(WDB\Wrapper\iColumn $columnWrapper, iTable $tableWebUI)
{
if (!$columnWrapper instanceof WDB\iSelectOptions)
{
throw new Exception\BadArgument("column wrapper for ColumnSelect webui must implement \\WDB\\iSelectOptions");
}
parent::__construct($columnWrapper, $tableWebUI);
$this->unselectedText = WDB\Lang::s('webui.edit.noneSelected');
$this->nullKey = '=!null';
while (array_key_exists($this->nullKey, $this->getOptions()))
{
$this->nullKey .= '=';
}
}
public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
{
return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_select'));
}
public function getEditingHTML(WDB\Wrapper\iRecord $record)
{
 $input_name = $this->table->getRequest()->getSaveFieldName($this->name);
$output = '<select name="'.htmlspecialchars($input_name).'[value]" data-null="'.htmlspecialchars($this->nullKey).'">';
$current_value = $record->getField($this->name)->getStringValue();
 $option = new HTMLNode('option');
if ($current_value === NULL)
{
$option['selected'] = 'selected';
}
$option['rel']='null';
$option['value']=$this->nullKey;
$option->content = $this->unselectedText;
$output .= $option->__toString()."\n";
 foreach ($this->getOptions() as $key=>$text)
{
$option = new HTMLNode('option');
if ($key == $current_value)
{
$option['selected'] = 'selected';
}
$option['value']=$key;
$option->content = $text;
$output .= $option->__toString()."\n";
}
$output .= '</select>';
return $output;
}
public function parseInputValue(WDB\Wrapper\iRecord $record, $value)
{
return $this->parseInputValueNoNullable($record, $value);
}
public function getDetailedHTML(WDB\Wrapper\iRecord $record)
{
$opt = $this->getOptions();
return htmlspecialchars(isset($opt[$record->getField($this->name)->getStringValue()]) ? $opt[$record->getField($this->name)->getStringValue()] : WDB\Lang::s('webui.notset'));
}
public function getEditingHTML_nullator(WDB\Wrapper\iRecord $record)
{
 return '';
}
protected function fetchInputValue($v)
{
if ($v === $this->nullKey) return NULL;
return $v;
}
}}namespace WDB\WebUI{
use WDB;
/**
  * Simple column extension for text fields. Since Column class's default methods expects a textual information,
  * there is nothing extra in this class.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnText extends Column
{
/**
     * translates native value to HTML editing input field value when using default input type=text.
     *
     * @param mixed $v
     * @return string 
     */
protected function HTMLInputTextValue($v)
{
if ($v === NULL) return '';
return $v;
}
protected function getInputHTMLNode(WDB\Wrapper\iRecord $record)
{
$input = parent::getInputHTMLNode($record);
$input['value'] = $this->HTMLInputTextValue($input['value']);
return $input;
}
public function getEditingHTML(WDB\Wrapper\iRecord $record)
{
return $this->getInputHTMLNode($record)->__toString();
}
}}namespace WDB\WebUI{
use WDB;
/**
  * Time column implementation
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnTime extends ColumnText
{
public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
{
return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_time'));
}
protected function HTMLInputTextValue($v)
{
if ($v === NULL) return date('G:i:s');
if (is_object($v) && method_exists($v, 'format'))
{
return $v->format('G:i:s');
}
return (string)$v;
}
public function getDetailedHTML(WDB\Wrapper\iRecord $record)
{
$val = $record[$this->name];
return $this->getStringRepresentation($val);
}
private function getStringRepresentation($value)
{
if ($value === NULL) return WDB\Lang::s ('webui.notset');
return $value->format('G:i:s');
}
public function getFilterStringValue($val)
{
return $this->getStringRepresentation($val);
}
}}namespace WDB\WebUI{
use WDB,
WDB\Exception;
/**
 * iRequest implementation representing HTTP requests
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class HttpRequest extends WDB\BaseObject implements iRequest
{
const ACCEPT_GET = 1;
const ACCEPT_POST = 2;
const ACCEPT_BOTH = 3;

protected $requestId;

protected $accept;

protected $uri;

protected $pUri;
protected static $dynamicParams = array('state', 'selected', 'saved', 'newRecord', 'delete');
protected static $tablePersistentParams = array('order', 'desc', 'filter');
/**
     *
     * @param string request identifier
     * @param int enumeration of HttpRequest::ACCEPT_*
     */
public function __construct($requestId = '', $accept = self::ACCEPT_BOTH)
{
$this->requestId = $requestId == '' ? '' : WDB\Utils\Strings::escapeVarName($requestId);
$this->uri = new Uri($_SERVER['REQUEST_URI']);
 if ($this->requestId != '')
{
$params = $this->uri->getParam($this->requestId);
$this->uri->removeParams($this->requestId);
$this->pUri = clone $this->uri;
if ($params !== NULL)
{
foreach (self::$dynamicParams as $param)
{
unset($params[$param]);
}
$this->pUri->addParam($this->requestId, $params);
foreach (self::$tablePersistentParams as $param)
{
unset($params[$param]);
}
$this->uri->addParam($this->requestId, $params);
}
}
else
{
foreach (self::$dynamicParams as $param)
{
$this->uri->removeParams($param);
}
$this->pUri = clone $this->uri;
foreach (self::$tablePersistentParams as $param)
{
$this->uri->removeParams($param);
}
}
switch ($accept)
{
case self::ACCEPT_GET:
$this->accept = &$_GET;
break;
case self::ACCEPT_POST:
$this->accept = &$_POST;
break;
case self::ACCEPT_BOTH:
default:
$this->accept = &$_REQUEST;
break;
}
}
public function getRequestId()
{
return $this->requestId;
}
 public function getRequestedState()
{
if ($this->getData($_GET, 'state')!== NULL)
{
return $this->getData($_GET, 'state');
}
else
{
return iTable::STATE_DEFAULT;
}
}
public function getSaveFieldName($name)
{
return $this->getHttpIdentifier('data')."[$name]";
}
public function getEdited($key)
{
return $this->getData($this->accept, 'data', $key);
}
public function getAllEdited()
{
if ($this->requestId == '')
{
throw new Exception\InvalidOperation("Only HttpRequest with specified Request ID can call getAllEdited() ");
}
if (isset($this->accept[$this->requestId]))
{
return $this->accept[$this->requestId];
}
else
{
return NULL;
}
}
public function getStateLink($state, $selectedRecordKey = NULL)
{
$args = array('state'=>$state);
if ($selectedRecordKey !== NULL)
{
if ($selectedRecordKey === TRUE)
{
$args['newRecord'] = 1;
}
else
{
$args['selected'] = $selectedRecordKey;
}
}
$u = clone $this->uri;
$this->addUriArgs($u, $args);
return $u;
}
public function getSelectedRecordKey()
{
return $this->getData($_GET, 'selected');
}
public function getEditControlName()
{
return $this->getHttpIdentifier('edited');
}
public function isEditSubmitted()
{
return (bool)$this->getData($this->accept, 'edited');
}
public function isNewRecord()
{
return $this->getData($_GET, 'newRecord') !== NULL;
}
public function getDeleteLink($recordKey)
{
$args = array('delete'=>$recordKey);
$u = clone $this->pUri;
$this->addUriArgs($u, $args);
return $u;
}
public function redirectSuccessfulSave($recordKey = NULL)
{
$args = array('state'=>\WDB\WebUI\iTable::STATE_LIST,
'saved'=>$recordKey);
if ($this->requestId != '')
{
$args = array($this->requestId => $args);
}
$this->uri->redirect($args);
}
public function isDeleteRequest()
{
return $this->getData($_GET, 'delete') !== NULL;
}
public function getDeleteKey()
{
return $this->getData($_GET, 'delete');
}
public function getPageParamName()
{
return $this->getHttpIdentifier('page');
}
public function getCurrentPage()
{
return max(0, $this->getData($_GET, 'page')-1);
}
public function getOrderColumn()
{
return $this->getData($_GET, 'order');
}
public function isOrderAsc()
{
return !$this->getData($_GET, 'desc');
}
public function getFilters()
{
return $this->getData($_GET, 'filter');
}
public function configureDatasource(WDB\Wrapper\iTable $table)
{
$cfg = array();
$cfg['page'] = $this->getCurrentPage();
$cfg['sort'] = $this->getOrderColumn();
$cfg['asc'] = $this->isOrderAsc();
$cfg['filter'] = $this->getFilters();
return $table->getDatasource($cfg);
}
public function orderLink($column = NULL, $asc = TRUE)
{
$u = clone $this->pUri;
if ($column !== NULL)
{
$this->addUriArgs($u, array('order'=>$column));
}
else
{
$this->removeUriArg($u, 'order');
}
if ($asc)
{
$this->removeUriArg($u, 'desc');
}
else
{
$this->addUriArgs($u, array('desc'=>'1'));
}
return $u;
}
/**
     *
     * @param string $table
     * @return Uri 
     */
public function getSelectTableLink($table)
{
$u = clone $this->uri;
$this->addUriArgs($u, array('table'=>$table));
return $u;
}
public function getSelectedTable()
{
return $this->getData($_GET, 'table');
}

/**
     * get data from GET/POST or its subarray if requestId is set
     *
     * @param array source array (_GET, _POST, this->accept...)
     * @param string $path,... array-level keys to get
     * @return string|null
     */
protected function getData($source, $path)
{
$path = func_get_args();
array_shift($path);
if ($this->requestId == '')
{
$container = $source;
$path[0] = $this->prefixRequestKey($path[0]);
}
else
{
if (!isset($source[$this->requestId])) return NULL;
$container = $source[$this->requestId];
}
foreach ($path as $obj)
{
if (!isset($container[$obj])) return NULL;
$container = $container[$obj];
}
return $container;
}
protected function getHttpIdentifier($name)
{
if ($this->requestId == '')
{
return $this->prefixRequestKey($name);
}
else
{
return $this->requestId.preg_replace('~[^[]+~i', '[\\0]', $name);
}
}
protected function prefixRequestKey($key)
{
return 'wdb'.ucfirst($key);
}
protected function removeUriArg(Uri $uri, $arg)
{
if ($this->requestId || is_array($arg))
{
if ($this->requestId)
{
$p = $uri->getParam($master=$this->requestId);
}
else
{
$p = $uri->getParam($master=array_shift($arg));
}
if (!is_array($arg)) $arg = array($arg);
$rp = &$p;
foreach ($arg as $pathElement)
{
if (!isset($rp[$pathElement])) return;
$xp = &$rp[$pathElement];
unset($rp);
$rp = &$xp;
}
$rp = NULL;
$uri->removeParams($master);
$uri->addParam($master, $p);
}
else
{
$u->removeParams($this->prefixRequestKey($arg));
}
}
protected function addUriArgs(Uri $uri, array $args)
{
if ($this->requestId != '')
{
$args = array($this->requestId=>$args);
}
else
{
$out_args = array();
foreach ($args as $key=>$arg)
{
$out_args[$this->prefixRequestKey($key)] = $arg;
}
$args = $out_args;
}
$uri->addParams($args);
}
public function filterLink($column, $value)
{
$filters = $this->getFilters();
$u = clone $this->pUri;
if ($value === NULL)
{
$this->removeUriArg($u, array('filter', $column));
}
else
{
$this->addUriArgs($u, array('filter'=>array($column=>$value)));
}
return $u;
}
}
}namespace WDB\Wrapper{
use WDB,
WDB\Exception;
/**
 * Base class for common column wrapper implementation.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
abstract class AbstractColumn extends WDB\BaseObject implements iColumn, iOrderable, iFilterable
{

protected $rules;

protected $table;

protected $onSaved;

public $addDisplayModes = 0;

public $removeDisplayModes = 0;
protected function __construct(iTable $tableWrapper)
{
$this->table = $tableWrapper;
$this->rules = new WDB\Validation\ColumnRules($this->title);
$this->onSaved = new WDB\Event\Event();
}
 public function getDefault()
{
return NULL;
}
public function getTable()
{
return $this->table;
}
public function getTitle()
{
return $this->getName();
}
public function getRules()
{
return $this->rules;
}
public function isNullable()
{
return TRUE;
}
public function rule($rule, $message = NULL, $argument = NULL)
{
$this->rules->set($rule, $message, $argument);
return $this;
}
public function getWebUIClass()
{
return NULL;
}
public function getWebUIDisplayMode($original = NULL)
{
return ($original | $this->addDisplayModes) & ~$this->removeDisplayModes;
}
public function isAutoColumn()
{
return FALSE;
}
public function isAutoIncrement()
{
return FALSE;
}
public function valueToDatatype($value)
{
return WDB\Query\Element\Datatype\AbstractType::createDatatype($value);
}
public function valuesEqual($first, $second)
{
return $first == $second;
}
public function createField($isNew, $value, WDB\Query\SelectedRow $row = NULL)
{
return new Field($this, $isNew, $this->setFieldValue($value));
}
public function saveToRow(array &$row, $value)
{
$row[$this->name] = $value;
}
public function getOnSaved()
{
return $this->onSaved;
}
public function raiseOnSaved(WDB\Wrapper\iRecord $record)
{
$this->onSaved->raise($record);
}
public function isOrderable()
{
return false;
}
public function getFieldValue($value)
{
return $value;
}
public function setFieldValue($value)
{
return $value;
}
public function getStringValue($value)
{
return $value === NULL ? "=NULL" : preg_replace('~^=NULL~', '==NULL', (string)$value);
}
}}namespace WDB\Wrapper{
use WDB;
/**
 * Generic database column
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Column extends AbstractColumn implements iDBColumn
{

protected $columnAnalyzer;

protected $webUIClass = NULL;
public function __construct($tableWrapper, $columnAnalyzer)
{
$this->columnAnalyzer = $columnAnalyzer;
parent::__construct($tableWrapper);
$this->fetchValidationRules($this->rules);
}
protected function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
$this->columnAnalyzer->fetchValidationRules($rules);
if (isset($this->columnAnalyzer->getAnnotations()->minlength))
{
$rules->minLength($this->columnAnalyzer->getAnnotations()->last('minlength')->getText());
}
if (isset($this->columnAnalyzer->getAnnotations()->maxlength))
{
$rules->maxLength($this->columnAnalyzer->getAnnotations()->last('maxlength')->getText());
}
if (isset($this->columnAnalyzer->getAnnotations()->minvalue))
{
$rules->minValue($this->columnAnalyzer->getAnnotations()->last('minvalue')->getText());
}
if (isset($this->columnAnalyzer->getAnnotations()->maxvalue))
{
$rules->maxValue($this->columnAnalyzer->getAnnotations()->last('maxvalue')->getText());
}
if (isset($this->columnAnalyzer->getAnnotations()->required))
{
$rules->required();
}
if (isset($this->columnAnalyzer->getAnnotations()->integer))
{
$rules->integer();
}
if (isset($this->columnAnalyzer->getAnnotations()->float))
{
$rules->float();
}
if (isset($this->columnAnalyzer->getAnnotations()->equals))
{
$rules->equals($this->columnAnalyzer->getAnnotations()->last('equals')->getText());
}
if (isset($this->columnAnalyzer->getAnnotations()->pattern))
{
$rules->pattern($this->columnAnalyzer->getAnnotations()->last('pattern')->getText());
}
if (isset($this->columnAnalyzer->getAnnotations()->email))
{
$rules->email();
}
if (isset($this->columnAnalyzer->getAnnotations()->url))
{
$rules->url();
}
}
public function setFieldValue($value)
{
return $this->columnAnalyzer->setFieldValue($value);
}
public function getStringValue($value)
{
return $value === NULL ? "=NULL" : preg_replace('~^=NULL~', '==NULL', $this->columnAnalyzer->getStringValue($value));
}
 public function getName()
{
return $this->columnAnalyzer->getName();
}
public function getTitle()
{
return $this->columnAnalyzer->getTitle();
}
public function getDefault()
{
return $this->columnAnalyzer->getDefault();
}
public function isNullable()
{
return $this->columnAnalyzer->isNullable();
}
public function getWebUIClass()
{
$c = $this->columnAnalyzer->getWebUIClass();
if ($c === NULL)
{
return $this->webUIClass;
}
return $c;
}
public function getWebUIDisplayMode($original = NULL)
{
return parent::getWebUIDisplayMode($this->columnAnalyzer->getWebUIDisplayMode($original));
}
public function isAutoColumn()
{
return $this->columnAnalyzer->isAutoColumn();
}
public function isAutoIncrement()
{
return $this->columnAnalyzer->isAutoIncrement();
}
public function valueToDatatype($value)
{
return $this->columnAnalyzer->valueToDatatype($value);
}
public function fetchReadFields(&$fields)
{
$fields[] = new WDB\Query\Element\SelectField($this->columnAnalyzer->getIdentifier());
}
public function isOrderable()
{
return TRUE;
}
public function isFilterable()
{
return TRUE;
}
public function sortDatasource(WDB\iDatasource $datasource, $asc = TRUE, $prepend = TRUE)
{
$datasource->sort($this->getName(), $asc, $prepend);
}
public function filterDatasource(WDB\iDatasource $datasource, $value)
{
if (is_string($value) && $value{0} == '~')
{
$value = unserialize($value);
}
if (!$value instanceof WDB\Query\Element\Datatype\iDatatype)
{
$value = $this->columnAnalyzer->valueToDatatype($value);
}
$datasource->filter(WDB\Structure\FilterRule::create($this->columnAnalyzer->getIdentifier(), $value));
}
public function getFilterOptions()
{
$options = array();
foreach ($this->table->getDatasource()->select($this->name)->distinct()->run() as $row)
{
$val = $row[$this->name];
if ((!is_string($val) || $val{0} == '~') && !is_int($val))
{
$val = '~'.serialize($val);
}
$options[$val] = $row[$this->name];
}
return $options;
}
}}namespace WDB\Wrapper{
use WDB,
WDB\Exception,
WDB\Query\Element;
/**
 * Column representing database foreign key connected table.
 * 
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @todo remove MultiValue usages
 */
class ColumnForeign extends AbstractColumn implements iDBColumn, WDB\iSelectOptions, iOrderable, iFilterable
{
protected $webUIClass = '\\WDB\\WebUI\\ColumnSelect';
public function __construct(WDB\Structure\ForeignKeyData $key, iTable $table, array $analyzerColumns)
{
$this->key = $key;
$this->analyzerColumns = array();
$this->nullable = true;
foreach ($this->key->columns as $columnName)
{
$this->analyzerColumns[$columnName] = $analyzerColumns[$columnName];
if (!$analyzerColumns[$columnName]->isNullable())
{
$this->nullable = false;
}
}
parent::__construct($table);
$this->fetchValidationRules($this->rules);
}

protected $analyzerColumns;

protected $rules;

protected $table;

protected $key;

protected $nullable;

protected $optionCache = NULL;
public function setFieldValue($value)
{
if (is_string($value))
{
return WDB\Utils\Strings::unserializeCommaSeparatedList($value, $this->key->columns);
}
elseif (is_array($value) || is_null($value))
{
return $value;
}
else
{
throw new Exception\BadArgument("Only string or array is accepted as foreign field value");
}
}
public function createField($isNew, $value, WDB\Query\SelectedRow $row = NULL)
{
if ($row !== NULL)
{
$value = array();
foreach ($this->key->columns as $column)
{
$value[$column] = $row[$column];
}
}
return new Field($this, $isNew, $this->setFieldValue($value));
}
protected function firstColumnAnalyzer()
{
return $this->analyzerColumns[$this->key->columns[0]];
}
protected function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
foreach ($this->analyzerColumns as $ac)
{
if (isset($ac->annotations->required) || !$this->isNullable())
{
$rules->required();
break;
}
}
}
 public function getDefault()
{
$values = array();
foreach ($this->analyzerColumns as $column)
{
$values[$column->name] = $column->default;
}
return $values;
}
public function getName()
{
return $this->key->name;
}
public function getTitle()
{

return $this->firstColumnAnalyzer()->title;
}
public function isNullable()
{
return $this->nullable;
}
public function rule($rule, $message = NULL, $argument = NULL)
{
$this->rules->set($rule, $message, $argument);
return $this;
}
public function getWebUIClass()
{
return '\\WDB\\WebUI\\ColumnSelect';
}
public function getWebUIDisplayMode($original = NULL)
{
return $this->firstColumnAnalyzer()->getWebUIDisplayMode($original);
}
public function valueToDatatype($value)
{
if (is_string($value))
{
return WDB\Utils\Strings::unserializeCommaSeparatedList($value, $this->key->columns);
}
elseif (is_array($value))
{
return $value;
}
else
{
throw new Exception\BadArgument("only comma-separated string list or array is accepted");
}
}
public function fetchReadFields(&$fields)
{
foreach ($this->analyzerColumns as $column)
{
$fields[] = new Element\SelectField($column->identifier);
}
}
public function saveToRow(array &$row, $value)
{
if ($value === NULL)
{
foreach ($this->key->columns as $key)
{
$row[$key] = NULL;
}
}
else
{
foreach ($value as $key=>$val)
{
$row[$key] = $val;
}
}
}
public function getFieldValue($value)
{
if ($value !== NULL)
{
$targetTable = WDB\Wrapper\TableFactory::fromName($this->key->refTable,
$this->key->refSchema,
$this->table->getDatabase());
$records = $targetTable->elevateRecords($targetTable->datasource
->setFields(Element\ColumnIdentifier::ALL_COLUMNS)  ->filter(is_array($value) ? $value : $this->table->unserializeKey($value, $this->key->columns))
->run($this->table->database));
if (count($records) > 0)
{
return $records[0];
}
else
{
return NULL;
}
}
}
public function getStringValue($value)
{
return $value === NULL ? "=NULL" : preg_replace('~^=NULL~', '==NULL', $this->table->serializeKey($value));
}
public function isOrderable()
{
return TRUE;
}
public function sortDatasource(WDB\iDatasource $datasource, $asc = TRUE, $prepend = TRUE)
{
if (!$datasource instanceof WDB\Query\Select)
{
throw new Exception\BadArgument('Foreign column can be sorted only on a Select datasource type');
}
$condition = Element\LogicOperator::lAnd();
$clist = array_values($this->key->columns);
$refClist = array_values($this->key->refColumns);
reset($clist);reset($refClist);
do
{
$col = current($clist);
$refCol = current($refClist);
$condition->addExpression(Element\Compare::Equals(
Element\ColumnIdentifier::create($col, $this->key->table, $this->key->schema),
Element\ColumnIdentifier::create($refCol, $this->key->refTable, $this->key->refSchema)
));
next($refClist);
}
while (next($clist));
$datasource->setTable(
new Element\Join(
$datasource->getTable(),
new Element\TableIdentifier($this->key->refTable, $this->key->refSchema),
$condition,
Element\Join::LEFT
)
);
$datasource->sort(Element\ColumnIdentifier::create($this->getRefNameColumn(), $this->key->refTable, $this->key->refSchema), $asc, $prepend);
}

protected function getTargetTable()
{
return $this->table
->getDatabase()
->getTable(new Element\TableIdentifier($this->key->refTable, $this->key->refSchema));
}
protected function getRefNameColumn()
{
$tableAnalyzer = $this->getTargetTable();
$aColumns = $tableAnalyzer->getColumns();
if (isset($tableAnalyzer->annotations->nameColumn))
{
return $tableAnalyzer->annotations->last('nameColumn')->getText();
}
elseif (isset($aColumns[WDB\Config::read('recordNameColumn')]))
{
return WDB\Config::read('recordNameColumn');
}
else
{
throw new WDB\Exception\ConfigInsufficient("Table {$this->key->refTable} has not defined record name column");
}
}
public function isFilterable()
{
return TRUE;
}
public function filterDatasource(WDB\iDatasource $datasource, $value)
{
$filter = array();
$value = $this->setFieldValue($value);
foreach ($this->analyzerColumns as $column)
{
$filter[] = WDB\Structure\FilterRule::create($column->getIdentifier(), $value === NULL ? NULL : $value[$column->getName()]);
}
$datasource->filter($filter);
}
public function getFilterOptions()
{
return $this->getOptions();
}
 public function getOptions()
{
if ($this->optionCache === NULL)
{
$targetTable = WDB\Wrapper\TableFactory::fromName($this->key->refTable, $this->key->refSchema, $this->table->database);
$tableAnalyzer = $this->getTargetTable();
$nameColumn = $this->getRefNameColumn();
$options = $targetTable->datasource->select($this->key->refColumns, $nameColumn)->run();
$this->optionCache = array();
foreach ($options as $option)
{
$value = $option[$nameColumn];
$keyData = array();
foreach ($this->key->refColumns as $c)
{
$keyData[] = $option[$c];
}
$key = $this->table->serializeKey($keyData);
$this->optionCache[$key] = $value;
}
}
return $this->optionCache;
}
}}namespace WDB\Wrapper{
use WDB,
WDB\Exception;

class ColumnSelect extends Column implements \WDB\iSelectOptions
{
protected $webUIClass = '\\WDB\\WebUI\\ColumnSelect';
public function __construct($tableWrapper, $columnAnalyzer)
{
if (!$columnAnalyzer instanceof WDB\iSelectOptions)
{
throw new Exception\BadArgument("column analyzer for ColumnSelect class must implement \WDB\iSelectOptions interface");
}
parent::__construct($tableWrapper, $columnAnalyzer);
}
public function getOptions()
{
return $this->columnAnalyzer->getOptions();
}
}}namespace WDB\Wrapper{
/**
 * Table with base data stored in database under WDB\Database.
 * These methods are not intended for WebUI, only for records in this table which stores their data to database.
 * Database layer should be completely separated from WebUI.
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
interface iDBTable extends iTable
{
/**
     * Get table analyzer. NOT part of iTable interface - this serves to a Wrapper\Record bound to database
     * to create save queries.
     *
     * @return \WDB\Analyzer\TableView
     */
public function getTableAnalyzer();
/**
     * Return database connection where this table is stored.
     *
     * @return \WDB\Database
     */
public function getDatabase();
/**
     * Get source table query with columns listed in $readFields array.
     *
     * @param array $cfg
     * @return \WDB\Query\Select
     */
public function getDatasource(array $cfg = array());
}}namespace WDB\Wrapper{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class RecordCollection extends WDB\BaseCollection
{

private $table;
/**
     *
     * @param WDB\Query\SelectedRow[]
     * @param iTable
     */
public function __construct($rows, iTable $table)
{
$this->contents = $rows;
$this->table = $table;
}

protected function outValue($value, $offset)
{
if ($value === FALSE) return FALSE;
return $this->table->fetchRecord($value);
}
}
}namespace WDB\Wrapper\Structure{
use WDB;
/**
 * @property-read \WDB\Query\SelectedRow $row
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ExistingRecordInitializer extends RecordInitializer {}
}namespace WDB\Wrapper\Structure{
use WDB;
/**
 * @property-read array $data
 * @property-read int $insertMode
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class NewRecordInitializer extends RecordInitializer {}
}namespace WDB\Wrapper{
use WDB,
WDB\Exception;
/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Table extends WDB\BaseObject implements iDBTable
{

protected $database;

protected $customValidators = array();
public function getDatabase()
{
return $this->database;
}

protected $schema;

protected $tableAnalyzer;

protected $key;

protected $cache;

protected $useCache = TRUE; 

protected $readFields;

protected $columnWrapperClasses = array();

protected $columnWebUIClasses = array();

protected $columns = array();

protected $columnAliases = array();
protected function configureDatasource(WDB\iDatasource $datasource, array $cfg)
{
foreach ($cfg as $key=>$val)
{
if ($val === NULL) continue;
switch ($key)
{
case 'page':
$datasource->page($val);
break;
case 'sort':
if (!isset($this->columns[$val]) || !$this->columns[$val] instanceof iOrderable || !$this->columns[$val]->isOrderable())
throw new Exception\BadArgument('table '.$this->getName().' cannot be sorted by '.$val);
$this->columns[$val]->sortDatasource($datasource, $cfg['asc']);
break;
case 'filter':
foreach ($val as $filterKey=>$filterVal)
{
if (!isset($this->columns[$filterKey]) || !$this->columns[$filterKey] instanceof iFilterable || !$this->columns[$filterKey]->isFilterable())
throw new Exception\BadArgument('table '.$this->getName().' cannot be filtered by '.$filterKey);
$this->columns[$filterKey]->filterDatasource($datasource, $filterVal);
}
break;
}
}
}

public function getName()
{
return $this->tableAnalyzer->name;
}
public function getTitle()
{
return $this->tableAnalyzer->title;
}
public function serializeKey(array $k)
{
return WDB\Utils\Strings::serializeCommaSeparatedList($k);
}
public function unserializeKey($data, array $keys)
{
return WDB\Utils\Strings::unserializeCommaSeparatedList($data, $keys);
}
/**
     *
     * @throws Exception\ColumnNotFound
     * @throws Exception\NotFound
     */
public function getRecordByKey($key)
{
$key = $this->canonicalizeKey($key);
$result = $this->getDatasource()->filter($key)->run($this->database);
if (count($result) == 0) throw new Exception\NotFound ("record not found.");
return $this->fetchRecord($result->singleRow());
}
protected function canonicalizeKey($key)
{
if(!is_array($key))
{
$pk = $this->tableAnalyzer->getPrimaryKey();
if (count($pk) > 1) throw new Exception\BadArgument("Cannot use single-value primary key on a table with multi-value primary key");
return array($pk[0]=>$key);
}
return $key;
}
public function getRecordByStringPK($key)
{
$key = $this->unserializeKey($key, $this->getPrimaryKey());
return $this->getRecordByKey($key);
}
public function getPrimaryKey()
{
return $this->tableAnalyzer->getPrimaryKey();
}
public function getUniqueKeys()
{
return $this->tableAnalyzer->getUniqueKeys();
}
public function fetchRecord(WDB\Query\SelectedRow $row)
{
return RecordFactory::load($this, $row);
}
public function newRecord($data = array(), $mode = WDB\Query\Insert::UNIQUE)
{
return RecordFactory::create($this, $data, $mode);
}
public function elevateRecords($rows)
{
return new RecordCollection($rows, $this);
}
public function getColumnWrapperClass($columnName)
{
if (isset($this->columnWrapperClasses[$columnName])) return $this->columnWrapperClasses[$columnName];
if (isset($this->tableAnalyzer->columns[$columnName]))
{
return $this->tableAnalyzer->columns[$columnName]->getWrapperClass();
}
return NULL;
}
public function getColumnWebUIClass($columnName)
{
if (isset($this->columnWebUIClasses[$columnName])) return $this->columnWebUIClasses[$columnName];
$columns = $this->tableAnalyzer->getColumns();
if (isset($columns[$columnName]))
{
return $columns[$columnName]->getWebUIClass();
}
return NULL;
}
public function getWebUIClass()
{
return $this->tableAnalyzer->getWebUIClass();
}
public function getRecordWrapperClass()
{
return $this->tableAnalyzer->getRecordWrapperClass();
}
public function getColumns()
{
return $this->columns;
}
public function getColumnCName($alias)
{
if (isset($this->columnAliases[$alias])) return $this->columnAliases[$alias];
return $alias;
}
public function isListedInSchemaAdmin()
{
return $this->tableAnalyzer->isListedInSchemaAdmin();
}
public function isWebUIAccessible()
{
return $this->tableAnalyzer->isWebUIAccessible();
}
public function fetchValidators(WDB\Event\Event $event)
{
$this->tableAnalyzer->fetchValidators($event);
foreach ($this->customValidators as $validator)
{
$event->addListener($validator);
}
}
public function addValidator(WDB\Validation\iValidator $validator)
{
$this->customValidators[] = $validator;
}
public function getTableAnalyzer()
{
return $this->tableAnalyzer;
}
public function addColumn (iColumn $column, $name = NULL)
{
if ($name === NULL)
{
$name = $column->name;
}
$this->columns[$name] = $column;
if ($column instanceof iDBColumn)
{
$column->fetchReadFields($this->readFields);
}
}

/**
     * initializes readFields protected property. Override this to make own fields list in your table wrapper
     */
protected function initReadFields()
{
$this->readFields = array();
foreach ($this->columns as $column)
{
if ($column instanceof iDBColumn)
{
$column->fetchReadFields($this->readFields);
}
}
}
public function __construct(WDB\Analyzer\iTable $tableAnalyzer)
{
$this->schema = $tableAnalyzer->getSchema();
$this->database = $this->schema->getDatabase();
$this->tableAnalyzer = $tableAnalyzer;
$this->key = $this->tableAnalyzer->getPrimaryKey();
$this->cache = new WDB\Structure\TableWrapperCache();
$this->initBasicColumns($tableAnalyzer->getColumns());
$this->initForeignColumns($tableAnalyzer->getForeignKeys());
$this->initReadFields();
$this->init();
}
protected function initBasicColumns($columns)
{
 foreach ($columns as $column)
{
if (count($column->getForeignKeys()) == 0)
{
$this->addColumn(ColumnFactory::create($column, $this), $column->name);
}
else
{
$this->columnAliases[$column->name] = WDB\Utils\Arrays::first($column->getForeignKeys())->name;
}
}
}
protected function initForeignColumns($fKeys)
{
 foreach ($fKeys as $key)
{
if (isset($this->columns[$key->name])) throw new Exception\KeyException ("FK constraint name {$key->name} is duplicit to a column name in the same table");
$this->addColumn(ColumnFactory::createForeign($key, $this, $this->tableAnalyzer->columns), $key->name);
}
}
/**
     * for initialization of class descendants through overriding this method
     */
protected function init()
{
}

/**
     * Get Query object bound to database of this table with this table prepared as source table.
     *
     * @return WDB\Query\Select
     */
protected function getSourceTableQuery()
{
$q = new WDB\Query\Select(array(), $this->tableAnalyzer->identifier);
$q->setDatabase($this->database);
return $q;
}
public function getDatasource(array $cfg = array())
{
$ds = $this->getSourceTableQuery();
foreach ($this->readFields as $field)
{
$ds->addField($field);
}
$this->configureDatasource($ds, $cfg);
return $ds;
}

 public function count()
{
if ($this->useCache && $this->cache->count !== NULL)
{
return $this->cache->count;
}
else
{
$count = $this->getSourceTableQuery()->addField(new WDB\Query\Element\SelectField(WDB\Query\Element\DBFunction::countRows(), 'count'))->run()->singleValue();
if ($this->useCache)
{
$this->cache->count = $count;
}
return $count;
}
}

/**
     *
     * @return Query\iSelectResult
     */
private function getCachedDBData()
{
if ($this->useCache && $this->cache->data !== NULL)
{
return $this->cache->data;
}
else
{
$ds = $this->getDatasource();
$data = $ds->run($this->database);
if ($this->useCache)
{
$this->cache->data = $data;
}
return $data;
}
}
/**
     * Get table locator structure
     *
     * @return WDB\Structure\TableLocator
     */
public function getTableLocator()
{
return new WDB\Structure\TableLocator(array(
'table'=>$this->tableAnalyzer->getName(),
'schema'=>$this->tableAnalyzer->schema->getName(),
'connection'=>$this->database->getName(),
));
}
public function isUIDetail()
{
return $this->tableAnalyzer->isUIDetail();
}
public function isUIEdit()
{
return $this->tableAnalyzer->isUIEdit();
}
public function isUIDelete()
{
return $this->tableAnalyzer->isUIDelete();
}

public function offsetExists($offset)
{
return $this->getCachedDBData()->offsetExists($offset);
}
public function offsetGet($offset)
{
return $this->fetchRecord($this->getCachedDBData()->offsetGet($offset));
}
public function offsetSet($offset, $value)
{
return $this->getCachedDBData()->offsetSet($offset, $value);
}
public function offsetUnset($offset)
{
return $this->getCachedDBData()->offsetUnset($offset);
}

 public function current()
{
return $this->fetchRecord($this->getCachedDBData()->current());
}
public function key()
{
return $this->getCachedDBData()->key();
}
public function next()
{
return $this->fetchRecord($this->getCachedDBData()->next());
}
public function rewind()
{
return $this->getCachedDBData()->rewind();
}
public function valid()
{
return $this->getCachedDBData()->valid();
}
}}namespace WDB\Wrapper{
use WDB,
WDB\Exception,
WDB\GTO\WsqlLanguage,
WDB\Query\Element;
/**
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class TimeDependentTable extends Table
{
const FROM_COLUMN = '~from';
const TO_COLUMN = '~to';
const ID_COLUMN = 'id~td';
const GLOBAL_TABLE_ALIAS = '~tdGlobal';
const FROM_COLUMN_GLOBAL_ALIAS = '~fromGlobal';
const TO_COLUMN_GLOBAL_ALIAS = '~toGlobal';
protected $timeTableAnalyzer;

protected $currentDate;

protected $masterColumns;
protected $timeDependentColumns;
public function __construct(WDB\Analyzer\iTable $tableAnalyzer)
{
parent::__construct($tableAnalyzer);
$this->masterColumns = $this->columns;
$this->currentDate = time();
$this->timeTableAnalyzer = $tableAnalyzer->getTimeDependencyTable();
$this->initBasicColumns($this->timeTableAnalyzer->getColumns());
if (!isset($this->columns[self::FROM_COLUMN]) || !isset($this->columns[self::TO_COLUMN]))
{
throw new Exception\InvalidModel("Time dependency table {$this->name} must have ".self::FROM_COLUMN." and ".self::TO_COLUMN." date type columns.");
}
$this->columns[self::FROM_COLUMN]->removeDisplayModes |= WDB\WebUI\iColumn::DISPLAY_ALL;
$this->columns[self::TO_COLUMN]->removeDisplayModes |= WDB\WebUI\iColumn::DISPLAY_ALL;
$fk = $this->timeTableAnalyzer->getForeignKeys();
foreach ($fk as $name=>$key)
{
if ($key->refSchema == $this->schema->getName() && $key->refTable == $this->name)
{
unset($fk[$name]);
break;
}
}
$this->initForeignColumns($fk);
$this->timeDependentColumns = WDB\Utils\Arrays::diff($this->columns, $this->masterColumns);
}
public function getMasterColumns()
{
return $this->masterColumns;
}
public function getTimeDependentColumns()
{
return $this->timeDependentColumns;
}
/**
     *
     * @return WDB\Analyzer\iTable
     */
public function getTimeTableAnalyzer()
{
return $this->timeTableAnalyzer;
}
/**
     *
     * @return WDB\iDatasource
     */
public function getSourceTableQuery()
{
$stq = $this->getAllTimesSourceTableQuery();
if ($this->currentDate !== NULL)
{
$date = date('Y-m-d', $this->currentDate);
$fromColumn = WsqlLanguage::quoteIdentifier(self::FROM_COLUMN);
$toColumn = WsqlLanguage::quoteIdentifier(self::TO_COLUMN);
$tdName = WsqlLanguage::quoteIdentifier($this->getTimeTableAnalyzer()->getName());
$tdSchema = WsqlLanguage::quoteIdentifier($this->getTimeTableAnalyzer()->getSchema()->getName());
if ($tdSchema) $tdSchema .= '.';
$stq->addCondition(WsqlLanguage::parse(
sprintf("($tdSchema$tdName.$fromColumn <= '%s' OR $tdSchema$tdName.$fromColumn = NULL) AND ($tdSchema$tdName.$toColumn > '%s' OR $tdSchema$tdName.$toColumn = NULL)", $date, $date),
'Condition'));
}
return $stq;
}
protected function getAllTimesSourceTableQuery()
{
$stq = parent::getSourceTableQuery()
->join($this->timeTableAnalyzer->getIdentifier())
->join($this->timeTableAnalyzer->getIdentifier()->alias(self::GLOBAL_TABLE_ALIAS), Element\JoinUsing::create($this->getMasterPrimaryKey()));
foreach ($this->getMasterPrimaryKey() as $column)
{
$stq->addGroup(Element\ColumnIdentifier::create($column, $this->tableAnalyzer->getName(), $this->tableAnalyzer->getSchema()->getName()));
}
return $stq;
}
/**
     * @see getDatasource()
     * @param int\DateTime $time timestamp
     * @param array $cfg
     * @return WDB\iDatasource
     */
public function getDatasourceForTime($time, array $cfg = array())
{
return $this->timeSpanOperation('getDatasource', $time, array($cfg));
}
/**
     * @see getDatasource()
     * @param array $cfg
     * @return WDB\iDatasource
     */
public function getAllTimesDatasource(array $cfg = array())
{
return $this->timeSpanOperation('getDatasource', NULL, array($cfg));
}
/**
     *
     * @see getRecordByKey()
     * @param int|\DateTime $time timestamp
     * @param array|mixed $key
     * @return iRecord
     */
public function getRecordByKeyForTime($time, $key)
{
return $this->timeSpanOperation('getRecordByKey', $time, array($key));
}
/**
     *
     * @see getRecordByKey()
     * @param int|\DateTime $time timestamp
     * @param string $key
     * @return iRecord
     */
public function getRecordByStringPKForTime($time, $key)
{
return $this->timeSpanOperation('getRecordStringPK', $time, array($key));
}
protected function timeSpanOperation($method, $time, $args)
{
if ($time instanceof \DateTime)
{
$time = $time->getTimestamp();
}
$t = $this->currentDate;
$this->currentDate = $time;
$result = call_user_func_array(array('parent', $method), $args);
$this->currentDate = $t;
return $result;
}
/**
     *
     * @param array|mixed $key
     * @return Structure\TimeSpan[]
     */
public function getRecordTimeSpanList($key)
{
$result = array();
$key = $this->canonicalizeKey($key);
foreach ($this->getAllTimesDatasource()->select(self::FROM_COLUMN, self::TO_COLUMN, self::ID_COLUMN)->filter($key)->run($this->database) as $row)
{
$result[$row[self::ID_COLUMN]] = Structure\TimeSpan::create($this, $row[self::ID_COLUMN], $row[self::FROM_COLUMN], $row[self::TO_COLUMN]);
}
return $result;
}
public function getRecordByTimeId($id)
{
$result = $this->getAllTimesDatasource()->filter(array(self::ID_COLUMN=>$id))->run($this->database);
if (count($result) == 0) throw new Exception\NotFound ("record not found.");
return $this->fetchRecord($result->singleRow());
}
public function getRecordWrapperClass()
{
return WDB\Utils\System::coalesce(parent::getRecordWrapperClass(), '\\WDB\\Wrapper\\TimeDependentRecord');
}
public function fetchValidators(WDB\Event\Event $event)
{
parent::fetchValidators($event);
$this->timeTableAnalyzer->fetchValidators($event);
}
public function getPrimaryKey()
{
return array(self::ID_COLUMN);
}
public function getMasterPrimaryKey()
{
return parent::getPrimaryKey();
}
public function initReadFields()
{
parent::initReadFields();
$fromColumn = WsqlLanguage::quoteIdentifier(self::GLOBAL_TABLE_ALIAS).'.'.WsqlLanguage::quoteIdentifier(self::FROM_COLUMN);
$toColumn = WsqlLanguage::quoteIdentifier(self::GLOBAL_TABLE_ALIAS).'.'.WsqlLanguage::quoteIdentifier(self::TO_COLUMN);
$this->readFields[] = new WDB\Query\Element\SelectField(WsqlLanguage::parse("IF(SUM($fromColumn = NULL), NULL, MIN($fromColumn))", 'Expression'), self::FROM_COLUMN_GLOBAL_ALIAS);
$this->readFields[] = new WDB\Query\Element\SelectField(WsqlLanguage::parse("IF(SUM($toColumn = NULL), NULL, MAX($toColumn))", 'Expression'), self::TO_COLUMN_GLOBAL_ALIAS);
}
}}namespace WDB\Analyzer{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnDate extends ColumnTimeInfo
{
protected $stringFormat = 'j.n.Y';
protected $_wdbType = 'Date';
 public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\Date($value);
}
public function getWebUIClass()
{
return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnDate');
}
public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
parent::fetchValidationRules($rules);
$rules->pattern('~^'.ColumnTimeInfo::$datePattern.'$~x',
WDB\Lang::s('validator.messages.date'));
}

 public function hasTime() { return FALSE; }
public function hasDate() { return TRUE; }

}}namespace WDB\Analyzer{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class ColumnDateTime extends ColumnTimeInfo
{
protected $stringFormat = 'j.n.Y G:i:s';
 public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\DateTime($value);
}
public function getWebUIClass()
{
return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnDateTime');
}
public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
parent::fetchValidationRules($rules);
$rules->pattern('~^'.ColumnTimeInfo::$datePattern.ColumnTimeInfo::$timePattern.'$~x',
WDB\Lang::s('validator.messages.datetime'));
}

 public function hasTime() { return TRUE; }
public function hasDate() { return TRUE; }
}}namespace WDB\Analyzer{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class ColumnTime extends ColumnTimeInfo
{
protected $stringFormat = 'G:i:s';
 public function valueToDatatype($value)
{
return new WDB\Query\Element\Datatype\Time($value);
}
public function getWebUIClass()
{
return WDB\Utils\System::coalesce(parent::getWebUIClass(), '\\WDB\\WebUI\\ColumnTime');
}
public function fetchValidationRules(WDB\Validation\ColumnRules $rules)
{
parent::fetchValidationRules($rules);
$rules->pattern('~^'.ColumnTimeInfo::$timePattern.'$~x',
WDB\Lang::s('validator.messages.time'));
}

 public function hasTime() { return TRUE; }
public function hasDate() { return FALSE; }
public function getStringValue($value)
{
return $value instanceof \DateInterval ? $value->format($this->stringFormat) : (string)$value;
}
public function setFieldValue($value)
{
try
{
return $value instanceof \DateInterval ? $value : new \DateInterval($value);
}
catch (\Exception $ex)
{
return $value;
}
}
}}namespace WDB\Analyzer{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 *
 */
class Table extends TableView
{

protected $timeDependencyTable = NULL;

protected $isTimeDependency = FALSE;
public function __construct($tableName, $tableComment, WDB\Analyzer\iSchema $schema, WDB\SQLDriver\iSQLDriver $driver)
{
parent::__construct($tableName, $tableComment, $schema, $driver);
foreach ($this->tableColumns->foreign as $name=>$key)
{
foreach ($key->columns as $column)
{
$this->columns[$column]->addForeignKey($name);
}
}
}
public function onSchemaConstructed()
{
parent::onSchemaConstructed();
$this->initTimeDependencyTable();
}
protected function initTimeDependencyTable()
{
$tdDefault = $this->name.'~td';
if (isset($this->annotations->timeDependency))
{
$this->timeDependencyTable = $this->annotations->last('timeDependency')->text;
}
elseif (in_array($tdDefault, array_keys($this->schema->getTables())))
{
$this->timeDependencyTable = $tdDefault;
}
if (!$this->timeDependencyTable) return;
$t=$this->schema->getTables();
if (!isset($t[$this->timeDependencyTable]) || !$t[$this->timeDependencyTable] instanceof Table)
{
throw new Exception\InvalidModel("Time dependency table {$this->timeDependencyTable} configured for table {$this->name} not found or not a table");
}
$t[$this->timeDependencyTable]->setIsTimeDependency(TRUE);
}
public function setIsTimeDependency($isDependency)
{
$this->isTimeDependency = $isDependency;
}
public function getWrapperClass()
{
$w = parent::getWrapperClass();
if ($w !== NULL) return $w;
if ($this->timeDependencyTable) return '\\WDB\\Wrapper\\TimeDependentTable';
return NULL;
}
public function getTimeDependencyTable()
{
$t = $this->schema->getTables();
return $t[$this->timeDependencyTable];
}
public function getForeignKeys()
{
return $this->tableColumns->foreign;
}
public function isWebUIAccessible()
{
return !$this->isTimeDependency && parent::isWebUIAccessible();
}
}
}namespace WDB\Query\Element{
use WDB,
WDB\Exception;
/**
 * Compare query element.
 *
 * Represents generic equality, inequality, arithmetical greater[or equal] and less[or equal].
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Compare extends InfixOperator implements iCondition
{
const EQ = '=';
const LT = '<';
const LTE = '<=';
const GT = '>';
const GTE = '>=';
const NEQ = '!=';
protected static $supportedOps = array(self::EQ, self::LT, self::LTE, self::GT, self::GTE, self::NEQ);
/**
     * Factory method creating Equality comparation.
     *
     * @param iExpression left side of compare operator
     * @param iExpression right side of compare operator
     * @return Compare
     */
public static function Equals(iExpression $expr1, iExpression $expr2)
{
return new self($expr1, $expr2, self::EQ);
}
/**
     * Factory method creating Non-equality comparation.
     *
     * @param iExpression left side of compare operator
     * @param iExpression right side of compare operator
     * @return Compare
     */
public static function NEquals(iExpression $expr1, iExpression $expr2)
{
return new self($expr1, $expr2, self::NEQ);
}
/**
     * Factory method creating > comparation.
     *
     * @param iExpression left side of compare operator
     * @param iExpression right side of compare operator
     * @return Compare
     */
public static function Gt(iExpression $expr1, iExpression $expr2)
{
return new self($expr1, $expr2, self::GT);
}
/**
     * Factory method creating >= comparation.
     *
     * @param iExpression left side of compare operator
     * @param iExpression right side of compare operator
     * @return Compare
     */
public static function Gte(iExpression $expr1, iExpression $expr2)
{
return new self($expr1, $expr2, self::GTE);
}
/**
     * Factory method creating < comparation.
     *
     * @param iExpression left side of compare operator
     * @param iExpression right side of compare operator
     * @return Compare
     */
public static function Lt(iExpression $expr1, iExpression $expr2)
{
return new self($expr1, $expr2, self::LTE);
}
/**
     * Factory method creating <= comparation.
     *
     * @param iExpression left side of compare operator
     * @param iExpression right side of compare operator
     * @return Compare
     */
public static function Lte(iExpression $expr1, iExpression $expr2)
{
return new self($expr1, $expr2, self::LTE);
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_compare($this->expressions[0], $this->expressions[1], $this->op);
}
public function addExpression($expr1)
{
throw new Exception\InvalidOperation("Cannot chain comparison operators.");
}
/**
     * Create any supported comparation.
     *
     * @param iExpression left side of compare operator
     * @param iExpression right side of compare operator
     * @param string comparation type (enumeration of Compare::*)
     */
public function __construct(iExpression $expr1, iExpression $expr2, $compare_op = self::EQ)
{
parent::__construct(array($expr1, $expr2), $compare_op);
}
}
}namespace WDB\Query\Element\Datatype{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
abstract class AbstractType extends WDB\BaseObject implements iDatatype
{
protected $value;
public function equals(iDatatype $to)
{
return \get_class($to) === \get_class($this) && $this->value = $to->value;
}
public function isNull()
{
return $this->value === NULL;
}
public function getValue()
{
return $this->value;
}
public function __toString()
{
return (string)$this->getValue();
}
public static function createDatatype($value)
{
if ($value instanceof iDatatype) return $value;
if ($value instanceof \DateTime)
{
if (isset($value->_wdbType) && $value->_wdbType == 'Date')
{
return new Date($value);
}
else
{
return new DateTime($value);
}
}
if ($value instanceof \DateInterval)
{
return new Time($value);
}
if (is_array($value))
{
return new Set($value);
}
switch (gettype($value))
{
case 'boolean':
return new Integer($value ? 1 : 0);
case 'integer':
return new Integer($value);
case 'double':
return new Float($value);
case 'string':
return new String($value);
case 'NULL':
return new TNull();
}
throw new Exception\WDBException("unknown type: ".gettype($value)." or class: ".get_class($value));
}
}
}namespace WDB\Query\Element\Datatype{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @property-read \DateTime $value
 */
class DateTime extends AbstractType
{
/**
     *
     * @param string|\DateTime $value 
     * @throws Exception\InvalidValueFormat
     */
public function __construct($value)
{
if (!($value instanceof \DateTime) && $value !== NULL)
{
try
{
$value = new \DateTime($value);
}
Catch (Exception $e)
{
throw new Exception\InvalidValueFormat("string '$value' cannot be parsed as date/time information.", 0, $e);
}
}
$this->value = $value;
}
public function getValue()
{
return $this->value;
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_datetime($this->value, TRUE, TRUE);
}
public function __toString()
{
return $this->value === NULL ? '' : $this->value->format('Y-m-d H:i:s');
}
}
}namespace WDB\Query\Element\Datatype{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @property-read double $value
 */
class Float extends AbstractType
{
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_number($this->value);
}
public function __construct($value)
{
if ($value !== NULL) $value = (double)$value;
$this->value = $value;
}
}
}namespace WDB\Query\Element\Datatype{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property-read int $value
 */
class Integer extends AbstractType
{
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_number($this->value);
}
public function __construct($value)
{
if ($value !== NULL) $value = (int)$value;
$this->value = $value;
}
}
}namespace WDB\Query\Element\Datatype{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * 
 * @property array $value
 */
class Set extends AbstractType
{
/**
     *
     * @param array $value 
     * @throws Exception\BadArgument
     */
public function __construct($value)
{
if ($value !== NULL && !is_array($value)) throw new Exception\BadArgument("value of Set datatype must be an array or null");
$this->value = $value;
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_set($this->value);
}
public function equals(iDatatype $to)
{
return get_class($to) === get_class($this) && count(array_diff($this->value, $to->value)) == 0 && count(array_diff($to->value, $this->value)) == 0;
}
public function __toString()
{
return implode(', ', $this->value);
}
}
}namespace WDB\Query\Element\Datatype{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 * @property-read string $value
 */
class String extends AbstractType
{
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
$value = $this->value;
if (strtolower($driver->getCharset()) == 'utf8')
{
$value = WDB\Database::validateBrokenUTF($value);
}
return $driver->tosql_string($value);
}
public function __construct($value)
{
if ($value !== NULL) $value = (string)$value;
$this->value = $value;
}
}
}namespace WDB\Query\Element\Datatype{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Time extends DateTime
{
/**
     *
     * @param string|\DateInterval $value 
     * @throws Exception\InvalidValueFormat
     */
public function __construct($value)
{
if (!($value instanceof \DateInterval) && $value !== NULL)
{
try
{
if (preg_match('~([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})', $value))
{
$value = 'P0000-00-00T'.$value;
}
$value = new \DateInterval($value);
}
Catch (Exception $e)
{
throw new Exception\InvalidValueFormat("string '$value' cannot be parsed as date/time information.", 0, $e);
}
}
$this->value = $value;
}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_time($this->value);
}
public function __toString()
{
return $this->value === NULL ? '' : $this->value->format('H:i:s');
}
public static function webUIFieldClass()
{
return '\\WDB\\WebUI\\FieldTime';
}
}
}namespace WDB\Query\Element\Datatype{
use WDB,
WDB\Exception;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class TNull extends AbstractType
{
public function equals(iDatatype $to)
{
return $to instanceof TNull || $to->value == NULL;
}
public function getValue()
{
return NULL;
}
public function __toString()
{
return '';
}
public function __construct($value = null){}
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_null();
}
}
}namespace WDB\WebUI{
use WDB;
/**
  * Date column implementation
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  * @todo add some nice javascript calendar
  */
class ColumnDate extends ColumnText
{
public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
{
return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_date'));
}
protected function HTMLInputTextValue($v)
{
if ($v === NULL) return date('j.n.Y');
if (is_object($v) && method_exists($v, 'format'))
{
return $v->format('j.n.Y');
}
return (string)$v;
}
protected function getInputHTMLNode(WDB\Wrapper\iRecord $record)
{
$input = parent::getInputHTMLNode($record);
$input->addClass('tcal');
return $input;
}
public function getDetailedHTML(WDB\Wrapper\iRecord $record)
{
$val = $record[$this->name];
return $this->getStringRepresentation($val);
}
private function getStringRepresentation($value)
{
if ($value === NULL) return WDB\Lang::s ('webui.notset');
return $value->format('j.n.Y');
}
public function getFilterStringValue($val)
{
return $this->getStringRepresentation($val);
}
}}namespace WDB\WebUI{
use WDB;
/**
  * Datetime column implementation
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnDateTime extends ColumnText
{
public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
{
return array_merge(parent::getSurroundingCSSClasses($record), array('wdb_field_datetime'));
}
protected function HTMLInputTextValue($v)
{
if ($v === NULL) return date('j.n.Y G:i:s');  if (is_object($v) && method_exists($v, 'format'))
{
return $v->format('j.n.Y G:i:s');
}
return (string)$v;
}
public function getDetailedHTML(WDB\Wrapper\iRecord $record)
{
$val = $record[$this->name];
return $this->getStringRepresentation($val);
}
private function getStringRepresentation($value)
{
if ($value === NULL) return WDB\Lang::s ('webui.notset');
return $value->format('j.n.Y G:i:s');
}
public function getFilterStringValue($val)
{
return $this->getStringRepresentation($val);
}
}}namespace WDB\WebUI{
use WDB;
/**
  * Numeric column
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnNumeric extends ColumnText
{

protected $numPattern = '/^(-|\\+)?[0-9 ]*((\\.|,)[0-9 ]*)?$/';
public function getSurroundingCSSClasses(WDB\Wrapper\iRecord $record)
{
return array_merge(parent::getSurroundingCSSClasses($record), array(' wdb_field_numeric'));
}
public function getEditingHTML(WDB\Wrapper\iRecord $record)
{
$input = $this->getInputHTMLNode($record);
$input['onchange'] = $input['onkeyup'] = 'wdb.filterInput(this, '.$this->numPattern.')';
return $input->__toString();
}
}}namespace WDB\WebUI{
use WDB,
WDB\Exception;
/**
  * Password column.
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnPassword extends ColumnText
{
public function getEditingHTML(WDB\Wrapper\iRecord $record)
{
$input = $this->getInputHTMLNode($record);
$input['type'] = 'password';
$input['value'] = '';
$input->addClass('wdbPasswordInput');
if ($record->isNew())
{
return $input->__toString();
}
else
{
$input_name = $this->table->getRequest()->getSaveFieldName($this->name);
$doChange = new HTMLNode('input');
$doChange['type'] = 'checkbox';
$doChange['class'] = 'wdbPasswordInputChange';
$doChange['name'] = $input_name.'[doChange]';
$doChange['id'] = $input_name.'[doChange]';
return $input->__toString().$doChange->__toString()."<label for=\"{$input_name}[doChange]\">".WDB\Lang::s('webui.passwordFieldChange')."</label>";
}
}
public function parseInputValue(WDB\Wrapper\iRecord $record, $value)
{
if ($record->isNew() || !empty($value['doChange']))
{
parent::parseInputValue($record, $value);
}
}
protected function getDefaultDisplayModes()
{
return parent::getDefaultDisplayModes() &~ iColumn::DISPLAY_DETAIL &~ iColumn::DISPLAY_LIST;
}
public static function hash($v)
{
return sha1($v.WDB\Config::read('passwordSalt'));
}
protected function fetchInputValue($v)
{
return self::hash($v);
}
public function isFilterable()
{
return false;
}
public function isOrderable()
{
return false;
}
}}namespace WDB\Query\Element\Datatype{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @package WDB
 */
class Date extends DateTime
{
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_datetime($this->value, TRUE, FALSE);
}
public function equals(iDatatype $to)
{
return \get_class($to) === \get_class($this) && $this->value->format('Y-m-d') == $to->value->format('Y-m-d');
}
public function __toString()
{
return $this->value === NULL ? '' : $this->value->format('Y-m-d');
}
}
}namespace WDB\Query\Element\Datatype{
use WDB;
/**
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 * @property-read string $value
 */
class Enum extends String
{
public function toSQL(WDB\SQLDriver\iSQLDriver $driver)
{
return $driver->tosql_enum($this->value);
}
}
}namespace WDB\WebUI{
use WDB;
/**
  * Integer column
  * 
  * @author Richard Ejem <richard(at)ejem.cz>
  * @package WDB
  */
class ColumnInteger extends ColumnNumeric
{
protected $numPattern = '/^(-|\\+)?[0-9 ]*$/';
}}