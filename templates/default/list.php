<?php if ($this->table->isNewRecordCapable()): ?>
<p><a href="<?php echo htmlspecialchars($this->table->getRequest()->getStateLink(\WDB\WebUI\iTable::STATE_EDIT, TRUE)); ?>" class="btn btn-primary"><i class="icon-white icon-file"></i> <?php echo htmlspecialchars(\WDB\Lang::s('webui.newrecord')); ?></a></p>
<?php endif?>
<?php $this->table->getPaginatorTemplate()->show(); ?>
<table class="wdb_list table table-striped">
    <thead>
        <tr>
            <?php foreach ($this->table->getColumns() as $column) : if (!$column->isDisplayedList()) continue; ?>
            <th>
                <?php echo htmlspecialchars($column->getTitle());?>
            </th>
            <?php endforeach; ?>
            <th><?php echo htmlspecialchars(\WDB\Lang::s('webui.list.actions.heading')); ?></th>
        </tr>
        <tr>
            <?php foreach ($this->table->getColumns() as $column) : if (!$column->isDisplayedList()) continue; ?>
            <td class="wdb_commands">
                <div class="dropdown">
                <?php if ($column instanceof WDB\WebUI\iOrderable && $column->isOrderable()):?>
                    <a class="btn btn-mini<?php if ($column->isToggledOrder(TRUE)) echo ' btn-info active' ?>"
                       title="<?php echo htmlspecialchars(\WDB\Lang::s('webui.list.actions.orderAsc')); ?>"
                       href="<?php echo htmlspecialchars($this->table->getRequest()->orderLink($column->isToggledOrder(TRUE) ? NULL : $column->getName()));
                    ?>"><i class="icon-chevron-up"></i></a><a class="btn btn-mini<?php if ($column->isToggledOrder(FALSE)) echo ' btn-info active' ?>"
                       title="<?php echo htmlspecialchars(\WDB\Lang::s('webui.list.actions.orderDesc')); ?>"
                       href="<?php echo htmlspecialchars($column->isToggledOrder(FALSE) ? $this->table->getRequest()->orderLink()
                            : $this->table->getRequest()->orderLink($column->getName(), FALSE));
                    ?>"><i class="icon-chevron-down"></i></a><?php
                endif;
                if ($column instanceof WDB\WebUI\iFilterable && $column->isFilterable()):
                ?><a class="last wdb_filter btn btn-mini dropdown-toggle<?php if ($column->isToggledFilter()) echo ' btn-info active' ?>"
                   title="<?php echo htmlspecialchars(\WDB\Lang::s('webui.list.actions.filter')); ?>"
                   data-toggle="dropdown" href="#"><i class="icon-filter"></i></a>

                <ul class="dropdown-menu">
                    <?php if ($column->isToggledFilter()):?>
                    <li><a href="<?php echo htmlspecialchars($this->table->getRequest()->filterLink($column->getName(), NULL)) ?>" class="btn-info"><i class="icon-remove-sign"></i><?php echo htmlspecialchars($column->getFilteredValue()); ?></a></li>
                    <?php endif; ?>
                    <?php foreach ($column->getFilterOptions() as $key=>$title): ?>
                    <li><a href="<?php echo htmlspecialchars($this->table->getRequest()->filterLink($column->getName(), $key)) ?>"><?php echo htmlspecialchars($column->getFilterStringValue($title)); ?></a></li>
                    <?php endforeach; ?>
                </ul>
                </div>
                <?php endif;?>
            </td>
            <?php endforeach; ?>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($this->records as $record) { ?>
        <tr>
            <?php foreach ($this->table->getColumns() as $column) { if (!$column->isDisplayedList()) continue; ?>
            <td class="<?php echo implode(', ', $column->getSurroundingCSSClasses($record)); ?>"><?php echo $column->getSimpleHTML($record); ?></td>
            <?php } ?>
            <td class="wdb_commands">
                <?php if ($record->isUIDetail()):?><a href="<?php echo htmlspecialchars($this->table->getRequest()->getStateLink(\WDB\WebUI\iTable::STATE_DETAIL, $record->getStringKey())); ?>"
                    class="btn btn-mini" title="<?php echo htmlspecialchars(\WDB\Lang::s('webui.list.actions.detail')); ?>"><i class="icon-eye-open"></i></a><?php endif;
                 if ($record->isUIEdit()):?><a href="<?php echo htmlspecialchars($this->table->getRequest()->getStateLink(\WDB\WebUI\iTable::STATE_EDIT, $record->getStringKey())); ?>"
                    class="btn btn-mini" title="<?php echo htmlspecialchars(\WDB\Lang::s('webui.list.actions.edit')); ?>"><i class="icon-pencil"></i></a><?php endif;
                if ($record->isUIDelete()):?><a onclick="return confirm('Opravdu smazat záznam?');" href="<?php echo htmlspecialchars($this->table->getRequest()->getDeleteLink($record->getStringKey())); ?>"
                    class="btn btn-danger btn-mini" title="<?php echo htmlspecialchars(\WDB\Lang::s('webui.list.actions.delete')); ?>"><i class="icon-white icon-remove">&nbsp;</i></a>
                <?php endif; ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php $this->table->getPaginatorTemplate()->show(); ?>