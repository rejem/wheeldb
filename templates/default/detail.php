<p><a href="<?php echo htmlspecialchars($this->table->getRequest()->getStateLink(\WDB\WebUI\iTable::STATE_LIST));?>" class="btn"><i class="icon-arrow-left"></i> Zpět na seznam</a></p>
<table class="wdb_detail">
    <?php
    $record = $this->table->getSelectedRecord();
    foreach ($this->table->getColumns() as $column){ if (!$column->isDisplayedDetail()) continue; ?>
    <tr>
        <th><?php echo $column->getTitleHTML(); ?></th>
        <td class="<?php echo implode(', ', $column->getSurroundingCSSClasses($record)) ?>"><?php echo $column->getDetailedHTML($record); ?></td>
    </tr>
    <?php } ?>
</table>