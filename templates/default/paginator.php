<?php if ($this->pages == 1) return; ?>
<div class="wdbPaginator">
<?php
//previous page
if ($this->currentPage > 1) { ?>
    <a href="<?php echo $this->uri->fetch(array($this->uriPageParam => $this->currentPage-1)); ?>">&lt;</a>&nbsp;
    <?php } else { ?>&lt;&nbsp;<?php }
//first page
if ($this->currentPage > 1) { ?>
    <a href="<?php echo $this->uri->fetch(array($this->uriPageParam => 1)); ?>">1</a>&nbsp;
    <?php } else { ?>1&nbsp;<?php }

//dots if there are hidden page numbers
if ($this->currentPage-$this->around > 2)
{
    echo '...&nbsp;';
}

for ($x = max($this->currentPage-$this->around, 2); $x <= min($this->currentPage+$this->around, $this->pages-1); $x++)
{
    if ($x == $this->currentPage)
    {
        ?><span class="pgr_active"><?php echo $x; ?></span>&nbsp;<?php
    }
    else
    {
        ?><a href="<?php echo $this->uri->fetch(array($this->uriPageParam => $x)); ?>"><?php echo $x; ?></a>&nbsp;<?php
    }
}

//dots if there are hidden page numbers
if ($this->currentPage+$this->around < $this->pages - 1)
{
    echo '...&nbsp;';
}


//last page
if ($this->currentPage < $this->pages) { ?>
    <a <?php echo 'href="'.$this->uri->fetch(array($this->uriPageParam => $this->pages)).'">'.$this->pages; ?></a>&nbsp;
    <?php } else { echo $this->pages.'&nbsp;'; }

//next page
if ($this->currentPage < $this->pages) { ?>
    <a href="<?php echo $this->uri->fetch(array($this->uriPageParam => $this->currentPage+1)); ?>">&gt;</a>&nbsp;
    <?php } else { ?>&gt;&nbsp;<?php } ?>
</div>