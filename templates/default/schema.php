<div class="wdb_schema row-fluid">
    <div class="tablesMenu">
        <ul class="nav nav-tabs nav-stacked">
        <?php foreach ($this->schema->getTableSchemaList() as $table)
        {
            echo '<li><a href="'.htmlspecialchars($this->schema->getRequest()->getSelectTableLink($table->name)).'">'.htmlspecialchars($table->title).'</a></li>';
        }
        ?>
        </ul>
    </div>
    <div class="content main">
        <?php if ($this->schema->selectedTable === NULL) {
            echo \WDB\Lang::s('webui.schema.noSelectedTable');
        }
        else
        {
            $this->schema->selectedTable->show();
        }
        ?>
    </div>
</div>