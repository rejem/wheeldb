<p><a href="<?php echo htmlspecialchars($this->table->request->getStateLink(\WDB\WebUI\iTable::STATE_LIST));?>" class="btn"><i class="icon-arrow-left"></i> Zpět na seznam</a></p>
<form method="post" enctype="multipart/form-data" id="wdbForm_<?php echo htmlspecialchars($this->table->request->requestId);?>" action="<?php echo $this->formActionHTML ?>" onsubmit="return wdb.validate(this)">
    <div><?php echo $this->table->getHTMLEditStatus(); ?></div>
    <table class="wdb_edit">
        <?php
        $record = $this->table->getSelectedRecord();
        foreach ($this->table->getColumns() as $column): if (!$column->isDisplayedEditRead()) continue; ?>
        <tr>
            <th><?php echo $column->getTitleHTML(); ?></th>
            <?php if ($column->isDisplayedEdit()):?>
            <td class="wdb_editControlContainer nullator <?php echo implode(' ', $column->getSurroundingCSSClasses($record)); ?>"><?php echo $column->getEditingHTML_nullator($record); ?></td>
            <td class="wdb_editControlContainer <?php echo implode(' ', $column->getSurroundingCSSClasses($record)); ?>"><?php echo $column->getEditingHTML($record); ?></td>
            <?php else:?>
            <td class="<?php echo $column->getSurroundingCSSClass(); ?>">&nbsp;</td>
            <td class="<?php echo $column->getSurroundingCSSClass(); ?>"><?php echo $column->getDetailedHTML($record); ?></td>
            <?php endif?>
            <td class="wdb_editStatus"><?php echo $column->getHTMLEditStatus($record); ?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="4">
                <input type="hidden" name="<?php echo htmlspecialchars($this->table->getRequest()->getEditControlName()); ?>" value="1" />
                <button type="submit" class="btn btn-primary"><i class="icon-white icon-ok"></i> <?php echo htmlspecialchars(\WDB\Lang::s('webui.edit.save')) ?></button>
                <?php if (WDB\Config::read('debug')):?><button class="btn btn-info" onclick="wdb.validate(this.form);return false"><i class="icon-white icon-eye-open"></i> <?php echo htmlspecialchars(\WDB\Lang::s('webui.edit.validate')) ?></button><?php endif?>
            </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
/* <![CDATA[ */
    $(document).ready(function(){
    var frm = document.getElementById('wdbForm_'+<?php echo json_encode($this->table->getRequest()->getRequestId());?>);
    frm.wdbValidation = wdb.defaultProcessData(frm, <?php echo json_encode($this->table->getValidatorData($this->table->getSelectedRecord()))?>);
    frm.wdbValidation.eventDispatcher = wdb.validation.defaultEventDispatcher;
    frm.wdbValidation.requestId = <?php echo json_encode($this->table->getRequest()->getRequestId()); ?>;
    });
/* ]]> */
 </script>