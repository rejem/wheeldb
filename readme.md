__        __        __  _____    ___
\ \      /  \      / /  |  _ \   |   \
 \ \    / /\ \    / /   | | \ \  | ' /
  \ \  / /  \ \  / /    | |  ) ) |   \
   \ \/ /    \ \/ /     | |_/ /  | |) )
    \__/      \__/      |____/   |___/
---------------------------------------
WheelDB Framework 0.1
---------------------------------------

Usage:
- require loader.php for debug purposes (one class per file)
- require min.php for production purposes (single class file = faster loading)