<?php
return array(
    'validator'=>array(
        'messages'=>array(
            'minlength'=>'%name musí mít minimální počet znaků: %arg',
            'maxlength'=>'%name nesmí mít více než maximální počet znaků: %arg',
            'minvalue'=>'%name musí být větší nebo rovno %arg',
            'maxvalue'=>'%name musí být menší nebo rovno %arg',
            'integer'=>'%name musí být celé číslo',
            'float'=>'%name musí být (desetinné) číslo',
            'required'=>'%name je povinný údaj',
            'equals'=>'%name se musí shodovat s hodnotou pole %arg',
            'pattern'=>'%name nemá vyplněnou platnou hodnotu',
            'email'=>'%name není platná e-mailová adresa',
            'url'=>'%name není platná url adresa',
            'date'=>'%name: nesprávný formát data (dd.mm.rrrr)',
            'datetime'=>'%name: nesprávný formát data a času (dd.mm.rrrr hh::mm::ss)',
            'time'=>'%name: nesprávný formát času (hh:mm:ss)',
            'uniqueViolated'=>'%name musí být unikátní!',
            'uniqueGroupViolated'=>'Kombinace polí %fieldlist musí být unikátní!',
        ),
        'excmore'=>'(a %d dalších chyb)',
    ),
    'webui'=>array(
        'boolean' => array(
            'yes'=>'Ano',
            'no'=>'Ne',
        ),
        'schema'=>array(
            'noSelectedTable'=>'Vyberte, prosím, tabulku z nabídky',
        ),
        'newrecord'=>'Nový záznam',
        'notset'=>'Nenastaveno',
        'passwordNotMatch'=>'Hesla se neshodují!',
        'passwordFieldChange'=>'Změnit',
        'list'=>array(
            'actions'=>array(
                'heading'=>'Akce',
                'detail'=>'Detail',
                'edit'=>'Upravit',
                'delete'=>'Smazat',
                'orderAsc'=>'Seřadit vzestupně',
                'orderDesc'=>'Seřadit sestupně',
                'filter'=>'Filtrovat',
            ),
        ),
        'edit'=>array(
            'save'=>'Uložit',
            'validate'=>'Validovat',
            'noneSelected'=>'(vyberte)',
        ),
    ),
);
