<?php
//require 'min.php';return;
if (!class_exists('WDBLoader', FALSE))
{
/**
 * WheelDB class loader
 * include this file to use WheelDB in your application
 * Searches class in ./namespace/structure/classname.php or ./any-level-of-namespace.ns.php
 *
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class WDBLoader
{
    /**
     * autoloading method
     * 
     * @param string $classname
     * @return boolean
     */
    public static function wdbAutoload($classname)
    {
        $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $classname);
        $ext = 'php';
        do
        {
            if (file_exists(__DIR__.DIRECTORY_SEPARATOR."$path.$ext"))
            {
                require_once(__DIR__.DIRECTORY_SEPARATOR."$path.$ext");
                if (class_exists($classname, FALSE) || interface_exists($classname, FALSE)) return TRUE;
            }
            $r = strrpos($path, DIRECTORY_SEPARATOR);
            if ($r !== FALSE)
            {
                $path = substr($path, 0, $r);
            }
            $ext = 'ns.php';
        } while ($r !== FALSE);
        $p3rd = __DIR__.DIRECTORY_SEPARATOR.'3rdparty'.DIRECTORY_SEPARATOR."$classname.php";
        if (file_exists($p3rd))
        {
            require_once($p3rd);
            if (class_exists($classname, FALSE) || interface_exists($classname, FALSE)) return TRUE;
        }
        //throw new Exception("Class $classname not found.");
    }
    
    /**
     * register wdb class loader
     */
    public static function register()
    {
        spl_autoload_register("WDBLoader::wdbAutoload");
    }

    /**
     * unregister wdb class loader
     */
    public static function unregister()
    {
        spl_autoload_unregister("WDBLoader::wdbAutoload");
    }

}

WDBLoader::register();

}