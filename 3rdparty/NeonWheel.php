<?php

/**
 * This file is modified version of Neon.php file, which is part of the Nette Framework (http://nette.org)
 *
 * Copyright (c) 2004, 2011 David Grudl (http://davidgrudl.com)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 * @package Nette
 * 
 * Simple parser & generator for Nette Object Notation.
 * Modified by Richard Ejem for use in WheelDB, so that colon is almost unneeded  with new syntax.
 * We call this new syntax neon-wheel, nw in short.
 * 
 * General idea is that in neon, we have to write:
 * 
 * master-key:
 *     subkey:
 *         like-it: yes
 *         hate-it: no
 * 
 * With nw, this is enough:
 * master-key
 *     subkey
 *         like-it yes
 *         hate-it no
 * 
 * Incompatibilities with original Neon:
 * 
 * 
 * x:
 *     y z
 * neon: "x"=>"y z"
 * nw:   "x"=>["y"=>"z"]
 * 
 * 
 * p [: x
 * neon: "p ["=>"x"
 * nw: parse exception ... also for other brackets, { and (
 * 
 * 
 * foo:
 *     bar
 * neon: "foo"=>"bar"
 * nw:   "foo"=>["bar"=>TRUE]
 * 
 * However, all can be fixed with quotes:
 * x
 *     "y z"
 * "p [": x
 * foo:
 *     "bar"
 * #OR
 * foo: bar
 * will work fine in nw. Also, both cases should be rare or not used, it is weird to use brackets in array key.
 *
 * @author     David Grudl
 * @author     Richard Ejem
 */
class NeonWheel
{
    const BLOCK = 1;

    /** @var array */
    private static $patterns = array(
        '\'[^\']*\'|"(?:\\\\.|[^"\\\\])*"', // string
        '[:-](?=\s|$)|[,=[\]{}()]', // symbol
        '?:\#.*', // comment
        '\n[\t ]*', // new line + indent
        /*'[^#"\',=[\]{}()<>\x00-\x20!`] # not introduced by these characters
                    (?:                        # continues with any of these variants:
                        [^#,:=\]})>(\x00-\x1F]+  # any non-non-usual character (note that space is permitted)
                        |:(?!\s|$)               # colon NOT followed by white or end of string
                        |(?<!\s)\#                 # hash not preceded by white
                    )*(?<!\s)                 # end of string is NOT white'*/
        '[^#"\',=[\]{}()<>\x00-\x20!`](?:[^"\'#,:=[\]{}()>(\x00-\x1F]+|:(?!\s|$)|(?<!\s)#)*(?<!\s)', // literal / boolean / integer / float
        '?:[\t ]+', // whitespace
    );

    /** @var string */
    private static $re;
    private static $brackets = array(
        '[' => ']',
        '{' => '}',
        '(' => ')',
    );

    /** @var string */
    private $input;

    /** @var array */
    public $tokens;

    /** @var int */
    private $n = 0;

    /** @var bool */
    private $indentTabs;
    
    /** @var bool signalizes if last parsed value token was a literal or quoted token */
    private $lastValueLiteral;

    /**
     * Returns the NEON representation of a value.
     * @param  mixed
     * @param  int
     * @return string
     */
    public static function encode($var, $options = NULL)
    {
        if ($var instanceof DateTime)
        {
            return $var->format('Y-m-d H:i:s O');
        } elseif ($var instanceof NeonEntity)
        {
            return self::encode($var->value) . '(' . substr(self::encode($var->attributes), 1, -1) . ')';
        }

        if (is_object($var))
        {
            $obj = $var;
            $var = array();
            foreach ($obj as $k => $v)
            {
                $var[$k] = $v;
            }
        }

        if (is_array($var))
        {
            $isList = array_keys($var) === range(0, count($var) - 1);
            $s = '';
            if ($options & self::BLOCK)
            {
                if (count($var) === 0)
                {
                    return "[]";
                }
                foreach ($var as $k => $v)
                {
                    $v = self::encode($v, self::BLOCK);
                    $s .= ($isList ? '-' : self::encode($k) . ':')
                            . (strpos($v, "\n") === FALSE ? ' ' . $v : "\n\t" . str_replace("\n", "\n\t", $v))
                            . "\n";
                    continue;
                }
                return $s;
            } else
            {
                foreach ($var as $k => $v)
                {
                    $s .= ($isList ? '' : self::encode($k) . ': ') . self::encode($v) . ', ';
                }
                return ($isList ? '[' : '{') . substr($s, 0, -2) . ($isList ? ']' : '}');
            }
        } elseif (is_string($var) && !is_numeric($var)
                && !preg_match('~[\x00-\x1F]|^\d{4}|^(true|false|yes|no|on|off|null)$~i', $var)
                && preg_match('~^' . self::$patterns[4] . '$~', $var)
        )
        {
            return $var;
        } elseif (is_float($var))
        {
            $var = var_export($var, TRUE);
            return strpos($var, '.') === FALSE ? $var . '.0' : $var;
        } else
        {
            return json_encode($var);
        }
    }

    /**
     * Decodes a NEON string.
     * @param  string
     * @return mixed
     */
    public static function decode($input)
    {
        if (!is_string($input))
        {
            throw new InvalidArgumentException("Argument must be a string, " . gettype($input) . " given.");
        }
        if (!self::$re)
        {
            self::$re = '~(' . implode(')|(', self::$patterns) . ')~Ami';
        }

        $parser = new self;
        $parser->tokenize($input);
        $res = $parser->parse(0);
        
        //check that only empty lines are left after parser
        while (isset($parser->tokens[$parser->n]))
        {
            if ($parser->tokens[$parser->n][0] === "\n")
            {
                $parser->n++;
            } else
            {
                $parser->error();
            }
        }
        return $res;
    }

    private function tokenize($input)
    {
        $this->input = str_replace("\r", '', $input);
        $this->tokens = preg_split(self::$re, $this->input, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        if ($code = preg_last_error())
        {
            throw new NeonException('PREG error', $code);
        }
        if ($this->tokens && !preg_match(self::$re, end($this->tokens)))
        {
            $this->n = count($this->tokens) - 1;
            $this->error();
        }
    }

    /**
     * @param  int  indentation (for block-parser)
     * @param  mixed
     * @return array
     */
    private function parse($indent = NULL, $result = NULL)
    {
        $inlineParser = $indent === NULL;
        $value = $key = $object = NULL;
        $hasValue = $hasKey = FALSE;
        $tokens = $this->tokens;
        $n = & $this->n;
        $count = count($tokens);

        for (; $n < $count; $n++)
        {
            $t = $tokens[$n];

            if ($t === ',')
            { // ArrayEntry separator
                if (!$hasValue || !$inlineParser)
                {
                    $this->error();
                }
                $this->addValue($result, $hasKey, $key, $value);
                $hasKey = $hasValue = FALSE;
            } elseif ($t === ':' || $t === '=')
            { // KeyValuePair separator
                if ($hasKey || !$hasValue)
                {
                    $this->error();
                }
                if (is_array($value) || is_object($value))
                {
                    $this->error('Unacceptable key');
                }
                $key = (string) $value;
                $hasKey = TRUE;
                $hasValue = FALSE;
            } elseif ($t === '-')
            { // BlockArray bullet
                if ($hasKey || $hasValue || $inlineParser)
                {
                    $this->error();
                }
                $key = NULL;
                $hasKey = TRUE;
            } elseif (isset(self::$brackets[$t]))
            { // Opening bracket [ ( {
                if ($hasValue)
                {
                    if ($t !== '(')
                    {
                        if ($hasKey)
                        {
                            $this->error();
                        }
                        else
                        {
                            $hasKey = TRUE;
                            $key = $value;
                            $n++;
                            $value = $this->parse(NULL, array());
                        }
                    }
                    else
                    {
                        $n++;
                        $entity = new NeonEntity;
                        $entity->value = $value;
                        $entity->attributes = $this->parse(NULL, array());
                        $value = $entity;
                    }
                } else
                {
                    $n++;
                    $value = $this->parse(NULL, array());
                }
                $hasValue = TRUE;
                if (!isset($tokens[$n]) || $tokens[$n] !== self::$brackets[$t])
                { // unexpected type of bracket or block-parser
                    $this->error();
                }
            } elseif ($t === ']' || $t === '}' || $t === ')')
            { // Closing bracket ] ) }
                if (!$inlineParser)
                {
                    $this->error();
                }
                break;
            } elseif ($t[0] === "\n")
            { // Indent
                if ($inlineParser)
                {
                    if ($hasValue)
                    {
                        $this->addValue($result, $hasKey, $key, $value);
                        $hasKey = $hasValue = FALSE;
                    }
                } else
                {
                    while (isset($tokens[$n + 1]) && $tokens[$n + 1][0] === "\n")
                        $n++; // skip to last indent
                    if (!isset($tokens[$n + 1]))
                    {
                        break;
                    }

                    $newIndent = strlen($tokens[$n]) - 1;
                    if ($indent === NULL)
                    { // first iteration
                        $indent = $newIndent;
                    }
                    if ($newIndent)
                    {
                        if ($this->indentTabs === NULL)
                        {
                            $this->indentTabs = $tokens[$n][1] === "\t";
                        }
                        if (strpos($tokens[$n], $this->indentTabs ? ' ' : "\t"))
                        {
                            $n++;
                            $this->error('Either tabs or spaces may be used as indenting chars, but not both.');
                        }
                    }

                    if ($newIndent > $indent)
                    { // open new block-array or hash
                        if ($hasValue || !$hasKey)
                        {
                            //$n++;
                            // Modification by Richard Ejem here
                            // if an indentation gets bigger and we have an unkeyed value on the line
                            // above, we will consider the value to be a key.
                            if (is_array($value) || is_object($value))
                            {
                                $this->error('Unacceptable key');
                            }
                            $key = (string) $value;
                            $this->addValue($result, $key !== NULL, $key, $this->parse($newIndent));                            
                            // not any more... $this->error('Unexpected indentation.');
                            
                        } else
                        {
                            $this->addValue($result, $key !== NULL, $key, $this->parse($newIndent));
                            
                        }
                        $newIndent = isset($tokens[$n]) ? strlen($tokens[$n]) - 1 : 0;
                        $hasKey = $hasValue = FALSE;
                    } else
                    {
                        if ($hasValue && !$hasKey)
                        { // block items must have "key"; NULL key means list item
                            //Richard Ejem no-colon key may be present
                            if (!$this->noColon($key, $value, $hasKey, $hasValue, $newIndent == $indent && $this->lastValueLiteral))
                            {
                                break;
                            }
                        }
                        if ($hasKey)
                        {
                            $this->addValue($result, $key !== NULL, $key, $hasValue ? $value : NULL);
                            $hasKey = $hasValue = FALSE;
                        }
                    }

                    if ($newIndent < $indent)
                    { // close block
                        return $result; // block parser exit point
                    }
                }
            } else
            { // Value
                if ($hasValue)
                {
                    if (!$this->noColon($key, $value, $hasKey, $hasValue, $this->lastValueLiteral))
                    {
                        $this->error();
                    }
                }
                static $consts = array(
            'true' => TRUE, 'True' => TRUE, 'TRUE' => TRUE, 'yes' => TRUE, 'Yes' => TRUE, 'YES' => TRUE, 'on' => TRUE, 'On' => TRUE, 'ON' => TRUE,
            'false' => FALSE, 'False' => FALSE, 'FALSE' => FALSE, 'no' => FALSE, 'No' => FALSE, 'NO' => FALSE, 'off' => FALSE, 'Off' => FALSE, 'OFF' => FALSE,
                );
                $this->lastValueLiteral = FALSE;
                if ($t[0] === '"')
                {
                    $value = preg_replace_callback('#\\\\(?:u[0-9a-f]{4}|x[0-9a-f]{2}|.)#i', array($this, 'cbString'), substr($t, 1, -1));
                } elseif ($t[0] === "'")
                {
                    $value = substr($t, 1, -1);
                } elseif (isset($consts[$t]))
                {
                    $value = $consts[$t];
                } elseif ($t === 'null' || $t === 'Null' || $t === 'NULL')
                {
                    $value = NULL;
                } elseif (is_numeric($t))
                {
                    $value = $t * 1;
                } elseif (preg_match('#\d\d\d\d-\d\d?-\d\d?(?:(?:[Tt]| +)\d\d?:\d\d:\d\d(?:\.\d*)? *(?:Z|[-+]\d\d?(?::\d\d)?)?)?$#A', $t))
                {
                    $value = new DateTime($t);
                } else
                { // literal
                    $value = $t;
                    $this->lastValueLiteral = TRUE;
                }
                $hasValue = TRUE;
            }
        }

        if ($inlineParser)
        {
            if ($hasValue)
            {
                $this->addValue($result, $hasKey, $key, $value);
            } elseif ($hasKey)
            {
                $this->error();
            }
        } else
        {
            if ($hasValue && !$hasKey)
            { 
                //// block items must have "key"
                //Richard Ejem: no-colon key may be present
                if (!$this->noColon($key, $value, $hasKey, $hasValue, $this->lastValueLiteral)) {
                    if ($result === NULL)
                    {
                        //Richard Ejem the incompatibility issue:
                        //master:
                        //    key val
                        //    usual: key
                        //when a lone or first no-colon key is found in block,
                        //there is an incompatibility with original neon...
                        $result = $value; // simple value parser
                    } else
                    {
                        $this->error();
                    }
                }
            }
            if ($hasKey)
            {
                $this->addValue($result, $key !== NULL, $key, $hasValue ? $value : NULL);
            }
        }
        return $result;
    }

    private function addValue(&$result, $hasKey, $key, $value)
    {
        if ($hasKey)
        {
            if ($result && array_key_exists($key, $result))
            {
                $this->error("Duplicated key '$key'");
            }
            $result[$key] = $value;
        } else
        {
            $result[] = $value;
        }
    }
    
    /**
     * This method is called for help if a lone string value without a key is found
     * when not expected. It splits off the beginning of the string
     * to the first white character. Returns false if no white character is found.
     * 
     * @author Richard Ejem
     *
     * @param string $key
     * @param string $value
     * @param boolean $hasKey
     * @param boolean $hasValue
     * @param boolean $trueKey - if true, literal value not containing whitespace will be considered as a key with TRUE value
     * @return boolean
     */
    private function noColon(&$key, &$value, &$hasKey, &$hasValue, $trueKey = FALSE)
    {
        if ($hasKey)
        {
            $this->error('Internal error - looking for noColon key when already have a key');
        }
        if (!is_string($value)) $this->error();
        $keyval = preg_split('~\\s~', $value, 2);
        if (count($keyval) < 2)
        {
            if ($trueKey && $this->lastValueLiteral)
            {
                $key = $value;
                $value = TRUE;
                $hasKey = TRUE;
                return TRUE;
            }
            else
            {
                return FALSE; //no white-separated key
            }
        }
        else
        {
            $key = $keyval[0];
            $value = $keyval[1];
            $hasKey = TRUE;
            return TRUE;
        }
    }

    private function cbString($m)
    {
        static $mapping = array('t' => "\t", 'n' => "\n", '"' => '"', '\\' => '\\', '/' => '/', '_' => "\xc2\xa0");
        $sq = $m[0];
        if (isset($mapping[$sq[1]]))
        {
            return $mapping[$sq[1]];
        } elseif ($sq[1] === 'u' && strlen($sq) === 6)
        {
            return iconv('UTF-32BE', $encoding . '//IGNORE', pack('N', hexdec(substr($sq, 2))));
        } elseif ($sq[1] === 'x' && strlen($sq) === 4)
        {
            return chr(hexdec(substr($sq, 2)));
        } else
        {
            $this->error("Invalid escaping sequence $sq");
        }
    }

    private function error($message = "Unexpected '%s'")
    {
        $tokens = preg_split(self::$re, $this->input, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_OFFSET_CAPTURE | PREG_SPLIT_DELIM_CAPTURE);
        $offset = isset($tokens[$this->n]) ? $tokens[$this->n][1] : strlen($this->input);
        $line = $offset ? substr_count($this->input, "\n", 0, $offset) + 1 : 1;
        $col = $offset - strrpos(substr($this->input, 0, $offset), "\n");
        $token = isset($this->tokens[$this->n]) ? str_replace("\n", '<new line>', substr($this->tokens[$this->n], 0, 40)) : 'end';
        throw new NeonException(str_replace('%s', $token, $message) . " on line $line, column $col.");
    }

}

/**
 * The exception that indicates error of NEON decoding.
 */
class NeonEntity extends stdClass
{

    public $value;
    public $attributes;

}

/**
 * The exception that indicates error of NEON decoding.
 */
class NeonException extends Exception
{
    
}
